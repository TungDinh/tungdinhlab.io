const path = require('path');
const {BundleAnalyzerPlugin} = require('webpack-bundle-analyzer');
const BrotliGzipPlugin = require('brotli-gzip-webpack-plugin');
const nullPlugin = {apply(){}};

const BUILD_JOB_PAGE = !!process.env.BUILD_JOB_PAGE;
const DEVELOPMENT = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';
var webpack = require('webpack');

const pages = {
    app: {
        // entry for the page
        entry: BUILD_JOB_PAGE ? 'src/landing_page.ts' : 'src/main',
        // the source template
        template: BUILD_JOB_PAGE ? 'public/job/index.html' : 'public/index.html',
        // output as dist/index.html
        filename: 'index.html',
        // when using title option,
        // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
        title: BUILD_JOB_PAGE ? "Tuyển dụng" : 'Cloudjet Potential',
    },
};
if (DEVELOPMENT) {
    pages.job = {
        entry: 'src/landing_page.ts',
        // the source template
        template: 'public/job/index.html',
        // output as dist/index.html
        filename: 'job/index.html',
        // when using title option,
        // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
        title: "Tuyển dụng",
    }
}

module.exports = {
    /**
     * https://webpack.js.org/configuration/dev-server/
     */
    devServer: {
        disableHostCheck: true,
        compress: !!process.env.COMPRESS,
        historyApiFallback: {
            rewrites: [
                { from: /^\/(?!job).*/, to: '/index.html' },
                { from: /^\/job.*/, to: '/job/index.html' },
            ]
        }
    },

    /**
     * https://cli.vuejs.org/config/#pages
     * https://stackoverflow.com/questions/50857442/vue-cli-3-0-multi-page-setup-with-html5-history-mode
     */
    pages: pages,

    publicPath: undefined,
    outputDir: "dist",
    assetsDir: undefined,
    runtimeCompiler: undefined,
    productionSourceMap: false,
    parallel: undefined,

    filenameHashing: true,

    css: {
        sourceMap: (process.env.NODE_ENV !== "production"),
        extract: {
            filename: '[name].[hash].css',
        },
    },
    configureWebpack: {
        plugins: [
            new webpack.ProvidePlugin({
                'window.Quill': 'quill/dist/quill.js',
                'Quill': 'quill/dist/quill.js'
            }),

            /**
             * https://medium.com/js-dojo/how-to-reduce-your-vue-js-bundle-size-with-webpack-3145bf5019b7
             * `ANALYZER=true yarn serve`, then go to localhost:8888
             */
            process.env.ANALYZER ? new BundleAnalyzerPlugin({
                analyzerMode: process.env.NODE_ENV === 'production' ? 'static' : 'server',
                openAnalyzer: false,
                defaultSizes: 'gzip',
            }) : nullPlugin,

            /**
             * https://medium.com/@poshakajay/heres-how-i-reduced-my-bundle-size-by-90-2e14c8a11c11
             * https://medium.com/@samparkinson_/brotli-compression-34e436efca0
             * https://www.npmjs.com/package/brotli-gzip-webpack-plugin
             */
            process.env.NODE_ENV === 'production' ?
            /** fallback GZIP for almost browser and http */ new BrotliGzipPlugin({
                asset: '[path].gz[query]',
                algorithm: 'gzip',
                test: /\.(js|css|html|svg)$/,
                minRatio: 0.8,
            }) : nullPlugin,

            process.env.NODE_ENV === 'production' ?
            /** use Br if browser supports*/ new BrotliGzipPlugin({
                asset: '[path].br[query]',
                algorithm: 'brotli',
                test: /\.(js|css|html|svg)$/,
                minRatio: 0.8,
            }) : nullPlugin,
        ],
        resolve: {
            alias: {
                "@": path.resolve(__dirname, "./src"),
                styles: path.resolve(__dirname, "src/assets/styles"),
            }
        },
        performance: {
            maxEntrypointSize: 1024 * 1024, // 1MB
            maxAssetSize: 1024 * 1024, // 1MB
        },

        output: {
            filename: '[name].[hash].js',
            chunkFilename: '[name].[hash].js',
        }
    },
    // https://cli.vuejs.org/guide/webpack.html#chaining-advanced
    // https://forum.vuejs.org/t/vue-cli-3-add-loader/39209/2
    chainWebpack: config=>{
        config.module
            .rule('csv')
            .test(/\.(csv|xls|xlsx)$/)
            .use('file-loader')
            .loader('file-loader')
            .options({
                name: "[name].[hash].[ext]",
            }).end();
    }
};
