import {SR_007} from '../../../src/messages/SR_007';
import * as functions from "firebase-functions";
import * as admin from 'firebase-admin';
import {listStage} from "../../../src/common/pipeline";
import {PIPELINE_STAGE_COLLECTION} from "../../../src/models/interfaces";

async function createStageDefault(pipeline_id: string, firestore: admin.firestore.Firestore) {
    listStage.map(async (stage, index)=>{
        const _stage: any = {};
        if (stage.showInPositionPipelineStageModal){
            Object.assign(_stage, stage);
            _stage.pipeline_id = pipeline_id;
            _stage.index = index;
            await firestore.collection(PIPELINE_STAGE_COLLECTION).add(_stage);
        }
    });
}

export const onCreateCompany = function(_functions: functions.FunctionBuilder, _admin: admin.app.App) {
    return _functions.firestore
        .document('companies/{company_id}')
        .onCreate(async (change, context) => {
            const company_id = context.params.company_id;

            // Create pipeline default
            const res = await _admin.firestore().collection('Pipeline').add({
                is_default: true,
                title: SR_007.SR_007_DEFAULT_PIPELINE_TITLE,
                company_id: company_id,
                updatedAt: Date.now(), // Need update this field for query in function getAllPipelines
                createdAt: Date.now(), // Need update this field for query in function getAllPipelines
                used_positions: []
            });

            // Create stages default

            await createStageDefault(res.id, _admin.firestore());
            try{
                let portalData  = {
                    appearance_button_color: "#ff9472",
                    appearance_favicon: "https://firebasestorage.googleapis.com/v0/b/cloudjet-work.appspot.com/o/image%2Fportal%2F7yNHTJiCg8OoWmwfTP8L%2Flogo%2F7yNHTJiCg8OoWmwfTP8L7yNHTJiCg8OoWmwfTP8L_Symbol-white.png?alt=media&token=851a1fa4-7931-487d-b2d3-2a25066d1ae0",
                    appearance_header: "https://firebasestorage.googleapis.com/v0/b/cloudjet-work.appspot.com/o/image%2Fportal%2F7yNHTJiCg8OoWmwfTP8L%2FheaderImage%2F7yNHTJiCg8OoWmwfTP8L7yNHTJiCg8OoWmwfTP8L_799.jpg?alt=media&token=f137b64a-65c4-4591-b0d2-b5ee06594831",
                    appearance_header_backgroud_color: "rgb(58, 55, 62)",
                    appearance_link_color: "#5558da",
                    appearance_logo: "https://firebasestorage.googleapis.com/v0/b/cloudjet-work.appspot.com/o/image%2Fportal%2F7yNHTJiCg8OoWmwfTP8L%2Flogo%2F7yNHTJiCg8OoWmwfTP8L7yNHTJiCg8OoWmwfTP8L_Symbol-white.png?alt=media&token=851a1fa4-7931-487d-b2d3-2a25066d1ae0",
                    apply_by_indeed: true,
                    apply_by_linkedin: true,
                    blog: "https://blog.cloudjetpotential.com/",
                    company_address_url: "",
                    company_id: company_id,
                    config: "bold",
                    createdAt: Date.now(),
                    created_by: "",
                    created_date: Date.now(),
                    css: "@import url('https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap&subset=vietnamese'); h1, h2, h3, h4, h5, h6, body, #app { font-family: 'Nunito Sans', sans-serif; } /* POSITION */ .landing-page #position .container-wrapper h1.polygot { font-size: 36px!important; font-weight: 600; margin-bottom: 40px; color: #fe9274; -webkit-background-clip: text; -webkit-text-fill-color: transparent; background-image: radial-gradient( circle farthest-corner at 12.3% 19.3%, rgba(85,88,218,1) 0%, rgba(95,209,249,1) 100.2% ); } .landing-page .position-apply-button { background: rgb(255, 148, 114); border: none; } .landing-page .position-title { color: #ff9472; font-weight: 600; } .landing-page #position .container-wrapper .positions .transition { margin-top: 20px; border: 1px solid rgba(67, 69, 94, .1); } /* MENU */ .landing-page .header-nav .link a { color: #fff; font-size: 16px; } .landing-page .header-nav .link a:hover { color: #fe9373; } .landing-page .header-nav .align-items-center .links .bzyButtonColor { padding: 8px 16px; border: 1px solid #ffffff20; border-radius: 4px; } .bzyButtonColor:hover { background: #fe9373 !important; } .link.misc.social a { font-size: 22px !important; } / *FOOTER */ .bg-dark { max-height: 50px !important; background-color: #6273db!important; } .landing-page #heroBackgroundColor .banner .banner-text .btn-outline-light, .landing-page #heroBackgroundColor .banner .btn-outline-light { padding: 16px; border: none; font-size: 16px; font-weight: 600; color: #fff; border-radius: 6px; } #heroBackgroundColor { background-image: radial-gradient( circle farthest-corner at 12.3% 19.3%, rgba(85,88,218,.8) 0%, rgba(95,209,249,.8) 100.2% ); } .fixed-top { background-image: radial-gradient( circle farthest-corner at 12.3% 19.3%, rgba(85,88,218,1) 0%, rgba(95,209,249,1) 100.2% ) !important; box-shadow: 0px 2px 8px rgba(3, 50, 86, .1); border-bottom: 1px solid rgba(3, 50, 86, .1); } .btn-lg.btn-outline-light{ background: rgb(255, 148, 114) !important; box-shadow: 0 4px 8px #90193e50; } .btn-lg.btn-outline-light:hover { box-shadow: 0 16px 24px #90193e50; } .banner-text h1 { font-weight: 900 !important; line-height: 1 !important; font-size: 64px !important; margin-top: 50px !important; } /* PERK */ .icon-perk { background-image: radial-gradient( circle farthest-corner at 12.3% 19.3%, rgba(85,88,218,1) 0%, rgba(95,209,249,1) 100.2% )!important; } /* TESTIMOIAL */ .testimonials-wraper-content { background: #6271db; } #testimonials .container-wrapper .testimonials .testimonial .name { font-weight: 600; } #testimonials .container-wrapper .testimonials .testimonial { border: none !important; } .landing-page .testimonials-wraper-content { background: #fafbff; } .bg-dark { max-height: 50px !important; background-color: #6273db!important; }",
                    description: "",
                    employees: [],
                    facebook: "https://www.facebook.com/cloudjetsolutions/",
                    instagram: "https://www.youtube.com/user/cloudjetsolutions",
                    js: "",
                    layout: [
                        {
                            is_show: true,
                            name: 'general',
                        },
                        {
                            is_show: true,
                            name: 'positions_list',
                        },
                        {
                            is_show: true,
                            name: 'testimonials',
                        },
                        {
                            is_show: true,
                            name: 'perks',
                        },
                        {
                            is_show: true,
                            name: 'gallery',
                        }
                    ],
                    linkedin: "https://www.linkedin.com/company/cloudjet-solutions/",
                    messaging_header: "Life is short, work somewhere wonderful",
                    messaging_introduction: `<p><strong style="color: rgb(34, 34, 34);">OUR MISSION</strong></p><p><br></p><p><strong style="color: rgb(34, 34, 34);">Create a world where anyone can belong anywhere</strong></p><p><span style="color: rgb(34, 34, 34);">It’s an audacious, incredibly rewarding mission that our increasingly diverse team is dedicated to achieving.</span></p><p><span style="color: rgb(34, 34, 34);">Cloudjet is built around the idea that everyone should be able to take the perfect trip, including where they stay, what they do, and who they meet. To that end, we empower millions of people around the world to use their spaces, passions, and talents to become entrepreneurs.</span></p><p><span style="color: rgb(34, 34, 34);">Exciting challenges lie ahead—new regions, technologies, and businesses. Guided by our four core values, we’ll meet these challenges creatively and with the support of our global community. </span></p><p><br></p><p><span style="color: rgb(34, 34, 34);">Join us!</span></p>`,
                    messaging_subheader: "Dear, join with us in this wonderful journey!",
                    perks: [
                        {
                            icon: 'fa fa-heart',
                            name: "Comprehensive health plans",
                        },
                        {
                            icon: 'fa fa-child',
                            name: "Paid volunteer time",
                        },
                        {
                            icon: 'fa fa-utensils',
                            name: "Healthy food and snacks",
                        },
                        {
                            icon: 'fa fa-user',
                            name: "Generous parental and family leave",
                        },
                        {
                            icon: 'fa fa-graduation-cap',
                            name: "Học hỏi và phát triển",
                        },
                        {
                            icon: 'fa fa-plane',
                            name: "Du lịch hàng năm cùng công ty",
                        },
                    ],
                    photos: [
                        {
                            url: 'https://firebasestorage.googleapis.com/v0/b/just-landing-229509.appspot.com/o/image%2Fportal%2FB8jKwOq5p4C5lhOSE8Ja%2Fmedia%2Fphotos%2FB8jKwOq5p4C5lhOSE8JaB8jKwOq5p4C5lhOSE8Ja_7yNHTJiCg8OoWmwfTP8L7yNHTJiCg8OoWmwfTP8L_1257.jpg?alt=media&token=46c07546-9181-4841-b9af-cf58760e89df',
                        },
                        {
                            url: 'https://firebasestorage.googleapis.com/v0/b/just-landing-229509.appspot.com/o/image%2Fportal%2FB8jKwOq5p4C5lhOSE8Ja%2Fmedia%2Fphotos%2FB8jKwOq5p4C5lhOSE8JaB8jKwOq5p4C5lhOSE8Ja_7yNHTJiCg8OoWmwfTP8L7yNHTJiCg8OoWmwfTP8L_1633.jpg?alt=media&token=c73699c7-8d6f-44be-844e-46173a42face',
                        },
                        {
                            url: 'https://firebasestorage.googleapis.com/v0/b/just-landing-229509.appspot.com/o/image%2Fportal%2FB8jKwOq5p4C5lhOSE8Ja%2Fmedia%2Fphotos%2FB8jKwOq5p4C5lhOSE8JaB8jKwOq5p4C5lhOSE8Ja_7yNHTJiCg8OoWmwfTP8L7yNHTJiCg8OoWmwfTP8L_Screen%20Shot%202019-07-19%20at%2012.36.36%20AM.png?alt=media&token=d3f35800-4d95-4c54-8edf-b6b6a4031395',
                        }
                    ],
                    tax_code: "0312033924",
                    testimonials: [
                        {
                            name: "Hoàng Tùng",
                            photo: "https://firebasestorage.googleapis.com/v0/b/cloudjet-work.appspot.com/o/image%2Fportal%2F7yNHTJiCg8OoWmwfTP8L%2FTestimonials%2F7yNHTJiCg8OoWmwfTP8L7yNHTJiCg8OoWmwfTP8L_hoangtung.png?alt=media&token=119b118a-9119-4bfd-993e-557abf25a6bd",
                            position:  "Giám đốc kinh doanh",
                            testimonial: "Khoảng thời gian làm việc trong nhóm kinh doanh tại công ty đã cho tôi một trải nghiệm tuyệt vời. Được học hỏi rất nhiều từ sếp và đồng nghiệp.",
                        },
                        {
                            name: "Phương Nguyễn",
                            photo: "https://firebasestorage.googleapis.com/v0/b/cloudjet-work.appspot.com/o/image%2Fportal%2F7yNHTJiCg8OoWmwfTP8L%2FTestimonials%2F7yNHTJiCg8OoWmwfTP8L7yNHTJiCg8OoWmwfTP8L_tempImageForSave.JPG?alt=media&token=9764f120-5e9c-4ebb-954f-2cfb332b1a18",
                            position:  "Nhân viên thiết kế",
                            testimonial: "Ở đây mình được thỏa sức sáng tạo những ý tưởng, sáng kiến mới. Đi làm mỗi ngày đều vui và đầy năng lượng.",
                        },
                        {
                            name: "Hà Nguyễn",
                            photo: "https://firebasestorage.googleapis.com/v0/b/cloudjet-work.appspot.com/o/image%2Fportal%2F7yNHTJiCg8OoWmwfTP8L%2FTestimonials%2F7yNHTJiCg8OoWmwfTP8L7yNHTJiCg8OoWmwfTP8L_IMG_3388.JPG?alt=media&token=ae6e3d05-5f9c-413a-980d-3f93137657b5",
                            position:  "Thực tập sinh",
                            testimonial: "Làm việc tại đây rất vui vẻ và thoải mái. Được thử thách với các sản phẩm và giải quyết các vấn đề đau đầu nhưng cực kỳ thú vị.",
                        },
                    ],
                    title: "",
                    twitter: "https://twitter.com/cjs_kpi",
                    updatedAt: Date.now(),
                    updated_by: '',
                    updated_date: '',
                    video_position_page: true,
                    video_positions_page: false,
                    vimeo_video: "",
                    website: "http://www.cloudjetpotential.com/",
                    youtube_video: "https://www.youtube.com/watch?v=VoCvLd--P-o"
                };
                // Create portal
                await _admin.firestore().collection('Portal').add(portalData)
            }catch(e){
                console.log(e);
            }
        });
};


// This function use to add new field to old Model.
//TODO: Improve this, can add any add from request POST.
export const addNewFieldToModel = functions.https.onRequest((req, res) => {
    // This function use to fixing wrong value Data.
    // Fix candicate_scorecard => candidate_scorcard.
    // This function just use to fixed. Remove this when done.
    // return cors(req, res, () => {
    let db = admin.firestore();
    try{
        //Phase 1: Fix value in actions of PipelineStage.
        db.collection("Position")
            .get()
            .then(async (snapShot: any) => {
                if (snapShot.empty){
                    console.log('No data!');
                    // res.send('Nothing need to fix!!');
                }else{
                    const arrayProgress = snapShot.docs.map(async (doc: any)=>{
                       let positionAddress = doc.data().address;
                       if (!positionAddress){
                           await doc.ref.update({
                               address: ''
                           }).then(()=>{
                               console.log('Added at: ', doc.data().title);
                           })
                       }
                    })
                    await Promise.all(arrayProgress);
                    console.log('Done 100%');
                }

            }).catch((e)=>{console.log(e)});
        res.send('Completed!!!');
    }
    catch(e){
        res.send(e)
    }
    // })
});
