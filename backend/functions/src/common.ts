import * as admin from "firebase-admin";
import {Bucket} from "@google-cloud/storage";

export const getProjectId = ():string => {
    //@ts-ignore
    const adminConfig = JSON.parse(process.env.FIREBASE_CONFIG);
    return adminConfig.projectId;
};
// https://stackoverflow.com/questions/35949554/invoking-a-function-without-parentheses
export const email_domain={
    toString(): string {
        let emailDomain:string='cloudjetpotential.com';

        const projectId:string = getProjectId();
        if(projectId==='cloudjet-work'){
            emailDomain='cloudjetpotential.com';
        }else if(projectId==='testtempqc'){
            emailDomain='qc.cloudjetpotential.com';
        }else{//dev
            emailDomain='dev.cloudjetpotential.com';
        }
        return process.env.EMAIL_DOMAIN || emailDomain
    }
};

export const url_app={
    toString(): string {
        let linkApp:string='https://pro.cloudjetpotential.com';

        const projectId:string = getProjectId();
        if(projectId==='cloudjet-work'){
            linkApp='https://pro.cloudjetpotential.com';
        }else if(projectId==='testtempqc'){
            linkApp='https://qc-cloudjetpotential.netlify.com';
        }else{//dev
            linkApp='http://localhost:8080';
        }
        return linkApp;
    }
};

export const getAppOptions=(project_id?:string):{name:string, options:admin.AppOptions} => {
    // const result:{name:string, options:admin.AppOptions}[]=[];
    // https://stackoverflow.com/questions/40625484/create-multiple-instances-of-firebase-admin
    // @ts-ignore
    let options:admin.AppOptions = undefined;
    let app_name:string='cloudjet-work';
    // https://console.firebase.google.com/project/testtempqc/settings/serviceaccounts/adminsdk
    let serviceAccount;
    let projectId:string;
    if(!project_id){
        projectId = getProjectId();
    }else{
        projectId = project_id;
    }


    if(projectId==='cloudjet-work'){
        app_name = 'cloudjetpotential.com';
        // https://levelup.gitconnected.com/firebase-import-json-to-firestore-ed6a4adc2b57
        serviceAccount = require("./credentials/cloudjet-work-firebase-adminsdk-sqtqy-d20ce1fdd9.json");
        options = {
            // projectId: projectId, // no need
            databaseURL: "https://cloudjet-work.firebaseio.com",
            storageBucket: "cloudjet-work.appspot.com", // get default
            // credential: admin.credential.applicationDefault(),
            credential: admin.credential.cert(serviceAccount),

        };
    }else if(projectId==='testtempqc'){
        serviceAccount = require("./credentials/testtempqc-firebase-adminsdk-eter0-a38c43176e.json");
        app_name='qc.cloudjetpotential.com';
        options = {
            // projectId: projectId, // no need
            databaseURL: "https://testtempqc.firebaseio.com/",
            storageBucket: "testtempqc.appspot.com", // get default
            // credential: admin.credential.applicationDefault(),
            credential: admin.credential.cert(serviceAccount),

        };
    }else{//dev
        app_name='dev.cloudjetpotential.com';
        serviceAccount = require("./credentials/just-landing-229509-firebase-adminsdk-0oetv-268bc0e93e.json");
        options = {
            // projectId: projectId, // no need
            databaseURL: "https://just-landing-229509.firebaseio.com",
            storageBucket: "just-landing-229509.appspot.com", // get default
            // credential: admin.credential.applicationDefault(),
            credential: admin.credential.cert(serviceAccount),

        };

    }
    return {name:app_name, options:options}
};

// init admin tools
export const initAdminTools = (project_id?:string):{firebase_app:admin.app.App, store:admin.firestore.Firestore, bucket:Bucket} => {
// export const initAdminTools = ():{firebase_app:admin.app.App, store:admin.firestore.Firestore} => {
    console.error('-----------initAdminTools--------------------------');
    const {name, options} = getAppOptions(project_id);
    // const firebase_app:admin.app.App = admin.initializeApp(options, name);
    const firebase_app:admin.app.App = admin.initializeApp(options, name);

    const store:admin.firestore.Firestore = firebase_app.firestore();

    //@ts-ignore
    const bucket:Bucket = firebase_app.storage().bucket(); // get default bucket
    console.error('----------end -initAdminTools--------------------------');
    return {firebase_app, store, bucket}
    // return {firebase_app, store, bucket}
};
// if we do not init app, this message will be show:   Your code does not appear to initialize the 'firebase-admin' module, so we've done it automatically.
admin.initializeApp();



const {firebase_app:dev_app, store:dev_store, bucket:dev_bucket} = initAdminTools('just-landing-229509');
const {firebase_app:qc_app, store:qc_store, bucket:qc_bucket} = initAdminTools('testtempqc');
const {firebase_app:production_app, store:production_store, bucket:production_bucket} = initAdminTools('cloudjet-work');


export const getAdminTools = (project_id?:string):{app:admin.app.App, store:admin.firestore.Firestore, bucket:Bucket} =>{
    let projectId:string='';
    if(!project_id){
        projectId=getProjectId();
    }else{
        projectId = project_id;
    }
    if(projectId==='cloudjet-work'){
        return {app:production_app, store:production_store, bucket:production_bucket}
    }

    else if(projectId==='testtempqc'){
        return {app:qc_app, store:qc_store, bucket:qc_bucket}
    }
    else {
        return {app:dev_app, store:dev_store, bucket:dev_bucket}
    }
};
