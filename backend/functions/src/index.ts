import * as functions from 'firebase-functions';
import BaseConversation from '../../../src/models/BaseConversation';
import BaseCandidate from '../../../src/models/BaseCandidate';
import BaseInbox from '../../../src/models/BaseInbox';
import {
    IConversation,
    CANDIDATE_COLLECTION,
    CONVERSATION_COLLECTION, MailState,
    INBOX_COLLECTION,
    IInbox,
    POSITION_COLLECTION

} from "../../../src/models/interfaces";

import {File} from "@google-cloud/storage";
import {DocumentReference} from '@google-cloud/firestore';
const Busboy = require('busboy');
// @ts-ignore
// const admin = require('firebase-admin');
import * as admin from 'firebase-admin';
import {
    onCreateCompany as _onCreateCompany
} from "./firestoreTrigger";
const path = require('path');
const cors = require('cors')({origin: true});
const moment = require('moment');
const replyParser = require("node-email-reply-parser");
// const Email = require('email-templates');
const Email = require('cjs-email-templates');
import {CJPBackup as _CJPBackup} from "./backup";
import {email_domain, getAdminTools, getProjectId, url_app} from "./common";
import {api_app} from "./api";


// import _ from 'lodash';
const _ = require('lodash');
export const Messages = {
    SR_EMAIL_TITLE1:"đã mời bạn tham gia một cuộc hẹn với",
    SR_EMAIL_TITLE2:"ứng viên ứng tuyển vào vị trí",
    SR_EMAIL_TITLE3:"",
    SR_EMAIL_TITLE4:"Chi tiết",
    SR_EMAIL_TITLE5:"Mô tả:",
    SR_EMAIL_TITLE6:"Xem ứng viên",
    SR_EMAIL_TITLE7:"Cuộc hẹn với ứng viên:",
    SR_EMAIL_TITLE8:"Địa điểm:",
    // SR_EMAIL_TITLE_E9:"applied to position",
    SR_EMAIL_TITLE_E9:"đã ứng tuyển vào vị trí",
    SR_EMAIL_CANCEL_TITLE1:"đã hủy cuộc phỏng vấn với",
    SR_EMAIL_CANCEL_TITLE2:"vào vị trí",
    SR_EMAIL_CANCEL_TITLE3:"đã hủy cuộc hẹn phỏng vấn với ứng viên ",
    SR_EMAIL_CANCEL_TITLE4:"đã hủy cuộc phỏng vấn.",
    SR_EMAIL_CANCEL_TITLE5:"ứng tuyển vào vị trí ",
    SR_EMAIL_EDIT_TITLE6: "đã điều chỉnh thông tin cuộc phỏng vấn.",
    SR_EMAIL_TASKS_SUBJECT:"Bạn mới được giao một công việc",
    SR_EMAIL_TASKS_TITLE1: "Xin chào, \n",
    SR_EMAIL_TASKS_TITLE2: "Bạn vừa được giao công việc sau đây:",
    SR_EMAIL_TASKS_BTN:"Xem chi tiết công việc",
    SR_EMAIL_TASKS_DUE: "Hạn cuối",
    SR_EMAIL_TASKS_UPDATE:"Bạn vừa được giao lại công việc sau đây:",
    // SR_EMAIL_MANAGER_POSITION_TITLE1:"You have a new candidate for the",
    // SR_EMAIL_MANAGER_POSITION_TITLE2:"position",
    SR_EMAIL_MANAGER_POSITION_TITLE1:"Bạn có một ứng viên ứng tuyển vào vị trí",
    SR_EMAIL_MANAGER_POSITION_TITLE2:"position",

    SR_EMAIL_ASSIGN_CANDIDATE_TITLE1: "Xin chào!",
    SR_EMAIL_ASSIGN_CANDIDATE_TITLE2:"đã phân công ứng viên",
    SR_EMAIL_ASSIGN_CANDIDATE_TITLE3:"cho bạn",
    SR_EMAIL_ASSIGN_CANDIDATE_SUBJECT: "Bạn được phân công ứng viên",
    SR_EMAIL_ASSIGN_CANDIDATE_BTN:"Xem hồ sơ ứng viên",

    SR_EMAIL_CANDIDATE_RELY_TITLE1:"Email ứng viên phản hồi vào vị trí",
    SR_EMAIL_CANDIDATE_RELY_TITLE3:"Xem nội dung hội thoại",
    SR_EMAIL_CANDIDATE_RELY_TITLE4:"đã phản hồi vào vị trí",
};


/* TEST SEND MAIL
https://sendgrid.com/docs/API_Reference/Web_API_v3/Mail/index.html
https://stackoverflow.com/questions/10060098/getting-only-response-header-from-http-post-using-curl

curl -D - -X "POST" "https://api.sendgrid.com/v3/mail/send" -H "Authorization: Bearer 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"  -H "Content-Type: application/json"  -d '
{
  "personalizations": [
    {
      "to": [
        {
          "email": "quocduan@gmail.com"
        }
      ],
      "subject": "Hello, World!"
    }
  ],
  "from": {
    "email": "hello@qc.cloudjetpotential.com"
  },
  "content": [
    {
      "type": "text/plain",
      "value": "Hello, World!"
    }
  ]
}
'
 */


/*
*
*
* EMAIL CONFIGURATIONS
*
*
* */
/**
 * QUOC DUAN NOTE: THE FOLLOWING REFERENCES IS OBSOLETED, WHICH ONLY SUPPORT SENDGRID API V2 THAT NOT RESPONSE DETAIL ABOUT SENT-MESSAGES
 *
 * Here we're using Sendgrid to send
 * https://sendgrid.com/blog/sending-email-nodemailer-sendgrid/
 * https://www.npmjs.com/package/nodemailer-sendgrid-transport
 */
// var sgTransport = require('nodemailer-sendgrid-transport');
//
//
// const options = {
//     // auth: {
//     //     //TODO: move to env variable
//     //     api_user: 'app57762236@heroku.com', // SENDGRID_USERNAME
//     //     api_key: 'fexujzj22645'
//     // },
//     auth: {
//         //TODO: move to env variable
//         // api_user: 'apikey', // SENDGRID_USERNAME
//         // api_user: 'YXBpa2V5', // SENDGRID_USERNAME
//         // api_key: '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'
//         // cloudjet potential
//         api_key: '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'
//         // api_key: 'U0cuUDZRa1NZOUFRMkMxekd6YU9lZWlXdy44aXhKX25YcnVPQmNtSGJ2TzlaV3gtREZvR1MzQnJXLUZVMUstTUF0ZlNN'
//
//         // api_user: 'quocduan', // SENDGRID_USERNAME
//         // api_key: 'f8SIsm18qmMl'
//     },
// }
// const emailClient = nodemailer.createTransport(sgTransport(options));


const nodemailer = require('nodemailer');
const nodemailerSendgrid = require('cjs-nodemailer-sendgrid');
const transporter = nodemailerSendgrid({
    // apiKey: process.env.SENDGRID_API_KEY
    apiKey: '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'
});
const transport = nodemailer.createTransport(transporter);

const sendMailCommon = (request:functions.Request, response:functions.Response, useTemplate:boolean=false, mailOptions:{[key:string]:any}, success?:Function, error?:Function, autoSendResponse:boolean=true) => {
    const conversation_id:string = request.body.conversation_id || null;
    const store = admin.firestore();
    const commonSuccess = async (res:[{statusCode:string|number, statusMessage?:string, headers:{"x-message-id":string, [other:string]:any},  [other:string]:any}, ...any[] ]) => {
        //@ts-ignore
        console.log(res.originalMessage);
        console.log('-------------------------------------------------');
        console.log(res.length);
        console.log(res[0].headers['x-message-id']);

        const message_id = res[0].headers['x-message-id'];
        // @ts-ignore
        const originalMessage = res.originalMessage || mailOptions;

        if (conversation_id) {
            console.log(`conversation_id ${conversation_id}`);
            try {


                // https://firebase.google.com/docs/firestore/manage-data/add-data#update-data
                //@ts-ignore
                // docRefs.update({mail_id: 'blabla'}).then(()=>{console.log('success')}).catch((e)=>{console.log('error'); console.error(e);});


                const docRefs: DocumentReference = store.collection(CONVERSATION_COLLECTION).doc(conversation_id);
                await new Promise(((resolve, reject) => {
                    // const mail_state:MailState='sent';
                    // docRefs.set({mail_id: message_id, object:{sent_mail_data: originalMessage, mail_state:mail_state}}, {merge: true})
                    docRefs.set({mail_id: message_id, object:{sent_mail_data: originalMessage}}, {merge: true})
                        .then(() => {
                            console.log('success');
                            resolve();
                        })
                        .catch(() => {
                            console.log('error');
                            reject();
                        });
                    // store.collection(CONVERSATION_COLLECTION).doc(conversation_id).update({mail_id: message_id}).then(()=>{console.log('success')}).catch(()=>{console.log('error')});
                    // store.collection(CONVERSATION_COLLECTION).doc(conversation_id).set({mail_id: message_id}, {merge:true}).then(()=>{console.log('success')}).catch(()=>{console.log('error')});

                    // store.collection(CONVERSATION_COLLECTION).doc(conversation_id).get().then(async (doc:any) => {
                    //         if (doc.exists) {
                    //
                    //             const conversation: BaseConversation = new BaseConversation();
                    //             Object.assign(conversation, doc.data());
                    //             conversation.id = conversation_id;
                    //             conversation.mail_id=message_id;
                    //
                    //
                    //         }
                    //     }
                    // );
                }));

            } catch (e) {
                console.log(e);
            }

        }

    };
    const commonError = async (e:any)=>{
        console.error(e);
        // @ts-ignore
        const originalMessage = e.originalMessage || mailOptions;
        console.log('-------------------------------------------------');

        console.error(originalMessage);
        if (conversation_id) {
            const mail_state:MailState='failed';

            await  new Promise((resolve, reject) => {
                // store.collection(CONVERSATION_COLLECTION).doc(conversation_id).set({sent_mail_data: originalMessage, object:{mail_state:mail_state}}, {merge:true})
                store.collection(CONVERSATION_COLLECTION).doc(conversation_id).set({object:{mail_state:mail_state, sent_mail_data: originalMessage}}, {merge:true})
                    .then(()=>{console.log('success'); resolve();})
                    .catch(()=>{console.log('error'); reject();});
            });
        }

    };

    if(useTemplate===true){
        const email = new Email({
            // use globale nodemailer version
            transport: transport, // will raise error: TypeError: mail.normalize is not a function
            // QUOCDUAN NOTE: we also use `transporter` instead of `transport`, because `email-templates` auto create a transport based on provided `transporter`
            // note 2: `email-templates` use independent `nodemailer` package version not the top-level version of `nodemailer`
            // transport: transporter,
            send: true,
            preview: false,
        });

        email.send(mailOptions)
            .then(async (res:[{statusCode:string|number, statusMessage?:string, headers:{"x-message-id":string, [other:string]:any},  [other:string]:any},
            ...any[] ])=>{

            await commonSuccess(res);
            if (success){
                success(res);
            }else{
                if(autoSendResponse){
                    response.send('success');
                }
            }

        })
            .catch(async (e:any) => {
            await commonError(e);
            if(error){
                error(e);
            }else{
                // console.log(e.originalMessage);
                console.error(e);
                if(autoSendResponse){
                    response.send('error');
                }
            }

        });
    }else{
        transport.sendMail(mailOptions).then(async (res:[{statusCode:string|number, statusMessage?:string, headers:{"x-message-id":string, [other:string]:any},  [other:string]:any},
            ...any[] ])=>{

            await commonSuccess(res);
            if (success){
                success(res);
            }else{
                if(autoSendResponse){
                    response.send('success');
                }
            }
        }).catch(async (e:any)=>{
            await commonError(e);
            if(error){
                error(e);
            }else{
                if(autoSendResponse){
                    response.send('error');
                }
            }
        })
    }
};

export const resendMail = functions.https.onRequest((request, response) => {

    cors(request, response,  () => {
        // response.send({status:'hello'});
        // if (request.method !== 'POST') {
        //     response.send("Method not allowed");
        //     // Return a "method not allowed" error
        //     // return response.status(405).end();
        //     return
        //
        //
        // }
        //
        const conversation_id:string = request.body.conversation_id || null;
        const store = admin.firestore();
        const msg:{
            status:'success'|'error',
            message:string
        }={
            status:'success',
            message:''
        };

        if (conversation_id){
            const docRefs: DocumentReference = store.collection(CONVERSATION_COLLECTION).doc(conversation_id);




            docRefs.get().then(async (doc:any) => {
                if (doc.exists) {
                    // const mail_state:MailState='pending';
                    const target_mail_state:MailState='failed';

                    const conversation: BaseConversation = new BaseConversation();
                    Object.assign(conversation, doc.data());
                    conversation.id = conversation_id;
                    console.log(conversation.object);
                    console.log(conversation.object.sent_mail_data);
                    const sent_mail_data:any = conversation.object.sent_mail_data || null;
                    console.log(sent_mail_data);
                    if(conversation.object.mail_state === target_mail_state) {
                        // await new Promise((resolve, reject) => {
                        //     docRefs.set({object:{mail_state:mail_state}}, {merge:true})
                        //         .then(()=>{console.log('success'); resolve();})
                        //         .catch(()=>{console.log('error'); reject();});
                        // });
                        // resend mail
                        try{
                            sendMailCommon(request,response, false,
                                sent_mail_data,
                                function(res:any){
                                    msg.status='success';
                                    response.send(msg)
                                },
                                function(e:any){
                                    msg.status='error';
                                    response.send(msg)

                                }

                            );
                        }catch (e) {
                            msg.status='error';
                            response.send(msg)
                        }
                            // msg.status='error';
                            // response.send(msg)

                    }else{
                        msg.status='success';
                        msg.message='In sending progress';
                        response.send(msg)
                    }

                }else {
                    msg.status='error';
                    msg.message = 'Conversation not found';
                    response.send(msg)
                }

            }).catch((e:any)=>{
                msg.message='internal error';
                msg.status='error';
                response.send(msg)
            });
        }else{
            msg.message='internal error';
            msg.status='error';
            response.send(msg)
        }
    });



});

/* END EMAIL CONFIGURATION */
export const testSendMail = functions.https.onRequest((request, response) => {
    /**
     *  Send email test using @sendgrid/mail
     *  refs: https://www.npmjs.com/package/@sendgrid/mail
     *
     * @param {Object} request Cloud Function request context.
     *                     More info: https://expressjs.com/en/api.html#req
     * @param {Object} response Cloud Function response context.
     *                     More info: https://expressjs.com/en/api.html#res
     */
    console.log(request);
    console.log(request.body);

    // initAdminTools();
    console.error('testSendMail ');
    cors(request, response, async () => {
        const store = admin.firestore();
        try{
            const a=await store.collection(CONVERSATION_COLLECTION).limit(3).get();
            console.log(a);
        }catch (e) {
            console.error(e);
        }


        const mailOptions = {
            from: `"Cloudjet Potential" <help@${email_domain}>`,
            to: "quocduan@gmail.com",
            subject: "hello guy",
            //    text: "",
            html: "hello",
            attachments: [],
            cc: [],
            bcc: [],
            //https://sendgrid.com/docs/API_Reference/SMTP_API/unique_arguments.html
            // unique_args: {"customerAccountNumber": "55555", },
            // https://sendgrid.com/docs/for-developers/sending-email/personalizations/
            // https://sendgrid.com/docs/API_Reference/Web_API_v3/Mail/index.html#
            // https://sendgrid.com/docs/for-developers/tracking-events/event/#unique-arguments-and-custom-arguments
            // These are values that are specific to this personalization that will be carried along with the email, activity data, and links.
            custom_args: {"potentialServer": "qc.cloudjetpotential.com", }, // for checking event notification
        };

        console.log('---------------- BEFORE SEND MAIL-----------------');

        const sgMail = require('@sendgrid/mail');
        sgMail.setApiKey('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');
        sgMail.send(mailOptions, function(){
            console.log(arguments);
            console.log(arguments[1][0].headers); // to get x-message-id
            return response.send('ok')
        });

        // return emailPromise
    });

});
export const testSendMailWithTemplate = functions.https.onRequest((request, response) => {
    /**
     *
     * send email test using nodemailer-sendgrid and email-template
     * refs:
     *      + https://www.npmjs.com/package/nodemailer-sendgrid
     *      + https://www.npmjs.com/package/email-templates
     *
     * */
    cors(request, response, () => {


        sendMailCommon(request,response, true,
            {
                // template: `../../../inviteUserInterview`,
                // absolute path
                // https://www.npmjs.com/package/email-templates#absolute-path-to-templates
                // template: path.join(__dirname, '..', '..','..','inviteUserInterview'),
                template: path.join(__dirname, 'inviteUserInterview'),
                message: {
                    from: `Cloudjet Potential HR <no-reply@${email_domain}>`,
                    to: 'duan.tnq@outlook.com',
                    // attachments: [// this will add as text/calendar
                    //     {
                    //         "content": "QkVHSU46VkNBTEVOREFSDQpWRVJTSU9OOjIuMA0KUFJPRElEOi1DSlMtUE9URU5USUFMIDIuMC8vRU4NCkNBTFNDQUxFOkdSRUdPUklBTg0KTUVUSE9EOlJFUVVFU1QNCkJFR0lOOlZFVkVOVA0KVUlEOmNhbGVuZGFyLWFzZGZzZGZhc2RmDQpEVFNUQU1QOjIwMTkwODA2VDAzNTI1NA0KU1RBVFVTOkNPTkZJUk1FRA0KRFRTVEFSVDoyMDE5MDgyMlQwNDAwMDBaDQpEVEVORDoyMDE5MDgyM1QwNTAwMDBaDQpTVU1NQVJZOmR1YW4udG5xIE1lZXRpbmcgKEEpDQpERVNDUklQVElPTjpZb3UgYXJlIGNvbmZpcm1lZCBmb3IgdGhlIGZvbGxvd2luZyBpbnRlcnZpZXcgb24gV2VkIEF1ZyAyMToNCg0KVGl0bGU6IGR1YW4udG5xIE1lZXRpbmcgKEEpDQpTdGFydCBUaW1lOiAxMTowMCBBTSAoKzA3KQ0KDQoNCg0KDQpPUkdBTklaRVI7Q049UXVvY0R1YW46bWFpbHRvOnF1b2NkdWFuQGdtYWlsLmNvbQ0KQVRURU5ERUU7Q1VUWVBFPUlORElWSURVQUw7UlNWUD1UUlVFO1JPTEU9UkVRLVBBUlRJQ0lQQU5UO1BBUlRTVEFUPU5FRURTLUFDVElPTjtDTj0iZHVhbi50bnEiO1gtTlVNLUdVRVNUUz0wOk1BSUxUTzpkdWFuLnRucUBvdXRsb29rLmNvbQ0KQVRURU5ERUU7Q1VUWVBFPUlORElWSURVQUw7UlNWUD1UUlVFO1JPTEU9UkVRLVBBUlRJQ0lQQU5UO1BBUlRTVEFUPU5FRURTLUFDVElPTjtDTj0iZHVhbjEiO1gtTlVNLUdVRVNUUz0wOk1BSUxUTzp0aG9pbmdvY3RoaWVuYW5AZ21haWwuY29tDQpTRVFVRU5DRTowDQpTVEFUVVM6Q09ORklSTUVEDQpFTkQ6VkVWRU5UDQpFTkQ6VkNBTEVOREFS",
                    //         "type": "application/ics",
                    //         "name": "invite.ics",
                    //         "filename": "invite.ics",
                    //         "disposition": "attachment"
                    //     }
                    //
                    // ],
                    icalEvent:{
                        content: "QkVHSU46VkNBTEVOREFSDQpWRVJTSU9OOjIuMA0KUFJPRElEOi1DSlMtUE9URU5USUFMIDIuMC8vRU4NCkNBTFNDQUxFOkdSRUdPUklBTg0KTUVUSE9EOlJFUVVFU1QNCkJFR0lOOlZFVkVOVA0KVUlEOmNhbGVuZGFyLWFzZGZzZGZhc2RmDQpEVFNUQU1QOjIwMTkwODA2VDAzNTI1NA0KU1RBVFVTOkNPTkZJUk1FRA0KRFRTVEFSVDoyMDE5MDgyMlQwNDAwMDBaDQpEVEVORDoyMDE5MDgyM1QwNTAwMDBaDQpTVU1NQVJZOmR1YW4udG5xIE1lZXRpbmcgKEEpDQpERVNDUklQVElPTjpZb3UgYXJlIGNvbmZpcm1lZCBmb3IgdGhlIGZvbGxvd2luZyBpbnRlcnZpZXcgb24gV2VkIEF1ZyAyMToNCg0KVGl0bGU6IGR1YW4udG5xIE1lZXRpbmcgKEEpDQpTdGFydCBUaW1lOiAxMTowMCBBTSAoKzA3KQ0KDQoNCg0KDQpPUkdBTklaRVI7Q049UXVvY0R1YW46bWFpbHRvOnF1b2NkdWFuQGdtYWlsLmNvbQ0KQVRURU5ERUU7Q1VUWVBFPUlORElWSURVQUw7UlNWUD1UUlVFO1JPTEU9UkVRLVBBUlRJQ0lQQU5UO1BBUlRTVEFUPU5FRURTLUFDVElPTjtDTj0iZHVhbi50bnEiO1gtTlVNLUdVRVNUUz0wOk1BSUxUTzpkdWFuLnRucUBvdXRsb29rLmNvbQ0KQVRURU5ERUU7Q1VUWVBFPUlORElWSURVQUw7UlNWUD1UUlVFO1JPTEU9UkVRLVBBUlRJQ0lQQU5UO1BBUlRTVEFUPU5FRURTLUFDVElPTjtDTj0iZHVhbjEiO1gtTlVNLUdVRVNUUz0wOk1BSUxUTzp0aG9pbmdvY3RoaWVuYW5AZ21haWwuY29tDQpTRVFVRU5DRTowDQpTVEFUVVM6Q09ORklSTUVEDQpFTkQ6VkVWRU5UDQpFTkQ6VkNBTEVOREFS",
                        filename:'invite.ics',

                    },
                    alternatives: [
                        {
                            contentType: "text/calendar;method=REQUEST",
                            // content: "BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:-CJS-POTENTIAL 2.0//EN\nCALSCALE:GREGORIAN\nMETHOD:REQUEST\nBEGIN:VEVENT\nUID:calendar-asdfsdfasdf\nDTSTAMP:20190806T035254\nSTATUS:CONFIRMED\nDTSTART:20190822T040000Z\nDTEND:20190823T050000Z\nSUMMARY:duan.tnq Meeting (A)\nDESCRIPTION:You are confirmed for the following interview on Wed Aug 21:\n\nTitle: duan.tnq Meeting (A)\nStart Time: 11:00 AM (+07)\n\n\n\n\nORGANIZER;CN=QuocDuan:mailto:quocduan@gmail.com\nATTENDEE;CUTYPE=INDIVIDUAL;RSVP=TRUE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=\"duan.tnq\";X-NUM-GUESTS=0:MAILTO:duan.tnq@outlook.com\nATTENDEE;CUTYPE=INDIVIDUAL;RSVP=TRUE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=\"duan1\";X-NUM-GUESTS=0:MAILTO:thoingocthienan@gmail.com\nSEQUENCE:0\nSTATUS:CONFIRMED\nEND:VEVENT\nEND:VCALENDAR"
                            value: "BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:-CJS-POTENTIAL 2.0//EN\nCALSCALE:GREGORIAN\nMETHOD:REQUEST\nBEGIN:VEVENT\nUID:calendar-asdfsdf\nDTSTAMP:20190806T035254\nSTATUS:CONFIRMED\nDTSTART:20190824T040000Z\nDTEND:20190825T050000Z\nSUMMARY:duan.tnq Meeting (A)\nDESCRIPTION:You are confirmed for the following interview on Wed Aug 21:\n\nTitle: duan.tnq Meeting (A)\nStart Time: 11:00 AM (+07)\n\n\n\n\nORGANIZER;CN=QuocDuan:mailto:quocduan@gmail.com\nATTENDEE;CUTYPE=INDIVIDUAL;RSVP=TRUE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=\"duan.tnq\";X-NUM-GUESTS=0:MAILTO:duan.tnq@outlook.com\nATTENDEE;CUTYPE=INDIVIDUAL;RSVP=TRUE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=\"duan1\";X-NUM-GUESTS=0:MAILTO:thoingocthienan@gmail.com\nSEQUENCE:0\nSTATUS:CONFIRMED\nEND:VEVENT\nEND:VCALENDAR"
                        }
                    ],
                    // content: [
                    //     {
                    //         "type": "text/plain",
                    //         "value": "Plain Content"
                    //     },
                    //     {
                    //         "type":"text/calendar;method=REQUEST",
                    //         "value": "BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:-CJS-POTENTIAL 2.0//EN\nCALSCALE:GREGORIAN\nMETHOD:REQUEST\nBEGIN:VEVENT\nUID:calendar-asdfsdfasdf\nDTSTAMP:20190806T035254\nSTATUS:CONFIRMED\nDTSTART:20190822T040000Z\nDTEND:20190823T050000Z\nSUMMARY:duan.tnq Meeting (A)\nDESCRIPTION:You are confirmed for the following interview on Wed Aug 21:\n\nTitle: duan.tnq Meeting (A)\nStart Time: 11:00 AM (+07)\n\n\n\n\nORGANIZER;CN=QuocDuan:mailto:quocduan@gmail.com\nATTENDEE;CUTYPE=INDIVIDUAL;RSVP=TRUE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=\"duan.tnq\";X-NUM-GUESTS=0:MAILTO:duan.tnq@outlook.com\nATTENDEE;CUTYPE=INDIVIDUAL;RSVP=TRUE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=\"duan1\";X-NUM-GUESTS=0:MAILTO:thoingocthienan@gmail.com\nSEQUENCE:0\nSTATUS:CONFIRMED\nEND:VEVENT\nEND:VCALENDAR"
                    //     },
                    // ],
                    cc: [],
                    custom_args: {"potentialServer": "qc.cloudjetpotential.com", }, // for checking event notification
                },
                locals: {
                    title1: exports.Messages.SR_EMAIL_TITLE1,
                    title2: exports.Messages.SR_EMAIL_TITLE2,
                    title3: exports.Messages.SR_EMAIL_TITLE3,
                    title4: exports.Messages.SR_EMAIL_TITLE4,
                    title5: exports.Messages.SR_EMAIL_TITLE5,
                    title6: exports.Messages.SR_EMAIL_TITLE6,
                    title7: exports.Messages.SR_EMAIL_TITLE7,
                    title8: exports.Messages.SR_EMAIL_TITLE8,
                    actor_name: 'duan  ',
                    candidate_name: 'duan',
                    position_name: 'duan',
                    link: "google.com",
                    description: 'duan',
                    interview_guide: 'duan',
                    location: ''
                },
            },

            // function(res:[{statusCode:string|number, statusMessage?:string, headers:{"x-message-id":string, [other:string]:any},  [other:string]:any},
            //     ...any[] ]){
            //
            //     // }).then(function(res:any){
            //     console.log('fkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk');
            //
            //     console.log('-------------------------------------------------');
            //     // console.log(res.originalMessage);
            //     // console.log(rest.originalMessage);
            //     //@ts-ignore
            //     console.log(res.originalMessage);
            //     console.log('-------------------------------------------------');
            //     console.log(res.length)
            //     // console.log(...res);
            //     // console.log('-------------------------------------------------');
            //     // console.log(res[0]);
            //     // console.log('-------------------------------------------------');
            //     // console.log(res[1]);
            //     response.send(res[0].headers['x-message-id']);
            //     // response.send('success');
            //
            //
            // },
            // function(e:any){
            //     console.log('-------------------------------------------------');
            //     console.log(e.originalMessage);
            //     console.log('-------------------------------------------------');
            //
            //     console.log('buggggggggggggggggggggggggggggggggggggggg');
            //     console.log(e);
            //     response.send(e)
            // }
        )


    });
});

// Manual test:
// $ curl -d '{"from":"help@cloudjetkpi.com", "to":"hong@cjs.vn", "subject": "inviteUserInterview", "html":"Dear you!"}' -H "Content-Type: application/json" https://us-central1-cloudjet-work.cloudfunctions.net/sendConversationEmail
export const testSendConversationEmail = functions.https.onRequest((request, response) => {
    /**
     * Send email  using `nodemailer-sendgrid`
     * refs:
     *      + https://www.npmjs.com/package/nodemailer-sendgrid
     *
     * run:  $> firebase emulators:start
     * http://localhost:5001/testtempqc/us-central1/testSendConversationEmail
     */


    cors(request, response, async () => {

        const mailOptions = {
            from: `"Cloudjet Potential" <help@${email_domain}>`,
            to: "duan.tnq@outlook.com",
            subject: "Test send converstation email",
            //    text: "",
            // html: "hello",// will raise error if both `html|text` & `content` present at the same time
            content: [
                {
                    "type": "text/plain",
                    "value": "Plain Content"
                },

                {
                    "type": "text/html",
                    "value": "Plain html Content"
                },
                {
                    "type":"text/calendar;method=REQUEST",
                    "value": "BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:-CJS-POTENTIAL 2.0//EN\nCALSCALE:GREGORIAN\nMETHOD:REQUEST\nBEGIN:VEVENT\nUID:calendar-asdfsdfasdf\nDTSTAMP:20190806T035254\nSTATUS:CONFIRMED\nDTSTART:20190822T040000Z\nDTEND:20190823T050000Z\nSUMMARY:duan.tnq Meeting (A)\nDESCRIPTION:You are confirmed for the following interview on Wed Aug 21:\n\nTitle: duan.tnq Meeting (A)\nStart Time: 11:00 AM (+07)\n\n\n\n\nORGANIZER;CN=QuocDuan:mailto:quocduan@gmail.com\nATTENDEE;CUTYPE=INDIVIDUAL;RSVP=TRUE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=\"duan.tnq\";X-NUM-GUESTS=0:MAILTO:duan.tnq@outlook.com\nATTENDEE;CUTYPE=INDIVIDUAL;RSVP=TRUE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=\"duan1\";X-NUM-GUESTS=0:MAILTO:thoingocthienan@gmail.com\nSEQUENCE:0\nSTATUS:CONFIRMED\nEND:VEVENT\nEND:VCALENDAR"
                },
            ],
            // attachments: [
            //     {
            //         "content": "QkVHSU46VkNBTEVOREFSDQpWRVJTSU9OOjIuMA0KUFJPRElEOi1DSlMtUE9URU5USUFMIDIuMC8vRU4NCkNBTFNDQUxFOkdSRUdPUklBTg0KTUVUSE9EOlJFUVVFU1QNCkJFR0lOOlZFVkVOVA0KVUlEOmNhbGVuZGFyLWFzZGZzZGZhc2RmDQpEVFNUQU1QOjIwMTkwODA2VDAzNTI1NA0KU1RBVFVTOkNPTkZJUk1FRA0KRFRTVEFSVDoyMDE5MDgyMlQwNDAwMDBaDQpEVEVORDoyMDE5MDgyM1QwNTAwMDBaDQpTVU1NQVJZOmR1YW4udG5xIE1lZXRpbmcgKEEpDQpERVNDUklQVElPTjpZb3UgYXJlIGNvbmZpcm1lZCBmb3IgdGhlIGZvbGxvd2luZyBpbnRlcnZpZXcgb24gV2VkIEF1ZyAyMToNCg0KVGl0bGU6IGR1YW4udG5xIE1lZXRpbmcgKEEpDQpTdGFydCBUaW1lOiAxMTowMCBBTSAoKzA3KQ0KDQoNCg0KDQpPUkdBTklaRVI7Q049UXVvY0R1YW46bWFpbHRvOnF1b2NkdWFuQGdtYWlsLmNvbQ0KQVRURU5ERUU7Q1VUWVBFPUlORElWSURVQUw7UlNWUD1UUlVFO1JPTEU9UkVRLVBBUlRJQ0lQQU5UO1BBUlRTVEFUPU5FRURTLUFDVElPTjtDTj0iZHVhbi50bnEiO1gtTlVNLUdVRVNUUz0wOk1BSUxUTzpkdWFuLnRucUBvdXRsb29rLmNvbQ0KQVRURU5ERUU7Q1VUWVBFPUlORElWSURVQUw7UlNWUD1UUlVFO1JPTEU9UkVRLVBBUlRJQ0lQQU5UO1BBUlRTVEFUPU5FRURTLUFDVElPTjtDTj0iZHVhbjEiO1gtTlVNLUdVRVNUUz0wOk1BSUxUTzp0aG9pbmdvY3RoaWVuYW5AZ21haWwuY29tDQpTRVFVRU5DRTowDQpTVEFUVVM6Q09ORklSTUVEDQpFTkQ6VkVWRU5UDQpFTkQ6VkNBTEVOREFS",
            //         "type": "application/ics",
            //         "name": "invite.ics",
            //         "filename": "invite.ics",
            //         "disposition": "attachment"
            //     }
            //
            // ],
            icalEvent:{
                content: "QkVHSU46VkNBTEVOREFSDQpWRVJTSU9OOjIuMA0KUFJPRElEOi1DSlMtUE9URU5USUFMIDIuMC8vRU4NCkNBTFNDQUxFOkdSRUdPUklBTg0KTUVUSE9EOlJFUVVFU1QNCkJFR0lOOlZFVkVOVA0KVUlEOmNhbGVuZGFyLWFzZGZzZGZhc2RmDQpEVFNUQU1QOjIwMTkwODA2VDAzNTI1NA0KU1RBVFVTOkNPTkZJUk1FRA0KRFRTVEFSVDoyMDE5MDgyMlQwNDAwMDBaDQpEVEVORDoyMDE5MDgyM1QwNTAwMDBaDQpTVU1NQVJZOmR1YW4udG5xIE1lZXRpbmcgKEEpDQpERVNDUklQVElPTjpZb3UgYXJlIGNvbmZpcm1lZCBmb3IgdGhlIGZvbGxvd2luZyBpbnRlcnZpZXcgb24gV2VkIEF1ZyAyMToNCg0KVGl0bGU6IGR1YW4udG5xIE1lZXRpbmcgKEEpDQpTdGFydCBUaW1lOiAxMTowMCBBTSAoKzA3KQ0KDQoNCg0KDQpPUkdBTklaRVI7Q049UXVvY0R1YW46bWFpbHRvOnF1b2NkdWFuQGdtYWlsLmNvbQ0KQVRURU5ERUU7Q1VUWVBFPUlORElWSURVQUw7UlNWUD1UUlVFO1JPTEU9UkVRLVBBUlRJQ0lQQU5UO1BBUlRTVEFUPU5FRURTLUFDVElPTjtDTj0iZHVhbi50bnEiO1gtTlVNLUdVRVNUUz0wOk1BSUxUTzpkdWFuLnRucUBvdXRsb29rLmNvbQ0KQVRURU5ERUU7Q1VUWVBFPUlORElWSURVQUw7UlNWUD1UUlVFO1JPTEU9UkVRLVBBUlRJQ0lQQU5UO1BBUlRTVEFUPU5FRURTLUFDVElPTjtDTj0iZHVhbjEiO1gtTlVNLUdVRVNUUz0wOk1BSUxUTzp0aG9pbmdvY3RoaWVuYW5AZ21haWwuY29tDQpTRVFVRU5DRTowDQpTVEFUVVM6Q09ORklSTUVEDQpFTkQ6VkVWRU5UDQpFTkQ6VkNBTEVOREFS",
                filename:'invite-conversation.ics',

            },
            cc: [],
            bcc: [],
            custom_args: {"potentialServer": "qc.cloudjetpotential.com", }, // for checking event notification
        };

        // mailOptions.from = request.body.from || mailOptions.from;
        const candidate_id='testCandidateId';
        const current_user_name='duan';
        mailOptions.from = `${current_user_name} <candidate-${candidate_id}@${email_domain}>`;

        //@ts-ignore
        // sendMailCommon(request,response, false, null,
        sendMailCommon(request,response, false, mailOptions,
            (res:[{statusCode:string|number, statusMessage?:string, headers:{"x-message-id":string, [other:string]:any}}, ...any[]]) => {
                console.log('Message delivered with code %s %s', res[0].statusCode, res[0].statusMessage);
                console.log(`res.headers["x-message-id"]: ${res[0].headers["x-message-id"]}`);
                // const message_id:string=res.headers["x-message-id"];

                return response.send(res);
            },
            (err:any) => {
                console.log('Errors occurred, failed to deliver message');

                if (err.response && err.response.body && err.response.body.errors) {
                    err.response.body.errors.forEach((error:any) => console.log('%s: %s', error.field, error.message));
                } else {
                    console.log(err);
                }
            }

        )





    });

});

export const testSendCalendar = functions.https.onRequest((request, response) => {
    /**
     * Send email  using `nodemailer-sendgrid`
     * refs:
     *      + https://www.npmjs.com/package/nodemailer-sendgrid
     *
     * run:  $> firebase emulators:start
     * http://localhost:5001/testtempqc/us-central1/testSendCalendar
     */


    cors(request, response, async () => {

        const mailOptions = {
            from: `"Cloudjet Potential" <help@${email_domain}>`,
            to: "quocduan@gmail.com",
            subject: "hello guy",
            //    text: "",
            html: "hello",
            attachments: [
                {// this work!!!
                    "content": "QkVHSU46VkNBTEVOREFSDQpWRVJTSU9OOjEuMA0KUFJPRElEOi0vL2Nsb3VkamV0a3BpLmNvbS8vY2pzLXBvdGVudGlhbC8vRU4NCk1FVEhPRDpSRVFVRVNUDQpOQU1FOkludml0ZSBtZWV0aW5nDQpYLVdSLUNBTE5BTUU6SW52aXRlIG1lZXRpbmcNClRJTUVaT05FLUlEOkFzaWEvSG9fQ2hpX01pbmgNClgtV1ItVElNRVpPTkU6QXNpYS9Ib19DaGlfTWluaA0KQkVHSU46VkVWRU5UDQpVSUQ6Z3E4NEB1bmRlZmluZWQNClNFUVVFTkNFOjANCkRUU1RBTVA6MjAxOTA4MDFUMDUwOTA3Wg0KRFRTVEFSVDoyMDE5MDgxN1QwMTAwMDBaDQpEVEVORDoyMDE5MDgxN1QwMTMwMDBaDQpTVU1NQVJZOnF1b2MgZHVhbiBI4bq5biBwaOG7j25nIHbhuqVuIChUZXN0IENhdSBo4buPaSBt4bqrdSkNCkRFU0NSSVBUSU9OOkLhuqFuIHPhuq9wIGPDsyBideG7lWkgcGjhu49uZyB24bqlbiBzYXUgMTcgdGjDoW5nIDg6XG5cblRpw6p1IMSR4buBOiBxdW9jIGR1YQ0KIG4gSOG6uW4gcGjhu49uZyB24bqlbiAoVGVzdCBDYXUgaOG7j2kgbeG6q3UpXG5UaOG7nWkgZ2lhbiBi4bqvdCDEkeG6p3U6IDg6MDAgU0FcblxuDQpPUkdBTklaRVI7Q049IlR1bmciOm1haWx0bzpjYW5kaWRhdGUtcXVvY2R1YW5AcWMuY2xvdWRqZXRwb3RlbnRpYWwuY29tDQpBVFRFTkRFRTtST0xFPVJFUS1QQVJUSUNJUEFOVDtSU1ZQPVRSVUU7Q049dGhpZW5hbjptYWlsdG86dGhvaW5nb2N0aGllbmFuQGdtYWlsLmNvbQ0KQVRURU5ERUU7Uk9MRT1SRVEtUEFSVElDSVBBTlQ7UlNWUD1UUlVFO0NOPWR1YW4xOm1haWx0bzpxdW9jZHVhbkBnbWFpbC5jb20NCkVORDpWRVZFTlQNCkVORDpWQ0FMRU5EQVI=",
                    "type": "application/ics",
                    "name": "invite.ics",
                    "filename": "invite.ics",
                    "disposition": "attachment"
                }
            ],
            // icalEvent: {
            //     method: 'request',
            //     // content can be a string, a buffer or a stream
            //     // alternatively you could use `path` that points to a file or an url
            //     // content: 'BEGIN:VCALENDAR\r\nPRODID:-//ACME/DesktopCalendar//EN\r\nMETHOD:REQUEST\r\nVERSION:2.0\r\n...'
            //     content: "QkVHSU46VkNBTEVOREFSDQpQUk9ESUQ6LS8vR29vZ2xlIEluYy8vR29vZ2xlIENhbGVuZGFyIDcwLjkwNTQvL0VODQpWRVJTSU9OOjIuMA0KQ0FMU0NBTEU6R1JFR09SSUFODQpNRVRIT0Q6UkVQTFkNCkJFR0lOOlZFVkVOVA0KRFRTVEFSVDoyMDE5MDgxNlQwNTMwMDBaDQpEVEVORDoyMDE5MDgxNlQwNjMwMDBaDQpEVFNUQU1QOjIwMTkwODAyVDA0NDYxMloNCk9SR0FOSVpFUjtDTj1xdW9jZHVhbkBnbWFpbC5jb206bWFpbHRvOnF1b2NkdWFuQGdtYWlsLmNvbQ0KVUlEOjVpb3ZrbnVsODJtY2pqOWMzNjV1ZnIybDBnQGdvb2dsZS5jb20NCkFUVEVOREVFO0NVVFlQRT1JTkRJVklEVUFMO1JPTEU9UkVRLVBBUlRJQ0lQQU5UO1BBUlRTVEFUPURFQ0xJTkVEO0NOPWR1YW4udA0KIG5xQGNsb3VkamV0a3BpLmNvbTtYLU5VTS1HVUVTVFM9MDptYWlsdG86ZHVhbi50bnFAY2xvdWRqZXRrcGkuY29tDQpDUkVBVEVEOjIwMTkwODAyVDA0MzY0NloNCkRFU0NSSVBUSU9OOg0KTEFTVC1NT0RJRklFRDoyMDE5MDgwMlQwNDQ2MTBaDQpMT0NBVElPTjoNClNFUVVFTkNFOjANClNUQVRVUzpDT05GSVJNRUQNClNVTU1BUlk6dGVzdGV2ZW50IDINClRSQU5TUDpPUEFRVUUNCkVORDpWRVZFTlQNCkVORDpWQ0FMRU5EQVINCg"
            //     // content: `
            //     //     BEGIN:VCALENDAR
            //     //     VERSION:2.0
            //     //     CALSCALE:GREGORIAN
            //     //     PRODID:adamgibbons/ics
            //     //     BEGIN:VEVENT
            //     //     UID:a8b44b40-30fc-11e8-9cdf-ab421c95fe5f
            //     //     SUMMARY:Test Event
            //     //     DTSTAMP:20180326T125000Z
            //     //     DTSTART:20180529
            //     //     DTEND:20180530
            //     //     DESCRIPTION:This is a test event
            //     //     LOCATION:Somewhere in a building...
            //     //     STATUS:CONFIRMED
            //     //     ORGANIZER;CN=Admin:mailto:hello@qc.cloudjetpotential.com
            //     //     ATTENDEE;RSVP=TRUE;CN=quocduan@gmail.com:mailto:quocduan@gmail.com
            //     //     END:VEVENT
            //     //     END:VCALENDAR
            //     // `, // not work!!!
            // },
            cc: [],
            bcc: [],
            custom_args: {"potentialServer": "qc.cloudjetpotential.com", }, // for checking event notification
        };

        // mailOptions.from = request.body.from || mailOptions.from;
        const candidate_id='testCandidateId';
        const current_user_name='duan';
        mailOptions.from = `${current_user_name} <candidate-${candidate_id}@${email_domain}>`;

        //@ts-ignore
        sendMailCommon(request,response, false, mailOptions,
            (res:[{statusCode:string|number, statusMessage?:string, headers:{"x-message-id":string, [other:string]:any}}, ...any[]]) => {
                console.log('Message delivered with code %s %s', res[0].statusCode, res[0].statusMessage);
                console.log(`res.headers["x-message-id"]: ${res[0].headers["x-message-id"]}`);
                // const message_id:string=res.headers["x-message-id"];

                return response.send(res);
            },
            (err:any) => {
                console.log('Errors occurred, failed to deliver message');

                if (err.response && err.response.body && err.response.body.errors) {
                    err.response.body.errors.forEach((error:any) => console.log('%s: %s', error.field, error.message));
                } else {
                    console.log(err);
                }
            }

        )





    });

});



// Manual test:
// $ curl -d '{"from":"help@cloudjetkpi.com", "to":"hong@cjs.vn", "subject": "inviteUserInterview", "html":"Dear you!"}' -H "Content-Type: application/json" https://us-central1-cloudjet-work.cloudfunctions.net/sendConversationEmail
export const sendConversationEmail = functions.https.onRequest((request, response) => {
    /**
     * Send email  using nodemailer-sendgrid
     * refs:
     *      + https://www.npmjs.com/package/nodemailer-sendgrid
     *
     * @param {Object} request Cloud Function request context.
     *                     More info: https://expressjs.com/en/api.html#req
     * @param {Object} response Cloud Function response context.
     *                     More info: https://expressjs.com/en/api.html#res
     */


    // NEED GET CONVERSATION ID HERE


    cors(request, response, () => {

        const mailOptions = {
            from: `"Cloudjet Potential" <help@${email_domain}>`,
            to: "hong@cjs.vn",
            subject: "",
            //    text: "",
            html: "",
            attachments: [],
            cc: [],
            bcc: [],
            custom_args: {"potentialServer": getProjectId()}, // for checking event notification
        };

        // mailOptions.from = request.body.from || mailOptions.from;
        const candidate_id = request.body.candidate_id;
        const current_user_name = request.body.current_user_name;
        mailOptions.from = `${current_user_name} <candidate-${candidate_id}@${email_domain}>`;
        mailOptions.to = request.body.to || mailOptions.to;
        mailOptions.subject = request.body.subject || mailOptions.subject;
        mailOptions.html = request.body.html || mailOptions.html;
        mailOptions.attachments = request.body.attachments || mailOptions.attachments;

        const calendar_event:{base64_content:string, filename:string} = request.body.calendar_event || null;
        if(calendar_event){
            // @ts-ignore
            mailOptions.icalEvent = {
                content: calendar_event.base64_content,
                filename:calendar_event.filename || 'invite.ics',

            };
            //@ts-ignore
            mailOptions.alternatives=[
                {
                    contentType: "text/calendar;method=REQUEST",
                    value: Buffer.from(calendar_event.base64_content, 'base64').toString('utf-8')
                }
            ];

        }

        mailOptions.bcc = request.body.bcc || mailOptions.bcc;
        mailOptions.cc = request.body.cc || mailOptions.cc;
        console.error('KHONG CO GI DE NOI CA');
        // console.log(mailOptions.from);
        // console.log(mailOptions.to);

        sendMailCommon(request,response, false,
            mailOptions,
        );

    });
});


// https://hackernoon.com/https-medium-com-amanhimself-converting-a-buffer-to-json-and-utf8-strings-in-nodejs-2150b1e3de57

export const helloWorld = functions.https.onRequest((req, res) => {

    console.log(`${email_domain}`);
    console.log('abc');
    console.log(req.body);

    // console.log(process.env.GCLOUD_PROJECT);
    // console.log(process.env.FIREBASE_CONFIG);
    // console.log(request);
    // console.log(util.inspect(request, false, null, true /* enable colors */))
    // console.log(JSON.stringify(request))

    // console.log(request.body.toString('utf8'));
    // console.log(JSON.stringify(request.body))

    // const req=request;
    // const res=response;
    res.status(200).send(req.body);
    if (req.method !== 'POST') {
        // Return a "method not allowed" error
        // return res.status(405).end();
        // res.send("I've received mail!");
        return
    }




    const busboy = new Busboy({headers: req.headers});
    // const tmpdir = os.tmpdir();

    // This object will accumulate all the fields, keyed by their name
    const fields = {};

    // This object will accumulate all the uploaded files, keyed by their name.
    // const uploads = {};

    // This code will process each non-file field in the form.
    // @ts-ignore
    busboy.on('field', (fieldname, val) => {
        // TODO(developer): Process submitted field values here
        console.log(`Processed field ${fieldname}: ${val}.`);
        //@ts-ignore
        fields[fieldname] = val;
    });


    busboy.end(req.rawBody);

    // response.send("Hello from Cloudjet!");

    res.send("Hello from Cloudjet!");
});

const extractCandidateIdFromEmail = (email:string):string=> {
    let candidate_id:string = '';
    // parse candidate id from to_email: candidate-q5P3KN1LBrJSC6k4Iczh@cloudjetpotential.com
    // https://stackoverflow.com/questions/432493/how-do-you-access-the-matched-groups-in-a-javascript-regular-expression
    const myRegexp = () => (/^candidate-(.*)@(.*)$/g);
    // var mystr='candidate-q5P3KN1LBrJSC6k4Iczh@cloudjetpotential.com'

    const m = myRegexp().exec(email);
    console.log(m);
    if (m) {
        candidate_id = m[1]; // q5P3KN1LBrJSC6k4Iczh
    }
    return candidate_id
};

// https://cloud.google.com/functions/docs/writing/http#handling_multipart_form_uploads
// @ts-ignore
export const receiveIncomingEmail = functions.https.onRequest(async (req, res) => {

    if (req.method !== 'POST') {
        // Return a "method not allowed" error
        // return res.status(405).end();
        return
    }
    const busboy = new Busboy({headers: req.headers});


    // This object will accumulate all the fields, keyed by their name
    const fields = {} as any;


    // This code will process each non-file field in the form   .
    // @ts-ignore
    busboy.on('field', (fieldname, val) => {
        // TODO(developer): Process submitted field values here
        console.log(`Processed field ${fieldname}: ${val}`);
        //@ts-ignore
        fields[fieldname] = val;
    });


    const os = require('os');
    const fs = require('fs');
    const tmpdir = os.tmpdir();

// This object will accumulate all the uploaded files, keyed by their name.
    const uploads:{
        [key:string]:{file_path:string, file_name:string}
    } = { };
    // const uploads_data:{fieldname:string, file:any, filename:string}[]=[];
    const fileWrites:Promise<any>[] = [];

    // This code will process each file uploaded.
    busboy.on('file', (field:string, file:any, file_name:string) => {
        // Note: os.tmpdir() points to an in-memory file system on GCF
        // Thus, any files in it must fit in the instance's memory.
        console.log(`Processed file ${file_name}`);
        const file_path = path.join(tmpdir, file_name);
        uploads[field] = {file_path, file_name};





        // uploads_data.push({fieldname, file, filename});
        // console.log(`End Processed file ${filename}`);
        //
        // file.on('data', function(data:any) {
        //     console.log('File [' + fieldname + '] got ' + data.length + ' bytes');
        // });
        // file.on('end', function() {
        //     console.log('File [' + fieldname + '] Finished');
        // });



        const writeStream = fs.createWriteStream(file_path);
        file.pipe(writeStream);

        // File was processed by Busboy; wait for it to be written to disk.
        const promise = new Promise((resolve, reject) => {
            file.on('end', () => {
                writeStream.end();
            });
            writeStream.on('finish', resolve);
            writeStream.on('error', reject);
        });
        fileWrites.push(promise);
    });


    busboy.on('finish', function() {
        console.log('fkfk');
        // // envelope: {"to":["help@${email_domain}"],"from":"quocduan@gmail.com"}
        // // let candidate_ids:string[]=fields['envelope'].to;// split from to-address// or on-reply-to--> check original messages in  mail box
        // Nguyễn Hà <candidate-iWVdRcWoeaWsjO54e5HN@qc.cloudjetpotential.com>
        // let to_email:string=fields['to'];// split from to-address// or on-reply-to--> check original messages in  mail box

        // https://sendgrid.com/docs/for-developers/parsing-email/setting-up-the-inbound-parse-webhook/
        const envelope=JSON.parse(fields['envelope']);
        let to_email:string=envelope['to'][0];// split from to-address// or on-reply-to--> check original messages in  mail box
        console.log(to_email);
        let candidate_id=extractCandidateIdFromEmail(to_email);
        // let mail_text:string=fields['text'];
        // let mail_html:string = fields['html'];
        // let attachments:string = fields['attachments'];
        console.log(`candidate_id: ${candidate_id}`);
        if(candidate_id) {

            // https://stackoverflow.com/a/46568084
            const store = admin.firestore();
            // const bucket = admin.storage().bucket('testtempqc.appspot.com');
            const bucket = admin.storage().bucket(); // get default bucket
            store.collection(CANDIDATE_COLLECTION).doc(candidate_id).get().then(async (doc:any) => {
                if (doc.exists) {

                    const candidate:BaseCandidate = new BaseCandidate();
                    Object.assign(candidate, doc.data());
                    candidate.id=candidate_id;

                    console.log(candidate); // company_id


                    // https://cloud.google.com/functions/docs/writing/http
                    await Promise.all(fileWrites);


                    let uploaded_file_urls:{file_name:string, file_url:string}[]=[];
                    console.warn(uploads);
                    // TODO(developer): Process saved files here
                    const upload_manager:Promise<any>[]=[];
                    for (const field in uploads) {

                        const localFile = uploads[field].file_path;
                        const file_name = uploads[field].file_name;
                        const random_dir_name=new Date().getTime();
                        const destPath:string = `/company/${candidate.company_id}/candidate/${candidate.id}/conversation/${random_dir_name}/${file_name}`;
                        // https://medium.com/@tuanngominh/how-to-upload-using-firebase-cloud-functions-c8f85b7af9ce
                        // https://stackoverflow.com/questions/39848132/upload-files-to-firebase-storage-using-node-js




                        const upload_promise = new Promise((resolve, reject)=>{
                            bucket.upload(localFile, {
                                    // destination: "csv-folder/file.csv",
                                    destination: destPath,
                                },
                                //@ts-ignore
                                function(err:Error, file:File) {
                                    console.error(arguments);
                                    if (!err) {// if no error
                                        // done
                                        // let file = data[0];
                                        // https://stackoverflow.com/questions/42956250/get-download-url-from-file-uploaded-with-cloud-functions-for-firebase
                                        // enable permission:
                                        // https://medium.com/@hiranya911/firebase-create-custom-tokens-without-service-account-credentials-d6049c2d2d85
                                        //
                                        file.getSignedUrl({
                                            action: 'read',
                                            expires: '03-17-2525'
                                        }, function(err1:Error | null, url?: string) { // check source code
                                            if (err1) {
                                                console.error(err1);
                                                reject(err1)
                                            }else{
                                                const file_url=url || '#';
                                                console.log(url);
                                                // handle url
                                                uploaded_file_urls.push({
                                                    file_name:file_name,
                                                    file_url:file_url
                                                });

                                                resolve();

                                            }

                                        })

                                    }else{
                                        console.error(err);
                                        reject(err);
                                    }
                                    // we need delete local file after upload
                                    fs.unlinkSync(localFile);
                                });
                        });
                        upload_manager.push(upload_promise);
                        // const upload_result:any = await bucket.upload(localFile, {
                        //         // destination: "csv-folder/file.csv",
                        //         destination: destPath,
                        // });
                        // console.error('after upload');
                        // console.warn(upload_result);
                        //
                        // const err=upload_result[0];
                        // const data=upload_result[1];
                        //
                        // if (!err) {
                        //     // done
                        //     let file = data[0];
                        //     // https://stackoverflow.com/questions/42956250/get-download-url-from-file-uploaded-with-cloud-functions-for-firebase
                        //     const url_data:any[] = await file.getSignedUrl({
                        //         action: 'read',
                        //         expires: '03-17-2525'
                        //     });
                        //     const err1=url_data[0];
                        //     const url=url_data[1];
                        //     if (err1) {
                        //         console.error(err1);
                        //         return;
                        //     }
                        //     console.log(url);
                        //     // handle url
                        //     uploaded_file_urls.push({
                        //         filename:name,
                        //         href:url[0]
                        //     });
                        //
                        // }else{
                        //     console.error(err)
                        // }



                        // fs.unlinkSync(localFile);
                    }
                    // res.send();



                    // console.error('uploads_data');
                    // console.error(uploads_data);
                    //
                    // if(uploads_data.length > 0){
                    //     console.log('uploading files');
                    //     const destDir:string = `/image/company/${candidate.company_id}/candidate/${candidate.id}/conversation/`;
                    //     uploads_data.forEach((upload:{fieldname:string, file:any, filename:string})=>{
                    //         const file_path = path.join(destDir, upload['filename']);
                    //         const bucket = admin.storage().bucket('testtempqc.appspot.com');
                    //         // const bucket = admin.storage().bucket();
                    //         uploads[upload.fieldname] = file_path;
                    //         console.error('error here');
                    //         console.error(upload.file);
                    //         // const up = fstorage.ref(file_path).put(upload.file);
                    //         // https://firebase.google.com/docs/storage/gcp-integration
                    //         const up = bucket.upload('/photos/zoo/zebra.jpg', function(err, file) {
                    //             if (!err) {
                    //                 // "zebra.jpg" is now in your bucket.
                    //                 // get download URL
                    //                 console.log(file);
                    //                 // file.getD
                    //             }
                    //         });
                    //
                    //         // THE FOLLOWING WAS NOT WORK ON ADMIN FUNCTION
                    //         // fileWrites.push(new Promise((resolve, reject)=>{
                    //         //     // https://firebase.google.com/docs/storage/web/upload-files
                    //         //     up.on('state_changed',
                    //         //         function(snapshot:any){// progress
                    //         //
                    //         //         },function(error:any) {
                    //         //             // Handle unsuccessful uploads
                    //         //             console.error('failed upload file')
                    //         //         },
                    //         //         function(){
                    //         //             // Handle successful uploads on complete
                    //         //             // For instance, get the download URL: https://firebasestorage.googleapis.com/...
                    //         //             up.snapshot.ref.getDownloadURL().then((downloadUrl:string)=>{
                    //         //                 console.warn('upload file successfully')
                    //         //                 resolve({filename:upload.fieldname, href:downloadUrl});
                    //         //             })
                    //         //         })
                    //         // }));
                    //
                    //     });
                    //
                    //
                    //     uploaded_file_urls = await Promise.all(fileWrites);
                    // }



                    await Promise.all(upload_manager);

                    const conversation:IConversation =  new BaseConversation() as IConversation;
                    // const conversation:BaseConversation=  new BaseConversation();
                    conversation.type = 'messageFromCandidatePosted';



                    const attachments = uploaded_file_urls.map((url:{file_name:string, file_url:string})=>{
                        return url
                    });

                    console.log(attachments);
                    const email_content = replyParser(fields.text);
                    conversation.object = {
                        cc: fields.cc || "",
                        bcc: fields.bcc || "",// always be empty :D

                        body: email_content.getVisibleText(),
                        subject: fields.subject,

                        attachments: attachments || [],

                        candidate: candidate,
                        candidate_id: candidate.id,
                        position_id: candidate.position_id,
                        mail_state: 'sent',
                        acting_user: {
                            id: candidate.id,
                            name: candidate.name,
                            photo_url: candidate.photoLink(),
                        }
                    };
                    conversation.candidate_id = candidate.id;
                    conversation.position_id = candidate.position_id;
                    conversation.company_id = candidate.company_id;


                    //await get position
                    console.log("Check position.....");

                    await store.collection(POSITION_COLLECTION).doc(candidate.position_id).get().then(async (posdoc:any) => {
                        if (posdoc.exists) {
                            var pos = posdoc.data();
                            var user_email = pos.position_assign.email;

                            //await create or update inbox
                            console.log("Check inbox data.....");
                            const records = await store.collection(INBOX_COLLECTION).where("candidate_id", "==", candidate_id)
                                .limit(1)
                                .get();

                            let check_inbox = null;
                            let inbox_id = "";

                            records.forEach((docSnapshot:any) => {
                                check_inbox = docSnapshot.data();
                                inbox_id = docSnapshot.id;
                            });





                            var inbox = new BaseInbox() as IInbox;

                            inbox.bcc = fields.cc || "";
                            inbox.cc = fields.cc || "";
                            inbox.subject = fields.subject;
                            inbox.email = user_email;
                            inbox.subject =   fields.subject;
                            inbox.candidate = candidate;
                            inbox.candidate_id =candidate.id;
                            inbox.position_id = candidate.position_id;
                            inbox.company_id = candidate.company_id;
                            inbox.body = email_content.getVisibleText();
                            inbox.acting_user = {
                                id: candidate.id,
                                name: candidate.name,
                                photo_url: candidate.photoLink(),
                            }

                            inbox.acting_user_ids =[];
                            inbox.is_showing = true;


                            console.log("Check inbox ???") ;
                            if (check_inbox) {
                                console.log("Inbox đã tồn tại: "+inbox_id);
                                // @ts-ignore
                                if (check_inbox.acting_user_ids.indexOf(pos.position_assign.id) == -1) {
                                    // @ts-ignore
                                    check_inbox.acting_user_ids.push(pos.position_assign.id);
                                }

                                // @ts-ignore
                                inbox.acting_user_ids =  check_inbox.acting_user_ids;

                                if(inbox_id != "" )
                                    await store.collection(INBOX_COLLECTION).doc(inbox_id).set(JSON.parse(JSON.stringify(inbox)));

                            } else {
                                console.log("Inbox chưa tồn tại");
                                inbox.acting_user_ids.push(pos.position_assign.id);

                                await store.collection(INBOX_COLLECTION).add(JSON.parse(JSON.stringify(inbox))).then(function(docRef) {
                                    console.log("Document inbox written with ID: ", docRef.id);
                                })
                                .catch(function(error) {
                                    console.error("Error adding document inbox: ", error);
                                });

                            }




                            const email = new Email({
                                transport: transporter,
                                send: true,
                                preview: false,
                            });


                            return email.send({
                                template: `${__dirname}/sendCandiateRely`,
                                message: {
                                    from: `Cloudjet Potential HR <no-reply@${email_domain}>`,
                                    to: user_email,
                                },
                                locals: {
                                    title1: Messages.SR_EMAIL_CANDIDATE_RELY_TITLE1,
                                    title3: Messages.SR_EMAIL_CANDIDATE_RELY_TITLE3,
                                    title4: Messages.SR_EMAIL_CANDIDATE_RELY_TITLE4,
                                    candidate_name:candidate.name,
                                    position_name: pos.title,
                                    content: inbox.body,
                                    link: url_app+"/candidate/"+candidate.id+'/resume?right=email',
                                    candidate_photo: candidate.profile_photo_url?candidate.profile_photo_url:"https://firebasestorage.googleapis.com/v0/b/cloudjet-work.appspot.com/o/logo%2FSymbol-color.png?alt=media&token=656256da-1fec-4565-a394-0062fd040fb2",
                                },
                            }).then((result: any) => console.log(result))
                                .catch((e: any) => console.log("Send email rely fail :"+ e));




                            // console.log(INBOX_COLLECTION);

                        }

                    }).catch((reason:any) => {
                        console.log("Lỗi inbox");
                        console.log(reason);
                        // res.send(reason)
                    });






                    // await conversation.insert();
                    const cv = await store.collection(CONVERSATION_COLLECTION).add(JSON.parse(JSON.stringify(conversation)));
                    console.log(cv);
                    console.log(conversation);
                    console.log('the fk thingaaaa Duan');


                    res.send("Received email successfully!");

                }
                else {
                    console.log("Nothing");
                    // res.send("Nothing")
                }

            }).catch((reason:any) => {
                console.log(reason);
                // res.send(reason)
            });


        }else{
            console.log('the fk thing111');
            res.send("Received email successfully!");
        }




        // let company_id:string=''; // from candidate id



        // get candidate, company
        // sender:
        // replier
        // add conversation



    });



    busboy.end(req.rawBody);




});





export const sendInterviewerInvitation = functions.https.onRequest((request, response)=> {

    cors(request, response, () => {

        let toWho = request.body.toWho;
        let data = request.body.data;
        const message_data={
            from: `Cloudjet Potential HR <no-reply@${email_domain}>`,
            to: toWho.email,
            // attachments: data.attachments || [],
            cc: toWho.cc || [],
            // icalEvent:{
            //     content: "QkVHSU46VkNBTEVOREFSDQpWRVJTSU9OOjIuMA0KUFJPRElEOi1DSlMtUE9URU5USUFMIDIuMC8vRU4NCkNBTFNDQUxFOkdSRUdPUklBTg0KTUVUSE9EOlJFUVVFU1QNCkJFR0lOOlZFVkVOVA0KVUlEOmNhbGVuZGFyLWFzZGZzZGZhc2RmDQpEVFNUQU1QOjIwMTkwODA2VDAzNTI1NA0KU1RBVFVTOkNPTkZJUk1FRA0KRFRTVEFSVDoyMDE5MDgyMlQwNDAwMDBaDQpEVEVORDoyMDE5MDgyM1QwNTAwMDBaDQpTVU1NQVJZOmR1YW4udG5xIE1lZXRpbmcgKEEpDQpERVNDUklQVElPTjpZb3UgYXJlIGNvbmZpcm1lZCBmb3IgdGhlIGZvbGxvd2luZyBpbnRlcnZpZXcgb24gV2VkIEF1ZyAyMToNCg0KVGl0bGU6IGR1YW4udG5xIE1lZXRpbmcgKEEpDQpTdGFydCBUaW1lOiAxMTowMCBBTSAoKzA3KQ0KDQoNCg0KDQpPUkdBTklaRVI7Q049UXVvY0R1YW46bWFpbHRvOnF1b2NkdWFuQGdtYWlsLmNvbQ0KQVRURU5ERUU7Q1VUWVBFPUlORElWSURVQUw7UlNWUD1UUlVFO1JPTEU9UkVRLVBBUlRJQ0lQQU5UO1BBUlRTVEFUPU5FRURTLUFDVElPTjtDTj0iZHVhbi50bnEiO1gtTlVNLUdVRVNUUz0wOk1BSUxUTzpkdWFuLnRucUBvdXRsb29rLmNvbQ0KQVRURU5ERUU7Q1VUWVBFPUlORElWSURVQUw7UlNWUD1UUlVFO1JPTEU9UkVRLVBBUlRJQ0lQQU5UO1BBUlRTVEFUPU5FRURTLUFDVElPTjtDTj0iZHVhbjEiO1gtTlVNLUdVRVNUUz0wOk1BSUxUTzp0aG9pbmdvY3RoaWVuYW5AZ21haWwuY29tDQpTRVFVRU5DRTowDQpTVEFUVVM6Q09ORklSTUVEDQpFTkQ6VkVWRU5UDQpFTkQ6VkNBTEVOREFS",
            //     filename:'invite.ics',
            //
            // },
            // alternatives: [
            //     {
            //         contentType: "text/calendar;method=REQUEST",
            //         // content: "BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:-CJS-POTENTIAL 2.0//EN\nCALSCALE:GREGORIAN\nMETHOD:REQUEST\nBEGIN:VEVENT\nUID:calendar-asdfsdfasdf\nDTSTAMP:20190806T035254\nSTATUS:CONFIRMED\nDTSTART:20190822T040000Z\nDTEND:20190823T050000Z\nSUMMARY:duan.tnq Meeting (A)\nDESCRIPTION:You are confirmed for the following interview on Wed Aug 21:\n\nTitle: duan.tnq Meeting (A)\nStart Time: 11:00 AM (+07)\n\n\n\n\nORGANIZER;CN=QuocDuan:mailto:quocduan@gmail.com\nATTENDEE;CUTYPE=INDIVIDUAL;RSVP=TRUE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=\"duan.tnq\";X-NUM-GUESTS=0:MAILTO:duan.tnq@outlook.com\nATTENDEE;CUTYPE=INDIVIDUAL;RSVP=TRUE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=\"duan1\";X-NUM-GUESTS=0:MAILTO:thoingocthienan@gmail.com\nSEQUENCE:0\nSTATUS:CONFIRMED\nEND:VEVENT\nEND:VCALENDAR"
            //         value: "BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:-CJS-POTENTIAL 2.0//EN\nCALSCALE:GREGORIAN\nMETHOD:REQUEST\nBEGIN:VEVENT\nUID:calendar-asdfsdf\nDTSTAMP:20190806T035254\nSTATUS:CONFIRMED\nDTSTART:20190824T040000Z\nDTEND:20190825T050000Z\nSUMMARY:duan.tnq Meeting (A)\nDESCRIPTION:You are confirmed for the following interview on Wed Aug 21:\n\nTitle: duan.tnq Meeting (A)\nStart Time: 11:00 AM (+07)\n\n\n\n\nORGANIZER;CN=QuocDuan:mailto:quocduan@gmail.com\nATTENDEE;CUTYPE=INDIVIDUAL;RSVP=TRUE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=\"duan.tnq\";X-NUM-GUESTS=0:MAILTO:duan.tnq@outlook.com\nATTENDEE;CUTYPE=INDIVIDUAL;RSVP=TRUE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=\"duan1\";X-NUM-GUESTS=0:MAILTO:thoingocthienan@gmail.com\nSEQUENCE:0\nSTATUS:CONFIRMED\nEND:VEVENT\nEND:VCALENDAR"
            //     }
            // ],
        };


        const calendar_event:{base64_content:string, filename:string} = data.calendar_event;
        if (calendar_event){
            //@ts-ignore
            message_data.icalEvent={
                content:calendar_event.base64_content,
                filename: calendar_event.filename || 'invite.ics'
            }
            //@ts-ignore
            message_data.alternatives=[
                {
                    contentType: "text/calendar;method=REQUEST",
                    value: Buffer.from(calendar_event.base64_content, 'base64').toString('utf-8')
                }
            ];
        }


        sendMailCommon(request,response, true,
            {
                template: `${__dirname}/inviteUserInterview`,
                message: message_data,
                locals: {
                    title1: Messages.SR_EMAIL_TITLE1,
                    title2: Messages.SR_EMAIL_TITLE2,
                    title3: Messages.SR_EMAIL_TITLE3,
                    title4: Messages.SR_EMAIL_TITLE4,
                    title5: Messages.SR_EMAIL_TITLE5,
                    title6: Messages.SR_EMAIL_TITLE6,
                    title7: Messages.SR_EMAIL_TITLE7,
                    title8: Messages.SR_EMAIL_TITLE8,
                    actor_name: data.actor_name,
                    candidate_name: data.candidate_name,
                    position_name: data.position_name,
                    link: data.link,
                    description: data.description,
                    interview_guide: data.interview_guide,
                    location: data.location
                },
            },

        );


    });
});

export const sendEmailTask = functions.https.onRequest((request: any, response: any)=>{


    cors(request, response, () => {





        let toWho = request.body.toWho;
        let data = request.body.data;

        sendMailCommon(request,response, true, {
                template: `${__dirname}/thePersonResponsibleForTheTask`,
                message: {
                    from: `Cloudjet Potential HR <no-reply@${email_domain}>`,
                    to: toWho.email,
                },
                locals: {
                    title1: Messages.SR_EMAIL_TASKS_TITLE1,
                    title2: Messages.SR_EMAIL_TASKS_TITLE2,
                    title3: Messages.SR_EMAIL_TASKS_SUBJECT,
                    title4: Messages.SR_EMAIL_TASKS_BTN,
                    title5: Messages.SR_EMAIL_TASKS_DUE,
                    title6: Messages.SR_EMAIL_TASKS_UPDATE,
                    company_name: data.company_name,
                    task_name: data.task_name,
                    task_deadline: data.task_deadline,
                    description: data.description,
                    link: data.link,
                    task_type: data.task_type
                },
            },

        );
    })
})

// send to candidate
export const sendEmailCancel = functions.https.onRequest((request, response)=>{

    cors(request, response, () => {


        let toWho = request.body.toWho;
        let data = request.body.data;

        let candidate_id= data.candidate_id;
        // get company name here

        console.log(data.attachments);

        if (data.type ==="cancelUserInterview") {
            console.log( "Send user "+ toWho.email);
            sendMailCommon(request,response, true,
                {
                    template: `${__dirname}/cancelUserInterview`,
                    message: {
                        from: `Cloudjet Potential HR <candidate-${candidate_id}@${email_domain}>`,
                        to: toWho.email,
                        cc:toWho.cc,
                        attachments: data.attachments || []
                    },
                    locals: {
                        title1: Messages.SR_EMAIL_CANCEL_TITLE1,
                        title2: Messages.SR_EMAIL_CANCEL_TITLE2,
                        title3: Messages.SR_EMAIL_TITLE3,
                        title6: Messages.SR_EMAIL_TITLE6,
                        title7: Messages.SR_EMAIL_CANCEL_TITLE3,
                        title8: Messages.SR_EMAIL_CANCEL_TITLE5,
                        actor_name: data.actor_name,
                        candidate_name: data.candidate_name,
                        position_name: data.position_name,
                        link: data.link,
                        description: data.description,
                        signatureHtml: data.signatureHtml,
                    },
                },

            );

        } else {
            console.log( "Send candidate "+ toWho.email);
            sendMailCommon(request,response, true,
                {
                    template: `${__dirname}/cancelCandidateInterview`,
                    message: {
                        from: `Cloudjet Potential HR <candidate-${candidate_id}@${email_domain}>`,
                        to: toWho.email,
                        cc:toWho.cc,
                        attachments:data.attachments || []
                    },
                    locals: {
                        title1: Messages.SR_EMAIL_CANCEL_TITLE4,
                        title7: Messages.SR_EMAIL_CANCEL_TITLE3,
                        title8: Messages.SR_EMAIL_CANCEL_TITLE5,
                        candidate_name: data.candidate_name,
                        actor_name: data.actor_name,
                        position_name: data.position_name,
                        description: data.description,
                        signatureHtml: data.signatureHtml,
                    },
                },

            );

        }
    })
});
// send to candidate
exports.sendEmailEdit = functions.https.onRequest((request, response) => {



    cors(request, response, () => {

        let toWho = request.body.toWho;
        let data = request.body.data;
        console.log(data.attachments);
        let candidate_id = data.candidate_id;
        sendMailCommon(request,response, true,
            {
                template: `${__dirname}/editCandidateInterview`,
                message: {
                    from: `Cloudjet Potential HR <candidate-${candidate_id}@${email_domain}>`,
                    to: toWho.email,
                    attachments: data.attachments || [],
                    cc:toWho.cc
                },
                locals: {
                    title1: Messages.SR_EMAIL_EDIT_TITLE6,
                    title7: Messages.SR_EMAIL_TITLE7,
                    candidate_name: data.candidate_name,
                    actor_name: data.actor_name,
                    position_name: data.position_name,
                    description: data.description,
                    signatureHtml: data.signatureHtml,
                },
            },


        );


    });
});

export const sendManagerPosition = functions.https.onRequest((request, response)=>{
    // gửi cho người tạo position khi ứng viên apply
    cors(request, response, () => {

        let data = request.body.data;
        console.log(data.candidate_photo_url)
        sendMailCommon(request,response, true,
            {
                template: `${__dirname}/sendManagerPosition`,
                message: {
                    from: `Cloudjet Potential HR <no-reply@${email_domain}>`,
                    to: data.position_assign_email,
                },
                locals: {
                    title1: Messages.SR_EMAIL_MANAGER_POSITION_TITLE1,
                    title2: Messages.SR_EMAIL_MANAGER_POSITION_TITLE2,
                    title3: Messages.SR_EMAIL_TITLE6,
                    title4: Messages.SR_EMAIL_TITLE_E9,
                    candidate_name:data.candidate_name,
                    position_name: data.position_name,
                    link: data.link,
                    candidate_photo: data.candidate_photo_url?data.candidate_photo_url:"https://firebasestorage.googleapis.com/v0/b/cloudjet-work.appspot.com/o/logo%2FSymbol-color.png?alt=media&token=656256da-1fec-4565-a394-0062fd040fb2",
                },
            },

        );

    })
});
// export const createCandidate = functions.firestore
//     .document('candidates/{id}')
//     .onCreate((snap, context) => {
//         // Get an object representing the document
//         // e.g. {'name': 'Marie', 'age': 66}
//         // const newValue = snap.data();
//
//         // access a particular field as you would any JS property
//         // const name = newValue.position._id;
//             const newValue = snap.data();
//
//             // @ts-ignore
//             const position_id = newValue.position._id;
//             // let candidate_name = newValue.name
//             console.log(newValue)
//             console.log(position_id);
//
//
//             let db = admin.firestore();
//             let db_1 = admin.firestore();
//
//             let url = 'http://localhost:8080/'
//
//             // db.collection("config").doc(param_url).get().then((snapshot_url : any)=>{
//             //     let url_object = snapshot_user.data()
//             //     console.log(url.url)
//             // })
//             const email = new Email({
//                 transport: emailClient,
//                 send: true,
//                 preview: false,
//             });
//             db.collection("Position").doc(position_id).get().then((snapshot_position : any) => {
//                 let position = snapshot_position.data()
//                 console.log(position.created_by)
//                 db_1.collection("Position").doc(position.created_by).get().then((snapshot_user : any) => {
//                     let user = snapshot_user.data()
//                     console.log(user.email)
//                     return email.send({
//                         template: `${__dirname}/sendManagerPosition`,
//                         message: {
//                             from: 'Cloudjet Potential HR <no-reply@${email_domain}>',
//                             to: "dinhquangtung9118@gmail.com",
//                         },
//                         locals: {
//                             title1: Messages.SR_EMAIL_CANCEL_TITLE1,
//                             title2: Messages.SR_EMAIL_CANCEL_TITLE2,
//                             title3: Messages.SR_EMAIL_TITLE3,
//                             title6: Messages.SR_EMAIL_TITLE6,
//                             title7: Messages.SR_EMAIL_CANCEL_TITLE3,
//                             title9: Messages.SR_EMAIL_TITLE_E9,
//                             actor_name: "actor_name",
//                             candidate_name:" candidate_name",
//                             position_name: position.title,
//                             link: `${window.location.origin}/candidate/experience`,
//                             description: "data.description",
//                         },
//                         }).catch((e: any) => console.log(e));
//                     }
//                     )
//                 }
//             )
//
//
//
//
//
//
//
//
//
//         // perform desired operations ...
//     });

export const sendEmailAssignCandidate = functions.https.onRequest((request: any, response: any)=>{

    cors(request, response, () => {



        let toWho = request.body.toWho;
        let data = request.body.data;
        sendMailCommon(request,response, true,
            {
                template: `${__dirname}/assignCandidate`,
                message: {
                    from: `Cloudjet Potential HR <no-reply@${email_domain}>`,
                    to: toWho.email,
                },
                locals: {
                    title1: Messages.SR_EMAIL_ASSIGN_CANDIDATE_TITLE1,
                    title2: Messages.SR_EMAIL_ASSIGN_CANDIDATE_TITLE2,
                    title3: Messages.SR_EMAIL_ASSIGN_CANDIDATE_TITLE3,
                    title4: Messages.SR_EMAIL_ASSIGN_CANDIDATE_SUBJECT,
                    title5: Messages.SR_EMAIL_ASSIGN_CANDIDATE_BTN,
                    candidate_name: data.candidate_name,
                    link: data.link,
                    actor: data.actor
                },
            },

        );

    })
})

// [START trigger]
exports.date = functions.https.onRequest((req, res) => {
    // [END trigger]
    // [START sendError]
    // Forbidding PUT requests.
    if (req.method === 'PUT') {
        return res.status(403).send('Forbidden!');
    }
    // [END sendError]

    // [START usingMiddleware]
    // Enable CORS using the `cors` express middleware.
    return cors(req, res, () => {
        // [END usingMiddleware]
        // Reading date format from URL query parameter.
        // [START readQueryParam]
        let format = req.query.format;
        // [END readQueryParam]
        // Reading date format from request body query parameter
        if (!format) {
            // [START readBodyParam]
            format = req.body.format;
            // [END readBodyParam]
        }
        // [START sendResponse]
        const formattedDate = moment().format(format);
        console.log('Sending Formatted date:', formattedDate);
        res.status(200).send(formattedDate);
        // [END sendResponse]
    });
});
// [END all]



export const fixWrongValueFuntion = functions.https.onRequest((req, res) => {
    // This function use to fixing wrong value Data.
    // Fix candicate_scorecard => candidate_scorcard.
    // This function just use to fixed. Remove this when done.
    // return cors(req, res, () => {
        let db = admin.firestore();
        let company_id = req.body.company_id;
        console.log(company_id);
        try{
            //Phase 1: Fix value in actions of PipelineStage.

            db.collection("PipelineStage")
                .where("actions", ">", [])  // :pray:
                .get()
                .then(async (snapShot: any) => {
                    if (snapShot.empty){
                        console.log('Nothing need to fix!!');
                        // res.send('Nothing need to fix!!');
                    }else{
                        console.log('-----------FIX PIPELINE STAGE-----------');
                        let data:any = [];
                        const arrayStage = snapShot.docs.map(async (doc: any)=>{
                            let actions = doc.data().actions;
                            let needUpdate = false;
                            actions = actions.map((action: any)=>{
                                if (action.action_type == 'candicate_scorecard'){
                                    action.action_type = 'candidate_scorecard';
                                    needUpdate = true;
                                }
                                return action;
                            })
                            if (needUpdate){
                                await db.collection("PipelineStage").doc(doc.id).update({actions: actions});
                                data.push(doc.data());
                                console.log('Fixed ', doc.data().title);
                            }
                            return doc;
                        });
                        await Promise.all(arrayStage);
                        console.log('Fixed 100%');
                        // res.send({data:data})
                    }

                }).catch(()=>{console.log('error')});

            //Phase 2: Fix value in actions of PositionPipelineAction.
            //@ts-ignore
            db.collection("PositionPipelineAction")
                .where("actions", ">", [])  // :pray:
                .get()
                .then(async (snapShot: any) => {
                    if (snapShot.empty){
                        console.log('Nothing need to fix!!');
                        // res.send('Nothing need to fix!!');
                    }else{
                        console.log('-----------FIX POSITION PIPELINE ACTION-----------');
                        let data:any = [];
                        const arrayStage = snapShot.docs.map(async (doc: any)=>{
                            let actions = doc.data().actions;
                            let needUpdate = false;
                            actions = actions.map((action: any)=>{
                                if (action.action_type == 'candicate_scorecard'){
                                    action.action_type = 'candidate_scorecard';
                                    needUpdate = true;
                                }
                                return action;
                            })
                            if (needUpdate){
                                await db.collection("PositionPipelineAction").doc(doc.id).update({actions: actions});
                                data.push(doc.data());
                                console.log('Fixed ', doc.data().stage_id);
                            }
                            return doc;
                        });
                        await Promise.all(arrayStage);
                        console.log('Fixed 100%');
                        // res.send({data:data})
                    }

                }).catch(()=>{console.log('error')});
            res.send('Completed!!!');
        }
        catch(e){
            res.send(e)
        }
    // })
});


export const mailEventNotificationParser  = functions.https.onRequest((req, res) => {

    interface ISendGridEvent {
        "email":string,
        "timestamp":number,
        "smtp-id":string,
        "event":string,
        "sg_message_id":string,
        "sg_event_id":string,
        "potentialServer"?:string,
        [other:string]:any
    }


    type Mail_Failed = 'dropped'|'deferred'|'bounce';
    type Mail_Pending = 'processed';
    type Mail_Delivered = 'delivered';
    type Mail_Open = 'open';
    type Mail_Other = 'spamreport'|'unsubscribe'|'group_unsubscribe'|'group_resubscribe';
    type Mail_Type=Mail_Failed | Mail_Pending | Mail_Delivered | Mail_Open| Mail_Other;

    // interface IEventType {
    //     [key:Mail_Type]: string
    // };


    const EVENT_TYPE_FAILEDS:Mail_Failed[]=['dropped','deferred','bounce'];
    const EVENT_TYPE_PENDINGS:Mail_Pending[]=['processed'];
    const EVENT_TYPE_DELIVERED:Mail_Delivered[]=['delivered'];
    const EVENT_TYPE_OPENS:Mail_Open[]=['open'];



    // @ts-ignore
    const EVENT_TYPES = {
        // Delivery events: https://sendgrid.com/docs/for-developers/tracking-events/event/#delivery-events
        "processed": "Message has been received and is ready to be delivered.",
        "dropped": "You may see the following drop reasons: Invalid SMTPAPI header, Spam Content (if Spam Checker app is enabled), Unsubscribed Address, Bounced Address, Spam Reporting Address, Invalid, Recipient List over Package Quota",
        "delivered":"Message has been successfully delivered to the receiving server.",
        "deferred": "Receiving server temporarily rejected the message.",
        "bounce": "Receiving server could not or would not accept the message. If a recipient has previously unsubscribed from your emails, the message is dropped.",

        // Engagement events: https://sendgrid.com/docs/for-developers/tracking-events/event/#engagement-events
        "open": "Recipient has opened the HTML message. Open Tracking needs to be enabled for this type of event.",
        "click": "Recipient clicked on a link within the message. Click Tracking needs to be enabled for this type of event.",
        "spamreport": "Recipient marked message as spam.",
        "unsubscribe": "Recipient clicked on the 'Opt Out of All Emails' link (available after clicking the message's subscription management link). Subscription Tracking needs to be enabled for this type of event.",
        "group_unsubscribe": "Recipient unsubscribed from a specific group either by clicking the link directly or updating their preferences. Subscription Tracking needs to be enabled for this type of event.",
        "group_resubscribe": "Recipient resubscribed to a specific group by updating their preferences. Subscription Tracking needs to be enabled for this type of event.",
        // test type
        "test": "Recipient resubscribed to a specific group by updating their preferences. Subscription Tracking needs to be enabled for this type of event.",
    };

    const extractMailId = (sg_message_id:string):string => {
        /**
         * https://stackoverflow.com/questions/33664494/how-to-associate-sendgrid-webhook-sg-message-id-to-sent-mail
         * X-Message-Id  = "MUvGg3V1ThOu3oe8eRqFrA"
         * sg_message_id = "MUvGg3V1ThOu3oe8eRqFrA.filter0001p2iad2-21183-5AF0BD9B-E.0"
         */
        let mail_id:string='';
        const myRegx=/^(.*)\.filter(.*)$/g;
        const m = myRegx.exec(sg_message_id);
        if (m) {
            mail_id = m[1]; // MUvGg3V1ThOu3oe8eRqFrA
        }
        return mail_id


    };

    console.error(req.body);
    const email_events:ISendGridEvent[]=req.body;

    // note: SEND GRID ALREADY ORDER EVENT BY `timestamp asc`
    const email_event_groups:{[message_id:string]:ISendGridEvent[]} = _.groupBy(email_events, (event:ISendGridEvent)=>{
        return event.sg_message_id
    });

    cors(req, res, async () => {
        try{
            const promises:Promise<any>[]=[];
            _.each(email_event_groups, (events:ISendGridEvent[])=>{
                const event:ISendGridEvent = _.last(events);
                if(event.hasOwnProperty('potentialServer')){
                    //@ts-ignore
                    const dest_server:string=event.potentialServer;
                    const {store} = getAdminTools(dest_server);

                    const mail_id:string = extractMailId(event.sg_message_id);
                    // update mail_id, mail_opened_count
                    const event_type:Mail_Type = event.event as Mail_Type;
                    console.log(`event_type mail_id dest_server: ${event_type} ${mail_id} ${dest_server}` )
                    if(mail_id){
                        let mail_state:MailState='other';
                        //@ts-ignore
                        if(EVENT_TYPE_OPENS.indexOf(event_type)!==-1){
                            mail_state='open';
                        }
                        //@ts-ignore
                        else if(EVENT_TYPE_PENDINGS.indexOf(event_type)!==-1){
                            mail_state='pending';
                        }
                        //@ts-ignore
                        else if(EVENT_TYPE_DELIVERED.indexOf(event_type)!==-1){
                            mail_state='sent';
                        }
                        //@ts-ignore
                        else if(EVENT_TYPE_FAILEDS.indexOf(event_type)!==-1){
                            mail_state='failed';
                        }else{
                            mail_state='other';
                        }
                        console.error(mail_state);

                        if(mail_state!=='other'){

                            //@ts-ignore
                            const p:Promise<any> = new Promise((resolve, reject) => {
                                store.collection(CONVERSATION_COLLECTION).where("mail_id", "==", mail_id).limit(1).get()
                                    .then(querySnapshot=>{
                                        console.error(`get conversation with mail_id = ${mail_id}`);
                                        const cvs:BaseConversation[] = [];
                                        console.log(querySnapshot);
                                        querySnapshot.forEach(documentSnapshot => {
                                            const cv:BaseConversation = new BaseConversation();
                                            console.log(`Found document at ${documentSnapshot.ref.path}`);
                                            Object.assign(cv, documentSnapshot.data());
                                            cv.id = documentSnapshot.id;
                                            cvs.push(cv);
                                        });
                                        console.error('update document');
                                        if(cvs.length > 0){

                                            const conversation:BaseConversation =  cvs[0];
                                            // conversation.mail_opened_count = conversation.mail_opened_count + 1;
                                            // conversation.object.mail_state = mail_state;
                                            let incr:number=0;
                                            let doUpdate:boolean=true;
                                            // let is_first_time_open_mail:boolean=false;
                                            if(mail_state==='open'){
                                                incr = 1;

                                                // if(conversation.object.mail_state==='open'){
                                                //     is_first_time_open_mail = false;
                                                // } else {
                                                //     is_first_time_open_mail = true;
                                                // }
                                                let is_second_time_open_mail:boolean = false;
                                                // if(!is_first_time_open_mail && conversation.object.is_first_time_open_mail){
                                                //     is_second_time_open_mail = true;
                                                // }
                                                if(conversation.object.mail_opened_count === 1){
                                                    is_second_time_open_mail = true;
                                                }

                                                if(is_second_time_open_mail && conversation.object.last_mail_event_timestamp && (event.timestamp - conversation.object.last_mail_event_timestamp) < 20){
                                                    // don't update open event if time-delta < 1 minute
                                                    doUpdate = false;
                                                }
                                            }
                                            if(doUpdate){
                                                //https://fireship.io/snippets/firestore-increment-tips/
                                                const increment = admin.firestore.FieldValue.increment(incr);
                                                // @ts-ignore
                                                // store.collection(CONVERSATION_COLLECTION).doc(conversation.id).update({mail_opened_count:increment, mail_state:mail_state}).then(()=>{console.log('success')}).catch(()=>{console.log('error')});
                                                store.collection(CONVERSATION_COLLECTION).doc(conversation.id)
                                                    .set({
                                                            mail_opened_count:increment,
                                                            object:{
                                                                mail_state:mail_state,
                                                                last_mail_event_timestamp:event.timestamp,
                                                                // is_first_time_open_mail: is_first_time_open_mail
                                                            }
                                                        },
                                                        {merge:true}
                                                    )
                                                    .then(()=>{console.log('success'); resolve();})
                                                    .catch(()=>{console.log('error'); reject();});
                                            }


                                        }else{
                                            console.error('document not found');
                                            reject();
                                        }

                                    })
                                    .catch(()=>{
                                        console.error('error');
                                        reject();
                                    });


                            });

                            promises.push(p);


                            console.error('end update async')

                        }

                    }

                }

            });


            await Promise.all(promises);
            res.send('done');
        }
        catch (e) {
            console.error(e);
            res.send('error')

        }


    });
    // res.send('asdfadfasdf');


});

export const removeEmailTemplateRemoved = functions.https.onRequest((req, res) => {
    // This function use to remove action send email when email Template has deleted.
    // Use async await
    return cors(req, res, async () => {
        let db = admin.firestore();
        let emailTemplateID = req.body.emailTemplateID;

        try {
            //Phase 1: Remove action of PipelineStage.
            await db.collection("PipelineStage")
                .where("actions", ">", [])  // :pray:
                .get()
                .then(async (snapShot: any) => {
                    if (snapShot.empty) {
                        console.log('Nothing need to fix!!');
                        // res.send('Nothing need to fix!!');
                    } else {
                        console.log('-----------FIX PIPELINE STAGE-----------');
                        const arrayStage = snapShot.docs.map(async (doc: any) => {
                            // @ts-ignore
                            let actions = doc.data().actions;
                            let existActionWithTemplateEmailID = actions.findIndex((action: any) => {
                                return action.currentTemplate == emailTemplateID;
                            });
                            if (existActionWithTemplateEmailID > -1) {
                                //@ts-ignore
                                actions = actions.filter((action) => {
                                    return action.action_type != 'emails';
                                });
                                await db.collection("PipelineStage").doc(doc.id).update({actions: actions});
                                console.log('Fixed ', doc.data().title);
                            }
                            return doc;
                        });
                        await Promise.all(arrayStage);
                        console.log('Fixed 100%');
                    }

                }).catch(() => {
                console.log('error')
            });

            //Phase 2: Remove aciton send email of PositionPipelineAction.
            //@ts-ignore
            await db.collection("PositionPipelineAction")
                .where("actions", ">", [])  // :pray:
                .get()
                .then(async (snapShot: any) => {
                    if (snapShot.empty) {
                        console.log('Nothing need to fix!!');
                        // res.send('Nothing need to fix!!');
                    } else {
                        console.log('-----------FIX POSITION PIPELINE ACTION-----------');
                        const arrayStage = snapShot.docs.map(async (doc: any) => {
                            // @ts-ignore
                            let actions = doc.data().actions;
                            let existActionWithTemplateEmailID = actions.findIndex((action: any) => {
                                return action.currentTemplate == emailTemplateID;
                            });
                            if (existActionWithTemplateEmailID > -1) {
                                //@ts-ignore
                                actions = actions.filter((action) => {
                                    return action.action_type != 'emails';
                                });
                                await db.collection("PositionPipelineAction").doc(doc.id).update({actions: actions});
                                console.log('Fixed ', doc.data().stage_id);
                            }
                            return doc;
                        });
                        await Promise.all(arrayStage);
                        console.log('Fixed 100%');
                        // res.send({data:data})
                    }

                }).catch(() => {
                console.log('error')
            });
            res.send('Completed!!!');
        } catch (e) {
            res.send(e)
        }
    });
});


export const onCreateCompany = _onCreateCompany(functions as any, admin.app());
export const CJPBackup = _CJPBackup(functions, admin);
exports.api = functions.https.onRequest(api_app);
