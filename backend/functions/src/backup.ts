import * as _functions from 'firebase-functions';
import * as _admin from 'firebase-admin';
import axios from 'axios';

export function CJPBackup(functions: typeof _functions, admin: typeof _admin) {
    return functions.runWith({memory: "128MB"}).pubsub
        .schedule("30 9/24 * * *")
        // everyday at 9:30 AM, when people in CJS start writing `MITS`
        // https://firebase.google.com/docs/functions/schedule-functions
        .onRun(async ()=>{
            const credential = admin.app().options.credential;
            const oauthToken = credential ? await credential.getAccessToken() : null;

            const accessToken = oauthToken ? oauthToken.access_token : null;
            if (!accessToken) {
                console.error({
                    funcName: 'backup',
                    error: 403,
                    message: 'UnAuthorization',
                });
                return;
            }
            const projectId = admin.app().options.projectId;
            // https://firebase.google.com/docs/firestore/reference/rest/v1beta1/projects.databases/exportDocuments
            const url = `https://firestore.googleapis.com/v1beta1/projects/${projectId}/databases/(default):exportDocuments`;

            try {
                const res = await axios.post(url, {
                    outputUriPrefix: `gs://${projectId}-backups`,
                }, {
                    headers: {
                        Authorization: 'Bearer ' + accessToken,
                    },
                });
                console.debug({
                    funcName: 'backup',
                    res: res.data
                });
            } catch (error) {
                console.error({
                    funcName: 'backup',
                    error
                });
            }
        });
}
