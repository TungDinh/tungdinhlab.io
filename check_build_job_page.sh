#!/usr/bash
if [ $BUILD_JOB_PAGE ]; then
    echo "Build Job page"

    # for netlify build
    rm -rf ./dist/job/
    ls -l ./dist/

    rm -rf ./public/job/
    ls -l ./public/
fi
