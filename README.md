# Các Repo chính:

1. app | https://gitlab.com/cjs/app.cloudjet.work/ :  để develop
2. production: https://gitlab.com/cjs/pro.cloudjetpotential.com : để chạy app (sau khi login)
3. Career Site: https://gitlab.com/cjs/careersite.cloudjetpotential.com : để chạy các career sites (cái này nên hạn chế thay đổi )


# https://pro.cloudjetpotential.com


## Quy trình deploy qua production  ( [https://pro.cloudjetpotential.com](https://pro.cloudjetpotential.com) )

1. Tag version

```
$ git tag v0.1
$ git push --tags
```

2. Sau đó click vào button deploy ở CI: https://gitlab.com/cjs/app.cloudjet.work/pipelines


# https://career.cloudjetpotential.com

Được lưu trữ trên netlify https://app.netlify.com/sites/career-cloudjetpotential/overview
Account: team@cloudjetkpi.com / password: liên hệ Hồng 


## Project setup

Cài đặt `Yarn` **(Optional)**: https://yarnpkg.com/lang/en/docs/install/#mac-stable


```bash
yarn install
// or
npm install
```

### Compiles and hot-reloads for development

```bash
yarn run serve 
// or 
npm run serve
```

### Compiles and minifies for production

```bash
yarn build
// or
npm run build
```

### Run your unit tests

```bash
yarn test:unit
// or
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## Quy trình push code

1. Tạo Branch mới: `git checkout -b ten-branch`
2. Push lên gitlab và sau đó tạo merge request 

## Quy trình set task: 

1. Vào đây check task: https://kanbanflow.com/board/24pZJ4
2. Mỗi task cần bổ sung vào tiêu đề trước khi làm: [Số giờ - Deadline], ví dụ: [10h - 15/4/2019] Viết code
