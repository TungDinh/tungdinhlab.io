import Vue from 'vue'
import {Message} from "element-ui";
import Router, {Route} from 'vue-router'
import Home from './views/Home.vue';
import Login from './views/Login.vue';
import Portal from './views/Portal.vue';
import ResetPassword from './views/ResetPassword.vue';
import CreateUser from "@/views/CreateUser.vue";
// import LandingPage from "@/route/Landingpage";
import TaskDetails from "@/components/tasks/TaskDetails.vue";
import {store} from '@/store/store';
import PositionMiddleware from "@/permission/PositionPermissionMiddleware";
// @ts-ignore
import NProgress from 'nprogress';

import Page401 from '@/views/401.vue';
import MultiGuard from 'vue-router-multiguard';

import {cloneDeep as _cloneDeep} from 'lodash';

import CandidateModalRoutes from '@/route/CandidateModal';
import PermissionMiddleware from "@/permission/PermissionMiddleware";
import AuthMiddleware from "@/permission/AuthMiddleware";
import Position from "@/models/Position";
import {fauth} from "@/firebase";
import {Unsubscribe} from "firebase";

export const PositionNameRouter = [
    'PositionDetailTab',
    'PositionApplicationTab',
    'PositionPipelineTab',
    'PositionScorecardTab',
    'PositionCloneTab'
]

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL || '/',
    linkActiveClass: 'activeTab',
    routes: [
        {
            path: '/',
            name: 'home',
            component: /* webpackChunkName: 'home' */ Home,
        },
        {
            path: '/create-user',
            name: 'create_user',
            meta: {
                noAuth: true,
                noSidebar: true,
                forget: true,
            },
            component: /* webpackChunkName: 'create_user' */ CreateUser,
        },
        {
            path: '/reset-password',
            name: 'ResetPassword',
            meta: {
                noAuth: true,
                noSidebar: true,
                forget: true,
            },
            beforeEnter(to, from, next) {
                const mode = to.query.mode;
                if (mode === "signIn") {
                    return next({
                        path: '/create-user',
                        params: to.params,
                        query: to.query,
                    });
                }
                next();
            },
            component: /* webpackChunkName: 'reset_password' */ ResetPassword,
        },
        CandidateModalRoutes,
        {
            path: '/portal',
            name: 'Portal',
            components: {
                default: Home,
                modal_content: () => import(/* webpackChunkName: 'portal' */'./views/Portal.vue')
            },
            redirect: '/portal/type',
            meta: {
                showModal: true,
                onlyAdmin: true,
            },
            children: [
                {
                    path: 'type',
                    name: "portal-type",
                    components: {modal_child: () => import (/* webpackChunkName: 'portal' */'./components/portal/Type.vue')},

                },
                {
                    path: 'appearance',
                    name: "portal-appearance",
                    components: {modal_child: () => import (/* webpackChunkName: 'portal' */'./components/portal/Appearance.vue')},
                    meta: {
                        showModal: true,
                    },
                },
                {
                    path: 'messaging',
                    name: "portal-messaging",
                    components: {modal_child: () => import (/* webpackChunkName: 'portal' */'./components/portal/Messaging.vue')},
                    meta: {
                        showModal: true,
                    },
                },
                {
                    path: 'social',
                    name: "portal-social",
                    components: {modal_child: () => import (/* webpackChunkName: 'portal' */'./components/portal/Social.vue')},
                    meta: {
                        showModal: true,
                    },
                },
                {
                    path: 'perks',
                    name: "portal-perks",
                    components: {modal_child: () => import (/* webpackChunkName: 'portal' */'./components/portal/Perks.vue')},
                    meta: {
                        showModal: true,
                    },
                },
                {
                    path: 'testimonials',
                    name: "portal-testimonials",
                    components: {modal_child: () => import (/* webpackChunkName: 'portal' */'./components/portal/Testimonials.vue')},
                    meta: {
                        showModal: true,
                    },
                },
                {
                    path: 'employees',
                    name: "portal-employees",
                    components: {modal_child: () => import (/* webpackChunkName: 'portal' */'./components/portal/Employees.vue')},
                    meta: {
                        showModal: true,
                    },
                },
                {
                    path: 'media',
                    name: "portal-media",
                    components: {modal_child: () => import (/* webpackChunkName: 'portal' */'./components/portal/Media.vue')},
                    meta: {
                        showModal: true,
                    },
                },
                {
                    path: 'layout',
                    name: "portal-layout",
                    components: {modal_child: () => import (/* webpackChunkName: 'portal' */'./components/portal/Layout.vue')},
                    meta: {
                        showModal: true,
                    },
                },
                {
                    path: 'meta',
                    name: "portal-meta",
                    components: {modal_child: () => import (/* webpackChunkName: 'portal' */'./components/portal/Meta.vue')},
                    meta: {
                        showModal: true,
                    },
                }, {
                    path: 'custom-css-js',
                    name: "portal-custom-css",
                    components: {modal_child: () => import (/* webpackChunkName: 'portal' */'./components/portal/CustomCssJs.vue')},
                    meta: {
                        showModal: true,
                    },
                },{
                    path: 'general',
                    name: "portal-general",
                    components: {modal_child: () => import (/* webpackChunkName: 'portal' */'./components/portal/General.vue')},
                    meta: {
                        showModal: true,
                    },
                },

            ],
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: 'about' */'./views/About.vue')
        },
        {
            path: '/login',
            name: 'Login',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: Login,
            meta: {
                noAuth: true,
                noSidebar: true,
            }
        },
        {
            path: '/inbox',
            name: 'Inbox',

            component: () => import(/* webpackChunkName: 'inbox' */'./views/Inbox.vue')
        },
        {
            path: '/tasks',
            name: 'Tasks',

            // component: ()=> import("./components/tasks/TaskPage.vue"),
            component: () => import(/* webpackChunkName: 'task' */'./views/Tasks.vue'),
            children: [
                {
                    path: ":task_id",
                    name: "taskDetail",
                    component: /* webpackChunkName: 'task' */TaskDetails,
                },

            ],
        },
        {
            path: '/calendar',
            name: 'Calendar',

            component: () => import(/* webpackChunkName: 'calendar' */'./views/Calendar.vue')
        },
        {
            path: '/analytics',
            redirect: "analytics/overview",
            name: 'Analytics',
            component: () => import(/* webpackChunkName: 'report' */'./views/Report.vue'),
            children: [
                {
                    path: "overview",
                    name: "ReportOverview",
                    component: ()=>import(/* webpackChunkName: 'report' */"./components/Analytics/Overview/Index.vue"),
                },
                {
                    path: "applied",
                    name: "ReportApplied",
                    component: () => import(/* webpackChunkName: 'report' */"./components/Analytics/Applied/Index.vue"),
                }
                ,
                {
                    path: "sourced",
                    name: "ReportSourced",
                    component: () => import(/* webpackChunkName: 'report' */"./components/Analytics/Sourced/Index.vue"),
                },
                {
                    path: "referred",
                    name: "ReportReferred",
                    component: () => import(/* webpackChunkName: 'report' */"./components/Analytics/Referred/Index.vue"),
                },
                {
                    path: "recruited",
                    name: "ReportRecruited",
                    component: () => import(/* webpackChunkName: 'report' */"./components/Analytics/Recruited/Index.vue"),
                },
                {
                    path: "pipeline-performance",
                    name: "ReportPipelinePerformance",
                    component: () => import(/* webpackChunkName: 'report' */"./components/Analytics/PipelinePerformance/Index.vue"),
                },
                {
                    path: "time-to-fill",
                    name: "ReportTimeToFill",
                    component: () => import(/* webpackChunkName: 'report' */"./components/Analytics/TimeToFill/Index.vue"),
                },
                {
                    path: "messages",
                    name: "ReportMessages",
                    component: () => import(/* webpackChunkName: 'report' */"./components/Analytics/Messages/Index.vue"),
                },
                {
                    path: "questionaires",
                    name: "ReportQuestionaires",
                    component: () => import(/* webpackChunkName: 'report' */"./components/Analytics/Questionaires/Index.vue"),
                },
                {
                    path: "open-positions",
                    name: "ReportOpenPositions",
                    component: () => import(/* webpackChunkName: 'report' */"./components/Analytics/OpenPositions/Index.vue"),
                },
            ],
        },
        {
            path: '/positions',
            name: 'Positions',
            component: () => import(/* webpackChunkName: 'position' */'./views/Positions.vue'),
        },
        {
            path: '/candidates',
            name: 'Candidates',

            component: () => import(/* webpackChunkName: 'candidate' */'./views/Candidates.vue')
        },
        {
            path: '/pipeline',
            name: 'Pipeline',
            meta: {
                positionCheck: true,
            },
            component: () => import(/* webpackChunkName: 'pipeline' */'./views/Pipeline.vue')
        },
        {
            path: '/profile',
            name: 'Profile',

            component: () => import(/* webpackChunkName: 'profile' */'./views/Profile.vue'),
            meta: {
                showModal: true,
            },
        },
        // {
        //     path: '/w/:slug',
        //     name: 'LandingPage',
        //
        //     component: LandingPage,
        //     meta: {
        //         noAuth: true,
        //         noSidebar: true,
        //     },
        // },
        // LandingPage,
        {
            path: '/company',
            name: 'Company',
            components: {
                default: Home,
                modal_content: () => import(/* webpackChunkName: 'company' */'./views/Company.vue'),
            },
            meta: {
                showModal: true,
                onlyAdmin: true,
            },
            children: [
                {
                    path: 'pipeline',
                    name: "setting-pipeline",
                    components: {modal_child: () => import(/* webpackChunkName: 'company' */'./components/company/pipeline.vue')},
                },
                {
                    path: 'questionnaires',
                    name: "setting-questionnaires",
                    components: {modal_child: () => import(/* webpackChunkName: 'company' */'./components/company/questionnaires.vue')},
                },
                {
                    path: 'interviewguides',
                    name: "setting-interview-guides",
                    components: {modal_child: () => import(/* webpackChunkName: 'company' */'./components/company/interviewguides.vue')},
                },
                {
                    path: 'messages-template',
                    name: "messages-template",
                    components: {modal_child: () => import(/* webpackChunkName: 'company' */'./components/company/MessagesTemplate.vue')},
                },
                {
                    path: 'scorecards',
                    name: "setting-scorecards",
                    components: {modal_child: () => import(/* webpackChunkName: 'company' */'./components/company/scorecards.vue')},
                },
                {
                    path: 'campaigns',
                    name: "setting-campaigns",
                    components: {modal_child: () => import(/* webpackChunkName: 'company' */'./components/company/campaigns.vue')},
                },
                {
                    path: 'tags',
                    name: "setting-tags",
                    components: {modal_child: () => import(/* webpackChunkName: 'company' */'./components/company/tags.vue')},
                },

            ],
        },

        {
            path: '/settings',
            name: 'Settings',
            components: {
                default: Home,
                modal_content: () => import(/* webpackChunkName: 'setting' */'./views/Settings.vue')
            },
            redirect: '/settings/general',
            meta: {
                showModal: true,
                onlyAdmin: true,
            },
            children: [
                {
                    path: 'general',
                    name: 'SettingsGeneral',
                    components: {modal_child: () => import (/* webpackChunkName: 'setting' */'./components/settings/General.vue')},
                },
                {
                    path: 'users',
                    name: 'SettingsMembers',
                    components: {modal_child: () => import (/* webpackChunkName: 'setting' */'./components/settings/Users.vue')},
                },
                {
                    path: 'connect-source-data',
                    name: 'ConnectSourceData',
                    components: {modal_child: () => import (/* webpackChunkName: 'setting' */'./components/settings/ConnectSourceData.vue')},
                },
                {
                    path: 'teams',
                    name: 'SettingsTeams',
                    components: {modal_child: () => import (/* webpackChunkName: 'setting' */'./components/settings/Teams.vue')},
                },
            ],
        },

        {
            path: '/settings/user', // with Modal
            name: 'UserPopupSetting',
            redirect: '/settings/user/profile',
            components: {
                default: Home,
                modal_content: () => import (/* webpackChunkName: 'setting' */'./components/modals/UserSetting.vue')
            },
            meta: {
                showModal: true,
            },
            children: [
                {
                    path: 'profile',
                    name: 'UserSettingProfile',
                    components: {modal_child: () => import(/* webpackChunkName: 'setting' */'./components/settings/UserProfile.vue')},
                },
                {
                    path: 'password',
                    name: 'UserSettingPassword',
                    components: {modal_child: () => import(/* webpackChunkName: 'setting' */'./components/settings/UserPassword.vue')},
                },
                {
                    path: 'notification',
                    name: 'UserSettingNotification',
                    components: {modal_child: () => import(/* webpackChunkName: 'setting' */'./components/settings/UserNotify.vue')},
                },
                {
                    path: 'sign',
                    name: 'UserSettingSignature',
                    components: {modal_child: () => import(/* webpackChunkName: 'setting' */'./components/settings/UserSignature.vue')},
                }
            ]
        },

        {
            path: "/event/:event_id",
            name: "EditEvent",
            component: Home,
            children: [
                {
                    path: '/',
                    name: "EventModal",
                    component: () => import(/* webpackChunkName: 'event' */"./views/EventMetting.vue"),
                    meta: {
                        showModal: true,
                    },
                }
            ]
        },

        // position detail + new position modal
        {
            path: "/positions",
            name: "NewPositionModal",
            beforeEnter(to, from, next) {
                if (to.params.position_id) {
                    new Position().getById(to.params.position_id).then((data) => {
                        store.dispatch('setCurrentPosition', data);
                        store.commit('setCachePosition', _cloneDeep(data));
                    })
                }
                next();
            },
            components: {
                default: Home,
                modal_content: ()=> import(/* webpackChunkName: 'position_detail' */"./components/Position/NewPositionModal.vue"),
            },
            meta: {
                showModal: true,
            },
            children: [
                {
                    path: ":type",
                    name: "NewPosition",
                    components: {
                        modal_child: () => import(/* webpackChunkName: 'position_detail' */"./components/Position/PositionDetailTab.vue"),
                    },
                    props: true,
                },
                {
                    path: ":position_id",
                    name: 'PositionDetailModal',
                    redirect: ":position_id/edit/detail",
                    meta: {
                        positionCheck: true,
                    },
                },
                {
                    path: ":position_id/:type/detail",
                    name: 'PositionDetailTab',
                    components: {
                        modal_child: ()=>import(/* webpackChunkName: 'position_detail' */"./components/Position/PositionDetailTab.vue"),
                    }
                },
                {
                    path: ":position_id/:type/application",
                    name: PositionNameRouter[1],
                    components: {
                        modal_child: ()=>import(/* webpackChunkName: 'position_detail' */"./components/Position/PositionApplicationTab.vue"),
                    }
                },
                {
                    path: ":position_id/:type/pipeline",
                    name: PositionNameRouter[2],
                    components: {
                        modal_child: ()=>import(/* webpackChunkName: 'position_detail' */"./components/Position/PositionPipelineTab.vue"),
                    }
                },
                {
                    path: ":position_id/:type/scorecard",
                    name: PositionNameRouter[3],
                    components: {
                        modal_child: ()=>import(/* webpackChunkName: 'position_detail' */"./components/Position/PositionScorecardTab.vue"),
                    }
                },
                {
                    path: ":type",
                    name: PositionNameRouter[4],
                    components: {
                        modal_child: ()=>import(/* webpackChunkName: 'position_detail' */"./components/Position/PositionDetailTab.vue"),
                    }
                },
                // {
                //     path: ":position_id/:type/promote",
                //     name: "PositionPromoteTab",
                //     components: {
                //         modal_child: ()=>import("./components/Position/PositionPromoteTab.vue"),
                //     }
                // },
                // {
                //     path: ":position_id/:type/team",
                //     name: "PositionTeamTab",
                //     components: {
                //         modal_child: ()=>import("./components/Position/PositionTeamTab.vue"),
                //     }
                // }
            ]
        },

        // Tab in Position page
        {
            path: "/p/:position_id",
            name: "PositionDetail",
            meta: {
                positionCheck: true,
            },
            component: ()=>import(/* webpackChunkName: 'position_pipeline' */'./views/PositionDetail.vue'),
            redirect: "/p/:position_id/pipeline",
            children: [
                {
                    path: "pipeline",
                    name: "Pipeline",
                    component: ()=> import(/* webpackChunkName: 'position_pipeline' */"./components/Position/Pipeline.vue"),
                },
                {
                    path: "candidates",
                    name: "PositionCandidates",
                    component: ()=> import(/* webpackChunkName: 'candidate' */"./components/Candidate/CandidatePage.vue"),
                },
                {
                    path: "calendar",
                    name: "PositionCalendar",
                    component: ()=> import(/* webpackChunkName: 'calendar' */"./components/Calendar/CalendarPage.vue"),
                },
                {
                    path: "inbox",
                    name: "PositionInbox",
                    component: ()=> import(/* webpackChunkName: 'inbox' */"./components/Inbox/InboxPage.vue"),
                },
                {
                    path: "analytics",
                    name: "PositionReport",
                    component: ()=> import(/* webpackChunkName: 'report' */"./components/Analytics/Analytics.vue"),
                    meta:{
                        isPositionReport:true,
                    },
                    redirect: 'analytics/overview',
                    children: [
                        {
                            path: "overview",
                            name: "ReportOverview",
                            component: ()=>import(/* webpackChunkName: 'report' */"./components/Analytics/Overview/Index.vue"),
                        },
                        {
                            path: "applied",
                            name: "ReportApplied",
                            component: () => import(/* webpackChunkName: 'report' */"./components/Analytics/Applied/Index.vue"),
                        }
                        ,
                        {
                            path: "sourced",
                            name: "ReportSourced",
                            component: () => import(/* webpackChunkName: 'report' */"./components/Analytics/Sourced/Index.vue"),
                        },
                        {
                            path: "referred",
                            name: "ReportReferred",
                            component: () => import(/* webpackChunkName: 'report' */"./components/Analytics/Referred/Index.vue"),
                        },
                        {
                            path: "recruited",
                            name: "ReportRecruited",
                            component: () => import(/* webpackChunkName: 'report' */"./components/Analytics/Recruited/Index.vue"),
                        },
                        {
                            path: "pipeline-performance",
                            name: "ReportPipelinePerformance",
                            component: () => import(/* webpackChunkName: 'report' */"./components/Analytics/PipelinePerformance/Index.vue"),
                        },
                        {
                            path: "time-to-fill",
                            name: "ReportTimeToFill",
                            component: () => import(/* webpackChunkName: 'report' */"./components/Analytics/TimeToFill/Index.vue"),
                        },
                        {
                            path: "messages",
                            name: "ReportMessages",
                            component: () => import(/* webpackChunkName: 'report' */"./components/Analytics/Messages/Index.vue"),
                        },
                        {
                            path: "questionaires",
                            name: "ReportQuestionaires",
                            component: () => import(/* webpackChunkName: 'report' */"./components/Analytics/Questionaires/Index.vue"),
                        },
                        {
                            path: "open-positions",
                            name: "ReportOpenPositions",
                            component: () => import(/* webpackChunkName: 'report' */"./components/Analytics/OpenPositions/Index.vue"),
                        },
                    ],
                },
                // {
                //     path: "activity",
                //     name: "PositionActivity",
                //     component: ()=> import("./components/Activity/Activity.vue"),
                // },
                {
                    path: "tasks",
                    name: "PositionTasks",
                    component: ()=> import(/* webpackChunkName: 'task' */"./components/tasks/TaskPage.vue"),
                    children: [
                        {
                            path: ":task_id",
                            name: "positionTaskDetail",
                            component: /* webpackChunkName: 'task' */TaskDetails,
                        },

                    ],
                },
            ]
        },
        {
            path: "/positions/candidates",
            name: "Pipeline",
            component: ()=>import('./views/Pipeline.vue'),
        },

        {
            path: "/401",
            name: "MissingPermission",
            components: {
                default: Page401,
                modal_content: Page401,
            },
            meta: {
                noAuth: true,
            },
        },

        {
            path: '*',
            name: "NotFound",
            components: {
                default: () => import('./views/404.vue'),
                modal_content: () => import('./views/404.vue'),
            },
        },
    ]
});
function showModalMiddleware(to: Route, from: Route, next) {
    // console.log(to, from);
    if (to.matched.some(route=>route.meta.showModal)) {
        const lastFrom = from.matched[0];
        if (lastFrom) {
            // to.matched[0].meta.returnPath = lastFrom.path;
            to.matched[0].components.default = lastFrom.components.default;
            to.matched[to.matched.length - 1].meta.showModal = true;

            for (let i = 1; i < from.matched.length; ++i) {
                if (from.matched[i]) {
                    if (to.matched[i])
                        to.matched[i].components.default = from.matched[i].components.default;
                    else
                        to.matched.push(from.matched[i]);
                }
            }
        }
    } else {
        store.state.last_route = to.fullPath;
    }
    next();
}

router.beforeEach(MultiGuard([
    function(to, from, next) {
        NProgress.start();
        next();
    },
    AuthMiddleware,
    PermissionMiddleware,
    PositionMiddleware,
    showModalMiddleware,
]));

router.afterEach(()=>{
    NProgress.done();
});
router.onError((error)=>{
    Message.error(error.message);
    NProgress.done();
    if (!router.currentRoute.matched.length)
        router.replace('/');
});

Vue.$router = router;
export default router;
