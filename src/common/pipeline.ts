import {messages} from "../messages/";

export const time_plan = [
    {
        value: 1,
        title: 'No delay'
    },
    {
        value: 2,
        title: 'Delay 1 hour'
    },
    {
        value: 3,
        title: 'Delay 4 hour'
    },
    {
        value: 4,
        title: 'Delay 2 days'
    },
    {
        value: 5,
        title: 'Delay 3 days'
    },
    {
        value: 6,
        title: 'Delay 1 week'
    },
    {
        value: 7,
        title: 'Delay 2 week'
    },
    {
        value: 8,
        title: 'Delay 1 month'
    },
    {
        value: 9,
        title: 'Delay 6 months'
    },
]
export const custom_data_action = {
    emails: {
        placeholder: messages.COMMON_TXT_SELECT_TEMPLATE,
        template: [],
        title: messages.COMMON_SEND_EMAIL,
        content: messages.SR_007_ACTIONS_SEND_EMAIL_CONTENT,
        optional: [
            {
                value: null,
                title: 'Specify Sender Name (Optional)',
                description: 'Set a specific name to use as the sender of this email.',
                status: false,
                isInput: true,
            },
            {
                value: null,
                title: 'No-Reply Email Address (Optional)',
                description: 'Send from an email address that doesn\'t allow replies.',
                status: false,
                isInput: false
            }
        ],
        time_plan: time_plan
    },
    questionnaires: {
        title: 'Send Questionnaire',
        content: 'Automatically send a questionnaire to candidates entering this stage.',
        placeholder: 'Select a Questionnaire',
        template: [],
        optional: [],
        time_plan:time_plan
    },
    candidate_scorecard: {
        title: messages.SR_007_CANDIDATE_SCORECARD,
        content: messages.SR_007_CANDIDATE_SCORECARD_DESC,
        // template: [],
        time_plan: [],
        optional: [],
    },
}
export const listStage = [
    {
        title: messages.SR_007_MODAL_PIPELINE_STAGE_APPLIED,
        icon: 'fa fa-user',
        value: 'applied',
        hide: true,
        showInPositionPipelineStageModal: true,
        stage_type: 'applied',
    },
    {
        title: messages.SR_007_MODAL_PIPELINE_STAGE_FEEDBACK,
        icon: 'fa fa-users',
        value: 'feedback',
        showInPositionPipelineStageModal: true,
        stage_type: 'feedback',
    },
    {
        title: messages.SR_007_MODAL_PIPELINE_STAGE_INTERVIEW,
        icon: 'fa fa-comments',
        value: 'interview',
        showInPositionPipelineStageModal: true,
        stage_type: 'interview',
    },
    {
        title: messages.SR_007_MODAL_PIPELINE_STAGE_PHONESCREEN,
        icon: 'fa fa-phone',
        value: 'phonescreen',
        showInPositionPipelineStageModal: false,
        stage_type: 'phonescreen',
    },
    {
        title: messages.SR_007_MODAL_PIPELINE_STAGE_MADEOFFER,
        icon: 'fa fa-gift',
        value: 'madeoffer',
        showInPositionPipelineStageModal: true,
        stage_type: 'madeoffer',
    },
    {
        title: messages.SR_007_MODAL_PIPELINE_STAGE_DISQUALIFIED,
        icon: 'fa fa-trash',
        value: 'disqualified',
        showInPositionPipelineStageModal: true,
        stage_type: 'disqualified',
    },
    {
        title: messages.SR_007_MODAL_PIPELINE_STAGE_HIRED,
        icon: 'fa fa-trophy',
        value: 'hired',
        showInPositionPipelineStageModal: true,
        stage_type: 'hired',
    },
    {
        title: messages.SR_007_MODAL_PIPELINE_STAGE_SOURCED,
        icon: 'fa fa-upload',
        value: 'sourced',
        showInPositionPipelineStageModal: false,
        stage_type: 'sourced',
    },
    {
        title: messages.SR_007_MODAL_PIPELINE_STAGE_OTHER,
        icon: 'fa fa-list-ul',
        value: 'other',
        showInPositionPipelineStageModal: false,
        stage_type: 'other',
    }
];
