import axios from "axios";
import {requestFunctionUrl} from "@/firebase";
import moment from "moment";

export class ServerTime {
    private serverTime = new Date();
    private timeInterval = null;

    constructor() {
        console.log("Getting server time");
        this.getServerTime();
    }

    get now() {
        return new Date(this.serverTime);
    }

    async getServerTime() {
        let response = await axios.get(requestFunctionUrl('date'));
        let serverTime = new Date();
        if (response.status == 200) {
            serverTime = new Date(response.data);
        }
        this.serverTime = serverTime;
        this.loopTime();
    }

    loopTime() {
        let that = this as any;
        this.timeInterval = setInterval(() => {
            that.serverTime = new Date(moment(that.serverTime).add(1, 'seconds').format());
            // @ts-ignore
            window.serverTime = that.serverTime
        }, 1000)
    }
}
