export interface IPositionCategory {
    value:string,
    name: string
}
import {messages} from "@/messages";

export const positioncategories:IPositionCategory[] = [
    {
        "value": "software",
        "name": messages.SR_021_KANBAN_SOFTWARE_DEV
    },
    {
        "value": "design",
        "name": messages.SR_021_KANBAN_INTERACTIVE_DESIGN
    },
    {
        "value": "product",
        "name": messages.SR_021_KANBAN_PRODUCT_MANAGEMENT
    },
    {
        "value": "sysadmin",
        "name": messages.SR_021_KANBAN_SYSTEM_ADMIN
    },
    {
        "value": "devops",
        "name": messages.SR_021_KANBAN_DEVOPS
    },
    {
        "value": "finance",
        "name": messages.SR_021_KANBAN_FINANCE
    },
    {
        "value": "custserv",
        "name": messages.SR_021_KANBAN_CUSTOM_SERVICE
    },
    {
        "value": "sales",
        "name": messages.SR_021_KANBAN_SALES
    },
    {
        "value": "marketing",
        "name": messages.SR_021_KANBAN_MARKETING
    },
    {
        "value": "pr",
        "name": messages.SR_021_KANBAN_COMMUNICATIONS
    },
    {
        "value": "design-not-interactive",
        "name": messages.SR_021_KANBAN_DESIGN
    },
    {
        "value": "hr",
        "name": messages.SR_021_KANBAN_HUMAN_RESOURCES
    },
    {
        "value": "management",
        "name": messages.SR_021_KANBAN_MANAGEMENT
    },
    {
        "value": "operations",
        "name": messages.SR_021_KANBAN_OPERATIONS
    },
    {
        "value": "other",
        "name": messages.SR_021_KANBAN_OTHER
    }
];
