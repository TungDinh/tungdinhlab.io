import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/storage';
import "firebase/performance";
import {ServerTime} from "../common/ServerTime";

let config = {};
let emailDomain='cloudjetpotential.com';
console.log(process.env);
console.log('---------------------');
if (process.env.VUE_APP_USE_FIREBASE_ENV_CONFIG) { // add this variable in CI
    //@ts-ignore
    config = JSON.parse(process.env.VUE_APP_FIREBASE_CONFIG); // add json content in 1 line
    if(process.env.EMAIL_DOMAIN){
        emailDomain=process.env.EMAIL_DOMAIN;
    }
} else if (process.env.VUE_APP_USE_PRODUCTION_DATABASE) {
    config = {
        // Production Keys:
        apiKey: "AIzaSyCphd7p-BHxKBj9Xaxk85ilcYYRm-HCC-Q",
        authDomain: "cloudjet-work.firebaseapp.com",
        databaseURL: "https://cloudjet-work.firebaseio.com",
        projectId: "cloudjet-work",
        storageBucket: "cloudjet-work.appspot.com",
        messagingSenderId: "736915700044",
        appId: "1:736915700044:web:972207a8bebea959",
    };
    emailDomain='cloudjetpotential.com';
} else if (process.env.VUE_APP_USE_QC_DATABASE) {
    config = {
        apiKey: "AIzaSyAMXINHzFZQL3nSC6dGw3o4fGs-4-fbCaQ",
        authDomain: "testtempqc.firebaseapp.com",
        databaseURL: "https://testtempqc.firebaseio.com/",
        projectId: "testtempqc",
        storageBucket: "testtempqc.appspot.com",
        messagingSenderId: "352876016721",
        appId: "1:352876016721:web:f62f3fbb70236dbf"
    };
    emailDomain='qc.cloudjetpotential.com';
} else {
   config = {
       apiKey: "AIzaSyAmaUlIFpaBnzX45MYJ2LjVV7GrgTNti8E",
       authDomain: "just-landing-229509.firebaseapp.com",
       databaseURL: "https://just-landing-229509.firebaseio.com",
       projectId: "just-landing-229509",
       storageBucket: "just-landing-229509.appspot.com",
       messagingSenderId: "512260630417",
   };
    emailDomain='dev.cloudjetpotential.com';
}
//@ts-ignore
const LOCAL_DEV:boolean = !!process.env.VUE_APP_LOCAL_DEV; // https://stackoverflow.com/a/50831022

// https://stackoverflow.com/questions/44078037/how-to-get-firebase-project-name-or-id-from-cloud-function
export const requestFunctionUrl=(function_path:string):string => {
    // http://localhost:5001/just-landing-229509/us-central1/
    // @ts-ignore
    const projectId:string=config.projectId;

    if(LOCAL_DEV){
        console.log('its local dev');
        return `http://localhost:5001/${projectId}/us-central1/${function_path}`
    }else{
        console.log('asdfasd');
        return `https://us-central1-${projectId}.cloudfunctions.net/${function_path}`
    }


};
export const email_domain=emailDomain;


firebase.initializeApp(config);
firebase.firestore().enablePersistence({
    synchronizeTabs: true,
}).catch(function (err) {
    if (err.code == 'failed-precondition') {
        // NOTE: when cannot handle this, give it threw in the screen
        throw err;
    } else if (err.code == 'unimplemented') {
        // NOTE: when cannot handle this, give it threw in the screen
        throw err;
    }
});

export const db = firebase.firestore();
export const fauth = firebase.auth();
export const fstorage = firebase.storage();
export const perf = firebase.performance();
export const server_time = new ServerTime();

import authError from './error/auth';
import FirestoreError from './error/firestore';
export const ERROR = {
    AUTH: authError,
    FIRESTORE: FirestoreError,
};
export default firebase;
