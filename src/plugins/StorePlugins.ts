/**
 * @author Dan An
 * @why Global Access Vuex Store by `Vue.$store`, no need `import {store} from '@/store/`
 *      Vue.$store is a dynamic store, it means Vue.$store will difference in `app` bundle and `job` bundle
 */

import {Store} from "vuex";
import {PluginObject} from "vue";
import {Vue as _Vue} from "vue/types/vue";

export interface IStorePluginsOptions {
    store: Store<any>;
}

const StorePlugins: PluginObject<IStorePluginsOptions> = {
    install(Vue: typeof _Vue, options?: IStorePluginsOptions)
    {
        Vue.$store = options.store;
    }
};

export default StorePlugins;
