import Vue from 'vue';
import {VueRouter} from "vue-router/types/router";
import {urlLandingpage} from "@/helpers/joblinks";
import {Store} from "vuex";

declare module 'vue/types/vue' {
    export interface VueConstructor<V extends Vue = Vue> {
        $log: {
            debug(...args: any[]): void;
            info(...args: any[]): void;
            warn(...args: any[]): void;
            error(...args: any[]): void;
            fatal(...args: any[]): void;
        };

        $router: VueRouter;

        $urlLandingPage: typeof urlLandingpage;

        /**
         * @author An
         * access global, no need to `import {store} from '@/store/` anymore
         */
        $store: Store<any>;
    }
}


