import Model from './base';
import Company from "@/models/Company";
import {store} from "@/store/store";
import User from "@/models/User";
import {messages} from "@/messages";
import {server_time} from "@/firebase";

export default class Portal extends Model {
    title: string = "";
    company_id : string = "";
    company_address_url: string = "";
    description: string = ""
    // START type page
    config: string = "bold"; // bold | simple
    // END type page
    // START appearance page
    appearance_logo: string = "";
    appearance_header: string = "";
    appearance_header_backgroud_color: string = "rgb(58, 55, 62)";
    appearance_favicon: string = "";
    appearance_button_color: string = "rgb(134, 198, 143)";
    appearance_link_color: string = "rgb(134, 198, 143)";
    apply_by_linkedin: boolean = true;
    apply_by_indeed: boolean = true;
    // END appearance page

    // START messaging page
    messaging_header: string = messages.SR_003_CONFIG_HEADER;
    messaging_subheader: string = messages.SR_003_CONFIG_SUBHEADER;
    messaging_introduction: string = messages.SR_003_CONFIG_INTRODUCTION;
    // END messaging page

    // START social page
    website: string = "";
    twitter: string = "";
    facebook: string = "";
    linkedin: string = "";
    instagram: string = "";
    blog: string = "";
    tax_code: string = "";
    // END social page


    perks: { icon: string, name: string }[] = [];

    testimonials: { name: string, position: string, testimonial: string, photo: string }[] = [];

    employees: { name: string, position: string, photo: string }[] = [];

    // START media page
    youtube_video: string = "";
    vimeo_video:string = "";
    video_positions_page:boolean=false;
    video_position_page:boolean=false;
    photos: { url: string } [] = [];
    // END media page

    // START custom page
    js: string = "";
    css: string = "";
    // END custom page

    layout: [
        object,
        object,
        object,
        object,
        object
    ] = [

            {
                name:'general',
                is_show:true,
            },
            {
                name: 'perks',
                is_show: true,
            },
            {
                name:'gallery',
                is_show: true,
            },
            {
                name:'testimonials',
                is_show: true,
            },
            {
                name: 'positions_list',
                is_show: true,
            },
            // {
            //     name:'employees',
            //     is_show: true,
            // }
    ];
    constructor() {
        super("Portal");
    };

    public async getAllPortal(company_id:string): Promise<Portal>{
        const results = new Portal();
        const modelCollection = this.firestore.collection(this.COLLECTION);
        await modelCollection
            .where("company_id", "==", company_id).get()
            .then(  async (querySnapshot)=> {
            if (querySnapshot.empty == true) {
                    let _newPortal = new Portal();
                    _newPortal.company_id = company_id;
                    await _newPortal.insert().then((new_portal) => {
                        Object.assign(results, new_portal);
                        results.id = new_portal.id;
                    });

            }else {
                await querySnapshot.forEach(docSnapshot => {
                    Object.assign(results, docSnapshot.data());
                    results.id = docSnapshot.id;
                });
            }
        })
        return await results
    }
    //override
    async insert(): Promise<Portal> {
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let result = {...this};
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        //const {TIMESTAMP} = firebase.database.ServerValue;
        result.created_date = server_time.now.valueOf(); //TIMESTAMP;
        result.updated_date = server_time.now.valueOf(); //TIMESTAMP;

        //const {TIMESTAMP} = firebase.database.ServerValue;
        result.createdAt = server_time.now.valueOf(); //TIMESTAMP;
        result.updatedAt = server_time.now.valueOf(); //TIMESTAMP;

        const model = await modelCollection.add(JSON.parse(JSON.stringify(result)));
        this.id = model.id;

        return this;

    }

    async update(): Promise<Portal> {

        const modelCollection = this.firestore.collection(this.COLLECTION);

        let result = {...this};
        let id = result['id'];
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        // const {TIMESTAMP} = firebase.database.ServerValue;
        result.updated_date = server_time.now.valueOf();
        result.updated_by =  store.getters.currentUser.id;

        // @ts-ignore
        const model = await modelCollection.doc(id).set(JSON.parse(JSON.stringify(result), {merge: true}));

        return this;
    }
    // static get instance(): Portal {
    //     return new Portal();
    // }

}
