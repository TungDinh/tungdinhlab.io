import Model from './base';
import Company from "@/models/Company";
import {store} from "@/store/store";
import User from "@/models/User";
import Questionnaire from "@/models/Questionnaire";
import * as firebase from "firebase";
import Query = firebase.firestore.Query;
import {IPositionCategory} from "@/common/positioncategories";
import moment from "moment";
import {server_time} from "@/firebase";



export default class PipelineReport extends Model {

    // move candidate in & out of stage
    movement_date: {
        in: number,
        out: number
    }={
        in: 0,
        out:0
    };
    company_id: string = "";


    pipeline = {
        id: '',
        name: '',
        is_default: false,
    };

    stage = {
        id:'',
        name:'',
        type: '',
    };


    position = {
        id: '',
        name: '',
        user_id: '',
        status: '',// draft, published, closed
    };

    category:IPositionCategory = {
        value:'',
        name: ''
    };





    source =  {
        id: "",
        name: "",// vi du: Portal, Facebook, ...
        type: "", // form, import, Portal (when candidate is added)
        origin: "", //applied | recruiter | referral | sourced
        // nguoi tao candidate truc tiep tu he thong
        sourced_by: {
            id:  "",
            email_address:  "",
            name:  ""
        }
    };
    candidate = {
        id: '',
        name: '',

    };



    constructor() {
        super("pipeline-report"); // what is the super??
    }

    static get instance(): PipelineReport {
        return new PipelineReport(); // what for? --> PipelineReport.instance.<instance-method>
    }

    public async getReports(conditions:{fieldPath: string | firebase.firestore.FieldPath, opStr: firebase.firestore.WhereFilterOp, value: any}[]=null, limit:number=20): Promise<PipelineReport[]> {

        const results: PipelineReport[] = new Array();

        let company = store.getters.currentCompany;

        if (company) {


            let cursor:Query = this.firestore.collection(this.COLLECTION);
            cursor = cursor.where("company_id", "==", company.id)

            if (conditions){
                conditions.forEach((condition)=>{
                    cursor=cursor.where(condition.fieldPath, condition.opStr, condition.value)
                })
            }
            if(limit){
                cursor = cursor.limit(limit)
            }

            const records = await cursor.get()


            records.forEach(docSnapshot => {

                let record = new PipelineReport();

                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                results.push(record);

            });
        }


        return results
    }

    public async getReportByStage(stage_id: string, candidate_id: string): Promise<Model> {
        let report = new PipelineReport();
        let company = store.getters.currentCompany;
        if (company) {
            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection
                .where("company_id", "==", company.id)
                .where("stage.id", "==", stage_id)
                .where("candidate.id", "==", candidate_id)
                .orderBy("createdAt", "desc")
                .get();
            records.forEach(docSnapshot => {
                //console.log(docSnapshot.data());
                Object.assign(report, docSnapshot.data());
                report.id = docSnapshot.id;
            });
        }

        return report;
    }

    public async aggregateNewCandidates(): Promise<Number> {
        let company = store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let start_date = moment(server_time.now).subtract(6, "day").startOf("day").valueOf();
        const records = await modelCollection.where("company_id", "==", company.id)
            .where('createdAt', ">=", start_date)
            .where('position.user_id', '==', store.state.user.id)
            .where('source.type', '==', "Portal")
            .get();
        let count = 0, data, temp_group = {};
        records.forEach((docSnapshot) => {
            data = docSnapshot.data();
            if (!(data.candidate.id in temp_group)) {
                count += 1;
                temp_group[data.candidate.id] = data;
            }
        })

        const records_1 = await modelCollection.where("company_id", "==", company.id)
            .where('createdAt', ">=", start_date)
            .where('position.user_id', '==', store.state.user.id)
            .where('source.type', '==', "form") // added from form
            .get();

        records_1.forEach((docSnapshot) => {
            data = docSnapshot.data();
            if (!(data.candidate.id in temp_group)) {
                count += 1;
                temp_group[data.candidate.id] = data;
            }
        })
        return count;
    }
    public async aggregateCandidatesMoved(): Promise<Number> {
        let company = store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let start_date = moment(server_time.now).subtract(6, "day").startOf("day").valueOf();
        let count = 0;
        let temp_group = {}, data;
        const records = await modelCollection.where("company_id", "==", company.id)
            .where('created_by', '==', store.state.user.id)
            .where('createdAt', ">=", start_date)
            //.where('stage.type', ">", "applied") // ko filter dc
            .get();

        records.forEach((docSnapshot) => {
            data = docSnapshot.data();
            if (data.stage && data.stage.type != 'applied' && !(data.candidate.id in temp_group) && data.movement_date && data.movement_date.in > 0) {
                count += 1;
                temp_group[data.candidate.id] = data;
            }
        })
        return count
    }
    public async aggregateCandidateHired(): Promise<Number> {
        let company = store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let start_date = moment(server_time.now).subtract(6, "day").startOf("day").valueOf();
        const records = await modelCollection.where("company_id", "==", company.id)
            .where('created_by', '==', store.state.user.id)
            .where('stage.type', "==", "hired")
            .where('createdAt', ">=", start_date)
            .get();

        let count = 0, data, temp_group = {};
        records.forEach((docSnapshot) => {
            data = docSnapshot.data();
            if (!(data.candidate.id in temp_group) && data.movement_date && data.movement_date.in > 0) {
                count += 1;
                temp_group[data.candidate.id] = data;
            }
        })
        return count;
    }

}
