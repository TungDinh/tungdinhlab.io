import Model from './base';


export default class DeletedPipeline extends Model {

    company_id: string = "";
    pipeline:{
        id: string,
        name:string
    };




    public async getAllDeletedPipelines(company_id: string): Promise<DeletedPipeline[]> {
        const results: DeletedPipeline[] = new Array();

        if (company_id) {
            const modelCollection = this.firestore.collection(this.COLLECTION);

            await modelCollection.where("company_id", "==", company_id)
                .get()
                .then(async (querySnapshot) => {
                    querySnapshot.forEach(docSnapshot => {
                        let record = new DeletedPipeline();
                        Object.assign(record, docSnapshot.data());
                        record.id = docSnapshot.id;
                        results.push(record);
                    });

                })
        }
        return await results;
    }



    async update(): Promise<DeletedPipeline> {
        // do not allow update
        return this
    }

    constructor() {
        super("DeletedPipeline");
    }
    async delete(): Promise<void> {
        // do not allow delete
        return
    }

}
