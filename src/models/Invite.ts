import Model from './base';
import firebase from "@/firebase";
import Company from "@/models/Company";
// import Router from '../router';
import {store} from "@/store/store";
import Team from "@/models/Team";
import Query = firebase.firestore.Query;
import User from "@/models/User";


export default class Invite extends Model {

    email: string = "";
    name: string = "";
    company_id = "";
    role: string = User.ROLE.MEMBER;
    invitedflag:boolean = false;
    day_create:any ="";
    invite_by: string ="";//userid nguoi tao
    invite_name: string = "";//ten nguoi tao


    constructor() {
        super("invites");
    }

    static get instance(): Invite {
        return new Invite();
    }

    public async getCompany(email: string): Promise<Invite> {
        let results:Invite = null ;
        const records = await this.collection()
            .where("email", "==", email)
            .limit(1)
            .get();

        records.forEach(docSnapshot => {
            let record = Invite.from(docSnapshot.data(), docSnapshot.id) as Invite;
            results  =  record;
        });

        return results;
    }

    public async getbyEmail(email: string): Promise<Invite> {
        let results:Invite = null ;
        const records = await this.collection()
            .where("email", "==", email)
            .limit(1)
            .get();

        records.forEach(docSnapshot => {
            let record = Invite.from(docSnapshot.data(), docSnapshot.id) as Invite;
            results  =  record;
        });

        return results;
    }

    public async getbyUserInvite(invite_by: string): Promise<Invite[]> {
        const  results: Invite[] = [];
        const records = await this.collection()
            .where("invite_by", "==", invite_by)
            .get();

        records.forEach(docSnapshot => {
            let record = Invite.from(docSnapshot.data(), docSnapshot.id) as Invite;
            results.push(record);
        });

        return results;
    }

    public getAllInvites(company_id: string): Query {
        const  results: Invite[] = [];
        return this.collection()
            .where("company_id", "==", company_id)
    }

    async update(): Promise<Invite> {
        return await super.update() as Invite;
    }

    static listenRealtime(company_id?: string): void {
        if (company_id) {
            this.instance.collection().where("company_id", '==', company_id)
                .onSnapshot((snapshots) => {
                    snapshots.docChanges().forEach((change) => {
                        const model = this.from(change.doc.data(), change.doc.id);
                        switch (change.type) {
                            case "added":
                                this._onAdded(model);
                                break;
                            case "modified":
                                this._onUpdated(model);
                                break;
                            case "removed":
                                this._onDeleted(model);
                            default:
                                break;
                        }
                    });
                });
        }
    }
}
