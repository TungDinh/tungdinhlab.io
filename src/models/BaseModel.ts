import {IModel} from "./interfaces";

export default class BaseModel implements IModel{
    static COLLECTION: string;
    COLLECTION: string;
    id: string = "";
    created_date: number = new Date().getTime();
    updated_date: number = new Date().getTime();
    created_by: string = "";
    updated_by: string = "";
    createdAt: number = new Date().getTime();
    updatedAt: number = new Date().getTime();
    constructor(COLLECTION: string) {
        this.COLLECTION=COLLECTION;
        const thisStatic = this.constructor as any;
        thisStatic.COLLECTION = COLLECTION;
    }
}
