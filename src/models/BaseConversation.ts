
import BaseModel from "./BaseModel";
import {CONVERSATION_COLLECTION, IConversation, IConversationObject} from "./interfaces";

export default class BaseConversation extends BaseModel implements IConversation {
    company_id: string = '';
    type: string = '';
    object: IConversationObject = {mail_state:'pending'};
    // object is custom follow by type;
    position_id: string = "";
    candidate_id: string = "";
    mail_id: string='';
    mail_opened_count:number=0;
    constructor(){
        super(CONVERSATION_COLLECTION)
    }

}


