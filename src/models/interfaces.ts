
export interface IModel {
    id: string;
    created_date: number;
    updated_date: number;
    created_by: string;
    updated_by: string;
    createdAt: number;
    updatedAt: number;
};


export interface IQuestionnaire {
    name: string;
    company_id: string;
    has_move_rule: Boolean;
}
export const CANDIDATE_COLLECTION = "candidates";
export const CANDIDATE_QUESTIONNAIRE = "candidateQuestionnaire";
export interface ICandidateModel extends IModel{
    name: string;
    company_id: string;
    // seen: boolean = false; don't use this field use unseen instead
    position_id: string;
    position: {
        "_id":string,
        "name" : string,

        "location" : {
            "name" : string
        },
        "user_id": string,
        "status": string,// draft, published, closed
    };
    hired: {
        "date": number // timestamp
    }
    disqualified: {
        "date": number // timestamp
    }

    assignee_id: string;
    photo: string;
    address: string;
    cover_letter: string;
    education: Array<any>;
    // [{
    //    school_name: '',
    //    field_of_study: '',
    //    summary: ''
    //  },]
    references: Array<object>;
    /*
    * {
    * name: string
    * email_address: string
    * phone_number: string
    * type: professional/personal,
    * created_at:'string'
    * }
    *
    *
    * */

    email_address: string ;
    headline: string;
    initial: string;
    origin: string; //applied | recruiter | referral | sourced
    overall_score:{
        "very_good": Array<any>, //User._id
        "good": Array<any>,//User._id
        "neutral": Array<any>,//User._id
        "poor": Array<any>,//User._id
        "very_poor": Array<any>,//User._id
        "scored_count": number,
        "score": number,
    };
    phone_number: string;
    alt_phone_number: string;
    profile_photo_url: string;
    questionnaire: IQuestionnaire[];
    recruited_by: {
        "_id": string,
        "email_address": string,
        "name": string
    };
    referred_by: {
        "_id": string,
        "email_address": string,
        "name": string
    };

    // nguoi tao candidate truc tiep tu he thong
    sourced_by:{
        _id:  string,
            "email_address"?:  string,
            "name"?:  string
    };
    resume: {
        "file_name": string,
        "url": string,
        "pdf_url": string
    };
    social_profiles: Array<{
                                "type": string,
                                "typeId": string,
                                "typeName": string,
                                "url": string
                            }>;

    // vi du: Portal,
    // co the co source hoặc không có source (trong trường hợp được tạo trực tiếp)
    source:  {
        "id": string,
            "name": string,
            "type": string
    };
    stage: {
        "id": string,
        "name": string,
        "type": string, /// applied, feedback, interview, ...
    };
    stage_id: string;

    stage_actions_enabled:boolean;

    summary: string;
    tags: any[];
    work_history: Array<
        {
            "company_name":  string,
            "title": string,
            "summary":  string,
            "is_current": boolean,
            "start_date": {
                "month":  string,
                "year":  string
            },
            "end_date": {
                "month":  string,
                "year":  string
            }
        }
    >;
    custom_attributes : Array<object>;
    //sameple custom_attributes
    // {
    //         name: string,
    //         id: string,
    //         secure: boolean,
    //         value: string
    //     }
    attachfiles: {
        name: string,
            owner: string,
            created_at: string,
            "url": string
    }[] ;


    disposition_date?: Date;
    disposition_reason: {
        "_id":string,
        "name": string,
    };
    starred: Boolean;
    unseen: Boolean;
    follow_by: String;
    seen_list:any[]; // danh sach user id da xem

    photoLink: () => string

}
// https://stackoverflow.com/a/35312363
// https://github.com/Microsoft/TypeScript/wiki/What's-new-in-TypeScript#string-literal-types
export type MailState = 'pending'|'sent'|'failed'|'open'|'other'; // String Literal Types

export interface IConversationObject {
    mail_state:MailState,
    sent_mail_data?: any,
    last_mail_event_timestamp?:number,
    // is_first_time_open_mail?:boolean,
    [other:string]:any

}

export const CONVERSATION_COLLECTION='Conversation';
export interface IConversation extends IModel{
    company_id: string;
    type: string;
    object: IConversationObject;// Mail state in here
    // object is custom follow by type;
    position_id: string;
    candidate_id: string;
    mail_id?:string
    mail_opened_count:number

}

export const PIPELINE_COLLECTION = 'Pipeline';
export interface IPipeline extends IModel {
    company_id: string;
    is_default: boolean;
    title: string;
    used_positions: any[];
}

export const PIPELINE_STAGE_COLLECTION = 'PipelineStage';
export interface IPipelineStage extends IModel {
    title: string;
    pipeline_id: string;
    stage_type: string; // applied, feedback, interview, ...
    index: number;
    icon: string;
    hide: boolean; // Có 1 stage default applied là không cho phép xóa.
    showInPositionPipelineStageModal: boolean; // Có 1 số item thuộc list default stage không hiển thị.
    actions: Array<object>;  // State actions
}

export const INBOX_COLLECTION = 'Inbox';
export interface IInbox extends IModel {
    email: string ;
    cc: Array<string>;
    bcc: Array<string> ;
    subject: string;
    position_id: string ;
    candidate_id: string ;
    candidate: object ;
    company_id :string;
    body: string;
    attachments: Array<object> ;
    acting_user: object ; // Last user tuong tat
    acting_user_ids: Array<string> ; // Lưu list danh sach nhan vien tuong tac voi ung vien nay
    is_showing: boolean;
}

export const POSITION_COLLECTION = 'Position';
export interface IPosition extends IModel {
    title: string;
    company_id: string;
    department: string;
    internal_id: string;
    location: object
    position_type: object;
    category: object;
    education: object;
    experience: object;
    job_description: object;
    state: string;
    tags: Array<object>;
    other_information: Array<object>;
    is_remote: boolean;
    candidate_type: string;//enabled, unlisted, disabled /
    application_form: {};
    questionnaire_id: string;
    position_pipeline_action: Array<object>;
    pipeline_id: string;
    automated_email_sender: string;
    scorecard_id: string;
    status: string; // published, draft, closed
    count_candidate :number;
    last_view_date: number;
    position_assign: object;

    // For report
    hired: object;
    disqualified: object;
    address: string;
}
