import Model from './base';
import Company from "@/models/Company";
import {store} from "@/store/store";
import User from "@/models/User";
import firebase from 'firebase';
import Query = firebase.firestore.Query;

export default class Activity extends Model {
    candidate_id: string = "";
    company_id: string = "";
    position_id: string = "";
    attachments: { name: string, url: string }[] = [];
    content: string = "";
    actor: Object = {} || null;
    action: string = ""; // add,update,cancel,assigned,comment,applied,meeting
    action_text: string ="";
    target_object: {}; // meeting,log
    activity_type: string = ""; // log, comment, notes, document, meeting
    title: string = ""; // meeting
    currentStage: string = ""


    constructor() {
        super("Activity");
    };

    static get instance(): Activity {
        return new Activity();
    }
    public getCandidateNotes(candidate_id:string): Query {
        const modelCollection = this.firestore.collection(this.COLLECTION);
        return modelCollection
            .where("candidate_id", "==", candidate_id)
            .where("activity_type", "==", "notes")
    }

    public getPublicNotes(candidate_id:string): Query {
        let publicNotes = [];
        const modelCollection = this.firestore.collection(this.COLLECTION);
        return modelCollection
            .where("candidate_id", "==", candidate_id)
            .where("activity_type", "==", "notes")
            .where("candidate_note", "==", "public")
    }
    public getActivity(candidate_id: string): Query {
        let activities = [];
        const modelCollection = this.firestore.collection(this.COLLECTION);
        return modelCollection.where("candidate_id", "==", candidate_id)
    }
    static listenRealtime(company_id?: string): void {
        if (company_id) {
            this.instance.collection().where("company_id", '==', company_id)
                .onSnapshot((snapshots) => {
                    snapshots.docChanges().forEach((change) => {
                        const model = this.from(change.doc.data(), change.doc.id);
                        switch (change.type) {
                            case "added":
                                this._onAdded(model);
                                break;
                            case "modified":
                                this._onUpdated(model);
                                break;
                            case "removed":
                                this._onDeleted(model);
                            default:
                                break;
                        }
                    });
                });
        } else {
            this.instance.collection()
                .onSnapshot((snapshots) => {
                    snapshots.docChanges().forEach((change) => {
                        const model = this.from(change.doc.data(), change.doc.id);
                        switch (change.type) {
                            case "added":
                                this._onAdded(model);
                                break;
                            case "modified":
                                this._onUpdated(model);
                                break;
                            case "removed":
                                this._onDeleted(model);
                            default:
                                break;
                        }
                    });
                });
        }
    }


}
