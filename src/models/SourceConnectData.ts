import Model from './base';
import {store} from "@/store/store";
import firebase from "firebase";
import {perf} from "@/firebase";
import {server_time} from "@/firebase";
import Query = firebase.firestore.Query;


export default class SourceConnectData extends Model {

    company_id: string = "";
    service: string = "";
    username: string ="";
    password: string = "";
    is_active: boolean = false;
    last_updated_consumed: number = server_time.now.valueOf();
    constructor() {
        super("SourceConnectData");
    }

    public getAllSourceConnectData(): Query {
        let company = store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        return modelCollection.where("company_id", "==", company.id)

    }
    async insert(): Promise<Model> {
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let result = {...this};
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        //const {TIMESTAMP} = firebase.database.ServerValue;
        result.created_date = server_time.now.valueOf(); //TIMESTAMP;
        result.updated_date = server_time.now.valueOf(); //TIMESTAMP;
        result.last_updated_consumed = server_time.now.valueOf();
        //const {TIMESTAMP} = firebase.database.ServerValue;
        if(!result.createdAt){
            result.createdAt = server_time.now.valueOf(); //TIMESTAMP;
        }

        result.updatedAt = server_time.now.valueOf(); //TIMESTAMP;

        const {store} = require('@/store/store');
        if (store.getters.currentUser) {
            result.updated_by =  store.getters.currentUser.id;
            result.created_by = store.getters.currentUser.id;
            this.updated_by = store.getters.currentUser.id;
            this.created_by = store.getters.currentUser.id;
        }
        const model = await modelCollection.add(JSON.parse(JSON.stringify(result)));
        this.id = model.id;

        return this;
    }
    async update(): Promise<Model> {
        const modelCollection = this.firestore.collection(this.COLLECTION);

        let result = {...this};
        let id = result['id'];
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        // const {TIMESTAMP} = firebase.database.ServerValue;
        result.updated_date = server_time.now.valueOf();
        result.updatedAt = server_time.now.valueOf(); //TIMESTAMP;
        result.last_updated_consumed = server_time.now.valueOf();
        const {store} = require('@/store/store');
        if (store.getters.currentUser) {
            result.updated_by = store.getters.currentUser.id;
        }


        // @ts-ignore
        const model = await modelCollection.doc(id).set(JSON.parse(JSON.stringify(result), {merge: true}));

        return this;
    }

}
