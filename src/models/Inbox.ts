import Model from './base';
import Candidate from "@/models/Candidate";
import User from "@/models/User";
import {store} from "@/store/store";
import moment from "@/models/Schedule";
import Query = firebase.database.Query;
import Vue from "vue";
import {firestore} from "firebase";

export default class Inbox extends Model {

    email: string = "";
    cc: Array<string> = [];
    bcc: Array<string> = [];
    subject: string = "";
    position_id: string = "";
    position: object = {
        "_id": "",
        "name": "",
        "location": {
            "name": ""
        },
        "user_id": '',
        "status": '',// draft, published, closed
    };
    candidate_id: string = "";
    candidate: object = {};
    company_id = "";
    body: string = "";
    attachments: Array<object> = [];
    time_to_send: string = ""; // Time to send in datetime format
    acting_user: object = {}; // Last user tuong tat
    acting_user_ids: Array<string> = []; // Lưu list danh sach nhan vien tuong tac voi ung vien nay
    is_showing: boolean = true;


    constructor() {
        super("Inbox");
    }

    static get instance(): Inbox {
        return new Inbox()
    }

    public async getInboxByCandidate(candidate_id: string): Promise<Inbox> {
        const records = await this.collection()
            .where("candidate_id", "==", candidate_id)
            .limit(1)
            .get();

        let inbox = null;
        records.forEach(docSnapshot => {
            inbox = Inbox.from(docSnapshot.data(), docSnapshot.id) as Inbox;
        });
        return inbox;
    }

    public getInboxByPosition(position_id: string): firestore.Query {
        let company = Vue.$store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let records: any = modelCollection.where("company_id", "==", company.id)
            .where('position_id', '==', position_id);
        return records;
    }

    public getInboxForDashboard(user?: User) : Query {
        let company = Vue.$store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let records;
        if (user) {
            records = modelCollection.where("company_id", "==", company.id)
                .where('is_showing', '==', true)
                .where('acting_user_ids', 'array-contains', user.id)
                .orderBy('updatedAt', 'desc')
        } else {
            records = modelCollection.where("company_id", "==", company.id)
                .where('is_showing', '==', true)
                .orderBy('updatedAt', 'desc')
        }
        return records
    }

    public getPositionInboxes(position_id, user?,) : Query {
        let inbox = [] as Inbox[];
        let company = Vue.$store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let records;
        if (user && position_id) {
            // my conversations
            records = modelCollection
                .where("company_id", "==", company.id)
                .where('position_id', '==', position_id)
                .where('is_showing', '==', true)
                .where('acting_user_ids', 'array-contains', user.id)
        } else {
            // all conversations
            records = modelCollection
                .where("company_id", "==", company.id)
                .where('position_id', '==', position_id)
                .where('is_showing', '==', true)
        }

        return records;
    }

    static listenRealtime(company_id?: string, user_id?: string): void {
        if (company_id) {

            if (user_id) {
                this.instance.collection().where("company_id", '==', company_id)
                    .where("is_showing", '==', true)
                    .where("acting_user_ids", 'array-contains', user_id)
                    .onSnapshot((snapshots) => {
                        snapshots.docChanges().forEach((change) => {
                            const model = this.from(change.doc.data(), change.doc.id);
                            switch (change.type) {
                                case "added":
                                    this._onAdded(model);
                                    break;
                                case "modified":
                                    this._onUpdated(model);
                                    break;
                                case "removed":
                                    this._onDeleted(model);
                                default:
                                    break;
                            }
                        });
                    });
            } else{
                this.instance.collection().where("company_id", '==', company_id)
                    .where("is_showing", '==', true)
                    .onSnapshot((snapshots) => {
                        snapshots.docChanges().forEach((change) => {
                            const model = this.from(change.doc.data(), change.doc.id);
                            switch (change.type) {
                                case "added":
                                    this._onAdded(model);
                                    break;
                                case "modified":
                                    this._onUpdated(model);
                                    break;
                                case "removed":
                                    this._onDeleted(model);
                                default:
                                    break;
                            }
                        });
                    });
            }

        }
    }
}
