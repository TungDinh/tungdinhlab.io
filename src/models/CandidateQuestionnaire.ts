import Model from './base';
import {firestore} from "firebase";
import {CANDIDATE_QUESTIONNAIRE, IModel} from "@/models/interfaces";
import Vue from "vue";


export default class CandidateQuestionnaire  extends Model implements IModel {
    constructor() {
        super(CANDIDATE_QUESTIONNAIRE);
    }
    name: string = "";
    company_id: string = "";
    candidate_id: string = "";
    position_id: string = "";
    questionnaire_id: string = "";
    status: string = "";
    message_template: object = {
        type: "message",
        body: "Hi [[candidate_first_name]],\\r\\n\\r\\nWe'd like to ask you a few more questions. When you have a couple minutes, please click the following link and complete the form.\\r\\n\\r\\n[[questionnaire_link]]\\r\\n\\r\\nThank you,\\r\\n[[company_user_first_name]]"
    };
    options: object = {move_to:{}};
    questions: Array<object> = [];// list question
    acting_user: object = {};// object model user
    sent_by: object = {
        "name": "",
    };// object model user

    static get instance(): CandidateQuestionnaire {
        return new CandidateQuestionnaire();
    }
    static  async getCandidateQuestionnaire(candidate_question_id:string):Promise<CandidateQuestionnaire> {
        return (new CandidateQuestionnaire()).getById(candidate_question_id) as Promise<CandidateQuestionnaire>;
    }
    public getCandidateQuestionnaire(candidate): firestore.Query {
        return this.firestore.collection(this.COLLECTION)
            .where("candidate_id", "==", candidate.id)

    }
}
