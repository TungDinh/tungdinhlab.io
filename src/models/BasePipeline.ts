import {IPipeline, PIPELINE_COLLECTION} from "./interfaces";
import BaseModel from "./BaseModel";
import {messages} from "@/messages";

class BasePipeline extends BaseModel implements IPipeline {
    title: string = "";
    company_id: string = "";
    is_default: boolean = false;
    used_positions: any[] = [];

    constructor() {
        super(PIPELINE_COLLECTION);
    }

    public async createPipelineDefault(company_id: string): Promise<BasePipeline> {
        let defaultPineLine = new BasePipeline();
        defaultPineLine.title = messages.SR_007_DEFAULT_PIPELINE_TITLE;
        defaultPineLine.company_id = company_id;
        defaultPineLine.is_default = true;
        return defaultPineLine;
    }
}

export default BasePipeline;
