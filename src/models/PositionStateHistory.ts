import Model from './base';
import * as firebase from "firebase";
import Query = firebase.firestore.Query;
import {IPositionCategory} from "@/common/positioncategories";
import {store} from "@/store/store";
import _ from "lodash";
import moment from "moment";
import {server_time} from "@/firebase";


export default class PositionStateHistory extends Model {
    company_id: string
    position_id: string
    state: string; // public, draft, archived, closed

    constructor() {
        super("PositionStateHistory"); // what is the super??
    }

    static get instance(): PositionStateHistory {
        return new PositionStateHistory(); // what for? --> PipelineReport.instance.<instance-method>
    }

    static async createHistory(position_id, state) {
        let pos = new PositionStateHistory()
        pos.company_id = store.state.company.id;
        pos.position_id = position_id;
        pos.state = state;
        return pos.insert()
    }

    public async aggregatePositionsActivated(): Promise<Number> {
        let company = store.getters.currentCompany;
        const user = store.getters.currentUser;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        const records = await modelCollection.where("company_id", "==", company.id)
            .where('created_by',"==", user.id)
            .where('state', "==", "published")
            .get();

        let count = 0, data, positions = [];
        records.forEach((docSnapshot) => {
            data = docSnapshot.data();
            positions.push(data);
        })

        let group_positions = _.groupBy(positions, 'position_id')
        let objs = [];
        let start_date = moment(server_time.now).subtract(6, "day").startOf("day").valueOf();
        Object.keys(group_positions).forEach((position_id) => {
            objs = _.orderBy(group_positions[position_id], 'createdAt', 'asc')
            if (objs[0].createdAt >= start_date) {
                count += 1
            }
        })
        return count;
    }
}
