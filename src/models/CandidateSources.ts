import Model from './base';
import {store} from "@/store/store";
import firebase from "firebase";
import Query = firebase.firestore.Query;

export default class CandidateSources extends Model {

    sources: object = {
        id:"",
        name:"",
        type:""
    }
    company_id: string = "";
    // is_encrypt: boolean = false;

    constructor() {
        super("CandidateSources");
    }

    public getAllCandidateSources(): Query {
        let company = store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        return modelCollection.where("company_id", "==", company.id)

    }


}
