import Model from './base';
import Company from "@/models/Company";
import {store} from "@/store/store";
import Candidate from "@/models/Candidate";
import User from "@/models/User";
import moment from "moment";

import {Settings} from 'luxon';
import firebase from "firebase";
import Query = firebase.firestore.Query;
import {server_time} from "@/firebase";
import Vue from "vue";



Settings.defaultLocale = 'vi';
export default class Task extends Model {
    title: string = "";
    description: string = "";
    is_done: boolean = false;
    private _deadline: number = server_time.now.valueOf();
    files: { name: string, url: string }[] = [];
    assignee_id = "";
    company_id = "";
    candidate_id = "";
    position_id = "";
    assignee: object = {};
    add_log:Array<object> = [];
    type = "";

    constructor() {
        super("tasks");
        // this.title = "Tên công việc";
        this._deadline = server_time.now.valueOf();
        try {
            this.company_id = store.getters.currentCompany.id;
        } catch (e) {

        }

        try {
            this.assignee_id = store.getters.currentUser.id;
        } catch (e) {

        }
    }

    static get instance(): Task {
        return new Task();
    }

    get deadline(): string {
        return new Date(this._deadline).toISOString();
    }

    set deadline(val: string) {
        try {
            this._deadline = new Date(val).getTime();
        } catch (e) {
            this._deadline = server_time.now.getTime();
        }
    }

    async insert(): Promise<Task> {

        //delete this['assignee']; // do not want to save this back to firestore
        return super.insert() as Promise<Task>;

    }


    async update(): Promise<Task> {

        //delete this['assignee']; // do not want to save this back to firestore
        return super.update() as Promise<Task>;

    }


    // public async getAssignee(): Promise<User> {
    //     if (this.assignee_id != "") {
    //         if (!this.assignee) {
    //             this.assignee = await User.instance.getById(this.assignee_id) as User;
    //         }
    //         return this.assignee;
    //     } else {
    //         return await new User();
    //     }
    //
    //
    // }


    public async getCompany(): Promise<Company> {

        return (new Company()).getById(this.company_id) as Promise<Company>;
    }


    public async getCandidate(): Promise<Candidate> {

        return (new Candidate()).getById(this.candidate_id) as Promise<Candidate>;
    }

    public async getAllTasks(): Promise<Task[]>  {
        const results: Task[] = new Array();
        let company = Vue.$store.getters.currentCompany;
        if (company) {
            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection
                .where("company_id", "==", company.id)
                .orderBy("updatedAt", "desc")
                .get();
            const docs = records.docs;
            for (let index in docs) {
                let docSnapshot = docs[index];
                const record = Task.from(docSnapshot.data(), docSnapshot.id) as Task;
                //record.assignee = await record.getAssignee();
                // console.log(record.assignee);
                results.push(record);
            }
        }
        return results
    }

    public getFilteredTasks(filter_field: string, value: string, position_id: string): Query {
        let company = Vue.$store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let records;
        if (filter_field == 'assignee_id') {
            if(position_id != ''){
                records = modelCollection
                    .where("company_id", "==", company.id)
                    .where('assignee_id', "==", value)
                    .where("position_id", "==", position_id)
            }else {
                records = modelCollection
                    .where("company_id", "==", company.id)
                    .where('assignee_id', "==", value)
            }
        } else {
            if(position_id != ''){
                records = modelCollection
                    .where("company_id", "==", company.id)
                    .where('created_by', "==", value)
                    .where("position_id", "==", position_id)
            }else {
                records = modelCollection
                    .where("company_id", "==", company.id)
                    .where('created_by', "==", value)
            }
        }
        return records
    }

    public getTasksCandidate(filter_field: string, value: string, candidate_id: string): Query {
        let company = Vue.$store.getters.currentCompany;
        let modelCollection = this.firestore.collection(this.COLLECTION);
        let records;
        if (filter_field == 'assignee_id') {
            records = modelCollection
                .where("company_id", "==", company.id)
                .where("candidate_id", "==", candidate_id)
                .where('assignee_id', "==", value)
                .where('is_done',"==", false)
        } else {
            records = modelCollection
                .where("company_id", "==", company.id)
                .where("candidate_id", "==", candidate_id)
                .where('created_by', "==", value)
                .where('is_done',"==", false)
        }
        return records;
    }

    public async getTaskByAssigneeID(assignee_id: string): Promise<Task[]> {
        let tasks: Task[] = [];
        let company = Vue.$store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        const records = await modelCollection
            .where("company_id", "==", company.id)
            .where("assignee_id", "==", assignee_id)
            .where('is_done', '==', false)
            .orderBy("_deadline", "asc")
            .get();

        const docs = records.docs;
        docs.forEach((docSnapshot) => {
            const record = Task.from(docSnapshot.data(), docSnapshot.id) as Task;
            tasks.push(record);
        });

        return tasks;
    }

    public async getTaskAssigneeID(assignee_id: string): Promise<Task[]> {
        let tasks: Task[] = [];
        let company = Vue.$store.getters.currentCompany;
        if(company){
            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection
                .where("company_id", "==", company.id)
                .where("assignee_id", "==", assignee_id)
                .get();
            const docs = records.docs;
            docs.forEach((docSnapshot) => {
                const record = Task.from(docSnapshot.data(), docSnapshot.id) as Task;
                tasks.push(record);
            });
            return tasks;
        }
    }

    static listenRealtime(company_id?: string): void {
        if (company_id) {
            this.instance.collection().where("company_id", '==', company_id)
                .onSnapshot((snapshots) => {
                    snapshots.docChanges().forEach((change) => {
                        const model = this.from(change.doc.data(), change.doc.id);
                        switch (change.type) {
                            case "added":
                                this._onAdded(model);
                                break;
                            case "modified":
                                this._onUpdated(model);
                                break;
                            case "removed":
                                this._onDeleted(model);
                            default:
                                break;
                        }
                    });
                });
        }
    }

}
