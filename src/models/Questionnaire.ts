import Model from './base';
import Company from "@/models/Company";
import {store} from "@/store/store";
import User from "@/models/User";
import {IQuestionnaire} from "@/models/interfaces";
import Vue from "vue"

export default class Questionnaire extends Model implements IQuestionnaire{
    name: string = "";
    company_id: string = "";
    has_move_rule: Boolean = false;
    message_template: String = "";

    constructor() {
        super("questionnaire");
    }

    public async getCompany(): Promise<Company> {
        return (new Company()).getById(this.company_id) as Promise<Company>;
    }

    public async getAllQuestionnaires(): Promise<Questionnaire[]> {
        const results: Questionnaire[] = new Array();
        let companyID = Vue.$store.getters.currentCompany.id
        if (companyID) {
            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection
                .where("company_id", "==", companyID)
                .get();
            records.forEach(docSnapshot => {
                let record = new Questionnaire();
                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                results.push(record);
            });
        }
        return results
    }
}
