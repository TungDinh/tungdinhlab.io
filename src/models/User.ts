import Model from './base';
import firebase, {db, server_time} from "@/firebase";
import Company from "@/models/Company";
// import Router from '../router';
import {store} from "@/store/store";
import Team from "@/models/Team";
import Query = firebase.firestore.Query;
import Task from "@/models/Task";
import Candidate from "@/models/Candidate";



export default class User extends Model {
    email: string ="";
    name: string = "";
    company_id = "";
    photoURL: string = "";
    role:string ="1";//1 la admin ,2 la member
    photoName: string = "";
    signatureFlag: boolean = false;
    signatureHtml: string = "";
    meetingRemind:boolean = true;
    candidateAssign: boolean = true;
    taskAssign:  boolean = true;
    taskDuel:  boolean = true;
    status: string="1";
    candidate_star=[];
    position_liked=[];
    candidate_followed = [];

    static readonly ROLE = {
        ADMIN: "1",
        MEMBER: "2",
    };

    constructor() {
        super("users");
    }

    static get instance(): User {
        return new User();
    }

    public async getCompany(): Promise<Company> {
        return Company.instance.getById(this.company_id) as Promise<Company>;
    }

    // public async getTeams(): Promise<Team[]> {
    //     const result = [] as Team[];
    //     for (const teamRef of this.teams) {
    //         const response = await teamRef.get();
    //         const team = Team.from(response.data()) as Team;
    //         team.id = teamRef.id;
    //         result.push(team);
    //     }
    //     return result;
    // }

    public async getCurrentUser(): Promise<User> {
        return store.getters.currentUser;
    }


    public getAllUsers(company_id: string): Query {
        return this.collection()
            .where("company_id", "==", company_id)
            .where("status", "==", "1")
    }

    public async getUserbyEmail(email: string): Promise<User> {
        let result: User = null;

        const records = await this.collection()
            .where("email", "==", email)
            .limit(1)
            .get();

        records.forEach(docSnapshot => {
            let record = User.from(docSnapshot.data(), docSnapshot.id) as User;
            result = record;
        });
        return result
    }

    public async getAllUsersNotMe(company_id: string, user_id: string): Promise<User[]> {
        const results: User[] = [];

        const records = await this.collection()
            .where("company_id", "==", company_id)
            .get();

        records.forEach(docSnapshot => {
            let record = User.from(docSnapshot.data(), docSnapshot.id) as User;
            if  (record.id != user_id)
                results.push(record);
        });
        return results;
    }


    // override

    async update(): Promise<User> {

        const modelCollection = this.firestore.collection(this.COLLECTION);

        let result = {...this};
        let id = result['id'];
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        // const {TIMESTAMP} = firebase.database.ServerValue;
        result.updated_date = server_time.now.valueOf();
        result.updated_by =  store.getters.currentUser.id;

        // @ts-ignore
        const model = await modelCollection.doc(id).set(JSON.parse(JSON.stringify(result), {merge: true}));

        return this;
    }

    static listenRealtime(company_id?: string): void {
        if (company_id) {
            this.instance.collection().where("company_id", '==', company_id)
                .onSnapshot((snapshots) => {
                    snapshots.docChanges().forEach((change) => {
                        const model = this.from(change.doc.data(), change.doc.id);
                        switch (change.type) {
                            case "added":
                                this._onAdded(model);
                                break;
                            case "modified":
                                this._onUpdated(model);
                                break;
                            case "removed":
                                this._onDeleted(model);
                            default:
                                break;
                        }
                    });
                });
        }
    }

    static async updateCurrentUserCompany(user){
        let user_data,batch = db.batch(), sfRef, tasks = [];
        user_data = user.cloneNewObject();
        //Update current user company for task
        let records = await new Task().getTaskAssigneeID(user.id);
        records.forEach((docSnapshot)=>{
            let record = new Task();
            Object.assign(record, docSnapshot);
            record.id = docSnapshot.id;
            tasks.push(record)
        });
        tasks.forEach((task)=>{
            sfRef = db.collection(new Task().COLLECTION).doc(task.id);
            if(task.assignee.id == user.id){
                batch.update(sfRef, {
                    assignee: user_data
                })
            }
        });
        return batch.commit();

    }
}
