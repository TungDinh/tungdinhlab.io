import Model from './base';
import * as firebase from "firebase";
import {store} from "@/store/store";
import Query = firebase.firestore.Query;
import Company from "@/models/Company";

export default class PositionView extends Model {
    position = {
        id: '',
        user_id: '',
        status: 'draft', // publish, draft, ....
    }

    source =  {
        "id": "",
        "name": "",
        "type": ""
    }

    company_id:string = ''


    constructor() {
        super("PositionView");
    }
    public async getCompany(): Promise<Company> {
        return (new Company()).getById(this.company_id) as Promise<Company>;
    }


    public async getPositionViews(conditions:{fieldPath: string | firebase.firestore.FieldPath, opStr: firebase.firestore.WhereFilterOp, value: any}[]=null, limit:number=20):Promise<PositionView[]>{
        const results: PositionView[] = new Array();

        let company = store.getters.currentCompany;

        if (company) {

            let cursor:Query = this.firestore.collection(this.COLLECTION);
            cursor = cursor.where("company_id", "==", company.id)

            if (conditions){
                conditions.forEach((condition)=>{
                    cursor=cursor.where(condition.fieldPath, condition.opStr, condition.value)
                })
            }
            if(limit){
                cursor = cursor.limit(limit)
            }

            const records = await cursor.get()


            records.forEach(docSnapshot => {

                let record = new PositionView();

                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                results.push(record);

            });
        }
        // console.info(`Candidates:`);
        // console.info(results);


        return results
    }

}
