import Model from './base';
import {store} from "@/store/store";
import ROLE from "@/permission/type";
import firebase, {firestore} from 'firebase';
import {messages} from "@/messages";
import {db, server_time} from "@/firebase";
import {find as _find, findIndex as _findIndex} from "lodash";
import PipelineStage from "@/models/PipelineStage";
import Candidate from "@/models/Candidate";
import PositionPipelineAction from "@/models/PositionPipelineAction";
import Pipeline from "@/models/Pipeline";
import Inbox from "@/models/Inbox";
import {IPosition} from "@/models/interfaces";

export const PositionTypes = [
    {
        title: messages.SR_021_KANBAN_FULLTIME,
        value: 'fulltime'
    },
    {
        title: messages.SR_021_KANBAN_PARTTIME,
        value: 'parttime'
    },
    {
        title: messages.SR_021_KANBAN_CONTRACT,
        value: 'contract',
    },
    {
        title: messages.SR_021_KANBAN_TEMPORARY,
        value: 'temporary'
    },
    {
        title: messages.SR_021_KANBAN_OTHER,
        value: 'other'
    }
];
export const Education = [
    {title: messages.SR_021_KANBAN_UNSPECIFILED, value: "unspecifiled"},
    {title: "Trung học cơ sở", value: "Trung học cơ sở"},
    {title: "Trung học phổ thông", value: "Trung học phổ thông"},
    {title: "Trung cấp", value: "Trung cấp"},
    {title: "Cao đẳng", value: "Cao đẳng"},
    {title: "Đại học ", value: "Đại học "},
    {title: "Thạc sĩ", value: "Thạc sĩ"},
    {title: "Tiến sĩ", value: "Tiến sĩ"},
    {title: "Chứng chỉ liên quan", value: "Chứng chỉ liên quan"},
];
export const DefaultApplicationForm = {
    coverletter: 1,
    email: 0,
    education: 1,
    experience: 1,
    name: 0,
    phone: 1,
    resume: 1,
    workhistory: 1
}
export const Experience = [
    {
        title: 'Not applicable',
        value: 'notapplicable'
    },
    {
        title: 'Internship',
        value: 'internship'
    },
    {
        title: 'Entry Level',
        value: 'entrylevel',
    },
    {
        title: 'Associate',
        value: 'associate'
    },
    {
        title: 'Mid Level',
        value: 'midlevel'
    },
    {
        title: 'Senior',
        value: 'senior'
    },
    {
        title: 'Executive',
        value: 'executive'
    },
];

export default class Position extends Model implements IPosition {

    title: string = "";
    company_id: string = "";
    department: string = "";
    internal_id: string = "";
    location: object = {
        title: '',
        value: ''
    };
    position_type: object = {};
    category: object = {};
    education: object = {};
    experience: object = {};
    job_description: object = {};
    state: string = "";
    tags: Array<object> = [];
    other_information: Array<object> = [];
    is_remote: boolean = false;
    candidate_type: string = '';//enabled, unlisted, disabled /
    application_form: {};
    questionnaire_id: string = '';
    position_pipeline_action: Array<object> = [];
    pipeline_id: string = '';
    automated_email_sender: string = "";
    scorecard_id: string = '';
    status: string = "draft"; // published, draft, closed
    count_candidate :number = 0;
    last_view_date: number;
    position_assign = {
        id: "",
        email: "",
        name:  "",
    };

    // For report
    hired = {
        "date": 0 //TimeStamp
    };
    disqualified = {
        "date": 0 //TimeStamp
    };
    address: string = '';
    constructor() {
        super("Position");
    }
    static get instance(): Position {
        return new Position();
    }

    static getFormatedLocationPositions(positions:Position[]):{}[] {

        let formatedPostionLocations = positions.map((position) => {
            let formated_location = []
            let formated_postion = position.cloneNewObject()

            formated_location = [position.location['title']]
            if (position.state) {
                formated_location.unshift(position.state)
            }
            formated_postion['formated_location'] = formated_location.join(', ').trim()

            return formated_postion
        })

        return formatedPostionLocations

    }

    public getAllPositionRealtime(position_id: string): firestore.Query {
        let company = store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        return modelCollection
            .where("company_id", "==", company.id)
            .where("position_id", "==", position_id)
    }
    async insert(): Promise<Model> {
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let result = {...this};
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        //const {TIMESTAMP} = firebase.database.ServerValue;
        result.created_date = server_time.now.valueOf(); //TIMESTAMP;
        result.updated_date = server_time.now.valueOf(); //TIMESTAMP;

        //const {TIMESTAMP} = firebase.database.ServerValue;
        if(!result.createdAt){
            result.createdAt = server_time.now.valueOf(); //TIMESTAMP;
        }

        result.updatedAt = server_time.now.valueOf(); //TIMESTAMP;

        const {store} = require('@/store/store');
        if (store.getters.currentUser) {
            result.updated_by =  store.getters.currentUser.id;
            result.created_by = store.getters.currentUser.id;
            this.updated_by = store.getters.currentUser.id;
            this.created_by = store.getters.currentUser.id;
            this.position_assign = {
                id: store.getters.currentUser.id,
                email: store.getters.currentUser.email,
                name: store.getters.currentUser.name
            }
        }
        const model = await modelCollection.add(JSON.parse(JSON.stringify(result)));
        this.id = model.id;

        return this;
    }



    public async getAllPositions(company_object: object = null): Promise<Position[]> {
        const results: Position[] = new Array();
        let company = store.getters.currentCompany;
        if (company_object != null) company = company_object;
        if (company) {
            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection.where("company_id", "==", company.id)
                .orderBy("createdAt", "desc")
                .get();

            records.forEach(docSnapshot => {
                let record = new Position();
                //console.log(docSnapshot.data());
                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                if (record.status != 'archived')
                    results.push(record);
            });
        }

        return results
    }

    public getListPositions(company_object: object = null): firestore.Query {
        let company = store.getters.currentCompany;

        if (company_object != null) company = company_object;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        return modelCollection.where("company_id", "==", company.id)
                .orderBy("createdAt", "desc")
    }

    public async getPublishPositions(company_id): Promise<Position[]> {
        const results: Position[] = new Array();
        if (company_id) {
            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection.where("company_id", "==", company_id)
                .where("status", "==", "published")
                .where("candidate_type","==","enabled")
                .get();
            records.forEach(docSnapshot => {
                let record = new Position();
                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                results.push(record);
            });
        }
        return results
    }

    public getFilteredPositions(company_id: string, createdBy?: string): firestore.Query {
        const results: Position[] = new Array();
        let records = [] as any;
        let that = this;
        // Ko filter dc != 'archived' nen can handle khi goi ham nay
        if (createdBy) {
            return this.firestore.collection(this.COLLECTION).where("company_id", "==", company_id)
                .where('created_by', '==', createdBy)
                .orderBy('createdAt', 'desc')
        } else {
            const modelCollection = this.firestore.collection(this.COLLECTION);
            return modelCollection.where("company_id", "==", company_id)
                .orderBy('createdAt', 'desc')
        }
    }

    public async updateLastViewDate(): Promise<Position> {
        this.last_view_date = server_time.now.valueOf();
        if (this.id) {
            return this.update() as Promise<Position>;
        }
    }

    public async aggregateCandidatesCount(posid): Promise<Number> {
        const modelCollection = this.firestore.collection('candidates');
        const records = await modelCollection.where("position._id", "==", posid)
            .get();
        return records.docs.length;
    }

    public async changePipeline(position_id, old_pipeline_id, new_pipeline_id, new_stages_with_actions:Array<object>=[]) {
        const {store} = require('@/store/store');
        let position = await new Position().getById(position_id) as Position;
        position.pipeline_id = new_pipeline_id;
        position.update();

        let new_pipeline = _find(store.getters.listPipeline, {id: new_pipeline_id}) as Pipeline;
        if (old_pipeline_id) {
            let old_pipeline = _find(store.getters.listPipeline, {id: old_pipeline_id}) as Pipeline;
            // Update the used positions for the old pipeline
            if (old_pipeline) {
                let index = _findIndex(old_pipeline.used_positions, {id: position.id});
                if (index != -1) {
                    old_pipeline = Object.assign(new Pipeline(), old_pipeline);
                    old_pipeline.used_positions.splice(index, 1);
                    old_pipeline.update();
                }
            }
        }
        // Update the used positions for the new pipeline
        let index = _findIndex(new_pipeline.used_positions, {id: position.id});
        if (index == -1) {
            new_pipeline = Object.assign(new Pipeline(), new_pipeline)
            new_pipeline.used_positions.push({
                'title': position.title,
                'id': position.id
            });
            new_pipeline.update();
        }

        // Moved candidate to new stage with the corresponding stage type
        let new_stages: Array<object> = store.getters.pipelineStages[new_pipeline_id] || [];
        let candidates = [], batch = db.batch(), sfRef, position_data;
        if (new_stages_with_actions.length > 0) {
            new_stages = new_stages_with_actions;
        }

        if (old_pipeline_id) {
            let records = await new Candidate().getCandidatesByPosition(position_id).get()
            records.forEach(docSnapshot => {
                let record = new Candidate();
                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                candidates.push(record);
            });
            let missing_stage = await new PipelineStage().getMissingStage(new_pipeline_id);
            position_data = {
                _id: position.id,
                name: position.title,
                location: {
                    // @ts-ignore
                    name: position.location.title,
                },
                user_id: position.created_by,
                status: position.status
            }
            candidates.forEach((candidate) => {
                let new_stage: any = new_stages.find((stage: any) => {
                    if (candidate.stage.type === "missing")
                        return stage.stage_type == candidate.stage.previous_type;
                    return stage.stage_type == candidate.stage.type;
                })
                if (new_stage) {
                    // Move to new stage
                    sfRef = db.collection(new Candidate().COLLECTION).doc(candidate.id);
                    batch.update(sfRef, {
                        position: position_data,
                        stage_id: new_stage.id,
                        stage: {
                            id: new_stage.id,
                            name: new_stage.title,
                            type: new_stage.stage_type,
                            previous_type: ''
                        }
                    })
                } else {
                    // Move to missing stage
                    sfRef = db.collection(new Candidate().COLLECTION).doc(candidate.id);
                    batch.update(sfRef, {
                        position: position_data,
                        stage_id: missing_stage.id,
                        stage: {
                            id: missing_stage.id,
                            name: missing_stage.title,
                            type: missing_stage.stage_type,
                            previous_type: candidate.stage.type
                        }
                    })
                }
            })
        }
        // Remove old actions
        let actions = await new PositionPipelineAction().getItemByPipelineID(old_pipeline_id)
        actions.forEach((action) => {
            sfRef = db.collection(new PositionPipelineAction().COLLECTION).doc(action.id);
            batch.delete(sfRef);
        });
        // Create new pipeline actions
        new_stages.forEach((stage: any) => {
            let pipelineAction = new PositionPipelineAction();
            pipelineAction.pipeline_id = position.pipeline_id;
            pipelineAction.position_id = position.id;
            pipelineAction.stage_id = stage.id;
            pipelineAction.actions = stage.actions;
            pipelineAction.insert().then((data) => {
                console.log('created actions !!!', stage.title);
            })
        })
        return batch.commit();
    }

    public async moveOrCopyCandidateToAnotherPosition(oldPositionID, newPositionObject, candidateObject, stageData, isEnableTriggerctionInStage): Promise<Object>{
        candidateObject.position_id = newPositionObject.id;
        candidateObject.position = {
            _id: newPositionObject.id,
            user_id: store.getters.currentUser.id,
            status: newPositionObject.status,
            name: newPositionObject.title,
            location: newPositionObject.location
        };
        candidateObject.enable_pipelinestage_actions = isEnableTriggerctionInStage;
        candidateObject.origin = 'sourced';
        candidateObject.overall_score = {
            "very_good": [], //User._id
            "good": [],//User._id
            "neutral": [],//User._id
            "poor": [],//User._id
            "very_poor": [],//User._id
            "scored_count": 0,
            "score": 0,
        };
        candidateObject.stage_id = stageData.id;
        candidateObject.stage = {
            id: stageData.id,
            name: stageData.title,
            type: stageData.stage_type
        };

        await candidateObject.upsert().then( async ()=>{
            // Update number of candidate in new Position
            let totalCandidateNewPosition = await new Position().aggregateCandidatesCount(newPositionObject.id);

            //@ts-ignore
            newPositionObject.count_candidate = totalCandidateNewPosition;
            await newPositionObject.update();

            // Update number of candidate in old Position
            let totalCandidateOldPosition  = await new Position().aggregateCandidatesCount(oldPositionID);
            let oldPosition = await this.getById(oldPositionID);
            //@ts-ignore
            oldPosition.count_candidate = totalCandidateOldPosition;
            await oldPosition.update();
        });
        return candidateObject;
    }

    static async updateStatusRelation(position) {
        let candidates = [], batch = db.batch(), sfRef, position_data, inboxes = [];

        position_data = {
            _id: position.id,
            name: position.title,
            location: {
                // @ts-ignore
                name: position.location.title,
            },
            user_id: position.created_by,
            status: position.status
        }

        // Update status position for Candidate
        let records = await new Candidate().getCandidatesByPosition(position.id).get();
        records.forEach(docSnapshot => {
            let record = new Candidate();
            Object.assign(record, docSnapshot.data());
            record.id = docSnapshot.id;
            candidates.push(record);
        });
        candidates.forEach((candidate) => {
            sfRef = db.collection(new Candidate().COLLECTION).doc(candidate.id);
            batch.update(sfRef, {
                position: position_data
            })
        });


        // Update status position for Inbox
        records = await new Inbox().getInboxByPosition(position.id).get();
        records.forEach(docSnapshot => {
            let record = new Inbox();
            Object.assign(record, docSnapshot.data());
            record.id = docSnapshot.id;
            inboxes.push(record);
        });
        inboxes.forEach((inbox) => {
            sfRef = db.collection(new Inbox().COLLECTION).doc(inbox.id);
            batch.update(sfRef, {
                position: position_data
            })
        });
        return batch.commit();
    }
}
