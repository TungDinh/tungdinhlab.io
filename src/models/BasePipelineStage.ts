import {IPipelineStage, PIPELINE_COLLECTION, PIPELINE_STAGE_COLLECTION} from "./interfaces";
import BaseModel from "./BaseModel";

class BasePipelineStage extends BaseModel implements IPipelineStage {
    title: string = "";
    pipeline_id: string;
    stage_type: string = ""; // applied, feedback, interview, ...
    index: number = 0;
    icon: string = "";
    hide: boolean = false; // Có 1 stage default applied là không cho phép xóa.
    showInPositionPipelineStageModal: boolean = true; // Có 1 số item thuộc list default stage không hiển thị.
    actions: Array<object> = [];  // State actions


    constructor() {
        super(PIPELINE_STAGE_COLLECTION);
    }
}

export default BasePipelineStage;
