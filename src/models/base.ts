import {db, server_time} from '@/firebase/index'
import * as firebase from "firebase";
import DocumentSnapshot = firebase.firestore.DocumentSnapshot;
import Type from "@/components/portal/Type.vue";
import {store} from "@/store/store";

// import {WhereFilterOp} from "firebase";
import Query = firebase.firestore.Query;
import DocumentReference = firebase.firestore.DocumentReference;
import {IModel} from "@/models/interfaces";
import BaseModel from "@/models/BaseModel";

/*

Đây là base Model, chúng ta sẽ viết những methods thường xuyên sử dụng cho các Models, ví dụ:

- insert
- update
- delete
- getById
- query...

 */

// static listenRealtimeDynamic(conditions:{fieldPath: string | firebase.firestore.FieldPath, opStr: firebase.firestore.WhereFilterOp, value: any}[]=null,
//                                  callbacks:{onAdded?:Function, onUpdated?:Function, onDeleted?:Function} =  {onAdded:()=>{}, onUpdated:()=>{}, onDeleted:()=>{} } ): void {
interface IListenRealtimeCallbacks {

    // callbacks:{onAdded?:Function, onUpdated?:Function, onDeleted?:Function}
    onAdded?:Function
    onUpdated?:Function
    onDeleted?:Function
}

export class ListenRealtimeCallbacks implements IListenRealtimeCallbacks{
    onAdded:Function=()=>{}

    onUpdated:Function=()=>{}

    onDeleted:Function=()=>{}

    // https://stackoverflow.com/a/37682352
    // constructor(onAdded?:Function, onUpdated?:Function, onDeleted?:Function) {
    constructor(init?:Partial<ListenRealtimeCallbacks>) {
        // this.onAdded = onAdded
        // this.onUpdate = onUpdated
        // this.onDeleted = onDeleted
        Object.assign(this, init);
    }
};


export default class Model extends BaseModel{
    static COLLECTION: string;
    COLLECTION: string;
    firestore: firebase.firestore.Firestore;
    id: string = "";
    created_date: number = server_time.now.valueOf();
    updated_date: number = server_time.now.valueOf();
    created_by: string = "";
    updated_by: string = "";
    createdAt: number = server_time.now.valueOf();
    updatedAt: number = server_time.now.valueOf();

    // todo: remove the three followings
    static _onAdded: Function = ()=>{} ;
    static _onUpdated: Function = ()=>{} ;
    static _onDeleted: Function = ()=>{} ;

    // create new instance, not single instance, instead of (new Model())
    static get instance(): Model {
        throw new TypeError('override this');
    }

    static from(data: any = {}, id?: string) {
        const newObj = this.instance;
        const obj = typeof data === 'object' ? data : {};
        if (!id && !data.id) {
            throw new Error("id not found");
        }

        Object.assign(newObj, {id}, obj, {id});

        return newObj;
    }

    constructor(COLLECTION: string) {
        super(COLLECTION);
        this.firestore = db;
        this.COLLECTION = COLLECTION;
        this.constructor['COLLECTION'] = COLLECTION;
    }


    deserialize(data: Object) {
        Object.assign(this, data);
    }

    async insert(): Promise<Model> {
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let result = {...this};
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        //const {TIMESTAMP} = firebase.database.ServerValue;
        result.created_date = server_time.now.valueOf(); //TIMESTAMP;
        result.updated_date = server_time.now.valueOf(); //TIMESTAMP;

        //const {TIMESTAMP} = firebase.database.ServerValue;
        if(!result.createdAt){
            result.createdAt = server_time.now.valueOf(); //TIMESTAMP;
        }

        result.updatedAt = server_time.now.valueOf(); //TIMESTAMP;

        const {store} = require('@/store/store');
        if (store.getters.currentUser) {
            result.updated_by =  store.getters.currentUser.id;
            result.created_by = store.getters.currentUser.id;
            this.updated_by = store.getters.currentUser.id;
            this.created_by = store.getters.currentUser.id;
        }
        const model = await modelCollection.add(JSON.parse(JSON.stringify(result)));
        this.id = model.id;

        return this;
    }

    async upsert(): Promise<Model> {
        if (this.id) {
            return await this.update();
        } else {

            return await this.insert();
        }
    }

    async update(): Promise<Model> {
        const modelCollection = this.firestore.collection(this.COLLECTION);

        let result = {...this};
        let id = result['id'];
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        // const {TIMESTAMP} = firebase.database.ServerValue;
        result.updated_date = server_time.now.valueOf();
        result.updatedAt = server_time.now.valueOf(); //TIMESTAMP;

        const {store} = require('@/store/store');
        if (store.getters.currentUser) {
            result.updated_by = store.getters.currentUser.id;
        }


        // @ts-ignore
        const model = await modelCollection.doc(id).set(JSON.parse(JSON.stringify(result), {merge: true}));

        return this;
    }

    async delete(): Promise<void> {

        const modelCollection = this.firestore.collection(this.COLLECTION);
        const record = await modelCollection.doc(this.id);

        return record.delete();
    }

    public collection(): firebase.firestore.CollectionReference {
        return db.collection(this.COLLECTION);
    }


    public async getById(id: string): Promise<Model> {
        const modelCollection = this.firestore.collection(this.COLLECTION);
        const record = await modelCollection.doc(id).get();
        if (!record.exists) {
            throw new Error(`${this.COLLECTION} ${id} not found`);
        }

        this.id = record.id;
        Object.assign(this, record.data());

        return this;
    }

    public getDocumentById(id: string): DocumentReference {
        const modelCollection = this.firestore.collection(this.COLLECTION);
        return modelCollection.doc(id)
    }

    static listenRealtime(): void {
        let cursor=this.instance.collection()
            .onSnapshot((snapshots) => {
                snapshots.docChanges().forEach((change) => {
                    const model = this.from(change.doc.data(), change.doc.id);
                    switch (change.type) {
                        case "added":
                            this._onAdded(model);
                            break;
                        case "modified":
                            this._onUpdated(model);
                            break;
                        case "removed":
                            this._onDeleted(model);
                        default:
                            break;
                    }
                });
            });


    }
    // static listenRealtimeDynamic(conditions:{fieldPath: string | firebase.firestore.FieldPath, opStr: firebase.firestore.WhereFilterOp, value: any}[]=null,
    //                              callbacks:{onAdded?:Function, onUpdated?:Function, onDeleted?:Function} =  {onAdded:()=>{}, onUpdated:()=>{}, onDeleted:()=>{} } ): void {

    static listenRealtimeDynamic(conditions:{fieldPath: string | firebase.firestore.FieldPath, opStr: firebase.firestore.WhereFilterOp, value: any}[]=null,
                                 callbacks: IListenRealtimeCallbacks=(new ListenRealtimeCallbacks()) ): void {

        let cursor:Query = this.instance.collection()

        if (conditions){
            conditions.forEach((condition)=>{
                cursor=cursor.where(condition.fieldPath, condition.opStr, condition.value)
            })
        }
        cursor.onSnapshot((snapshots) => {
            snapshots.docChanges().forEach((change) => {
                const model = this.from(change.doc.data(), change.doc.id);
                switch (change.type) {
                    case "added":
                        callbacks.onAdded(model); //object
                        break;
                    case "modified":
                        callbacks.onUpdated(model);
                        break;
                    case "removed":
                        callbacks.onDeleted(model);
                    default:
                        break;
                }
            });
        });

    }

    static onUpdated(callback: Function) {
        this._onUpdated = callback;
    }
    static onAdded(callback: Function) {
        this._onAdded = callback;
    }
    static onDeleted(callback: Function) {
        this._onDeleted = callback;
    }
    cloneNewObject(){
        // TODO: this function --> clone một object mới hoàn toàn không còn vùng nhớ ở object cữ
        let newObject={}
        let result = {...this};
        delete result['firestore'];// xóa vì khi parse JSON báo lỗi
        newObject = JSON.parse(JSON.stringify(result))
        return newObject
    }
    public async deleteByCandidate(candidate_id:string) : Promise<void> {

        db.collection(this.COLLECTION).where("candidate_id", "==", candidate_id).get() .then(function(querySnapshot) {
            // Once we get the results, begin a batch
            var batch = db.batch();

            querySnapshot.forEach(function(doc) {
                // For each doc, add a delete operation to the batch
                batch.delete(doc.ref);
            });

            // Commit the batch
            return batch.commit();
        }).then(function() {

        });

    }
}
