import {CANDIDATE_COLLECTION, ICandidateModel, IQuestionnaire} from "./interfaces";
import BaseModel from "./BaseModel";


export default class BaseCandidate extends BaseModel implements ICandidateModel {

    name: string = "";
    company_id: string = "";
    // seen: boolean = false; don't use this field use unseen instead
    position_id: string = "";
    position = {
        "_id": "",
        "name": "",

        "location": {
            "name": ""
        },
        "user_id": '',
        "status": '',// draft, published, closed
    };
    hired = {
        "date": 0 // timestamp
    }
    disqualified = {
        "date": 0 // timestamp
    }

    assignee_id: string = "";
    photo: string = '';
    address: string = "";
    cover_letter: string = "";
    education = [];
    // [{
    //    school_name: '',
    //    field_of_study: '',
    //    summary: ''
    //  },]
    references: Array<object> = [];
    /*
    * {
    * name: string
    * email_address: string
    * phone_number: string
    * type: professional/personal,
    * created_at:'string'
    * }
    *
    *
    * */

    email_address: string = "";
    headline: string = "";
    initial: string = "";
    origin: string = "sourced"; //applied | recruiter | referral | sourced
    overall_score = {
        "very_good": [], //User._id
        "good": [],//User._id
        "neutral": [],//User._id
        "poor": [],//User._id
        "very_poor": [],//User._id
        "scored_count": 0,
        "score": 0,
    };
    phone_number: string = "";
    alt_phone_number: string = "";
    profile_photo_url: string = "";
    questionnaire: IQuestionnaire[] = [];
    recruited_by = {
        "_id": "",
        "email_address": "",
        "name": ""
    };
    referred_by = {
        "_id": "",
        "email_address": "",
        "name": ""
    };

    // nguoi tao candidate truc tiep tu he thong
    sourced_by: {
        _id: string,
        "email_address"?: string,
        "name"?: string
    } = {
        _id: "",
        "email_address": "",
        "name": ""
    };
    resume = {
        "file_name": "",
        "url": "",
        "pdf_url": ""
    };
    social_profiles = [
        {
            "type": "",
            "typeId": "",
            "typeName": "",
            "url": ""
        }
    ];
    // vi du: Portal,
    // co the co source hoặc không có source (trong trường hợp được tạo trực tiếp)
    source: {
        "id": string,
        "name": string,
        "type": string
    } = {
        "id": '',
        "name": '',
        "type": ''
    };

    stage = {
        "id": "",
        "name": "",
        "type": "", /// applied, feedback, interview, ...
    };
    stage_id: string = "";

    stage_actions_enabled: boolean = false

    summary: string = "";
    tags = [];
    work_history = [
        {
            "company_name": "",
            "title": "",
            "summary": "",
            "is_current": true,
            "start_date": {
                "month": "",
                "year": ""
            },
            "end_date": {
                "month": "",
                "year": ""
            }
        }
    ];
    custom_attributes: Array<object> = [];
    //sameple custom_attributes
    // {
    //         name: string,
    //         id: string,
    //         secure: boolean,
    //         value: string
    //     }
    attachfiles: {
        name: string,
        owner: string,
        created_at: string,
        "url": string
    }[] = [];


    // disposition_date: Date = null as Date;
    disposition_date?: Date;
    disposition_reason = {
        "_id": "",
        "name": "",
    };
    starred: Boolean = false;
    unseen: Boolean = true;
    follow_by: String = '';
    seen_list = []; // danh sach user id da xem

    constructor() {
        super(CANDIDATE_COLLECTION);
    }

    // static get instance(): Candidate {
    //     return new Candidate();
    // }
    //
    // static async getCandidate(candidate_id: string): Promise<Candidate> {
    //     return (new Candidate()).getById(candidate_id) as Promise<Candidate>;
    // }

    public photoLink(): string {

        return this.profile_photo_url
            || "https://ui-avatars.com/api/?name=" + this.name.slice(0, 1) + '&size=64&background=c00&color=fff';
    }


}
