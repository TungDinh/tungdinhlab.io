import Model from './base';
import Company from "@/models/Company";
import {store} from "@/store/store";
import User from "@/models/User";
import Vue from "vue"

export default class Question extends Model {
    questionnaire_id: string = "";
    response_type: null;
    name: string = '';
    description: string = '';
    options = [];
    required: boolean=false;
    order: number = 0;
    constructor() {
        super("question");
    }
    public async getAllQuestion(questionnaire_id:string, company_id?): Promise<Question[]> {
        const results: Question[] = new Array();
        let companyId;
        if(Vue.$store.getters.currentCompany){
            companyId = Vue.$store.getters.currentCompany.id
        }else {
            companyId = company_id
        }
        if (companyId) {
            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection
                .where("questionnaire_id", "==", questionnaire_id)
                .get();
            records.forEach(docSnapshot => {
                let record = new Question();
                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                results.push(record);

            });
        }
        return results
    }
}
