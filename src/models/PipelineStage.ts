import Model from './base';
import {messages} from "@/messages";
import {store} from "@/store/store";
import {server_time} from "@/firebase";
import Candidate from "@/models/Candidate";
import {listStage} from '@/common/pipeline';

export default class PipelineStage extends Model {

    title: string = "";
    pipeline_id: string = "";
    stage_type: string = ""; // applied, feedback, interview, ...
    index: number = 0;
    icon: string = "";
    hide: boolean = false; // Có 1 stage default applied là không cho phép xóa.
    showInPositionPipelineStageModal: boolean = true; // Có 1 số item thuộc list default stage không hiển thị.
    actions: Array<object> = [];  // State actions

    // Cần phải có list stage default với pipeline_id là default.
    // => Cần define các field mô tả như icon, title.
    // 1. Khi tạo mới 1 pipeline (PL), việc khởi tạo sẽ yêu cầu có những stage mặc định.
    // 2. Chỉ sử dụng Vuex để lưu, vì giữa các modal còn xử lý việc lưu tạm object nữa
    //     nên code rất dài so với việc lưu default trên DB và xử lý việc tạm ở Vuex.

    public async getAllStages(pipeline_id: string): Promise<PipelineStage[]> {
        const results: PipelineStage[] = new Array();
        const modelCollection = this.firestore.collection(this.COLLECTION);
        await modelCollection
            .where("pipeline_id", "==", pipeline_id)
            .orderBy('index', 'asc')
            .get().then(async (querySnapshot) => {
                querySnapshot.forEach(docSnapshot => {
                    let stage = new PipelineStage()
                    Object.assign(stage, docSnapshot.data());
                    stage.id = docSnapshot.id;
                    results.push(stage);
                });
            })
        return results
    }
    public async createStageDefault(pipeline_id: string): Promise<PipelineStage[]> {
        const results: PipelineStage[] = new Array();
        const Parray = listStage.map(async (stage, index)=>{
            let _stage = new PipelineStage();
            if (stage.showInPositionPipelineStageModal){
                Object.assign(_stage, stage);
                _stage.pipeline_id = pipeline_id;
                _stage.index = index;
                await _stage.insert().then((stageObject)=>{
                    //@ts-ignore
                    results.push(stageObject);
                    return stageObject;
                })
            }
        })
        await Promise.all(Parray)

        return results
    }
    // If already have Missing stage => GET
    // Else Create  !!!!
    public async getMissingStage(pipeline_id:string = ''): Promise<PipelineStage>{
        let record: PipelineStage = new PipelineStage();
        const modelCollection = this.firestore.collection(this.COLLECTION);
        await modelCollection
            .where("pipeline_id", "==", pipeline_id)
            .where('stage_type', "==", "missing")
            .get().then(async (querySnapShot)=>{
                if(querySnapShot.empty){
                    let missingStage = new PipelineStage();
                    missingStage.title = messages.SR_038_MISSING_STAGE;
                    missingStage.stage_type = 'missing';
                    missingStage.pipeline_id = pipeline_id;
                    missingStage.icon = 'fas fa-question-circle';
                    missingStage.index = -1;
                    missingStage.showInPositionPipelineStageModal = false;
                    missingStage.hide = false; // stage.hide = 'abc'  --> !hide == false --> !!hide == true.
                    await missingStage.insert().then(stage=>{
                        Object.assign(record, stage);
                        record['id'] = stage.id;
                    })
                }else{
                    querySnapShot.forEach((stage)=>{
                        Object.assign(record, stage.data());
                        console.log('tesst pile',stage.data())
                        record['id'] = stage.id;
                    })
                }
            });
        return record;
    }
    constructor() {
        super("PipelineStage");
    }

    async update(): Promise<Model> {
        const modelCollection = this.firestore.collection(this.COLLECTION);

        let result = {...this};
        let id = result['id'];
        let title = result['title'];
        let stage_type = result['stage_type'];
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        // const {TIMESTAMP} = firebase.database.ServerValue;
        result.updated_date = server_time.now.valueOf();
        result.updatedAt = server_time.now.valueOf(); //TIMESTAMP;

        const {store} = require('@/store/store');
        if (store.getters.currentUser) {
            result.updated_by = store.getters.currentUser.id;
        }


        // @ts-ignore
        const model = await modelCollection.doc(id).set(JSON.parse(JSON.stringify(result), {merge: true}));

        let candidate = await new Candidate().updateByStageName(id,title,stage_type) ;

        return this;
    }
}
