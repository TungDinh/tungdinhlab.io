import Model from './base';
import Company from "@/models/Company";
import {store} from "@/store/store";
import {perf, server_time} from "@/firebase";
import User from "@/models/User";
import position from "@/components/LandingPage/HomePageComponent/ComponentHomeBold/position.vue";
import moment from "moment";
import firebase from 'firebase';
import Query = firebase.firestore.Query;
import axios from 'axios';
import Candidate from "@/models/Candidate";
import _ from "lodash";
import {requestFunctionUrl, email_domain} from '@/firebase/index'

export default class Schedule extends Model {

    title: string = ""; // Title schedule
    start_time: string = ""; // hour => Datetime.
    end_time: string = "";
    duration: string =""; // miliseconds
    interviewers: Array<object> = [];
    interviewer_ids: Array<string> = [];
    location: string = "";
    description: string ="";
    interview_guide_id: string ="";
    interview_guide: object = {};

    company_id = "";
    candidate_name = ""; // => Display in calendar
    candidate_id = "";
    position_id = "";
    candidate: object = {} ;
    //organizer: object = {};

    acting_user: object = {};
    start_time_by_number: string = "";

    interview_date: string = ""; // Datetime
    timezone: string = ""; // Chưa biết nên để sao, tạm thời chưa dụng.

    constructor() {
        super("Schedules");
    }


    static get instance(): Schedule {
        return new Schedule();
    }


    public getCandidateSchedules(candidate_id: string): Query {
        const results: Schedule[] = new Array();
        const modelCollection = this.firestore.collection(this.COLLECTION);
        return modelCollection.where("candidate_id", "==", candidate_id)
    }


    public getAllSchedules(): Query {
        let company = store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        return modelCollection.where("company_id", "==", company.id);
    }


    public async getScheduleMeetingUser(position_id:string, candidate_id: string): Promise<Schedule[]> {
        const results: Schedule[] = new Array();

        let company = store.getters.currentCompany;

        if (company) {

            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection
                .where("company_id", "==", company.id)
                .where("position_id", "==", position_id)
                .where("candidate_id", "==", candidate_id)
                .get();
            records.forEach(docSnapshot => {

                let record = new Schedule();

                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                results.push(record);

            });
        }
        return results
    }

    public async getCurrentScheduleByCandidateID(candidate_id:string): Promise<Schedule>{
        const interviews = [];
        let company = store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        if (candidate_id) {
            let today = moment(server_time.now).format()
            let records = await modelCollection
                .where("candidate_id", "==", candidate_id)
                .where("start_time",">=",today)
                .get();
            records.forEach((querySnapshot) => {
                let object = {};
                Object.assign(object, querySnapshot.data());
                object['id'] = querySnapshot.id;
                interviews.push(object);
            });
        }
        // @ts-ignore
        return interviews;
    }

    public async getMySchedules(user): Promise<Schedule[]> {
        let schedules = [] as Schedule[];
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let company = store.getters.currentCompany;
        let start_date = moment(server_time.now).format();
        let end_date = moment(server_time.now).add(1, 'days').endOf('days').format();
        if (company) {
            const records = await modelCollection.where("company_id", "==", company.id)
                .where('interviewer_ids', "array-contains", user.id)
                .where('start_time', ">=", start_date)
                .where('start_time', "<=", end_date).limit(50).get();

            records.forEach(docSnapshot => {
                let record = new Schedule();
                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                schedules.push(record);

            });
        }
        return schedules
    }

    public getPositionSchedules(position_id: string): Query {
        let company = store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        return modelCollection
            .where("company_id", "==", company.id)
            .where("position_id", "==", position_id)
    }

    static listenRealtime(company_id?: string): void {
        if (company_id) {
            this.instance.collection().where("company_id", '==', company_id)
                .onSnapshot((snapshots) => {
                    snapshots.docChanges().forEach((change) => {
                        const model = this.from(change.doc.data(), change.doc.id);
                        switch (change.type) {
                            case "added":
                                this._onAdded(model);
                                break;
                            case "modified":
                                this._onUpdated(model);
                                break;
                            case "removed":
                                this._onDeleted(model);
                            default:
                                break;
                        }
                    });
                });
        }
    }

    async cancelSchedule(): Promise<void> {

        const modelCollection = this.firestore.collection(this.COLLECTION);
        const record = await modelCollection.doc(this.id);
        let result = {...this} as any;

        const ical = require('ical-generator');

        let cal = ical({
            prodId: {company: 'cloudjetkpi.com', product: 'cjs-potential'},
            name: 'Cancel meeting',
            timezone: 'Asia/Ho_Chi_Minh',
            method: 'cancel',
        });

        await record.delete();

        if (store.getters.currentUser) {
            const user = store.getters.currentUser;

            let attendees = result.interviewers.map((interview) => {

                    let attendee = {
                        name: interview.name,
                        email: interview.email
                    }
                    return attendee;

            })

            let candidate = await new Candidate().getById(result.candidate_id) as any;

            if (candidate) {

                let candiobject ={
                    name: candidate.name,
                    email: candidate.email_address,
                }

                // let attendees_list = attendees.filter((attendee) => {
                //     return attendee;
                // });

                attendees.push(candiobject);

                let event = cal.createEvent({
                    start: result.start_time,
                    end: result.end_time,
                    timestamp: moment(server_time.now),
                    summary: result.title,
                    organizer: `${user.name} <${user.email}>`,
                    attendees: attendees.filter((attendee) => {
                        return attendee;
                    }),
                });

                let attachByInterview = {
                    filename: 'invite.ics',
                    content: btoa(unescape(encodeURIComponent(cal.toString()))),
                    contentType: 'text/calendar'
                }

                let data = {
                    toWho: {
                        name: candidate.name,
                        email: candidate.email_address,
                        cc:user.email
                    },
                    data: {
                        actor_name: user.name,
                        candidate_name: candidate.name,
                        candidate_id: candidate.id,
                        position_name: candidate.position.name,
                        description: result.description,
                        attachments: [attachByInterview],
                        location: result.location,
                        type: "cancelCandidateInterview",
                        signatureHtml: user.signatureFlag == true?user.signatureHtml:'',
                    }
                }

                if(candidate.email_address) {
                    axios.post(requestFunctionUrl('sendEmailCancel'), data)
                        .then(function (response) {
                            console.log("Send Candidate ok!")
                        })
                        .catch(function (error) {
                            console.log(error)
                        });
                }


                //console.log("Send to");
                //console.log(attendees);
                //console.log(result.interviewers);

                result.interviewers.forEach((interviewer: any) => {


                    const event = cal.createEvent({
                        start: result.start_time,
                        end: result.end_time,
                        timestamp: moment(server_time.now),
                        summary: result.title,
                        organizer: `${user.name} <${user.email}>, Cloudjet Potential HR <no-reply@${email_domain}>`,
                        attendees:attendees.filter((attendee) => {
                            return attendee;
                        }),
                    });

                    let attachByInterview = {
                        filename: 'invite.ics',
                        content: btoa(unescape(encodeURIComponent(cal.toString()))),
                        contentType: 'text/calendar'
                    }

                    let data = {
                        toWho: {
                            name: interviewer.name,
                            email: interviewer.email,
                            cc:user.email
                        },
                        data: {
                            actor_name: user.name,
                            candidate_name: candidate.name,
                            candidate_id: candidate.id,
                            position_name: candidate.position.name,
                            link: `${window.location.origin}/candidate/${result.candidate_id}/experience`,
                            description: result.description,
                            attachments: [attachByInterview],
                            location: result.location,
                            type: "cancelUserInterview",
                            signatureHtml: user.signatureFlag == true?user.signatureHtml:'',
                        }
                    }

                    if(interviewer.email) {
                        axios.post(requestFunctionUrl('sendEmailCancel'), data)
                            .then(function (response) {
                                console.log("Send ok!")
                            })
                            .catch(function (error) {
                                console.log(error)
                            });
                    }

                })

            }



        }
    }

    async update(): Promise<Model> {
        const modelCollection = this.firestore.collection(this.COLLECTION);

        let result = {...this} as any;
        let id = result['id'];
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        // const {TIMESTAMP} = firebase.database.ServerValue;
        result.updated_date = server_time.now.valueOf();
        result.updatedAt = server_time.now.valueOf(); //TIMESTAMP;

        const {store} = require('@/store/store');
        if (store.getters.currentUser) {
            result.updated_by = store.getters.currentUser.id;
        }


        // @ts-ignore
        const model = await modelCollection.doc(id).set(JSON.parse(JSON.stringify(result), {merge: true}));



        if (store.getters.currentUser) {
            const user = store.getters.currentUser;

            const ical = require('ical-generator');

            let cal = ical({
                prodId: {company: 'cloudjetkpi.com', product: 'cjs-potential'},
                name: 'Update meeting',
                timezone: 'Asia/Ho_Chi_Minh',
                method: 'request',
            });


            // let attendees = result.interviewers.map((interview) => {
            //
            //     let attendee = {
            //         name: interview.name,
            //         email: interview.email
            //     }
            //     return attendee;
            //
            // })


            let attendees = [
                {
                    name: user.name,
                    email:user.email,
                }
            ]




            let candidate = await new Candidate().getById(result.candidate_id) as any;

            if (candidate) {

                let candiobject ={
                    name: candidate.name,
                    email: candidate.email_address,
                }

                // let attendees_list = attendees.filter((attendee) => {
                //     return attendee;
                // });

                //attendees.push(candiobject);

                console.log("Send to");
                console.log(attendees);

                let event = cal.createEvent({
                    start: result.start_time,
                    end: result.end_time,
                    timestamp: moment(server_time.now),
                    summary: result.title,
                    organizer: `${user.name} <${user.email}>`,
                    attendees: attendees
                });



                let attachByInterview = {
                    filename: 'invite.ics',
                    content: btoa(unescape(encodeURIComponent(cal.toString()))),
                    contentType: 'text/calendar'
                }

                let data = {
                    toWho: {
                        name: candidate.name,
                        email: candidate.email_address,
                        cc:user.email
                    },
                    data: {
                        actor_name: user.name,
                        candidate_name: candidate.name,
                        candidate_id: candidate.id,
                        position_name: candidate.position.name,
                        description: result.description,
                        attachments: [attachByInterview],
                        location: result.location,
                        signatureHtml: user.signatureFlag == true?user.signatureHtml:''
                    }
                }

                if(candidate.email_address) {
                    axios.post(requestFunctionUrl('sendEmailEdit'), data)
                        .then(function (response) {
                            console.log("Send Candidate ok!")
                        })
                        .catch(function (error) {
                            console.log(error)
                        });
                }


                //console.log("Send to");
                //console.log(attendees);
                //console.log(result.interviewers);

                result.interviewers.forEach((interviewer: any) => {


                    const event = cal.createEvent({
                        start: result.start_time,
                        end: result.end_time,
                        timestamp: moment(server_time.now),
                        summary: result.title,
                        organizer: `${user.name} <${user.email}>, Cloudjet Potential HR <no-reply@${email_domain}>`,
                        attendees:attendees
                    });

                    let attachByInterview = {
                        filename: 'invite.ics',
                        content: btoa(unescape(encodeURIComponent(cal.toString()))),
                        contentType: 'text/calendar'
                    }

                    let data = {
                        toWho: {
                            name: interviewer.name,
                            email: interviewer.email,
                            cc:user.email
                        },
                        data: {
                            actor_name: user.name,
                            candidate_name: candidate.name,
                            candidate_id: candidate.id,
                            position_name: candidate.position.name,
                            link: `${window.location.origin}/candidate/${result.candidate_id}/experience`,
                            description: result.description,
                            attachments: [attachByInterview],
                            location: result.location,
                            signatureHtml: user.signatureFlag == true?user.signatureHtml:''
                        }
                    }

                    if(interviewer.email) {
                        axios.post(requestFunctionUrl('sendEmailEdit'), data)
                            .then(function (response) {
                                console.log("Send ok!")
                            })
                            .catch(function (error) {
                                console.log(error)
                            });
                    }

                })

            }

        }


        return this;
    }
}
