import Model from './base';
import firebase from "firebase";
import Query = firebase.firestore.Query;
import {CONVERSATION_COLLECTION, IConversation, IConversationObject} from "@/models/interfaces";

class Conversation extends Model implements IConversation{
    company_id: string = '';
    type: string = '';
    object: IConversationObject = {mail_state:'pending'};
    // object is custom follow by type;
    position_id: string = "";
    candidate_id: string = "";
    mail_id: string = '';
    mail_opened_count:number = 0;
    constructor() {
        super(CONVERSATION_COLLECTION);
    }
    static get instance(): Conversation {
        return new Conversation();
    }

    public getConversation(candidate_id: string): Query {
        let emailCandidates: Conversation[] = new Array();
        const modelCollection = (this as any).firestore.collection(this.COLLECTION);
        return modelCollection
            .where("candidate_id", "==", candidate_id)
    }
    static listenRealtime(company_id?: string): void {
        if (company_id) {
            this.instance.collection().where("company_id", '==', company_id)
                .onSnapshot((snapshots) => {
                    snapshots.docChanges().forEach((change) => {
                        const model = this.from(change.doc.data(), change.doc.id);
                        switch (change.type) {
                            case "added":
                                this._onAdded(model);
                                break;
                            case "modified":
                                this._onUpdated(model);
                                break;
                            case "removed":
                                this._onDeleted(model);
                            default:
                                break;
                        }
                    });
                });
        } else {
            this.instance.collection()
                .onSnapshot((snapshots) => {
                    snapshots.docChanges().forEach((change) => {
                        const model = this.from(change.doc.data(), change.doc.id);
                        switch (change.type) {
                            case "added":
                                this._onAdded(model);
                                break;
                            case "modified":
                                this._onUpdated(model);
                                break;
                            case "removed":
                                this._onDeleted(model);
                            default:
                                break;
                        }
                    });
                });
        }
    }

}

export default Conversation;
