import Model from './base';
import Company from "@/models/Company";
// import {store} from "@/store/store";
import User from "@/models/User";
import Position from "@/models/Position";
import ROLE from '@/permission/type';
import axios from 'axios'
import Inbox from "@/models/Inbox";
import Conversation from "@/models/Conversation";
import Activity from "@/models/Activity";
import $ from 'jquery';
import moment from "moment";
import {firestore} from "firebase";
import EmailTemplate from "@/models/EmailTemplate";
import {includes as _includes, trim as _trim, template as _template} from 'lodash';
import Schedule from "@/models/Schedule";
import CandidateScorecard from "@/models/CandidateScorecard";
import Task from "@/models/Task";
import {db, requestFunctionUrl, server_time} from '@/firebase/index'
import {CANDIDATE_COLLECTION, ICandidateModel, IQuestionnaire} from "@/models/interfaces";
import {urlLandingpage} from "@/helpers/joblinks";
import Vue from 'vue';

export default class Candidate  extends Model implements ICandidateModel{
    name: string = "";
    company_id: string = "";
    // seen: boolean = false; don't use this field use unseen instead
    position_id: string = "";
    position = {
        "_id": "",
        "name": "",

        "location": {
            "name": ""
        },
        "user_id": '',
        "status": '',// draft, published, closed
    };
    hired = {
        "date": 0 // timestamp
    }
    disqualified = {
        "date": 0 // timestamp
    }

    assignee_id: string = "";
    photo: string = '';
    address: string = "";
    cover_letter: string = "";
    education = [];
    // [{
    //    school_name: '',
    //    field_of_study: '',
    //    summary: ''
    //  },]
    references: Array<object> = [];
    /*
    * {
    * name: string
    * email_address: string
    * phone_number: string
    * type: professional/personal,
    * created_at:'string'
    * }
    *
    *
    * */

    email_address: string = "";
    headline: string = "";
    initial: string = "";
    origin: string = "sourced"; //applied | recruiter | referral | sourced
    overall_score = {
        "very_good": [], //User._id
        "good": [],//User._id
        "neutral": [],//User._id
        "poor": [],//User._id
        "very_poor": [],//User._id
        "scored_count": 0,
        "score": 0,
    };
    phone_number: string = "";
    alt_phone_number: string = "";
    profile_photo_url: string = "";
    questionnaire: IQuestionnaire[] = [];
    recruited_by = {
        "_id": "",
        "email_address": "",
        "name": ""
    };
    referred_by = {
        "_id": "",
        "email_address": "",
        "name": ""
    };

    // nguoi tao candidate truc tiep tu he thong
    sourced_by: {
        _id: string,
        "email_address"?: string,
        "name"?: string
    } = {
        _id: "",
        "email_address": "",
        "name": ""
    };
    resume = {
        "file_name": "",
        "url": "",
        "pdf_url": ""
    };
    social_profiles = [
        {
            "type": "",
            "typeId": "",
            "typeName": "",
            "url": ""
        }
    ];
    // vi du: Portal,
    // co the co source hoặc không có source (trong trường hợp được tạo trực tiếp)
    source: {
        "id": string,
        "name": string,
        "type": string
    } = {
        "id": '',
        "name": '',
        "type": ''
    };

    stage = {
        "id": "",
        "name": "",
        "type": "", /// applied, feedback, interview, ...
        'previous_type': ''//for missing stage
    };
    stage_id: string = "";

    stage_actions_enabled: boolean = false

    summary: string = "";
    tags = [];
    work_history = [
        {
            "company_name": "",
            "title": "",
            "summary": "",
            "is_current": true,
            "start_date": {
                "month": "",
                "year": ""
            },
            "end_date": {
                "month": "",
                "year": ""
            }
        }
    ];
    custom_attributes: Array<object> = [];
    //sameple custom_attributes
    // {
    //         name: string,
    //         id: string,
    //         secure: boolean,
    //         value: string
    //     }
    attachfiles: {
        name: string,
        owner: string,
        created_at: string,
        "url": string
    }[] = [];


    disposition_date: Date;
    disposition_reason = {
        "_id": "",
        "name": "",
    };
    starred: Boolean = false;
    unseen: Boolean = true;
    follow_by: String = '';
    seen_list = []; // danh sach user id da xem

    constructor() {
        super(CANDIDATE_COLLECTION);
    }

    static get instance(): Candidate {
        return new Candidate();
    }
    static  async getCandidate(candidate_id:string):Promise<Candidate> {
        return (new Candidate()).getById(candidate_id) as Promise<Candidate>;
    }

    public async getAssignee(): Promise<User> {
        return (new User()).getById(this.assignee_id) as Promise<User>;
    }


    public async getCompany(): Promise<Company> {

        return (new Company()).getById(this.company_id) as Promise<Company>;
    }

    public async getCandidatesByPipelineID(position_id, stage_id): Promise<Candidate[]> {
        const results: Candidate[] = [];
        let company = Vue.$store.getters.currentCompany;
        if (company) {
            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection
                .where("company_id", "==", company.id)
                .where("position._id", "==", position_id)
                .where("stage.id", "==", stage_id)
                // TODO  .orderBy("updatedAt", "desc")
                .get();
            records.forEach(docSnapshot => {
                let record = new Candidate();
                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                results.push(record);
            });
        }
        return results
    }
    public getCandidatesByPosition(position_id: string): firestore.Query {
        let company =Vue.$store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        return modelCollection
            .where("company_id", "==", company.id)
            .where("position_id", "==", position_id)
    }
    public async checkExistCandidateByEmail(company_id, email, position_id): Promise<boolean> {
        let result: boolean = false
        if (company_id && email && position_id) {
            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection.where("company_id", "==", company_id)
                .where(new firestore.FieldPath( 'position', '_id'), "==", position_id)
                .where("email_address", "==", email)
                // TODO  .orderBy("updatedAt", "desc")
                .get().then((querySnapshot) => {
                    if (!querySnapshot.empty) {
                        result = true
                    }
                })
        }
        return result
    }

    public getCandidates(): firestore.Query {
        let company =Vue.$store.getters.currentCompany;
        return this.firestore.collection(this.COLLECTION).where("company_id", "==", company.id)
    }

    private async checkValidate():Promise<boolean>{
        // alert('check validate new candidate');
        let is_valid:boolean=false;
        let is_update_email_address:boolean=false;
        let is_existed_email_address:boolean=false;
        const email_address:string=_trim(this.email_address).toLowerCase();

        if(!email_address){
            is_valid=true;
        }
        else{
            const is_update:boolean = !!this.id;
            is_existed_email_address = await this.checkExistCandidateByEmail(this.company_id, email_address, this.position_id);
            if(is_update){
                const this_fresh_candidate:Candidate = await new Candidate().getById(this.id) as Candidate;
                is_update_email_address = !(_trim(this_fresh_candidate.email_address).toLowerCase() == email_address);
                if(is_update_email_address){
                    const is_existed_new_email_address:boolean = is_existed_email_address;
                    if(is_existed_new_email_address){
                        is_valid=false
                    }else{
                        is_valid = true
                    }
                }else{//  update, but not update email address)
                    is_valid = true
                }
            }else{ // insert
                is_valid = !is_existed_email_address
            }

            // const this_fresh_candidate:Candidate = await this.getById(this.id) as Candidate;
            // is_update_email_address = !(this_fresh_candidate.email_address == this.email_address);
            // const is_existed_new_email_address:boolean = is_existed_email_address;
            //
            // is_valid = is_update ? (is_update_email_address ? (!is_existed_new_email_address) : true) : (!is_existed_email_address)

        }

        if (!is_valid){
            // debugger
            // show error notification
            if(window.hasOwnProperty('vm')){
                // window['vm'].$message.error('fk this');
                // await window['vm'].$alert(
                window['vm'].$alert(
                    `Email <b style="white-space: nowrap">${this.email_address}</b> đã tồn tại trong vị trí này`,
                    'Lỗi', {
                    confirmButtonText: 'OK',
                    callback: action => {
                        // window['vm'].$message({
                        //     type: 'info',
                        //     message: `action: ${ action }`
                        // });
                    },
                    dangerouslyUseHTMLString: true
                });
            }
        }
        return is_valid
    }
    async update(): Promise<Model> {
        this.email_address = _trim(this.email_address).toLowerCase();

        const is_valid:boolean = await this.checkValidate();
        const that = this;
        // return await new Promise<Candidate>(async (resolve, reject) => {
        const result =  await new Promise<Candidate>(async (resolve, reject) => {
            if (!is_valid){
                reject('Email existed');
            }
            else{
                await super.update()
                resolve(this);

                // const updated_candidate:Candidate = await super.update.call(that) as Candidate;
                // resolve(updated_candidate);

            }
        });
        return result


    }
    async insert(): Promise<Candidate> {
        this.email_address = _trim(this.email_address).toLowerCase();

        const is_valid:boolean = await this.checkValidate();
        // await this.checkExistCandidateByEmail(result.company_id, result.email_address, result.position_id);

        // https://stackoverflow.com/a/47272118
        return new Promise<Candidate>((resolve, reject) => {

            // const addNewCandidate = async ():Promise<Candidate> => {
            const addNewCandidate = async () => {
                const modelCollection = this.firestore.collection(this.COLLECTION);
                let result = {...this};
                delete result['firestore'];
                delete result['COLLECTION'];
                delete result['id'];




                //const {TIMESTAMP} = firebase.database.ServerValue;
                result.created_date = server_time.now.valueOf(); //TIMESTAMP;
                result.updated_date = server_time.now.valueOf(); //TIMESTAMP;

                //const {TIMESTAMP} = firebase.database.ServerValue;
                result.createdAt = server_time.now.valueOf(); //TIMESTAMP;
                result.updatedAt = server_time.now.valueOf(); //TIMESTAMP;

                const user = Vue.$store.getters.currentUser;
                if (user) {
                    result.updated_by = user.id;
                    result.created_by = user.id;
                    result.headline = "Thêm bởi "+ user.name;
                }
                // need validate candidate email_address here


                const model = await modelCollection.add(JSON.parse(JSON.stringify(result))) as any;
                this.id = model.id;
                // console.log(model);


                let count = await new Position().aggregateCandidatesCount(result.position_id);
                await this.firestore.collection('Position').doc(result.position_id).update({
                    count_candidate: count,
                });


                // await new Candidate().getById(model.id).then(async (candiate: any) => {
                //     if (candiate.position_id) {
                //         let position = await new Position().getById(candiate.position_id) as Position;
                //         position.count_candidate = position.count_candidate + 1;
                //         position.update();
                //     }
                // });

                // return this;
                // resolve(this);
            }


            if (!is_valid){
                reject('Email existed');
            }
            else{
                // await addNewCandidate.call(this);
                addNewCandidate.call(this).then(()=>{
                    resolve(this);
                });


            }


        });

    }
    static listenRealtime(company_id?: string, position_id?:string): void {
        if (company_id) {
            if (position_id) {
                this.instance.collection()
                    .where("company_id", '==', company_id)
                    .where("position._id", '==', position_id)
                    .onSnapshot((snapshots) => {
                        snapshots.docChanges().forEach((change) => {
                            const model = this.from(change.doc.data(), change.doc.id);
                            switch (change.type) {
                                case "added":
                                    this._onAdded(model);
                                    break;
                                case "modified":
                                    this._onUpdated(model);
                                    break;
                                case "removed":
                                    this._onDeleted(model);
                                default:
                                    break;
                            }
                        });
                    });
            } else {
                this.instance.collection()
                    .where("company_id", '==', company_id)
                    .onSnapshot((snapshots) => {
                        snapshots.docChanges().forEach((change) => {
                            const model = this.from(change.doc.data(), change.doc.id);
                            switch (change.type) {
                                case "added":
                                    this._onAdded(model);
                                    break;
                                case "modified":
                                    this._onUpdated(model);
                                    break;
                                case "removed":
                                    this._onDeleted(model);
                                default:
                                    break;
                            }
                        });
                    });
            }
        } else {
            this.instance.collection()
                .onSnapshot((snapshots) => {
                    snapshots.docChanges().forEach((change) => {
                        const model = this.from(change.doc.data(), change.doc.id);
                        switch (change.type) {
                            case "added":
                                this._onAdded(model);
                                break;
                            case "modified":
                                this._onUpdated(model);
                                break;
                            case "removed":
                                this._onDeleted(model);
                            default:
                                break;
                        }
                    });
                });
        }
    }
    public photoLink(): string {

        return this.profile_photo_url
            || "https://ui-avatars.com/api/?name=" + this.name.slice(0, 1) + '&size=64&background=c00&color=fff';
    }

    public async createOrUpdateInbox(candidate, emailObject) {
        let inbox = await new Inbox().getInboxByCandidate(candidate.id);
        if (!inbox) {
            inbox = new Inbox();
        }
        inbox.email = candidate.email_address;
        if (emailObject.hasOwnProperty('cc')) {
            let emailCC = emailObject.cc.split(',');
            inbox.cc = emailCC.map((email) => {
                return email.trim();
            })
        }

        if (emailObject.hasOwnProperty('bcc')) {
            let emailBCC = emailObject.bcc.split(',');
            inbox.bcc = emailBCC.map((email) => {
                return email.trim();
            })
        }

        let user = Vue.$store.getters.currentUser;
        const company = Vue.$store.getters.currentCompany;

        inbox.subject = emailObject.subject;

        if (!user) {
            user = await User.instance.getById(candidate.created_by) as User;
        }
        inbox.acting_user =  user.cloneNewObject();
        if (inbox.acting_user_ids.indexOf(user.id) == -1) {
            inbox.acting_user_ids.push(user.id);
        }

        //inbox.company_id =Vue.$store.getters.currentCompany.id;
        inbox.company_id = candidate.company_id;

        inbox.body = $(emailObject.body).text();
        inbox.candidate_id = candidate.id;
        inbox.candidate = {
            id: candidate.id,
            name: candidate.name,
            email_address: candidate.email_address,
            profile_photo_url: candidate.profile_photo_url,
            position_id: candidate.position_id,
            position: candidate.position,
            headline:candidate.headline,
        }
        if (candidate.position_id) {
            inbox.position_id = candidate.position_id;
        }

        inbox.is_showing = true;

        // @ts-ignore
        // _Inbox.time_to_send = emailObject.time_to_send == "" ? Date.now() : new Date(emailObject.time_to_send).getTime();
        inbox.attachments = emailObject.attachments;
        return inbox.upsert();
    }
    public async createConversation(candidate, interviewObject): Promise <void>{
        return new Promise( resolve => {
            const {store} = require('@/store/store');
            let conversation = new Conversation();
            conversation.type = 'candidatePanelInterviewAdded';
            conversation.candidate_id = candidate.id;
            conversation.position_id = candidate.position_id;
            conversation.company_id = candidate.company_id;
            conversation.object = {
                candidate: candidate,
                candidate_id: candidate.id,
                position_id: candidate.position_id,
                acting_user: {
                    id:Vue.$store.getters['currentUser'].id,
                    name:Vue.$store.getters['currentUser'].name,
                    photo_url:Vue.$store.getters['currentUser'].photoURL,
                },
                mail_state:'pending',
                panel_interview: interviewObject
            };

            conversation.upsert().then((data)=>{
                console.log('Created conversation ', data);
                //@ts-ignore
                resolve(data);
            });
        })
        // if (hasSendMail){
        //     _Convertion.insert().then((data)=>{
        //         this.sendEmail(candidate, emailObject).then(()=>{
        //         })
        //     });
        // }
    }
    public async sendEmail(candidate, emailObject, hasCreateInbox = true, hasCreateConversation = true, senderName = '') {
        const {store} = require('@/store/store');
        if (hasCreateInbox) this.createOrUpdateInbox(candidate, emailObject);
        let candidateConverted = candidate;
        if (candidate.hasOwnProperty('firestore')){
            candidateConverted = candidate.cloneNewObject()
        }

        // Usecase: If candidate was applied from portal. We'll be send a message to him/her.
        if (hasCreateConversation){
            let conversation = new Conversation();
            conversation.type = 'messageToCandidatePosted';
            let user = new User();
            if (Vue.$store.getters.currentUser) {
                user = Vue.$store.getters.currentUser;
            } else {
                user = await new User().getById(candidate.created_by)  as User;
            }

            senderName = user.name;

            conversation.object = {
                cc: emailObject.cc || "",
                bcc: emailObject.bcc || "",
                body: emailObject.body,
                subject: emailObject.subject,
                attachments: emailObject.attachments || [],
                candidate: candidateConverted,
                candidate_id: candidateConverted.id,
                position_id: candidateConverted.position_id,
                mail_state: 'pending',// sent
                acting_user: {
                    id: user.id,
                    name: user.name,
                    photo_url: user.photoURL,
                }
            };


            conversation.candidate_id = candidate.id;
            conversation.position_id = candidate.position_id;
            conversation.company_id = candidate.company_id;

            // Mail state có thể là bounced | waiting | sent;
            // bounced  => Send mail fail;
            // @ts-ignore
            // _Convertion.object['mail_state'] = _Inbox.time_to_send < Date.now() ? 'sent' : 'waitting';
            // new Candidate().sendEmail(candidate.email_address, _Inbox.subject, _Inbox.attachments, _Inbox.body);
            // axios.post(requestFunctionUrl('mailService'), data)

            return conversation.insert().then((conversation:Conversation) => {
                // @ts-ignore
                // if (conversation.object.mail_state == 'pending') {
                    // Prepare Data To Send <3
                    let data =
                        {
                            // "from": `candidate-${candidate.id}@cloudjetpotential.com`,
                            candidate_id: candidate.id,
                            conversation_id: conversation.id,
                            current_user_name: senderName || Vue.$store.getters['currentUser'].name,
                            "to": candidate.email_address,
                            "subject": emailObject.subject,
                            "attachments": emailObject.attachments.map((attach) => {

                                // @ts-ignore
                                attach['filename'] = attach.file_name;
                                // @ts-ignore
                                attach['href'] = attach.file_url;
                                return attach;

                                }),
                            "calendar_event": emailObject.calendar_event || null,
                            "html": emailObject.body,
                            "cc": emailObject.cc || [],
                            "bcc": emailObject.bcc || []
                        };



                        // new Candidate().sendEmail(candidate.email_address, _Inbox.subject, _Inbox.attachments, _Inbox.body);
                    // axios.post(requestFunctionUrl('mailService'), data)
                // debugger
                    axios.post(requestFunctionUrl('sendConversationEmail'), data)
                        .then(function (response) {
                            console.log(response)
                        })
                        .catch(function (error) {
                            console.log(error)
                        });



                    return conversation;
                // }
            });
        }else{
            let data =
                {
                    // "from": `candidate-${candidate.id}@cloudjetpotential.com`,
                    candidate_id: candidate.id,
                    current_user_name: senderName || Vue.$store.getters['currentUser'].name,
                    "to": candidate.email_address,
                    "subject": emailObject.subject,
                    "attachments": emailObject.attachments.map((attach) => {

                        // @ts-ignore
                        attach['filename'] = attach.file_name;
                        // @ts-ignore
                        attach['href'] = attach.file_url;
                        return attach;
                    }),
                    "calendar_event": emailObject.calendar_event || null,
                    "html": emailObject.body,
                    "cc": emailObject.cc || [],
                    "bcc": emailObject.bcc || []
                }

                // debugger
            axios.post(requestFunctionUrl('sendConversationEmail'), data)
                .then(function (response) {
                })
                .catch(function (error) {
                    console.log(error)
                });



        }
    }
    public getNewCandidates(): firestore.Query {
        let candidates: Candidate[] = [];
        let company = Vue.$store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let start_date = moment(server_time.now).subtract(7, "day").startOf("day").valueOf();
        let end_date = moment(server_time.now).endOf('day').valueOf();
        return modelCollection.where("company_id", "==", company.id)
            .where('createdAt', ">=", start_date)
            .where('createdAt', "<=", end_date)
    }

    public getUnseenCandidates(): firestore.Query {
        let candidates: Candidate[] = [];
        let company = Vue.$store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        return modelCollection.where("company_id", "==", company.id)
            .where('unseen', '==', true);
    }

    public getCandidatesByAssignee(assignee_id: string): firestore.Query {
        let candidates: Candidate[] = [];
        let company = Vue.$store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        return modelCollection.where("company_id", "==", company.id)
            .where('assignee_id', '==', assignee_id)
    }

    public async getCandidateByStage(stage_id: string): Promise<Candidate[]> {
        let candidates: Candidate[] = [];
        let company = Vue.$store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        const records = await modelCollection.where("company_id", "==", company.id)
            .where('stage.id', '==', stage_id)
            .get();


        records.forEach(docSnapshot => {
            let record = new Candidate();

            Object.assign(record, docSnapshot.data());
            record.id = docSnapshot.id;
            candidates.push(record);
        })
        return candidates;
    }

    public async getCandidateByPosition(position_id) {
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let company = Vue.$store.getters.currentCompany;
        let candidates = [];
        const records = await modelCollection.where("company_id", "==", company.id)
            .where('position_id', '==', position_id)
            .get();

        records.forEach(docSnapshot => {
            let record = new Candidate();
            Object.assign(record, docSnapshot.data());
            record.id = docSnapshot.id;
            candidates.push(record);
        })
        return candidates;
    }

    public async sendMailByActions(templateEmailID, candidate, position, senderName = ''): Promise<void>{
        let userActing = Vue.$store.getters.currentUser;
        let notSendFromPortal = true;
        if (!userActing){
            userActing = {
                id: position.position_assign.id,
                name: position.position_assign.name,
                email: position.position_assign.email
            }
            notSendFromPortal = false;
        }
        let company = Vue.$store.getters.currentCompany || Vue.$store.getters.currentCompanyLandingPage;

        const getPositionLink = () => {
            return urlLandingpage(`/position/${position.id}`, company.domain);
        }
        function replaceInfoByCandidate(string)  {
            let arrayField = [
                {
                    title: 'candidate_first_name',
                    value: 'name'
                },
                {
                    title: 'candidate_name',
                    value: 'name'
                },
                {
                    title: 'candidate_email_address',
                    value: 'email'
                },
                {
                    title: 'position_link',
                    value: 'position_link'
                },
                {
                    title: 'position_title',
                    value: 'position_title'
                },
                {
                    title: 'company_name',
                    value: 'company_name'
                },
                {
                    title: 'company_user',
                    value: 'company_user'
                },
                {
                    title: 'company_user_first_name',
                    value: 'company_user_first_name'
                },
            ];

            var compiled = _template(string, {interpolate: /\[\[([\s\S]+?)\]\]/g});
            let result = '';
            let object = {};
            arrayField.forEach((field, index) => {
                switch (field.title) {
                    case 'candidate_first_name':
                        object[field.title] = candidate.name.split(" ").pop();
                        break;
                    case 'candidate_name':
                        object[field.title] = candidate.name;
                        break;
                    case 'company_user_first_name':
                        object[field.title] = userActing.name.split(" ").pop();
                        break;
                    case 'company_name':
                        object[field.title] = company.name;
                        break;
                    case 'company_user':
                        object[field.title] = userActing.name;
                        break;
                    case 'position_title':
                        object[field.title] = position.title;
                        break;
                    case 'candidate_email_address':
                        object[field.title] = candidate.email_address;
                        break;
                    case 'position_link':
                        object[field.title] = getPositionLink();
                        break;
                    default:
                        object[field.title] = field.value;
                        break;
                }
            });
            result = compiled(object);
            return result;
        }

        new EmailTemplate().getDocumentById(templateEmailID).get().then((record)=>{
            //@ts-ignore
            if (!record.exists) {
                // Get actions
                let actions = [];
                Vue.$store.dispatch('deleteActionPipeline',position)
                // new PositionPipelineAction().getActionsImported(position.id, position.pipeline_id).then((actionData)=>{
                //     actions = actionData;
                //     actionData.forEach((data)=>{
                //         let indexActionSendMail = data.actions.findIndex((action)=>{return action['action_type'] == 'emails'});
                //         if (indexActionSendMail > -1){
                //             data.actions.splice(indexActionSendMail, 1);
                //             data.update();
                //         }
                //     })
                // })
            }else{
                let templateEmail = record.data()
                    templateEmail.id = record.id;
                try{
                    let emailObject = {
                        //@ts-ignore
                        subject: replaceInfoByCandidate(templateEmail.subject),
                        //@ts-ignore
                        body: replaceInfoByCandidate(templateEmail.content),
                        //@ts-ignore
                        attachments: templateEmail.attachments || [],
                    };
                    // Warning: If email's body is empty, it cannot send.
                    if (emailObject.body.trim() != '' && candidate.email_address.trim() != ''){
                        new Candidate().sendEmail(candidate, emailObject, true, true, senderName).then(()=>{
                            console.log('Send Email by Action Success');
                        })
                    }
                }catch(err){
                    console.error('Cannot send Email, please check again!', err);
                }
            }

        })

    }
    public async seenMe(): Promise<void> {
        const user = Vue.$store.getters.currentUser;
        if((user.id)&&(!_includes(this.seen_list, user.id))) {
            this.seen_list.push(user.id);
            await this.collection().doc(this.id).update({
                seen_list: this.seen_list,

            });
        }
    }

    public  checkSeenCandidate(candidate:any): boolean {
        const user = Vue.$store.getters.currentUser;
        let flag = false;
        if((user.id)&&(_includes(candidate.seen_list, user.id))) {
            flag = true;
        }
        return flag;
    }

    async delete(): Promise<void> {

        const modelCollection = this.firestore.collection(this.COLLECTION);
        const record = await modelCollection.doc(this.id);
        let result = {...this};
        await record.delete();

        let count = await new Position().aggregateCandidatesCount(result.position_id);
        await this.firestore.collection('Position').doc(result.position_id).update({
            count_candidate: count,
        });

        let inbox = await new Inbox().deleteByCandidate(this.id);

        let schedule= await new Schedule().deleteByCandidate(this.id);

        let converation = await new Conversation().deleteByCandidate(this.id);

        let scorecard =  await  new CandidateScorecard().deleteByCandidate(this.id);

        let task = await new Task().deleteByCandidate(this.id);

        let activity = await new Activity().deleteByCandidate(this.id);

        return Promise.all([inbox, schedule, converation, scorecard, task,activity]).then(values => {
            console.log("Delete Candidate "+this.id+ " done!");
            //console.log(values);
        }).catch(reason => {
            console.log("Delete Candidate "+this.id+ " fail!");
           console.log(reason);
        });

    }

    public async getPositionOfCopyCandidate(candidate) {
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let company = Vue.$store.getters.currentCompany;
        let candidates = [];
        if (candidate.email_address.trim() != ''){
            const records = await modelCollection.where("company_id", "==", company.id)
                .where('email_address', '==', candidate.email_address)
                .get();

            records.forEach(docSnapshot => {
                let record = new Candidate();
                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                candidates.push(record);
            })
        }
         return candidates;

    }

    public async updateByStageName(stage_id:string,stage_name:string, stage_type:string) : Promise<void> {
        db.collection(this.COLLECTION).where("stage_id", "==", stage_id)
            .get()
            .then(function(querySnapshot) {
            // Once we get the results, begin a batch
            var batch = db.batch();

            querySnapshot.forEach(function(doc) {
                // For each doc, add a update operation to the batch
                batch.update(doc.ref,{
                    'stage.name':stage_name,
                    'stage.type':stage_type
                });
            });

            // Commit the batch
            return batch.commit();
        }).then(function() {

        });
    }

}
