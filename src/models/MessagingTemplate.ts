import Model from './base';
import Company from "@/models/Company";
import {store} from "@/store/store";
import User from "@/models/User";

export default class MessagingTemplate extends Model {

    default_subject: string = "";
    company_id: string = "";


    public async getCompany(): Promise<Company> {
        return (new Company()).getById(this.company_id) as Promise<Company>;
    }

    constructor() {
        super("messaging_template");
    }

    public async createMessagingTemplate(company_id:string): Promise<MessagingTemplate> {
        let new_template = new MessagingTemplate();
        new_template.company_id = company_id;
        new_template.default_subject = "Re: [[company_name]] opportunity"
        new_template.insert().then((t) => {
            new_template.id =  t.id;
        })
        return new_template;
    }

    public async getMessagingTemplate(company_id: string): Promise<MessagingTemplate> {
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let template: MessagingTemplate;
        const records = await modelCollection.where("company_id", "==", company_id).get()
        if (records.docs.length > 0) {
            let result = records.docs[0];
            template = new MessagingTemplate();
            template.id = result.id;
            Object.assign(template, result.data());
        } else {
            template = await this.createMessagingTemplate(company_id);
        }
        return template;
    }
}
