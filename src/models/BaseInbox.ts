
import BaseModel from "./BaseModel";
import {INBOX_COLLECTION, IInbox} from "./interfaces";

export default class BaseInbox extends BaseModel implements IInbox {
    email: string = "";
    cc: Array<string> = [];
    bcc: Array<string> = [];
    subject: string = "";
    position_id: string = "";
    candidate_id: string = "";
    candidate: object = {};
    company_id = "";
    body: string = "";
    attachments: Array<object> = [];
    time_to_send: string = ""; // Time to send in datetime format
    acting_user: object = {}; // Last user tuong tat
    acting_user_ids: Array<string> = []; // Lưu list danh sach nhan vien tuong tac voi ung vien nay
    is_showing: boolean = true;
    constructor(){
        super(INBOX_COLLECTION)
    }

}


