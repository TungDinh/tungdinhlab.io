import Model from './base';
import {store} from "@/store/store";
import firebase from 'firebase';
import Query = firebase.firestore.Query;
import {server_time} from "@/firebase";


export class criteria {
    criteria_name:string="";
    score: string=""
    constructor(text: string) {
        this.criteria_name = text;
        this.score = text;
    }
}

export class section {
    section_name:string="";
    list_criteria:criteria[] =[];

    constructor(text: string,criteria: criteria[] ) {
        this.section_name = text;
        this.list_criteria = criteria;
    }

}

export default class Scorecard extends Model {
    scorecard_name:string="";
    company_id:string = "";
    sections:section[] = [
        {
            section_name: "",
            list_criteria: [
                {criteria_name: "",score:"",}
            ]
        },
    ];

    constructor() {
        super("scorecards");
    }

    static get instance(): Scorecard {
        return new Scorecard();
    }

    //override
    async insert(): Promise<Scorecard> {
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let result = {...this};
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        //const {TIMESTAMP} = firebase.database.ServerValue;
        result.created_date = server_time.now.valueOf(); //TIMESTAMP;
        result.updated_date = server_time.now.valueOf() //TIMESTAMP;

        //const {TIMESTAMP} = firebase.database.ServerValue;
        result.createdAt = server_time.now.valueOf() //TIMESTAMP;
        result.updatedAt = server_time.now.valueOf() //TIMESTAMP;

        result.updated_by =  store.getters.currentUser.id;
        result.created_by = store.getters.currentUser.id;


        const model = await modelCollection.add(JSON.parse(JSON.stringify(result)));
        this.id = model.id;

        return this;

    }

    async update(): Promise<Scorecard> {

        const modelCollection = this.firestore.collection(this.COLLECTION);

        let result = {...this};
        let id = result['id'];
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        // const {TIMESTAMP} = firebase.database.ServerValue;
        result.updated_date = server_time.now.valueOf();
        result.updatedAt = server_time.now.valueOf()
        result.updated_by =  store.getters.currentUser.id;

        // @ts-ignore
        const model = await modelCollection.doc(id).set(JSON.parse(JSON.stringify(result), {merge: true}));

        return this;
    }

    static listenRealtime(company_id?: string): void {
        if (company_id) {
            this.instance.collection().where("company_id", '==', company_id)
                .onSnapshot((snapshots) => {
                    snapshots.docChanges().forEach((change) => {
                        const model = this.from(change.doc.data(), change.doc.id);
                        switch (change.type) {
                            case "added":
                                this._onAdded(model);
                                break;
                            case "modified":
                                this._onUpdated(model);
                                break;
                            case "removed":
                                this._onDeleted(model);
                            default:
                                break;
                        }
                    });
                });
        }
    }

    public getAllScoreCard(): Query {
        let company = store.getters.currentCompany;
        const modelCollection = this.firestore.collection(this.COLLECTION);
        return modelCollection.where("company_id", "==", company.id)
    }

}
