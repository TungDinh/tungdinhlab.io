import Model from './base';
import {store} from "@/store/store";
import firebase from "firebase";
import Query = firebase.firestore.Query;

export default class CandidateScorecard extends Model {
    scorecard: object = {
        "score": "", // good, bad
        "creation_date": null,
        "note": "", // feedback
        "sections": [] // [ { "list_criteria": [ {"criteria_name": "1234", "score": "poor" }, { "criteria_name": "1234", "score": "good" } ], "section_name": "Ten scorecard" } ]
    };
    version:number = 0
    candidate_id: string = "";
    position_id: string = "";
    acting_user_id: string = "";
    company_id:string="";
    acting_user: object = {};// User

    constructor() {
        super("CandidateScorecard");
    }

    static get instance(): CandidateScorecard {
        return new CandidateScorecard();
    }

    static listenRealtime(company_id?: string): void {
        if (company_id) {
            this.instance.collection().where("company_id", '==', company_id)
                .onSnapshot((snapshots) => {
                    snapshots.docChanges().forEach((change) => {
                        const model = this.from(change.doc.data(), change.doc.id);
                        switch (change.type) {
                            case "added":
                                this._onAdded(model);
                                break;
                            case "modified":
                                this._onUpdated(model);
                                break;
                            case "removed":
                                this._onDeleted(model);
                            default:
                                break;
                        }
                    });
                });
        }
    }

    public getCandidateScoreCards(candidate_id: string): Query {
        return this.firestore.collection(this.COLLECTION)
            .where('candidate_id', "==", candidate_id)
    }
    public async getCandidateScoreCardsByUserID(candidate_id:string,user_id:string): Promise<CandidateScorecard> {
        let results =  new CandidateScorecard()
        if (candidate_id && user_id) {
            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection.where('candidate_id', "==", candidate_id)
                .where('acting_user_id',"==",user_id).get();
            if(records.empty){
                return new CandidateScorecard()
            }
            Object.assign(results, records.docs[0].data());
            results.id = records.docs[0].id;
        }
        return results
    }
}
