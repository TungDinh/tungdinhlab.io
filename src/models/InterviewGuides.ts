import Model from './base';
import Company from "@/models/Company";
import {store} from "@/store/store";
import User from "@/models/User";
import {messages} from "@/messages";

export default class InterviewGuides extends Model {
    title: string = "";
    company_id : string = "";

    name: string = "";
    attachment_files = [];


    sections: Array<object> = [{
        section_name:'',
        list_criteria:[{criteria_name:""}]
    },{
        section_name:'',
        list_criteria:[{criteria_name:""}]
    }];
    constructor() {
        super("InterviewGuides");
    };
    public async getAllInterviewGuides(company_id:string): Promise<InterviewGuides[]>{
        let results: InterviewGuides[] = [];
        const modelCollection = this.firestore.collection(this.COLLECTION);
        await modelCollection
            .where("company_id", "==", company_id).get()
            .then(  async (querySnapshot)=> {
                querySnapshot.forEach(docSnapshot => {
                    let result = new InterviewGuides();
                    Object.assign(result, docSnapshot.data());
                    result.id = docSnapshot.id;
                    results.push(result);
                });
        });
        return results;
    }
    public async deleteInterviewGuideById(interview_guide_id:string) {
        if(interview_guide_id){
            const modelCollection = this.firestore.collection((this.COLLECTION));
            await modelCollection
                .where("id","==",interview_guide_id).get()
        }
    }



}
