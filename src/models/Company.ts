import Model from './base';
import firebase, {server_time} from "@/firebase";
import User from "@/models/User";
import {store} from "@/store/store";
import slug from "slugify";


export default class Company extends Model {
    name: string = "";
    slug: string = '';
    domain!: string;
    watchedIntroVideo: boolean=false;

    constructor() {
        super("companies");
    }

    static get instance(): Company {
        return new Company();
    }
    //
    // public async getCurrentCompany(): Promise<Company> {
    //     let user  = store.getters.currentUser;
    //     return new Company().getById(user.company_id) as Promise<Company>
    //
    // }

    onUsersChange(callback: Function) {
        const collection = this.firestore.collection(this.COLLECTION);
        collection.onSnapshot((snapshot)=>{
            snapshot.docChanges().forEach(docChange=>{
                if (typeof callback === "function") {
                    callback(
                        docChange.doc.id,
                        docChange.doc.data(),
                    );
                }
            })
        })
    }

    async update(): Promise<Company> {

        const modelCollection = this.firestore.collection(this.COLLECTION);

        let result = {...this};
        let id = result['id'];
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        // const {TIMESTAMP} = firebase.database.ServerValue;
        result.updated_date = server_time.now.valueOf();
        if(result.slug == ''){
            result.slug = slug(this.name);
        }

        result.updated_by =  store.getters.currentUser.id;

        // @ts-ignore
        const model = await modelCollection.doc(id).set(JSON.parse(JSON.stringify(result), {merge: true}));

        return this;
    }
    async getBySlug(slug:string): Promise<Company>{
        let result = new Company()
        let modelColections = this.firestore.collection(this.COLLECTION)
        await modelColections
            .where("slug","==",slug).get()
            .then(async (querySnapshot)=>{
                if(querySnapshot.empty == true){

                }
                else{
                    await querySnapshot.forEach(docSnapshot => {
                        Object.assign(result, docSnapshot.data());
                        result.id = docSnapshot.id;
                });
                }
            })
        return result
    }
    static listenRealtime(company_id?: string): void {
        if (company_id) {
            this.instance.collection().where("company_id", '==', company_id)
                .onSnapshot((snapshots) => {
                    snapshots.docChanges().forEach((change) => {
                        const model = this.from(change.doc.data(), change.doc.id);
                        switch (change.type) {
                            case "added":
                                this._onAdded(model);
                                break;
                            case "modified":
                                this._onUpdated(model);
                                break;
                            case "removed":
                                this._onDeleted(model);
                            default:
                                break;
                        }
                    });
                });
        } else {
            this.instance.collection()
                .onSnapshot((snapshots) => {
                    snapshots.docChanges().forEach((change) => {
                        const model = this.from(change.doc.data(), change.doc.id);
                        switch (change.type) {
                            case "added":
                                this._onAdded(model);
                                break;
                            case "modified":
                                this._onUpdated(model);
                                break;
                            case "removed":
                                this._onDeleted(model);
                            default:
                                break;
                        }
                    });
                });
        }
    }

    async getByHostname(hostname: string) : Promise<Company | null>
    {
        hostname = hostname.trim();
        if (!hostname) return null;

        const companyDocs = await this.collection()
            .where("hostname", "==", hostname)
            .limit(1)
            .get();

        if (companyDocs.empty) return null;
        const doc = companyDocs.docs[0];

        return Company.from(doc.data(), doc.id) as Company;
    }

    async getByAlias(alias: string) {
        alias = alias.trim();
        if (!alias) return null;

        const companyDocs = await this.collection()
            .where("domain", "==", alias)
            .limit(1)
            .get();

        if (companyDocs.empty) return null;
        const doc = companyDocs.docs[0];

        return Company.from(doc.data(), doc.id) as Company;
    }
}
