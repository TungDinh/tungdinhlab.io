export default function() {
    try {
        let listDomains: string[] | string = process.env.VUE_APP_LIST_DOMAINS + "";
        listDomains = listDomains.split(";");
        listDomains = listDomains
                            .map(dm=>dm.trim())
                            .filter(dm=>!!dm);
        return listDomains;
    } catch (e) {
        console.error(e);
        return [];
    }
}

