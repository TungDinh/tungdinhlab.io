// import {VueRouter} from "vue-router/types/router";
// import CustomerDomainList from "@/proxy/CustomerDomainList";
// import AppRouter from "@/router";
// import LandingPageRouter from '@/route/Landingpage';
// import Router from 'vue-router';
// import Vue from 'vue';
// import Company from "@/models/Company";
//
// Vue.use(Router);
//
// export type CJPProxyHandler = (router: VueRouter) => void;
// const customerDomainList = CustomerDomainList();
// const appURL = 'https://pro.cloudjetpotential.com';
//
// async function CJPProxy(handler: CJPProxyHandler) {
//     const hostname = window.location.hostname.trim();
//
//     if (customerDomainList.includes(hostname)) {
//         try {
//             const company = await Company.instance.getByHostname(hostname);
//             if (!company) {
//                 return handler(AppRouter);
//             }
//             const slug = company.slug;
//             const routes = LandingPageRouter;
//
//             routes.path = '/';
//             routes.props = {
//                 slug,
//                 default: {
//                     slug,
//                 },
//                 customClass: 'landing-page',
//             };
//             const router = new Router({
//                 mode: "history",
//                 base: '',
//                 routes: [routes],
//             });
//
//             return handler(router);
//         } catch (e) {
//             console.error(e);
//         }
//     } else {
//         const aliasMatch = hostname.match(/^([A-Za-z0-9\-]+)\.cloudjetpotential\.com$/);
//         const aliasName = aliasMatch && aliasMatch.length >= 1 ? aliasMatch[1] : "";
//         if (aliasName && !['pro', 'app', 'www'].includes(aliasName)) {
//             const company = await Company.instance.getByAlias(aliasName);
//             if (!company) {
//                 return location.href = appURL;
//                 // return handler(AppRouter);
//             }
//             const slug = company.slug;
//             const routes = LandingPageRouter;
//
//             routes.path = '/';
//             routes.props = {
//                 slug,
//                 default: {
//                     slug,
//                 },
//                 customClass: 'landing-page',
//             };
//             const router = new Router({
//                 mode: "history",
//                 base: '',
//                 routes: [routes],
//             });
//
//             return handler(router);
//         } else {
//             return handler(AppRouter);
//         }
//     }
//     // default: goto app
//     return handler(AppRouter);
// }
//
// export default CJPProxy;
//
