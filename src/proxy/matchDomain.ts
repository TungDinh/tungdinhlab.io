/**
 *  Match by [domain].cloudjetpotential.com
 *  which domain = [A-Za-z0-9\-]+
 *  @author Dan An
 *  @param {string} _hostname find other hostname, default will get current hostname
 */
import Company from "@/models/Company";

export function matchSubDomain(_hostname?: string) : string | null
{
    const hostname = _hostname || window.location.hostname.trim();
    const regex = /^([A-Za-z0-9\-]+)\.cloudjetpotential.com$/;

    const match = hostname.match(regex);
    if (!match || match.length < 2) return null;

    const subdomain = match[1];
    if (['app', 'qc', 'pro'].includes(subdomain)) return null;

    return subdomain;
}

/**
 * Math all hostname
 * @return company of this hostname
 * @author Dan An
 * @param {string} _hostname find other hostname, default will get current hostname
 */
export async function matchHostname(_hostname?: string): Promise<Company | null>
{
    try {
        const hostname = _hostname || window.location.hostname.trim();
        return await Company.instance.getByHostname(hostname);
    } catch (errorWhenMatchHostname) {
        console.error({errorWhenMatchHostname});
        return null;
    }
}
