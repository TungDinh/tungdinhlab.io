import Vuex, {Store} from "vuex";
import {Action, MutationTree, StoreOptions, Module} from "vuex";
import Vue from 'vue';
import firebase from 'firebase/app';
import SourceConnectData from "@/models/SourceConnectData";
import Query = firebase.firestore.Query;
import {PositionStage} from "@/store/dashboard/store";
import {reject} from "q";

Vue.use(Vuex);
export interface ModalConnectSourceData {
    listServices: Array<object>,
    unsubscribeConnectSourceData: any,

}

const store: Module<ModalConnectSourceData, any> = { // any for test
    namespaced: false,
    state: {
        listServices:[],
        unsubscribeConnectSourceData:null,

    },
    getters: {
        currentListService(state){
            return state.listServices;
        },
        unsubscribeConnectSourceData(state) {return state.unsubscribeConnectSourceData},
    },
    actions: {
        getListService: async (context) => {
            let query = new SourceConnectData().getAllSourceConnectData();
            if (context.state.unsubscribeConnectSourceData) {
                context.state.unsubscribeConnectSourceData;
            }
            context.state.unsubscribeConnectSourceData = query.onSnapshot((querySnapshot) => {
                let source_connect_data: SourceConnectData[] = [];
                querySnapshot.forEach(function(doc) {
                    let record = new SourceConnectData();
                        Object.assign(record, doc.data());
                        record.id = doc.id;
                        source_connect_data.push(record);
                });
                context.state.listServices = source_connect_data;
            });
        },
        fakeapi: async (context,data)=>{
            let response = {
                status : 200
            }
            if (!data.username || !data.password || !data.service){
                response.status = 400
            }
            return await new Promise((resolve, reject) => {
                setTimeout(()=>{
                    resolve(response);
                }, 1000)
            });

        }


    },
};
export default store;
