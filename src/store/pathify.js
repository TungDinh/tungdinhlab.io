// https://davestewart.github.io/vuex-pathify/#/setup/config
import pathify from 'vuex-pathify'
export default pathify

// options
// name 	path    state	getter	mutation	action	notes
// standard	/foo	foo	    foo	    SET_FOO	    setFoo	Used by most Vue developers
pathify.options.mapping = 'standard'

pathify.options.deep = true


