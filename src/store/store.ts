import Vue from 'vue';
import Vuex from 'vuex';
import firebase from 'firebase/app';
import Company from "@/models/Company";
import User from "@/models/User";
import Task from "@/models/Task";
import {abilityPlugin} from '@/permission/ability';
import axios from 'axios'
import {requestFunctionUrl} from '@/firebase/index';

// import pathify from 'vuex-pathify'
// pathify config
import pathify from './pathify' // <-- note the ./ denoting a local file!

Vue.use(Vuex);

import ModalModule from './modals/store';
import TemplateModule from './template/store';
import InterviewGuides from './interviewGuide/store';
import PortalModule from './portals/store';
import QuestionModule from './Questionnaires/store';
import PositionPipeline from './position_pipeline/store';
import Invite from "@/models/Invite";
import ScheduleInterviews from "./ScheduleInterviews/store";
import CandidateModule from "./candidate/store";
import DashboardModule from "./dashboard/store";
import Scorecard from "@/models/Scorecard";
import Tasks from  "./task/store";
import Candidate from "@/models/Candidate";
import KanbanPositionState from './position/kanban_store';
import InboxModule from "./inbox/store";
import ReportModule from "./report/store";
import Position from './position/store';
import Query = firebase.firestore.Query;
import Conversation from "@/models/Conversation";
import PositionPipelineAction from "@/models/PositionPipelineAction";
import sendMailManagerPositionModule from "@/store/SendMailManagerPosition/store";
import ConnectSourceDataModule from "./CrawlData/store";

export const store = new Vuex.Store<any>({ // any for test
    // https://davestewart.github.io/vuex-pathify/#/setup/config
    plugins: [
        pathify.plugin,
        abilityPlugin,
    ], // activate plugin
    modules: {
        modals: ModalModule,
        questionnaire: QuestionModule,
        portal: PortalModule,
        position_pipeline: PositionPipeline,
        templates: TemplateModule,
        schedule_interviews: ScheduleInterviews,
        interview_guide: InterviewGuides,
        position: Position,
        KanbanPosition:KanbanPositionState,
        tasks: Tasks,
        candidate: CandidateModule,
        dashboard: DashboardModule,
        inbox:InboxModule,
        report: ReportModule,
        sendMailManagerPositionModule,
        connectSourceData:ConnectSourceDataModule,
    },
    state: {
        user: null as User | null,
        users: [] as User[] | [],
        company: null as Company | null,
        task: null as Task | null,
        pending_invitation: [] as Invite[] | [],
        last_route: '/',
        score_cards: [] as Scorecard[] | [],
        unsubscribeCompany: null,
        unsubscribeUser: null,
        unsubscribeUsers: null,
        unsubscribeInvites: null,
        unsubscribeCompanyScorecard: null,
        loading_home:false,

    },
    getters: {
        currentUser: state => {
            return state.user;
        },
        companyUsers: state => {
            return state.users;
        },
        getTask: state => {
            if (state.task == null) {
                state.task = new Task();
            }
            return state.task;
        },
        currentCompany: state => {
            return state.company;
        },
        pendingInvitation: state => {
            return state.pending_invitation;
        },
        currentScorecard: state => {
            return state.score_cards;
        },
        loadingHome(state){
            return state.loading_home;
        }
    },
    mutations: {
        updateCurrentTask(state, task) {
            state.task = task;
            state.modals.addTask = true;
        },
        setUser(state, user) {
            state.user = user;
        },
        setCompanyUser(state, data) {
            const userIndex = state.users.findIndex(val => val.id === data.id);
            state.users[userIndex] = data;
        },
        setUsers(state, users) {
            state.users = users;
        },
        setCompany(state, company) {
            state.company = company;
        },
        clearCurrentTask(state) {
            state.task = new Task();
        },
        setPendingInvite(state, invites) {
            state.pending_invitation = invites;
        },
        setScorecard(state, scorecards) {
            state.score_cards = scorecards;
        },
    },
    actions: {
        setUser: context => {
            return new Promise((resolve) => {
                firebase.auth().onAuthStateChanged(async fuser => {
                    if (fuser) {
                        let user = await new User().getById(fuser.uid) as User;
                        let company = await new Company().getById(user.company_id) as Company;
                        if(company.slug == '') company.update();

                        context.commit("setUser", user);
                        context.commit("setCompany", company);

                        let query = new Company().getDocumentById(user.company_id);
                        if (context.state.unsubscribeCompany) {
                            context.state.unsubscribeCompany();
                        }
                        context.state.unsubscribeCompany = query.onSnapshot((doc) => {
                            let record = new Company();
                            Object.assign(record, doc.data());
                            record.id = doc.id;
                            context.commit("setCompany", record);
                        });

                        let query1 = new User().getDocumentById(fuser.uid);
                        if (context.state.unsubscribeUser) {
                            context.state.unsubscribeUser();
                        }
                        context.state.unsubscribeUser = query1.onSnapshot((doc) => {
                            let record = new User();
                            Object.assign(record, doc.data());
                            record.id = doc.id;
                            context.commit("setUser", record);
                        });

                        let query2: Query = User.instance.getAllUsers(user.company_id);
                        if (context.state.unsubscribeUsers) {
                            context.state.unsubscribeUsers();
                        }
                        context.state.unsubscribeUsers = query2.onSnapshot((querySnapshot) => {
                            let users: User[] = [];
                            querySnapshot.forEach(function(doc) {
                                let record = new User();
                                Object.assign(record, doc.data());
                                record.id = doc.id;
                                users.push(record);
                            });
                            context.state.users = users;
                        });

                        // @ts-ignore
                        window.user_email = user.email;
                        // @ts-ignore
                        window.user_org = company.name;

                        context.dispatch('getScorecard');
                        context.dispatch('getInvitations');
                        context.dispatch('getPortal', company.id);
                        context.dispatch('getListPipeline', company.id);
                        await context.dispatch('getListPosition');
                        context.state.loading_home = false
                    } else {
                        // Router.push('/login')
                        /**
                         * Reload page for clear vuex store data
                         */
                        if (Vue.$router.currentRoute.path !== '/login')
                            window.location.href = '/login';
                    }
                    resolve();
                });
            });
        },
        getInvitations: async context => {
            let fuser = firebase.auth().currentUser;
            context.state.pending_invitation = [];

            if (fuser) {
                let user = await new User().getById(fuser.uid) as User;
                let query: Query = new Invite().getAllInvites(user.company_id);

                if (context.state.unsubscribeInvites) {
                    context.state.unsubscribeInvites();
                }
                context.state.unsubscribeInvites = query.onSnapshot((querySnapshot) => {
                    let invites: Invite[] = [];
                    querySnapshot.forEach(function(doc) {
                        let record = new Invite();
                        Object.assign(record, doc.data());
                        record.id = doc.id;
                        invites.push(record);
                    });
                    context.state.pending_invitation = invites;
                });

            } else {
                Vue.$router.push('/login')
            }

        },
        setCurrentCompany: async (context, data) =>{
            context.commit('setCompany',data)
        },
        getScorecard: async context => {
            let fuser = firebase.auth().currentUser;
            context.state.score_cards = [];

            if (fuser) {
                let user = await new User().getById(fuser.uid) as User;

                let query: Query = new Scorecard().getAllScoreCard();
                if (context.state.unsubscribeCompanyScorecard) {
                    context.state.unsubscribeCompanyScorecard();
                }
                context.state.unsubscribeCompanyScorecard = query.onSnapshot((querySnapshot) => {
                    let scorecards: Scorecard[] = [];
                    querySnapshot.forEach(function(doc) {
                        let record = new Scorecard();
                        Object.assign(record, doc.data());
                        record.id = doc.id;
                        scorecards.push(record);
                    });
                    context.state.score_cards = scorecards;
                });
            } else {
                Vue.$router.push('/login')
            }
        },

        async getUser(context, uid: string): Promise<User | undefined> {
            const users = context.getters.companyUsers as User[];
            return users.find(u=>{
                return u.id === uid;
            });
        },
        // validateUrlResume(context,url){
        //     let regexp = new RegExp(/(.*?)\.(docx|txt|doc|pdf)$/gm)
        //     let validate_type_file_resume = regexp.test(url)
        //     return validate_type_file_resume
        // },
        // sendEmailManagerPosition(context,data_send_email){
        //     let url = `${window.location.origin}/candidate/${data_send_email.candidate.id}/experience`
        //     let data = {
        //             data:{
        //                 candidate_name: data_send_email.candidate.name,
        //                 position_name: data_send_email.position.title,
        //                 link: url,
        //                 position_created_by:data_send_email.position.created_by,
        //                 position_assign_email:data_send_email.position.position_assign.email,
        //                 candidate_photo_url:data_send_email.candidate.profile_photo_url
        //             }
        //         }
        //     axios.post(requestFunctionUrl('sendManagerPosition'), data)
        //         .then(function (response) {
        //             console.log(response)
        //         })
        //         .catch(function (error) {
        //             console.log(error)
        //         });
        // },
        deleteActionPipeline(context ,position){
            new PositionPipelineAction().getActionsImported(position.id, position.pipeline_id).then((actionData)=>{
                actionData.forEach((data)=>{
                    let indexActionSendMail = data.actions.findIndex((action)=>{return action['action_type'] == 'emails'});
                    if (indexActionSendMail > -1){
                        data.actions.splice(indexActionSendMail, 1);
                        data.update();
                    }
                })
            })
        }
    },
});
