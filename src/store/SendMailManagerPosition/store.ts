import {Module} from "vuex";
import axios from "axios";
import {requestFunctionUrl} from "@/firebase";
import Candidate from "@/models/Candidate";
import Position from "@/models/Position";
import {urlCandidate} from '@/helpers/joblinks';

export interface ISendMailManagerPositionModuleState {

}

export interface IDataSendMail {
    candidate: Candidate,
    position: Position,
}

const sendMailManagerPositionModule: Module<ISendMailManagerPositionModuleState, any> = {
    actions: {
        validateUrlResume(context,url){
            let regexp = new RegExp(/(.*?)\.(docx|txt|doc|pdf)$/gm)
            let validate_type_file_resume = regexp.test(url)
            return validate_type_file_resume
        },
        async sendEmailManagerPosition(context, data_send_email: IDataSendMail){
            try {
                //const url = `${window.location.origin}/candidate/${data_send_email.candidate.id}/experience`;
                const url = urlCandidate(data_send_email.candidate.id);
                const data = {
                    data: {
                        candidate_name: data_send_email.candidate.name,
                        position_name: data_send_email.position.title,
                        link: url,
                        position_created_by:data_send_email.position.created_by,
                        position_assign_email:data_send_email.position.position_assign.email,
                        candidate_photo_url:data_send_email.candidate.profile_photo_url
                    }
                };

                const response = await axios.post(requestFunctionUrl('sendManagerPosition'), data);
                console.log(response);
            } catch (error) {
                console.log({error});
            }
        },
    }
};

export default sendMailManagerPositionModule;
