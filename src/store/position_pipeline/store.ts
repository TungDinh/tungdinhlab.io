import {Action, MutationTree, StoreOptions, Module} from "vuex";
import Vue from 'vue';
import Vuex from 'vuex';
import PipelineStage from "@/models/PipelineStage";
import Pipeline from "@/models/Pipeline";
import Position from "@/models/Position";
const _ = require('lodash');

Vue.use(Vuex);

export interface PositionPinelineState {
    listPipeline: Array<object>,
    listPipelineStage: Array<object>,
    currentPipeline: object,
    currentPipelineStage: object,
    cachePipeline: object
    defaultPipelineStage: Array<object>,
    pipeline_stages: object
}

const store: Module<PositionPinelineState, any> = {
    // namespaced: true,
    state: {
        listPipeline: [],
        listPipelineStage: [],
        currentPipeline: {
            used_positions: []
        },
        currentPipelineStage: {},
        cachePipeline: {},
        defaultPipelineStage:[],
        pipeline_stages: {} // Stages by pipeline {<pipeline_id1>: [stage1, stage2]}
    },
    mutations: {
        setDefaultPipelineStage(state, data){
            state.defaultPipelineStage = data;
        },
        setCachePipeline(state, data) {
            state.cachePipeline = data;
        },
        setListPipeline(state, data) {
            state.listPipeline = data;
        },
        setListPipelineStage(state, data) {
            state.listPipelineStage = [];
            state.listPipelineStage = data.map((stage)=>{
                let result = stage.cloneNewObject();
                return result;
            });
            // Object.freeze(state.listPipelineStage)

        },
        setCurrentPipeline(state, data) {
            state.currentPipeline = data;
        },
        setCurrentPipelineStage(state, data) {
            state.currentPipelineStage = data;
        }
    },
    getters: {
        defaultPipelineStage(state){
            return state.defaultPipelineStage;
        },
        cachePipeline(state, data) {
            return state.cachePipeline;
        },
        listPipeline(state) {
            return state.listPipeline;
        },
        listPipelineStage(state) {
            return state.listPipelineStage;
        },
        pipelineStages(state) {
            return state.pipeline_stages;
        },
        currentPipeline(state) {
            return state.currentPipeline;
        },
        currentPipelineStage(state) {
            return state.currentPipelineStage;
        },
    },
    actions: {
        getListPipelineStage: (context, type)=>{
            console.log(type);
            return new Promise((resolve, reject)=>{
                new PipelineStage().getAllStages(type).then((data)=>{
                    context.dispatch('setListPipelineStage', data);
                    resolve(data)
                })
            })
        },
        getListPipeline: (context, company_id)=>{
            return new Promise((resolve)=> {
                // @ts-ignore
                new Pipeline().getAllPipelines(company_id).then((pipelines)=>{
                    context.dispatch('setListPipeline', pipelines);
                    pipelines.forEach((pipeline) => {
                        new PipelineStage().getAllStages(pipeline.id).then((stages)=>{
                            let pipeline_stages = {};
                            Object.assign(pipeline_stages, context.state.pipeline_stages);
                            pipeline_stages[pipeline.id] = stages;
                            context.state.pipeline_stages = pipeline_stages;
                        })
                    });
                    resolve(pipelines);
                })
            })
        },
        editListPipeline: (context, data) => {
            return new Promise(async resolve => {
                switch (data.type) {
                    case 'remove':
                        let pipeline = data.content[data.index];
                        pipeline.deleteAllStateByID(pipeline.id);
                        pipeline.delete();

                        data.content.splice(data.index, 1);
                        context.dispatch('setListPipeline', data.content);
                        break;
                    case 'duplicate':
                        let _newPositionPipeline = new Pipeline();
                        Object.assign(_newPositionPipeline, data.content[data.index]);
                        _newPositionPipeline.is_default = false;
                        _newPositionPipeline.title+= ' (Copy)';
                        _newPositionPipeline.used_positions = [];
                        delete _newPositionPipeline['list_stage'];

                        _newPositionPipeline.insert().then(async (_data) => {
                            const pArray = data.content[data.index]['list_stage'].map(async (stage) => {
                                if (stage.pipeline_id != '' && stage.pipeline_id != 'default') {
                                    let _newState = new PipelineStage();
                                    Object.assign(_newState, stage);
                                    _newState.pipeline_id = _data.id;
                                    await _newState.insert().then((response) => {
                                        stage.id = response.id;
                                    });
                                }
                                return stage;
                            });
                            await Promise.all(pArray);
                            _newPositionPipeline.update().then((response) => {
                                data.content.push(response);
                                context.dispatch('setListPipeline', data.content);
                                resolve();
                            });
                        });
                        break;
                }
            })
        },
        setCachePipeline: async (context, data) => {
            context.commit("setCachePipeline", data);
        },
        setCurrentPipelineStage: async (context, data) => {
            context.commit("setCurrentPipelineStage", data);
        },
        setCurrentPipeline: async (context, data) => {
            context.commit("setCurrentPipeline", data);
        },
        setListPipelineStage: async (context, data) => {
            context.commit("setListPipelineStage", data);
        },
        setListPipeline: async (context, data) => {
            context.commit("setListPipeline", data);
        },
        setListStageToPipeline: async (context, data) =>{
            let listPipeline =  context.getters['listPipeline']
            let indexPipeline = listPipeline.findIndex((pipeline)=>{
                return pipeline.id == data.id;
            })
            listPipeline[indexPipeline]['list_stage'] = data.content;
            context.dispatch('setListPipeline', listPipeline);
        }
    },
};

export default store;
