import {Action, MutationTree, StoreOptions, Module} from "vuex";
import Vuex from 'vuex';
import firebase from 'firebase/app';
import Portal from "@/models/Portal";
import Router from '../../router';
import {async} from "q";
import User from "@/models/User";
import Scorecard from "@/models/Scorecard";

export interface PortalState {
    portal: object,
    origin_portal:object,
}

const store : Module<PortalState ,any> = { // any for test
    state: {
        portal: {
            messaging_introduction: ''
        },
        origin_portal:{},
    },
    getters: {
        currentPortal: state => {
            return state.portal
        },
        currentOriginPortal:state => {
            return state.origin_portal
        }
    },
    mutations: {
        setCurrentPortal(state, portal) {
            console.log('portal',portal)
            state.origin_portal = portal;
            state.portal = portal.cloneNewObject();

        },
        updateCurrentPortal(state, portal) {
            console.log('portal',portal)
            state.origin_portal = portal;
        },
        setNewPortal(state, data) {
            state.portal = data.map((stage)=>{
                delete stage['firestore'];
                return stage;
            });
        },
    },
    actions: {
        setNewPortal: async (context, data) => {
            context.commit("setNewPortal", data);
        },
        setCurrentPortal: async (context, data) => {
            context.commit("setCurrentPortal", data);
        },
        updateCurrentPortal: async (context,data) => {
            context.commit("updateCurrentPortal", data);
        },
        getPortal: async (context,company_id) => {
            // await Portal.listenRealtime();
            await new Portal().getAllPortal(company_id).then((data)=>{
                    context.commit('setCurrentPortal',data)
                })
        },
    },
};
export default store;
