import Vuex, {Store} from "vuex";
import {router} from "@/route/Landingpage";
import Portal from "@/models/Portal";
import Company from "@/models/Company";
import Position from '@/models/Position';
import Candidate from "@/models/Candidate";
import {groupBy as _groupBy} from 'lodash'; // if using only groupBy, just import groupBy
import {messages} from "@/messages";
import Vue from 'vue';
import kanban_store from "@/store/position/kanban_store";
import sendMailManagerPositionModule from "@/store/SendMailManagerPosition/store";
import Question from "@/models/Question";

Vue.use(Vuex);
export interface LandingPageState {
    portal_lading_page: object,
    company_landing_page: Company | {},
    positions_landing_page: Position[],
    // list_all_postions:Array<object>,
    list_department:Array<object>,
    list_location:Array<object>,
    position_view:object,
    show_menu:boolean,
    candidate:object,
    show_menu_link:boolean,
    show_footer:boolean,
    questions: Array<object>
}

const store = new Store<LandingPageState>({ // any for test
    modules: {
        kanban_store,
        sendMailManagerPositionModule,
    },
    state: {
        portal_lading_page: {},
        company_landing_page:{},
        positions_landing_page:[],
        list_department:[],
        list_location:[],
        position_view:{},
        show_menu: true,
        candidate:{},
        show_menu_link: true,
        show_footer: true,
        questions: [],
    },
    getters: {
        currentPortalLandingPage: state => {
            return state.portal_lading_page
        },
        currentCompanyLandingPage:state => {
            return state.company_landing_page
        },
        currentPositionLandingPage:state => {
            return state.positions_landing_page
        },
        currentDepartmentsLandingPage:state => {
            return state.list_department
        },
        currentLocationsLandingPage:state => {
            return state.list_location
        },
        convertLinkToEmbedLink:state=>{
            let link = ""
            if(state.portal_lading_page['vimeo_video']){

                link = state.portal_lading_page['vimeo_video'].replace("vimeo.com/","player.vimeo.com/video/")
            } else if(state.portal_lading_page['youtube_video']){
                link =  state.portal_lading_page['youtube_video'].replace("watch?v=","embed/")
            }else{
                // link = 'https://player.vimeo.com/video/watch'
                link = ''
            }
            return link
        },
        currentPositionView:state => {
            return state.position_view
        },
        showMenu:state => {
            return state.show_menu
        },
        currentCandidateLandingPage:state => {
            return state.candidate
        },
        showMenuLink:state => {
            return state.show_menu_link
        },
        showFotter:state => {
            return state.show_footer
        },
        currentQuestionsLandingPage:state => {
            return state.questions
        }
    },
    mutations: {
        setCurrentPortalLandingPage(state, portal) {
            state.portal_lading_page = portal;
        },
        setCurrentCompanyLandingPage(state, company) {
            state.company_landing_page = company;
        },
        setCurrentPositionsLandingPage(state, positions){
            state.positions_landing_page = positions
        },
        // setCurrentAllPositionsLandingPage(state,positions){
        //     state.list_all_postions = positions
        // },
        setCurrentDepartmentsLandingPage(state,departments){
            state.list_department = departments
        },
        setCurrentLocationsLandingPage(state,locations){
            state.list_location = locations
        },
        setCurrentPositionView(state, position){
            state.position_view = position
        },
        setShowMenu(state,show){
            state.show_menu = show
        },
        setCandidate(state,newcadidate){
            state.candidate= newcadidate
        },
        setQuestionsLandingPage(state, data) {
            state.questions = data;
        },
    },
    actions: {
        getQuestionsLandingPage: async (context, {questionnaire_id, company_id}) => {
            let results: Question[] = new Array();
            results = await new Question().getAllQuestion(questionnaire_id, company_id);
            context.commit("setQuestionsLandingPage", results);
            return results;
        },
        async getCompanyLandingPageByDomain({commit}, domain) {
            const com = await Company.instance.getByAlias(domain);
            if (!com) {
                throw new Error("Company not found");
            }

            commit('setCurrentCompanyLandingPage', com);
        },
        getPortalLandingPage: async (context,company_id) => {
            // await Portal.listenRealtime();
            const data = await new Portal().getAllPortal(company_id);
            context.commit('setCurrentPortalLandingPage',data)
        },
        getPositionsPublishLandingPage: async (context,company_id)=>{
            await new Position().getPublishPositions(company_id).then((data)=>{
                context.dispatch('AddFiledGroupLocations',data)


            })
        },
        // getAllPositionsLandingPage: async (context,company)=> {
        //     await new Position().getAllPositions(company).then((data)=>{
        //         context.commit('setCurrentAllPositionsLandingPage',data)
        //     })
        // },
        getDepartmentsLandingPage:async (context)=>{
            let new_group_by_departments = _groupBy(context.state.positions_landing_page, 'department')
            let new_departments = Object.keys(new_group_by_departments).map((department)=>{
                let text_department = department
                if (department == '')  text_department = messages.SR_046_FILTER_DEPARTMENT_ORTHER
                return {
                    value: department,
                    text: text_department
                }
            })
            console.log(new_departments)
            context.commit('setCurrentDepartmentsLandingPage',new_departments)
        },
        getLocationsLandingPage:async (context)=>{
            let list_position = _groupBy(context.state.positions_landing_page, 'group_location')
            let new_locations = Object.keys(list_position)
            console.log(new_locations)
            context.commit('setCurrentLocationsLandingPage',new_locations)
        },
        AddFiledGroupLocations:(context,list_positions)=>{
            let new_list_position = []
            if(list_positions.length > 0){
                new_list_position = list_positions.map(position => {

                    position['group_location'] = [position.location.title ]
                    if(position.state){
                        position['group_location'].unshift(position.state)
                    }
                    position['group_location'] = position['group_location'].join(', ').trim()

                    return position
                })
                context.commit('setCurrentPositionsLandingPage',new_list_position)
                context.dispatch('getDepartmentsLandingPage');
                context.dispatch('getLocationsLandingPage');
            }else{
                context.commit('setCurrentPositionsLandingPage',new_list_position)
            }
        },
        setPositionView: async (context, position_id)=>{
            try {
                if(position_id){
                    await new Position().getById(position_id).then((data)=>{
                        if(data['status'] == 'published' && data['candidate_type'] != "disabled"){
                            context.commit('setCurrentPositionView', data)
                        }else {
                            router.push({name: 'Home'});
                        }
                    })
                } else {
                    router.push({name: 'Home'});
                }
            } catch (e) {
                router.push({name: 'Home'});
            }
        },
        checkCurrentNamePage:(context,name_page)=>{
            let show_menu = true
            let show_menu_link = true
            let show_footer = true
            switch (name_page) {
                case "LandingPageHome":
                    show_menu = true
                    break
                case "LandingPageDetailPosition":
                    show_menu = false
                    break
                case "LandingPageApplyPosition":
                    show_menu = false
                    show_menu_link = false
                    break
                case "LandingPageApplyPositionSubmitted":
                    show_menu = false
                    show_menu_link = false
                    show_footer = false
                    break
                default:
                   //nothing
                    break
               }
            context.commit('setShowMenu', show_menu)
            context.state.show_menu_link = show_menu_link;
            context.state.show_footer = show_footer;
        },
        createCandidate:(context)=>{
            let candidate = new Candidate()
            let candidate_clone = candidate.cloneNewObject()
            candidate_clone['work_history'] = []
            candidate_clone['education'] = []
            // convert to object instead of firebase
            context.commit('setCandidate',candidate_clone)
        }

    },
});
export default store;
