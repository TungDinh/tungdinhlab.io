import {Action, MutationTree, StoreOptions, Module} from "vuex";
import Vuex from 'vuex';
import Router from "@/router";
import Task from "@/models/Task"
import _ from "lodash";
import User from "@/models/User";
import {messages} from "@/messages";
import Scorecard from "@/models/Scorecard";
import {firestore} from "firebase";
import Query = firestore.Query;
import axios from "axios";
import moment from "moment";
import {requestFunctionUrl, server_time} from '@/firebase/index'
export interface Tasks {
    _tasks: Array<Task>,
    _tasksCandidate: Array<object>,
    _selectedTask: object,
    filter_field: string,
    unsubscribeCandidateTasks: any,
    unsubscribeTasks: any,
}

const store: Module<Tasks, any> = {
    //namespaced: true,
    state: {
        _tasks: [],
        _tasksCandidate: [],
        _selectedTask: {},
        filter_field: "assignee_id",
        unsubscribeCandidateTasks: null,
        unsubscribeTasks: null,
    },
    mutations: {
        setTasks(state, data) {
            state._tasks = data;
        },
        setTaskCandidate(state,data){
            state._tasksCandidate = data;
        }
    },
    getters: {
        tasks(state){
            return state._tasks
        },
        tasksCandidate(state){
            return state._tasksCandidate
        },
        currentTask(state) {
            return state._selectedTask;
        },
        filterField: state => {
            return state.filter_field;
        },
        unsubscribeCandidateTasks(state) {
            return state.unsubscribeCandidateTasks;
        },
        unsubscribeTasks(state) {
            return state.unsubscribeTasks
        }
    },
    actions: {
        clearTasksCandidate: (context)=>{
            context.state._tasksCandidate = []
        },
        setFilterField: (context, field) => {
            context.state.filter_field = field;
        },
        setSelectedTask: (context, task) => {
            context.state._selectedTask = task;
        },
        createdTasks: async (context, {task, users}) => {
            let list_users = users.map((user) => {
                return new Promise((resolve, reject) => {
                    let new_task = new Task();
                    Object.assign(new_task, task);
                    new_task.company_id = context.getters.currentCompany.id;
                    new_task.assignee = user.cloneNewObject();
                    new_task.assignee_id = user.id;
                    new_task.type = "created";

                    new_task.insert().then((task: any) => {
                        let data = {
                            toWho:{
                                name: task.assignee.name,
                                email: task.assignee.email,
                            },
                            data:{
                                company_name: context.getters.currentCompany.name,
                                task_name: task.title,
                                task_deadline: moment(task._deadline).format("L"),
                                description: task.description,
                                link: `${window.location.origin}/tasks/${task.id}/`,
                                task_type: task.type,
                                }
                            };
                        axios.post(requestFunctionUrl('sendEmailTask'), data)
                            .then(function (response) {
                                console.log(response)
                            })
                            .catch(function (error) {
                                console.log(error)
                            });
                        resolve();
                    });
                })
            });
            return Promise.all(list_users);
        },

        updateTaskAction: async (context, {task, users}) =>{
            let update_task = new Task();
            if(users.length == 0){
                 Object.assign(update_task, task);
                 update_task.type = 'updated';
                 return update_task.update();
            }else {
                let res = users.map((user) => {
                    return new Promise((resolve, reject) => {
                        Object.assign(update_task, task);
                        if(update_task.assignee_id != user.id){
                            update_task.assignee = user.cloneNewObject();
                            update_task.assignee_id = user.id;
                            update_task.type = 'updated'
                        }
                        update_task.update().then((task: any) => {
                            let data = {
                                toWho:{
                                    name: task.assignee.name,
                                    email: task.assignee.email,
                                },
                                data:{
                                    company_name: context.getters.currentCompany.name,
                                    task_name: task.title,
                                    task_deadline: moment(task._deadline).format("L"),
                                    description: task.description,
                                    link: `${window.location.origin}/tasks/${task.id}/`,
                                    task_type: task.type,
                                }
                            };
                            axios.post(requestFunctionUrl('sendEmailTask'), data)
                                .then(function (response) {
                                    console.log(response)
                                })
                                .catch(function (error) {
                                    console.log(error)
                                });
                            resolve();
                        })
                    })
                });
                return Promise.all(res);
            }
        },

        markCompleteTask: async (context, {task, user}) =>  {
            task.add_log.push({
                timeUpdate: server_time.now,
                title: messages.SR_009_TASK_AS_COMPLETE,
                created_by: user.cloneNewObject()
            });
            task.type = 'updated';
            task.is_done = true;
            return task.update();
        },

        removeTask: async (context, task)=>{
            let _task = new Task();
            Object.assign(_task, task);
            return _task.delete();
        },

        getTask: async (context) => {
             new Task().getAllTasks().then((data) => {
                context.commit("setTasks", data)
            });
        },

        getFilteredTasks: async (context, {value, position_id}) => {
            let query: Query = new Task().getFilteredTasks(context.state.filter_field, value, position_id);
            if (context.state.unsubscribeTasks) {
                context.state.unsubscribeTasks();
            }
            context.state.unsubscribeTasks = query.onSnapshot((querySnapshot) => {
                let tasks: Task[] = [];
                querySnapshot.forEach(function(doc) {
                    let record = new Task();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    tasks.push(record);
                });
                context.commit('setTasks', tasks);
            });
            return new Promise((resolve, reject) => {
                query.get().then(() => {
                    resolve();
                })
            })
        },

        getTasksCandidate: async (context, {filter_field, value, candidate_id}) => {
            let query: Query = new Task().getTasksCandidate(filter_field, value, candidate_id);

            if (context.state.unsubscribeCandidateTasks) {
                context.state.unsubscribeCandidateTasks();
            }
            context.state.unsubscribeCandidateTasks = query.onSnapshot((querySnapshot) => {
                let tasks: Task[] = [];
                querySnapshot.forEach(function(doc) {
                    let record = new Task();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    tasks.push(record);
                });
                context.commit('setTaskCandidate', tasks);
            });
            return new Promise((resolve, reject) => {
                query.get().then(() => {
                    resolve();
                })
            })
        },

        getTaskByID: async (context, task_id): Promise<Task> => {
            let index = context.state._tasks.findIndex((item: any) => { return item.id == task_id});
            let task = {} as Task;
            if (index != -1) {
                task = context.state._tasks[index];
            }
            context.dispatch('setSelectedTask', task);
            return task;
        },

        hideMyTaskIsDone: (context, task) => {
            let index = context.state._tasksCandidate.findIndex((item: any) => {return item.id== task.id});
            if (index != -1) {
                context.state._tasksCandidate.splice(index, 1)
            }
        },

        setDefaultFilterField: (context) => {
            context.state.filter_field = "assignee_id"
        }

    }
};

export default store;
