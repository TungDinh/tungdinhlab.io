import {Action, MutationTree, StoreOptions, Module} from "vuex";
import Vuex from 'vuex';
import User from "@/models/User";
import Company from "@/models/Company";
import Router from "@/router";
import CandidateScorecard from "@/models/CandidateScorecard"
import _ from "lodash";

export interface ModalScoreCardCandidate {

}

const store: Module<ModalScoreCardCandidate, any> = {
    //namespaced: true,
    state: {

    },
    mutations: {

    },
    getters: {

    },
    actions: {


    }
};

export default store;
