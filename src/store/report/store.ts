import {Action, MutationTree, StoreOptions, Module} from "vuex";
import Vue from 'vue';
import Vuex from 'vuex';
import Candidate from "@/models/Candidate";

import PositionView from "@/models/PositionView";
// import OverViewReportModule from '@/store/report/overview/store'
import {IAnalyticsTimeRange} from "@/components/Analytics/AnalyticsTimeRange";
import AnalyticsTimeRanges from "@/components/Analytics/AnalyticsTimeRange";
import { getField, updateField } from 'vuex-map-fields';
import AnalyticsPositionFilterBySystemOptions, {IAnalyticsPositionFilterOption} from "@/components/Analytics/AnalyticsPositionFilterOption";
import * as firebase from "firebase";
import Moment from 'moment';
import _ from 'lodash'
import PipelineReport from "@/models/PipelineReport";
import Position from "@/models/Position";
import Pipeline from "@/models/Pipeline";
import PipelineStage from "@/models/PipelineStage";
import { make } from 'vuex-pathify'
import pipeline from "@/components/company/pipeline.vue";
import DeletedPipeline from "@/models/DeletedPipeline";
// import Positions from "@/views/Positions.vue";
import {IPositionCategory, positioncategories} from "@/common/positioncategories";
import router from "@/router";
import Query = firebase.firestore.Query;
import QuerySnapshot = firebase.firestore.QuerySnapshot;
import Model from "@/models/base";
import {server_time} from "@/firebase";

// import colors from 'nice-color-palettes'// not work
const colors = require('nice-color-palettes');

Vue.use(Vuex);


// console.warn(`AnalyticsTimeRanges1111: ${AnalyticsTimeRanges[0].start.format()}`)
export interface IPipelineWithStages {
    pipeline: Pipeline,
    stages: PipelineStage[],
}
export interface IXPipelines{
    // alive: Pipeline[],
    alive: IPipelineWithStages[],
    deleted: DeletedPipeline[],
}

export interface IPipelineReportsGroupByPipeline{
    pipeline: Pipeline,
    stages: PipelineStage[],
    pipeline_reports: PipelineReport[],
}


export interface IStageCount {
    [stage_id:string]:number
}
export interface IStageCountByAttrID {
    [attr_id:string]:IStageCount
}
export interface IStageCountByPosition extends IStageCountByAttrID{
    [position_id:string]:IStageCount
}
export interface IStageCountByCategory extends IStageCountByAttrID{
    [category_value:string]:IStageCount
}
export type NumberString=number|string;

// export interface IA {
//     [date:string]:NumberString,
//     stage_name:string
// }
export interface IPipelineReportsGroupByPipelineFullfillDates{
    pipeline: Pipeline,
    stages: PipelineStage[],
    movement_pipeline_reports_group_by_date: {[date:string]:{[stage_id:string]:PipelineReport[]}},
    movement_pipeline_reports: {[date:string]:NumberString, stage_name:string }[],
    full_pipeline_reports: {[stage_id:string]:{[date:string]:PipelineReport[]}},
}

export interface IPipelineFunnelReport {
    pipeline: Pipeline,
    stages: PipelineStage[],
    funnel_reports_count:IStageCount ,
    total_unique_candidates:number,
}



export interface IXPipelineFunnelReport {
    pipeline: Pipeline,
    stages: PipelineStage[],
    funnel_reports_count:IStageCount ,
    total_unique_candidates:number,
    prev_funnel_reports_count:IStageCount ,
    prev_total_unique_candidates:number,
}

export interface IPipelineInStageByPosition {
    pipeline: Pipeline,
    stages: PipelineStage[],
    positions: Position[],
    pipeline_report_by_positions:IStageCountByPosition ,
    pipeline_report_all_positions: IStageCount,

}


export interface IPipelineInStageByCategory {
    pipeline: Pipeline,
    stages: PipelineStage[],
    categories: IPositionCategory[],
    pipeline_report_by_categories:IStageCountByCategory,
    pipeline_report_all_categories:IStageCount,

}
export interface IPipelinePerformanceData {
    pipeline: Pipeline,
    stages:PipelineStage[],
    funnel_progression:IXPipelineFunnelReport,
    time_in_stage:{
        by_position: IPipelineInStageByPosition,
        by_category:IPipelineInStageByCategory
    },
    movement:IPipelineReportsGroupByPipelineFullfillDates
}
export interface ICandidatePipelineStages {
    position: Position,
    candidates:Candidate[],
    pipeline: Pipeline,
    stages:PipelineStage[],
    candidates_group_by_stages: {[stage_id:string]:Candidate[]}
}

// export interface IPipelinePerformanceData {
//     pipeline: Pipeline,
//     stages:PipelineStage[],
//     funnel_progression:{
//         funnel_reports_count:{[stage_id:string]:number} ,
//         total_unique_candidates:number,
//         prev_funnel_reports_count:{[stage_id:string]:number} ,
//         prev_total_unique_candidates:number,
//     },
//     time_in_stage:{
//         by_position: {
//             positions: Position[],
//             pipeline_report_by_positions:{[position_id:string]:{[stage_id:string]:number}} ,
//             pipeline_report_all_positions: {[stage_id:string]:number},
//         },
//         by_category:{
//             categories: IPositionCategory[],
//             pipeline_report_by_categories:{[category_value:string]:{[stage_id:string]:number}} ,
//             pipeline_report_all_categories:{[stage_id:string]:number},
//         }
//     },
//     movement:{
//         movement_pipeline_reports_group_by_date: {[date:string]:{[stage_id:string]:PipelineReport[]}},
//         movement_pipeline_reports: {[stageid:string]:number}[],
//         full_pipeline_reports: {[stage_id:string]:{[date:string]:PipelineReport[]}},
//     }
// }




// https://stackoverflow.com/questions/49291360/vuex-sharing-common-functions-across-modules
// only can use in Vuex store, can not module
const myTruncatePlugin = store => {
    store.truncate = function(str) {
        return str.replace(/-/g, '') + ' ...was truncaaaated!'; // example implementation
    }
    // Note: I know this re-defines truncate() every time. This is just an example.
    // If it is such a problem, you could create it outside and just assign it here.
};


// https://stacko;verflow.com/a/42452721
const setCategories = (state) => {
    state.categories = state.products.map(product => product.category)
};

// const getAverageMovementTimeByStages=(rps_obj:{rps:PipelineReport[]}): {[stage_id:string]:number} => {
const getAverageMovementTimeByStages=(arr:PipelineReport[]): IStageCount => {
    // const c:{rps:{[stage_id:string]:number}} = _.mapValues(rps_obj,
    //     (arr:PipelineReport[])=>{
        return  _.mapValues(
            _.reduce(
                _.groupBy(arr, (p:PipelineReport) => p.stage.id) as {[stage_id:string]:PipelineReport[]},
                (arr_total, arr1:PipelineReport[], stage_id:string)=>{
                    // remove report which candidate has not move out stage yet
                    arr_total[stage_id]=_.filter(arr1, (p1:PipelineReport)=>(p1.movement_date.out > 0)) as PipelineReport[];
                    return arr_total
                }, {}) as {[stage_id:string]:PipelineReport[]},
            (arr2:PipelineReport[])=>{
                // console.error(arr2);
                const mean = _.meanBy(arr2, (p2:PipelineReport)=>{
                    const item_val = p2.movement_date.in > 0 ? (p2.movement_date.out - p2.movement_date.in): (p2.movement_date.out - p2.createdAt);
                    if (item_val>0) {
                        return item_val
                    } else{
                        return 0
                    }

                });
                if(_.isNaN(mean)){
                    // return NaN
                    return 0
                }else{
                    return mean
                }

            }

        ) as IStageCount

    // });
    //
    // return c.rps


};

export {getAverageMovementTimeByStages};

//get positionViewsGroupBySource():{[key:string]: PositionView[]} {//https://forums.asp.net/t/2138288.aspx?create+object+with+dynamic+keys+in+typescript
const fullfillDates = (obj:{}, time_range:IAnalyticsTimeRange, date_format:string="YYYY-MM-DD"):{[date:string]:any[]} => {
    const res = {}
    const current: Moment.Moment = Moment(time_range.start);
    while (current.isBefore(time_range.end)) {
        // let key = current.format("YYYY-MM-DD")
        let key = current.format(date_format);
        if(obj.hasOwnProperty(key)){
            res[key]=obj[key]
        }else{
            res[key]=[]
        }
        current.add(1, 'd');
    }
    return res;


};
export { fullfillDates };

const getPipelineReportsGroupByPipeline=(state, getters, pipeline_reports_by_pipelines): IPipelineReportsGroupByPipeline[]=>{
    // state.pipelines_with_stages
    // objects of pipeline-reports which keyed by pipeline-id
    // const grouped_prs:{[pipeline_id:string]:PipelineReport[]} = _.groupBy(state.pipeline_reports, (pr)=>{
    const grouped_prs:{[pipeline_id:string]:PipelineReport[]} = _.groupBy(pipeline_reports_by_pipelines, (pr)=>{
        return pr.pipeline.id
    });


    let r:IPipelineReportsGroupByPipeline[] = _.reduce(getters.pipelines.alive, (result, pws:IPipelineWithStages, index)=>{
        // undefined if not found
        let foundReports:PipelineReport[] =_.find(grouped_prs, (grouped_pr:PipelineReport[], pipeline_id:string)=>{
            return pipeline_id == pws.pipeline.id
        }) as PipelineReport[];

        result.push({
            // ...pws,
            pipeline: pws.pipeline,
            stages: pws.stages,
            pipeline_reports: foundReports || []
        } as IPipelineReportsGroupByPipeline);

        return result

    }, []);


    // process for pipeline-report that do not belong to any pipeline
    // for ex: case of pipeline has deleted before, so the report should be combined to report of the default pipeline

    // get PipelineReportsGroupByPipeline of default pipeline
    const defaultPipelineReportsGroupByPipeline:IPipelineReportsGroupByPipeline = _.find(r, (obj:IPipelineReportsGroupByPipeline, index)=>{
        return obj.pipeline.is_default == true
    });

    // if default pipeline should be appeared in report table,
    // we must include reports of deleted pipelines
    if (defaultPipelineReportsGroupByPipeline){
        const all_filtered_pipeline_ids_by_reports:string[] = _.keys(grouped_prs); // list of pipeline_ids
        // noted:
        // 1. `alive_pipeline_ids` may not be sub-set of `all_filtered_pipeline_ids_by_reports`
        // because there are positions have not reported yet

        const alive_pipeline_ids:string[] =  _.map(getters.pipelines.alive, (pws:IPipelineWithStages, index)=>{
            return pws.pipeline.id
        });
        // 2. if in `all_filtered_pipeline_ids_by_reports`, there was pipeline be deleted,
        // this mean that pipeline should not existed in list off all company's alive pipelines
        // therefore, if `all_filtered_pipeline_ids_by_reports` is not sub-set of `all_alive_pipeline_ids`, then: there are pipelines in report was deleted.
        const all_alive_pipeline_ids:string[] =  _.map(state.XPipelines_all.alive, (pws:IPipelineWithStages, index)=>{
            return pws.pipeline.id
        });

        // https://lodash.com/docs/4.17.11#difference
        // list of pipeline-ids had been deleted
        const deleted_pipeline_ids:string[] = _.difference(all_filtered_pipeline_ids_by_reports, all_alive_pipeline_ids);


        // if(_.size(deleted_pipeline_ids)>0){
        //
        //     const additional_pipeline_reports =  _.reduce(deleted_pipeline_ids, (reports, pipeline_id, index)=>{
        //         reports=[...reports, ...grouped_prs[pipeline_id]];
        //         return reports
        //     }, []);
        //
        //     if(defaultPipelineReportsGroupByPipeline==undefined){
        //         // add default pipeline to result `r` if not existed
        //         r.push({
        //             ...state.default_pipeline_with_stages,
        //
        //             pipeline_reports: additional_pipeline_reports,
        //         } as IPipelineReportsGroupByPipeline);
        //     }else{
        //         // add additional pipeline_report to defaultPipelineReportsGroupByPipeline
        //         defaultPipelineReportsGroupByPipeline.pipeline_reports.push(...additional_pipeline_reports);
        //     }
        // }

        if(_.size(deleted_pipeline_ids)>0){

            const additional_pipeline_reports =  _.reduce(deleted_pipeline_ids, (reports, pipeline_id, index)=>{
                reports=[...reports, ...grouped_prs[pipeline_id]];
                return reports
            }, []);

            // add additional pipeline_report to defaultPipelineReportsGroupByPipeline
            defaultPipelineReportsGroupByPipeline.pipeline_reports.push(...additional_pipeline_reports);

        }

    }


    return r

};
export {getPipelineReportsGroupByPipeline};
const getPipelineReportsGroupByPipelineFullfillDates=(state, getters,
                                                      pipelineReportsGroupByPipeline:IPipelineReportsGroupByPipeline[],
                                                      time_range:IAnalyticsTimeRange):IPipelineReportsGroupByPipelineFullfillDates[]=>{

    return _.map(pipelineReportsGroupByPipeline, (prg:IPipelineReportsGroupByPipeline, index)=>{
        let ps=_.groupBy(prg.pipeline_reports, (pr)=>{
            return Moment(pr.createdAt).format(state.date_format)
        });


        const rps:{[date:string]:PipelineReport[]}=fullfillDates(ps, time_range, state.date_format);

        const pipeline_reports:{[date:string]:{[stage_id:string]:PipelineReport[]}} =
            _.mapValues(rps, (r:PipelineReport[], date:string)=>{
                return _.reduce(prg.stages, (result, stage)=>{
                    let rp1:PipelineReport[]=[];
                    if(!state.is_position_report){
                        rp1 = _.filter(r, (rp:PipelineReport)=>{
                            return rp.stage.id==stage.id || (prg.pipeline.is_default==true && rp.stage.type==stage.stage_type);// map stage in case there was pipeline is deleted
                        });
                    }else{
                        // in this case, there is only one pipeline in pipelineReportsGroupByPipeline
                        rp1 = _.filter(r, (rp:PipelineReport)=>{
                            return (rp.stage.id==stage.id && rp.position.id==state.position_filter.value);
                        });
                    }

                    (result[stage.id] || (result[stage.id] = [])).push(...rp1);
                    return result
                }, {});
            });
        //@ts-ignore
       const movement_pipeline_reports_group_by_date:{[date:string]:{[stage_id:string]:PipelineReport[]}} =
           _.mapValues(pipeline_reports, (obj:{[stage_id:string]:PipelineReport[]}, date:string)=>{
               return _.mapValues(obj, (prs:PipelineReport[], stage_id:string)=>{
                   // only get reports which user moved in kanban
                   return _.filter(prs, (pr:PipelineReport)=>(pr.movement_date && pr.movement_date.in>0))
               })
           });

        /* Convert data for rendering table
        *
        *   {
                10-6: {
                    1: [],
                    2: [],
                },
                11-6: {
                    1: [],
                    2: [],
                },
                12-6: {
                    1: [],
                    2: [],
                }
            }

            =================>>>>>
            {
                1: {
                    10-6: [],
                    11-6: [],
                    12-6: [],
                },
                2: {
                    10-6: [],
                    11-6: [],
                    12-6: [],
                },
                3: {
                    10-6: [],
                    11-6: [],
                    12-6: [],
                },
            }
         */


        const a:{[date:string]:{[stage_id:string]:PipelineReport[]}} = pipeline_reports;
        /*
        * { '10-6': { '1': [], '2': [] },
          '11-6': { '1': [], '2': [] },
          '12-6': { '1': [], '2': [] } }

        * */

        // https://stackoverflow.com/a/28343437
        type rDate = string;
        type rStageId = string;
        type rStageName = string;

        // https://stackoverflow.com/a/29382420
        //@ts-ignore
        const b:Array<[rStageId, rDate, PipelineReport[]]> = _.flatMap(a, (obj:{[stage_id:string]:PipelineReport[]}, date:string)=>{
            //@ts-ignore
            const bb:[rStageId,rDate, PipelineReport[]][] = _.map(obj, (arr:PipelineReport[], stage_id:string)=> ([stage_id, date, arr]));
            // console.error(bb);
            return bb
        });
        // console.error(b);

        /*
        * [ [ '1', '10-6', [] ],
              [ '2', '10-6', [] ],
              [ '1', '11-6', [] ],
              [ '2', '11-6', [] ],
              [ '1', '12-6', [] ],
              [ '2', '12-6', [] ] ]
        * */
        // const c:{[id:string]:{[date:string]:PipelineReport[]}} = _.reduce(b, (result, value:[rStageId, rDate, PipelineReport[]])=>{
        //     (result[value[0]] || (result[value[0]] = {}))[value[1]]=value[2];
        //     return result
        // }, {});

        /*
        * { '1': { '10-6': [], '11-6': [], '12-6': [] },
              '2': { '10-6': [], '11-6': [], '12-6': [] } }

        * */



        const d:{[stage_id:string]:{[date:string]:PipelineReport[]}} = _.reduce(b, (result, value:[rStageId, rDate, PipelineReport[]])=>{
            (result[value[0]] || (result[value[0]] = {}))[value[1]]=value[2];
            return result
        }, {});
        /*
       * { '1': { '10-6': [], '12-6': [], '11-6': [],  },
            '2': { '11-6': [], '10-6': [],  '12-6': [] } }
       * */

        // movement report
        const d1:{[stage_id:string]:{[date:string]:number}} = _.reduce(b, (result, value:[rStageId, rDate, PipelineReport[]])=>{
            const prs:PipelineReport[]=_.filter(value[2], (pr:PipelineReport)=>(pr.movement_date && pr.movement_date.in > 0));
            (result[value[0]] || (result[value[0]] = {}))[value[1]]=prs.length;
            return result
        }, {});
        /*
       * { '1': { '10-6': 1, '12-6': 3, '11-6': 2,  },
            '2': { '11-6': 5, '10-6': 4,  '12-6': 6 } }
       * */

        // const e:{[stage_id:string]:number[]}  = _.mapValues(d1, (obj:{[date:string]:number})=>{
        //     return _.flatMap(_.sortBy(_.keys(obj), (o)=>o), (date)=>obj[date])
        // });
        // // { '1': [ 1, 2, 3 ], '2': [ 4, 5, 6 ] }


        // movement report
        const f:{[date:string]:NumberString, stage_name:string }[] = _.map(d1, (obj:{[date:string]:number}, stage_id:string)=>{
            const stage:PipelineStage = _.find(prg.stages,(stage)=> (stage.id == stage_id));
            return {
                stage_name:  stage? stage.title:'<unknown>',
                ...obj,
            }
        });

        return {
            pipeline: prg.pipeline,
            stages: prg.stages,
            movement_pipeline_reports_group_by_date: movement_pipeline_reports_group_by_date,
            movement_pipeline_reports: f, // table data
            full_pipeline_reports: d,
        } as IPipelineReportsGroupByPipelineFullfillDates
    })

};
export {getPipelineReportsGroupByPipelineFullfillDates};

const getPipelineFunnelReportCount=(state, getters,
                                    pipelineReportsGroupByPipelineFullfillDates:IPipelineReportsGroupByPipelineFullfillDates[],
                                    pipelineReportsGroupByPipeline:IPipelineReportsGroupByPipeline[]) => {

    const base:IPipelineReportsGroupByPipelineFullfillDates[] = pipelineReportsGroupByPipelineFullfillDates;
    const pipelineReportsGroupByPipelines:IPipelineReportsGroupByPipeline[]=pipelineReportsGroupByPipeline;
    return _.map(base, (prg:IPipelineReportsGroupByPipelineFullfillDates)=>{
        const full_pipeline_reports:{[stage_id:string]:{[date:string]:PipelineReport[]}} = prg.full_pipeline_reports;
        const funnel_reports_count:IStageCount = _.mapValues(full_pipeline_reports, (obj:{[date:string]:PipelineReport[]}, stage_id:string)=>{
            return _.reduce(obj, (total, prs:PipelineReport[], date:string)=>{
                const prByPositions:PipelineReport[]=_.filter(prs, (pr:PipelineReport)=>{
                    return !!_.find(getters.positions, (p:Position)=>(pr.position && pr.position.id == p.id))
                });
                return total + prByPositions.length
            }, 0);
        });


        const pipelineReportsGroupByPipeline:IPipelineReportsGroupByPipeline =_.find(pipelineReportsGroupByPipelines, (obj:IPipelineReportsGroupByPipeline)=>{
            return obj.pipeline.id == prg.pipeline.id
        });


        let total_unique_candidates:number=0;
        if (pipelineReportsGroupByPipeline){
            const d:PipelineReport[] = pipelineReportsGroupByPipeline.pipeline_reports;
            const prByPositions:PipelineReport[]=_.filter(d, (pr:PipelineReport)=>{
                return !!_.find(getters.positions, (p:Position)=>(pr.position.id == p.id))
            });
            // get total unique candidates
            total_unique_candidates = _.size(_.groupBy(prByPositions, (p:PipelineReport)=>p.candidate.id));
        }

        return { ...prg, funnel_reports_count:funnel_reports_count, total_unique_candidates:total_unique_candidates}
    });

};
export {getPipelineFunnelReportCount};
//----- CANNOT DO LIKE THIS ------------!!!!!!!!!!
// export const init_position_filter = ():IAnalyticsPositionFilterOption => {
//     //https://stackoverflow.com/a/47726574
//
//     const current_position_id:string = router.currentRoute.params.position_id;
//     alert(`init_position_filter  ${current_position_id}`);
//     console.error(router.currentRoute.params);
//     console.error(router.currentRoute);
//     console.error(router);
//     if (current_position_id){
//         return {
//             type: 'by_position',
//             value: `${current_position_id}`,
//             text: 'Specific Position'
//         } as IAnalyticsPositionFilterOption
//     }else{
//         return {
//             type: 'by_system',
//             value: 'all',
//             text: 'All Company Positions'
//         } as IAnalyticsPositionFilterOption
//     }
// };

// export const is_position_report:boolean=(init_position_filter.type=='by_position');
// alert(is_position_report);
export interface IPositionCandidates {
    position:Position,
    candidates: Candidate[],
    pipeline:Pipeline,
    stages: PipelineStage[],
}

export interface IReport {
    position_candidates: IPositionCandidates,
    // position_id:string,
    overview_chart_data_loaded:boolean,
    pipeline_performance_data_loaded:boolean,
    is_position_report: boolean,
    date_format:string,
    candidates: Candidate[],
    all_positions: Position[],
    // my_all_positions: Position[],
    pipeline_reports: PipelineReport[],
    pipeline_reports_by_pipelines: PipelineReport[],
    prev_pipeline_reports_by_pipelines: PipelineReport[],
    position_views: PositionView[],
    time_range: IAnalyticsTimeRange,

    position_filter: IAnalyticsPositionFilterOption,
    XPipelines_all: IXPipelines,

    default_pipeline_with_stages: IPipelineWithStages,

    pipeline_reports_time_to_fill: PipelineReport[],
    data_report_open_position: Object,
    // report_candidates_count: number,


}

const state = {
        open_position_report_loaded: false,
        position_candidates: null,
        overview_chart_data_loaded:false,
        pipeline_performance_data_loaded:false,
        // position_id:'',
        is_position_report: false,
        candidates: [],
        all_positions:[],
        pipeline_reports: [],
        pipeline_reports_by_pipelines:[],
        prev_pipeline_reports_by_pipelines:[],
        position_views: [],
        time_range: AnalyticsTimeRanges[0], // default last 7 days

        // position_filter: init_position_filter(),
        position_filter: AnalyticsPositionFilterBySystemOptions[0],
        // all_pipelines: [],
        XPipelines_all: {
            alive: [],
            deleted: []
        },

        // all_deleted_pipelines:[],
        // pipelines: [],
        // default_pipeline_with_stages: null,
        default_pipeline_with_stages: null,
        date_format:"YYYY-MM-DD",
        // report_candidates_count:0,
        pipeline_reports_time_to_fill: [],
        data_report_open_position: {}

    };

// make all mutations
const mutations = {
    ...make.mutations(state),

    // updateTimeRange(state, value:IAnalyticsTimeRange):void{
    updateTimeRange: (state, value:IAnalyticsTimeRange):void=>{
        state.time_range = value
    },
    // updatePositionFilter: (state, value)=>{
    //     state.position_filter = value
    // },
    set_candidates: (state, value)=> {
        state.candidates = value
    },
    set_pipeline_reports: (state, value)=> {
        state.pipeline_reports = value
    },
    set_position_views: (state, value)=> {
        state.position_views = value
    },
    set_all_positions: (state, value)=> {
        state.all_positions = value
    },

    set_pipeline_reports_time_to_fill: (state, value) =>{
        state.pipeline_reports_time_to_fill = value
    },

    updateField, // from vuex-map-fields
};

const getters = {
    // Vuex allows us to define "getters" in the store. You can think of them as computed properties for stores.
    // https://vuex.vuejs.org/guide/getters.html#getters
    getField, // by vuex-map-fields

    // make all getters (optional)
    ...make.getters(state),

    // // overwrite default `items` getter
    // items: state => {
    //     return state.items.map(item => new Item(item))
    // },
    //
    // // add new `filteredItems` getter
    // filteredItems: (state, getters) => {
    //     return getters.items.filter(item => item.title.includes(state.filters.search))
    // },

    // add new `time_range_ts` getter
    time_range_ts: (state):{start:number, end:number} => {
        // https://stackoverflow.com/a/47726574
        // alert(`router.currentRoute.params.position_id ${router.currentRoute.params.position_id}`);

        return {
            start: state.time_range.start.startOf('day').valueOf(),
            end: state.time_range.end.endOf('day').valueOf()
        }

        // return {
        //     start: Moment().startOf('day').valueOf(),
        //     end: Moment().endOf('day').valueOf()
        // }
    },
    prev_time_range:(state):{start:Moment.Moment, end:Moment.Moment}=>{
        const diff_days:number=state.time_range.end.diff(state.time_range.start, 'days');
        const prev_time_range:{
            start:Moment.Moment,
            end:Moment.Moment
        }={
            start: Moment(state.time_range.start).subtract(diff_days, 'd'),
            end: Moment(state.time_range.end).subtract(diff_days, 'd'),
        };
        return prev_time_range
    },
    prev_time_range_ts: (state, getters):{start:number, end:number} => {

        return {
            start: getters.prev_time_range.start.startOf('day').valueOf(),
            end: getters.prev_time_range.end.endOf('day').valueOf()
        }

    },

    my_all_positions: (state, getters, rootState, rootGetters): Position[] =>{
        return _.filter(state.all_positions, (p, index)=>{
            return p.created_by == rootGetters.currentUser.id
        });

    },
    my_published_positions: (state, getters, rootState, rootGetters): Position[]=>{
        return _.filter(getters.my_all_positions, (p, index)=>{
            // status: string = "draft"; // published, draft, closed
            return p.status == 'published'
        })
    },
    my_draft_positions: (state, getters): Position[]=>{
        return _.filter(getters.my_all_positions, (p, index)=>{
            return p.status == 'draft'
        })
    },
    positions(state, getters): Position[]{
        let positions:Position[] = [];
        if(state.position_filter.type == 'by_system' && state.position_filter.value == 'all'){
            positions = state.all_positions
        }
        else if(state.position_filter.type == 'by_system'){

            if (state.position_filter.value == 'my-all'){
                positions = getters.my_all_positions
                // return array

            }
            else if (state.position_filter.value == 'my-published'){
                positions = getters.my_published_positions;

            }
            else if (state.position_filter.value == 'my-draft'){
                positions = getters.my_draft_positions;

            }


        }else if(state.position_filter.type == 'by_position'){
            const position:Position = _.find(state.all_positions, (position)=>(position.id==state.position_filter.value));
            //
            // console.error(`position:Position ${position}`);
            // debugger
            if(position){

                positions=[position];
            }


        }
        return positions
    },
    // positions belonged pipelines (based on filted conditions), or, all pipelines
    pipelines: (state, getters): IXPipelines => {
        /*
        * extract pipelines from positions
        * */
        if(state.position_filter.type == 'by_system' && state.position_filter.value == 'all'){
            return state.XPipelines_all
        }

        const result:IXPipelines={
            alive: [],
            deleted: []
        };

        const positions:Position[]=getters.positions;

        const pipeline_ids:string[] = _.keys(
            // return objects which keyed by pipeline id
            _.groupBy(positions, (p)=>{
                if(p==undefined){
                    // debugger
                }
                return p.pipeline_id
            }));

        const alive_pipelines:IPipelineWithStages[] = _.filter(state.XPipelines_all.alive, (pws:IPipelineWithStages, index) => {
            return pipeline_ids.includes(pws.pipeline.id)
        });
        result.alive=alive_pipelines;

        // if pipeline_ids includes default pipeline, we must include all deleted pipelines for getting reports of default pipeline later
        if(state.default_pipeline_with_stages && pipeline_ids.includes(state.default_pipeline_with_stages.pipeline.id)){
            result.deleted = state.XPipelines_all.deleted;
        }


        return result

    },
    candidatesGroupByStages(state, getters): ICandidatePipelineStages {
        const position_candidates:IPositionCandidates = state.position_candidates;
        let result:ICandidatePipelineStages;

        if(position_candidates){
            const candidates_group_by_stages:{[stage_id:string]:Candidate[]} = _.groupBy(position_candidates.candidates, (candidate:Candidate)=>(candidate.stage.id));
            result = {
                position:position_candidates.position,
                candidates:position_candidates.candidates,
                pipeline: position_candidates.pipeline,
                stages:position_candidates.stages,
                candidates_group_by_stages: candidates_group_by_stages
            };

        }else{
            result = null;
        }
        return result


    },

    pipelineReportsGroupByPipeline(state, getters): IPipelineReportsGroupByPipeline[] {
        return getPipelineReportsGroupByPipeline(state, getters, state.pipeline_reports_by_pipelines);
    },
    prevPipelineReportsGroupByPipeline(state, getters): IPipelineReportsGroupByPipeline[] {
        return getPipelineReportsGroupByPipeline(state, getters, state.prev_pipeline_reports_by_pipelines);
    },

    pipelineReportsGroupByPipelineFullfillDates(state, getters):IPipelineReportsGroupByPipelineFullfillDates[]{
        return getPipelineReportsGroupByPipelineFullfillDates(state, getters, getters.pipelineReportsGroupByPipeline, state.time_range);

    },
    prevPipelineReportsGroupByPipelineFullfillDates(state, getters):IPipelineReportsGroupByPipelineFullfillDates[]{
        return getPipelineReportsGroupByPipelineFullfillDates(state, getters, getters.prevPipelineReportsGroupByPipeline, getters.prev_time_range);

    },

    pipelineFunnelReportCount:(state, getters):IPipelineFunnelReport[] => {
        return getPipelineFunnelReportCount(state, getters, getters.pipelineReportsGroupByPipelineFullfillDates, getters.pipelineReportsGroupByPipeline);
    },

    prevPipelineFunnelReportCount:(state, getters):IPipelineFunnelReport[] => {
        return getPipelineFunnelReportCount(state, getters, getters.prevPipelineReportsGroupByPipelineFullfillDates, getters.prevPipelineReportsGroupByPipeline);
    },
    pipelineInStageByPositions(state, getters):IPipelineInStageByPosition[]{
        const pipelineReportsGroupByPipeline:IPipelineReportsGroupByPipeline[]=getters.pipelineReportsGroupByPipeline;
        const a:IPipelineInStageByPosition[] = _.map(pipelineReportsGroupByPipeline, (prg:IPipelineReportsGroupByPipeline)=>{
            // this is not contain all positions belonged to the pipeline, but based on reports set
            const b:{[position_id:string]:PipelineReport[]} = _.groupBy(prg.pipeline_reports, (pr:PipelineReport)=>pr.position.id);

            const c:{[position_id:string]:IStageCount} = _.mapValues(b, (arr:PipelineReport[], position_id:string)=>{
                const c1 = getAverageMovementTimeByStages(arr);
                // console.error(c1);
                return c1
            });

            const d:IStageCount = getAverageMovementTimeByStages(_.filter(prg.pipeline_reports, (p:PipelineReport)=>(!!p.position.id)));

            // full list of all positions belonged to the pipeline
            const positions:Position[]=_.filter(getters.positions, (p:Position)=>(p.pipeline_id==prg.pipeline.id));
            return {
                pipeline: prg.pipeline,
                stages: prg.stages,
                positions: positions,
                pipeline_report_by_positions: c,
                pipeline_report_all_positions: d,
            } as IPipelineInStageByPosition
        });
        return a
    },
    pipelineInstageByCategories(state, getters):IPipelineInStageByCategory[]{
        const pipelineReportsGroupByPipeline:IPipelineReportsGroupByPipeline[]=getters.pipelineReportsGroupByPipeline;
        const a:IPipelineInStageByCategory[] = _.map(pipelineReportsGroupByPipeline, (prg:IPipelineReportsGroupByPipeline)=>{
            const b:{[category_value:string]:PipelineReport[]} = _.groupBy(prg.pipeline_reports, (pr:PipelineReport)=>{
                const pos:Position = _.find(getters.positions, (p:Position)=>(pr.position.id === p.id));
                // debugger
                if (pos){
                    // @ts-ignore
                    return pos.category.value
                }else{
                    return 'unknown'
                }
            });

            const c:{[category_value:string]:IStageCount} = _.mapValues(b, (arr:PipelineReport[])=>{
                return getAverageMovementTimeByStages(arr);
            });
            // the same with pipelineInStageByPositions
            const d:IStageCount = getAverageMovementTimeByStages(_.filter(prg.pipeline_reports, (p:PipelineReport)=>(!!p.category.value)));
            const positions:Position[]=_.filter(getters.positions, (p:Position)=>(p.pipeline_id==prg.pipeline.id));
            //@ts-ignore
            const categories_values:string[] = _.keys(_.groupBy(positions, (p:Position)=>(p.category.value)) as {[category_value:string]:Position[]});
            const categories:IPositionCategory[] = _.filter(positioncategories, (pc:IPositionCategory)=>(categories_values.includes(pc.value)));
            return {
                pipeline: prg.pipeline,
                stages: prg.stages,
                categories: categories,
                pipeline_report_by_categories:c ,
                pipeline_report_all_categories: d
            } as IPipelineInStageByCategory
        });
        return a

    },
    pipelineFunnelReportData:(state, getters):IXPipelineFunnelReport[] => {

        const frp:IPipelineFunnelReport[]=getters.pipelineFunnelReportCount;
        const prev_frp:IPipelineFunnelReport[] = getters.prevPipelineFunnelReportCount;
        return _.map(frp, (f:IPipelineFunnelReport)=>{
            const prev_f:IPipelineFunnelReport =  _.find(prev_frp, (fi:IPipelineFunnelReport)=>fi.pipeline.id == f.pipeline.id);

            return {
                ...f,
                prev_funnel_reports_count:prev_f.funnel_reports_count,
                prev_total_unique_candidates:prev_f.total_unique_candidates,
            } as IXPipelineFunnelReport
        });

    },

    pipelinePerformanceData(state, getters):IPipelinePerformanceData[]{
        // getters.pipelineFunnelReportData
        // getters.pipelineInStageByPositions
        // getters.pipelineInstageByCategories
        // getters.pipelineReportsGroupByPipelineFullfillDates
        return _.map(getters.pipelineFunnelReportData, (f:IXPipelineFunnelReport)=>{
            const p:IPipelineInStageByPosition=_.find(getters.pipelineInStageByPositions, (obj:IPipelineInStageByPosition)=>(obj.pipeline.id==f.pipeline.id));
            const c:IPipelineInStageByCategory=_.find(getters.pipelineInstageByCategories, (obj:IPipelineInStageByCategory)=>(obj.pipeline.id==f.pipeline.id));
            const m:IPipelineReportsGroupByPipelineFullfillDates=_.find(getters.pipelineReportsGroupByPipelineFullfillDates, (obj:IPipelineReportsGroupByPipelineFullfillDates)=>(obj.pipeline.id==f.pipeline.id));

            // const  {funnel_reports_count, total_unique_candidates, prev_funnel_reports_count, prev_total_unique_candidates}  = f;
            const r:IPipelinePerformanceData= {
                pipeline: f.pipeline,
                stages:f.stages,
                funnel_progression:{
                    // funnel_reports_count:f.funnel_reports_count ,
                    // total_unique_candidates:f.total_unique_candidates,
                    // prev_funnel_reports_count:f.prev_funnel_reports_count ,
                    // prev_total_unique_candidates:f.prev_total_unique_candidates,
                    ...f
                },
                time_in_stage:{
                    by_position: {
                        // positions: Position[],
                        // pipeline_report_by_positions:{[position_id:string]:{[stage_id:string]:number}} ,
                        // pipeline_report_all_positions: {[stage_id:string]:number},
                        ...p
                    },
                    by_category:{
                        // categories: IPositionCategory[],
                        // pipeline_report_by_categories:{[category_value:string]:{[stage_id:string]:number}} ,
                        // pipeline_report_all_categories:{[stage_id:string]:number},
                        ...c
                    }
                },
                movement:{
                    ...m
                }
            };
            // alert(r.pipeline.id);
            return r

        });

    },

    // example all pipelines count
    pipelinefunnelReportCandidatesCount:(state, getters):number=>{
        return  _.size(_.groupBy(state.pipeline_reports_by_pipelines, (pipeline_report:PipelineReport)=>{
            return pipeline_report.candidate.id
        }));
    },

    candidateAppliedPipelineReports: (state, getters):PipelineReport[] => {
        return state.pipeline_reports.filter((pr)=>{
            return pr.source.origin == 'applied'
        })

    },


    candidateAppliedPipelineReportsGroupBySource: (state, getters):{[source_name:string]: PipelineReport[]} => {
        // return an object, not Array
        // group candidates by date
        let c1=_.groupBy(getters.candidateAppliedPipelineReports, (pr)=>{
            return pr.source.name ? pr.source.name: 'unknown'
        })

        return c1

    },

    pipelineReportsGroupByDate: (state, getters):{[date:string]: PipelineReport[]} => {
        // return an object, not Array
        // group candidates by date
        let c1=_.groupBy(state.pipeline_reports, (pr)=>{
            return Moment(pr.createdAt).format(state.date_format)
        });

        return fullfillDates(c1, state.time_range, state.date_format)
        // return  c1
    },

    candidateHiredPipelineReports: (state, getters):PipelineReport[] => {
        return state.pipeline_reports.filter((pr)=>{
            return pr.stage.type=='hired' && pr.movement_date && pr.movement_date.in > 0
        })
    },
    candidateDisqualifiedPipelineReports: (state, getters):PipelineReport[] => {
        return state.pipeline_reports.filter((pr)=>{
            return pr.stage.type=='disqualified'
        })
    },



    candidateAppliedPipelineReportsGroupByDate(state, getters):{[date:string]: PipelineReport[]} {

        // @ts-ignore
        const objs:{[date:string]: PipelineReport[]} = _.mapValues(getters.pipelineReportsGroupByDate, (prs:PipelineReport[], date:string)=>{
            // return  prs.filter((pr)=>(pr.source.origin=='applied'))

            return  _.filter(prs,(pr)=>{
                return pr.source.origin == 'applied'
            })
        });

        return objs
    },

    candidateSourcedPipelineReportsGroupByDate(state, getters):{[date:string]: PipelineReport[]} {
        // @ts-ignore
        const objs:{[date:string]: PipelineReport[]} = _.mapValues(getters.pipelineReportsGroupByDate, (prs:PipelineReport[], date:string)=>{
            // return { [date]: prs.filter((pr)=>(pr.source.origin=='applied')) }
            return  prs.filter((pr)=>(pr.source.origin=='sourced'))
        });

        return objs
    },

    candidateHiredPipelineReportsGroupByDate(state, getters):{[date:string]: PipelineReport[]} {

        // @ts-ignore
        const objs:{[date:string]: PipelineReport[]} = _.mapValues(getters.pipelineReportsGroupByDate, (prs:PipelineReport[], date:string)=>{
            // return  prs.filter((pr)=>(pr.source.origin=='applied'))

            const a:{[candidate_id:string]:PipelineReport[]} = _.groupBy(_.filter(prs,(pr)=>(pr.stage.type=='hired' && pr.movement_date && pr.movement_date.in > 0)) as PipelineReport[],
                (p:PipelineReport)=>(p.candidate.id));
            return _.map(a, (obj:PipelineReport[])=>(obj[0]||[])) as PipelineReport[]
        });

        return objs
    },
    candidateDisqualifiedPipelineReportsGroupByDate(state, getters):{[date:string]: PipelineReport[]} {

        // @ts-ignore
        const objs:{[date:string]: PipelineReport[]} = _.mapValues(getters.pipelineReportsGroupByDate, (prs:PipelineReport[], date:string)=>{
            // return  prs.filter((pr)=>(pr.source.origin=='applied'))

            const a:{[candidate_id:string]:PipelineReport[]} = _.groupBy(_.filter(prs,(pr)=>(pr.stage.type=='disqualified')) as PipelineReport[],
                (p:PipelineReport)=>(p.candidate.id));
            return _.map(a, (obj:PipelineReport[])=>(obj[0]||[])) as PipelineReport[]
        });

        return objs
    },


    pipeline_reports_time_to_fill: (state, getters) => {
        const gr = _.groupBy(state.pipeline_reports_time_to_fill, (pr)=>{
            return pr.position.id
        });
        Object.keys(gr).forEach( (position_id) => {
            gr[position_id] = _.sortBy(gr[position_id], (item) => {
                return item.movement_date.in;
            });
        });
        return gr
    },

     pipeline_reports_category_time_to_fill: (state, getters) => {
        const grc = _.groupBy(state.pipeline_reports_time_to_fill, (pr)=>{
            return pr.category.value
        });

        Object.keys(grc).forEach( (category_value) => {
            grc[category_value] = _.sortBy(grc[category_value], (item: any) => {
                return item.movement_date.in;
            });
        });
        return grc
    },

    data_report_open_position: (state) => {
        return state.data_report_open_position;
    }

    // use getField instead
    // candidates: state => {
    //     return state.candidates;
    // },
    //
    // use getField instead
    // positionViews:  state => {
    //     return state.position_views;
    // },
};
const actions  = {
        // // automatically create only `setAllPipelines()` action
        // ...make.actions('all_pipelines'),
        // ...make.actions('pipelines'),
        ...make.actions(state),

        // manually add load all_pipelines action
        async getAllPipelines({dispatch, rootGetters}){
            const company_id=rootGetters.currentCompany.id;
            let all_alive_pipelines:Pipeline[]=[];
            let all_deleted_pipelines:DeletedPipeline[] = [];

            await Promise.all([
                new Promise((resolve, reject)=>{
                    new Pipeline().getAllPipelines(company_id).then(pipelines =>{
                        all_alive_pipelines=pipelines;
                        resolve(pipelines);
                    })
                }),
                new Promise((resolve, reject)=>{
                    new DeletedPipeline().getAllDeletedPipelines(company_id).then(deleted_pipelines =>{
                        all_deleted_pipelines = deleted_pipelines;
                        resolve(deleted_pipelines);
                    })
                }),
            ] );



            const all_alive_pipelines_with_stages: IPipelineWithStages[] = [];
            await Promise.all(_.reduce(all_alive_pipelines, (promises, pipeline:Pipeline, index)=>{
                const stages_promise =  new Promise((resolve, reject)=>{
                    new PipelineStage().getAllStages(pipeline.id).then(pipeline_stages =>{
                        all_alive_pipelines_with_stages.push({
                            pipeline: pipeline,
                            stages: pipeline_stages,
                        });
                        resolve(pipeline_stages);
                    })
                });

                promises.push(stages_promise);
                return promises
            }, []));

            const XPipelines_all:IXPipelines ={
                alive: all_alive_pipelines_with_stages,
                deleted: all_deleted_pipelines
            };
            dispatch('setXPipelines_all', XPipelines_all);



            // extract default pipeline with stages
            const defaultPipelineWithStages:IPipelineWithStages = _.find(XPipelines_all.alive, (pipelineWithStages:IPipelineWithStages, index)=>{
                return pipelineWithStages.pipeline.is_default == true
            });
            // note: although we can use commit here, like this: commit('SET_DEFAULT_PIPELINES_WITH_STAGES', defaultPipelineWithStages)
            // but, the mutation `SET_PIPELINES_WITH_STAGES` is generated by pathify and any mutation should be called by an action,
            // so, the better way is to use dispatch to call responsively action
            // dispatch('setDefault_pipeline_with_stages', defaultPipelineWithStages);
            dispatch('setDefault_pipeline_with_stages', defaultPipelineWithStages);



        },


        async getAllPositions({commit}){
            let all_positions = await new Position().getAllPositions()

            commit('set_all_positions', all_positions)

        },
        createSamplePositionView:async ({state, getters, commit, rootState, rootGetters}) => {
            let position_statuses=['published', 'draft', 'closed']
            let source_names= ['Portal', 'cà ri á bọt tồ', 'fk portal']
            let pv:PositionView
            let created_date=Moment()
            _.range(10).forEach((i)=>{
                pv = new PositionView()
                // alert(i)
                pv.company_id=rootGetters.currentCompany.id
                pv.position = {
                    id: '13412asdfasd',
                    user_id: rootGetters.currentUser.id,
                    status: position_statuses[_.random(0, 2)], // published, draft, closed
                }
                pv.source =  {
                    "id": "",
                    "name": source_names[_.random(0,2)],
                    "type": ""
                }
                created_date = Moment(server_time.now).subtract(_.random(0, 10), 'd')

                pv.createdAt=created_date.valueOf()
                pv.insert()


                // pv.update()

            })

            alert('create position view done')
            //

        },
        createSamplePipelineReports:async ({state, getters, commit, rootState, dispatch}) => {
            alert('create sample ')
            // this.createSamplePipelineReport()
            let all_positions = state.all_positions

            let prs= _.range(10).map((i)=>{
                return dispatch('createSamplePipelineReport', all_positions)
            })
            await Promise.all(prs)
            alert('create sample done')
        },

        createSamplePipelineReport:async ({state, getters, commit, rootState, rootGetters}, all_positions) => {
            let position_statuses=['published', 'draft', 'closed']

            let source_names= ['Portal', 'cà ri á bọt tồ', 'fk portal']
            let origins=['applied', 'recruiter', 'referral', 'sourced']
            let created_date=Moment()



            let random_position = all_positions[_.random(0, all_positions.length-1)]
            if(!random_position) return


            let pipeline_id=random_position.pipeline_id
            console.warn(`pipeline_id: ${pipeline_id}, position_id; ${random_position.id}`)

            let pipeline;
            let  all_pipeline_stages;

            if(typeof pipeline_id !== "string") return

            // await Promise.all([
            try{
                pipeline=await new Pipeline().getById(pipeline_id)
                all_pipeline_stages=await new PipelineStage().getAllStages(pipeline_id)
            }catch(err) {
                // alert(err)
            }

            // ])

            let random_stage=all_pipeline_stages[_.random(0, all_pipeline_stages.length-1)]


            let pr = PipelineReport.instance

            pr.company_id = rootGetters.currentCompany.id

            pr.pipeline = {
                id: pipeline.id,
                name: pipeline.title,
                is_default: pipeline.is_default,
            };

            pr.stage = {
                id:random_stage.id,
                name:random_stage.name,
                // type: random_stage.type,
                type: 'disqualified',
            };


            pr.position = {
                id: random_position.id,
                name: random_position.name,
                user_id: rootGetters.currentUser.id,
                status: random_position.status,// draft, published, closed
            };





            pr.source =  {
                id: "",
                "name": source_names[_.random(0,2)],
                type: "",
                origin: origins[_.random(0,3)], //applied | recruiter | referral | sourced
                // nguoi tao candidate truc tiep tu he thong
                sourced_by: {
                    id:  rootGetters.currentUser.id,
                    email_address:  "",
                    name:  ""
                }
            };
            let randome_id = _.random(1000000, 9000000).toString()
            pr.candidate= {
                id:randome_id,
                name: randome_id,
            }

            created_date = Moment(server_time.now).subtract(_.random(0, 10), 'd')

            pr.createdAt=created_date.valueOf()

            await pr.insert()
            // alert('insert')



        },

        getPipelineReportByFilterConditions: async ({state, getters, commit, rootState, rootGetters}) => {
            // context.getters.time_range_ts.start

            let conditions = []
            conditions.push({fieldPath: 'createdAt', opStr:'>=', value: getters.time_range_ts.start })
            conditions.push({fieldPath: 'createdAt', opStr: '<=', value: getters.time_range_ts.end })

            if(state.position_filter.type == 'by_system'){
                if(state.position_filter.value == 'all'){
                    // pass
                }
                else if (state.position_filter.value == 'my-all'){
                    // position.user_id
                    // get all my positions --> filter candidates belong to my positions
                    conditions.push({fieldPath: new firebase.firestore.FieldPath('position', 'user_id'), opStr:'==', value: rootGetters.currentUser.id })
                }
                else if (state.position_filter.value == 'my-published'){
                    conditions.push({fieldPath: new firebase.firestore.FieldPath('position', 'user_id'), opStr:'==', value: rootGetters.currentUser.id })
                    conditions.push({fieldPath: new firebase.firestore.FieldPath('position', 'status'), opStr:'==', value: 'published'})
                }
                else if (state.position_filter.value == 'my-draft'){
                    conditions.push({fieldPath: new firebase.firestore.FieldPath('position', 'user_id'), opStr:'==', value: rootGetters.currentUser.id })
                    conditions.push({fieldPath: new firebase.firestore.FieldPath('position', 'status'), opStr:'==', value: 'draft'})
                }
            }else if(state.position_filter.type == 'by_position'){
                conditions.push({fieldPath: new firebase.firestore.FieldPath('position', 'id'), opStr:'==', value: state.position_filter.value})
            }


            let pipeline_reports = await PipelineReport.instance.getReports(conditions, null)

            commit('set_pipeline_reports', pipeline_reports)

            return pipeline_reports

        },
        getPipelineReports: async ({state, getters, dispatch, commit, rootState, rootGetters}, time_range_ts:{start:number, end:number}):Promise<PipelineReport[]> => {
            const total_pipeline_reports:PipelineReport[]=[];
            const conditions = [];
            conditions.push({fieldPath: 'createdAt', opStr:'>=', value: time_range_ts.start });
            conditions.push({fieldPath: 'createdAt', opStr: '<=', value: time_range_ts.end });

            const pipeline_ids:string[] = _.map(getters.pipelines.alive, (pws:IPipelineWithStages)=>{
                return pws.pipeline.id
            });
            pipeline_ids.push(..._.map(getters.pipelines.deleted, (deleted_pipeline:DeletedPipeline)=>{
                return deleted_pipeline.pipeline.id
            }));

            await Promise.all(_.reduce(pipeline_ids, (promises, pipeline_id:string, index)=>{
                const individual_conditions=[...conditions];
                individual_conditions.push({fieldPath: new firebase.firestore.FieldPath('pipeline', 'id'), opStr:'==', value: pipeline_id});

                const reports_promise =  new Promise((resolve, reject)=>{
                    PipelineReport.instance.getReports(individual_conditions, null).then((pipeline_reports:PipelineReport[]) =>{
                        total_pipeline_reports.push(...pipeline_reports);
                        resolve(pipeline_reports);
                    })
                });

                promises.push(reports_promise);

                return promises
            }, []));




            return total_pipeline_reports
        },
        getPipelineReportByPipelines: async ({state, getters, dispatch, commit, rootState, rootGetters}):Promise<Array<PipelineReport[]>> => {
            type TotalPipelineReports=PipelineReport[];
            type PrevTotalPipelineReports=PipelineReport[];

            dispatch('setPipeline_performance_data_loaded', false);


            // const prev_total_pipeline_reports:PipelineReport[]=[];
            // await Promise.all([
            //     new Promise((resolve, reject)=>{
            //         dispatch('getPipelineReports', getters.time_range_ts).then((prs:PipelineReport[])=>{
            //             total_pipeline_reports.push(...prs);
            //             resolve(prs)
            //         })
            //     }),
            //     new Promise((resolve, reject)=>{
            //         dispatch('getPipelineReports', getters.prev_time_range_ts).then((prs:PipelineReport[])=>{
            //             prev_total_pipeline_reports.push(...prs);
            //             resolve(prs)
            //         })
            //     }),
            //
            // ]);
            // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/all
            const r:[TotalPipelineReports, PrevTotalPipelineReports] = await Promise.all([
                dispatch('getPipelineReports', getters.time_range_ts),
                dispatch('getPipelineReports', getters.prev_time_range_ts)
            ]);



            // note: although we can use commit here, like this: commit('SET_PIPELINES_WITH_STAGES', pipelines_with_stages)
            // but, the mutation `SET_PIPELINES_WITH_STAGES` is generated by pathify and any mutation should be called by an action,
            // so, the better way is to use dispatch to call responsively action
            dispatch('setPipeline_reports_by_pipelines', r[0]);
            dispatch('setPrev_pipeline_reports_by_pipelines', r[1]);
            dispatch('setPipeline_performance_data_loaded', true);

            return r
        },

        getPositionViewsByFilterConditions:async ({state, getters, commit, rootState, rootGetters}) => {
            let conditions = []
            conditions.push({fieldPath: 'createdAt', opStr:'>=', value: getters.time_range_ts.start })
            conditions.push({fieldPath: 'createdAt', opStr: '<=', value:getters.time_range_ts.end })

            if(state.position_filter.type == 'by_system'){
                if(state.position_filter.value == 'all'){
                    // pass
                }
                else if (state.position_filter.value == 'my-all'){
                    // position.user_id
                    // get all my positions --> filter candidates belong to my positions
                    conditions.push({fieldPath: new firebase.firestore.FieldPath('position', 'user_id'), opStr:'==', value: rootGetters.currentUser.id })
                }
                else if (state.position_filter.value == 'published'){
                    conditions.push({fieldPath: new firebase.firestore.FieldPath('position', 'user_id'), opStr:'==', value: rootGetters.currentUser.id })
                    conditions.push({fieldPath: new firebase.firestore.FieldPath('position', 'status'), opStr:'==', value: 'published'})
                }
                else if (state.position_filter.value == 'draft'){
                    conditions.push({fieldPath: new firebase.firestore.FieldPath('position', 'user_id'), opStr:'==', value: rootGetters.currentUser.id })
                    conditions.push({fieldPath: new firebase.firestore.FieldPath('position', 'status'), opStr:'==', value: 'draft'})
                }
            }else if(state.position_filter.type == 'by_position'){
                conditions.push({fieldPath: new firebase.firestore.FieldPath('position', 'id'), opStr:'==', value: state.position_filter.value })
            }


            let positionViews = await new PositionView().getPositionViews(conditions, null)

            // alert(positionViews)

            commit('set_position_views', positionViews);
            return positionViews
        },

        getCandidatesByPosition: async (context, position_id) => {

            const candidates: Candidate[] = new Array();
            let position:Position;
            let pipeline:Pipeline;
            let stages:PipelineStage[];

            const r1:[QuerySnapshot, Model] = await Promise.all([
                new Candidate().getCandidatesByPosition(position_id).get(),
                new Position().getById(position_id)
            ]);
            position=r1[1] as Position;
            const records:QuerySnapshot = r1[0];
            records.forEach(docSnapshot => {
                let record = new Candidate();
                //console.log(docSnapshot.data());
                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                candidates.push(record);
            });

            const r2:[Model, PipelineStage[]] = await Promise.all([
                new Pipeline().getById(position.pipeline_id),
                new PipelineStage().getAllStages(position.pipeline_id)
            ]);
            pipeline=r2[0] as Pipeline;
            stages=r2[1];

            const result:IPositionCandidates={
                position:position,
                candidates: candidates,
                pipeline:pipeline,
                stages: stages,
            };

            context.dispatch('setPosition_candidates', result);

            return result

        },

        getPipelineReportTimeToFill: async ({state, getters, commit, rootState, rootGetters}) => {
            let conditions = [];
            if(state.position_filter.type == 'by_system'){
                if(state.position_filter.value == 'all'){
                     conditions.push({fieldPath: new firebase.firestore.FieldPath('stage', 'type'), opStr:'==', value: 'hired'})
                }
                else if (state.position_filter.value == 'my-all'){
                    conditions.push({fieldPath: new firebase.firestore.FieldPath('position', 'user_id'), opStr:'==', value: rootGetters.currentUser.id });
                    conditions.push({fieldPath: new firebase.firestore.FieldPath('stage', 'type'), opStr:'==', value: 'hired'})
                }
                else if (state.position_filter.value == 'my-published'){
                    conditions.push({fieldPath: new firebase.firestore.FieldPath('position', 'user_id'), opStr:'==', value: rootGetters.currentUser.id });
                    conditions.push({fieldPath: new firebase.firestore.FieldPath('stage', 'type'), opStr:'==', value: 'hired'})
                }
                else if (state.position_filter.value == 'my-draft'){
                    conditions.push({fieldPath: new firebase.firestore.FieldPath('position', 'user_id'), opStr:'==', value: rootGetters.currentUser.id });
                    conditions.push({fieldPath: new firebase.firestore.FieldPath('stage', 'type'), opStr:'==', value: 'hired'})
                }
            }else if(state.position_filter.type == 'by_position'){
                conditions.push({fieldPath: new firebase.firestore.FieldPath('position', 'id'), opStr:'==', value: state.position_filter.value});
                conditions.push({fieldPath: new firebase.firestore.FieldPath('stage', 'type'), opStr:'==', value: 'hired'})
            }

            let pipeline_reports_time_to_fill = await PipelineReport.instance.getReports(conditions, null);
            commit('set_pipeline_reports_time_to_fill', pipeline_reports_time_to_fill);
            return pipeline_reports_time_to_fill

        },
        // getReportOpenPosition: async ({state, getters, commit, rootState, rootGetters}, {position_id, stages}) => {
        //     let candidates = await new Candidate().getCandidateByPosition(position_id);
        //     let group_by_stages = _.groupBy(candidates, (item) => {return item.stage.id});
        //     let count;
        //     stages.forEach((stage) => {
        //         count = group_by_stages[stage.id] ? group_by_stages[stage.id].length : 0;
        //         Vue.set(state.data_report_open_position, position_id+stage.id, count);
        //     })
        //
        // }


    };
const modules={};

const store: Module<IReport, any> = {
    // wtf is this??
    namespaced: true,
    state,
    modules,

    mutations,

    getters,

    actions,
};

export default store;
