import {Action, MutationTree, StoreOptions, Module} from "vuex";
import Vue from 'vue';
import Vuex from 'vuex';
import Candidate from "@/models/Candidate";
import User from "@/models/User";
import firebase from 'firebase/app';
import Activity from "@/models/Activity";
import Conversation from "@/models/Conversation";
import {ListenRealtimeCallbacks} from "@/models/base";
import Schedule from "@/models/Schedule";
import Position from "@/models/Position";
import Scorecard from "@/models/Scorecard";
import scorecards from "@/components/company/scorecards.vue";
import CandidateScorecard from "@/models/CandidateScorecard";
import CandidateQuestionnaire from "@/models/CandidateQuestionnaire"
import {perf} from "@/firebase";
import _ from "lodash";
import Query = firebase.firestore.Query;
import CandidateSources from "@/models/CandidateSources";
import position from "@/components/LandingPage/HomePageComponent/ComponentHomeBold/position.vue";
// import PositionPipeline from "@/models/Pipeline";

Vue.use(Vuex);

export interface ICandidate {
    candidates: Array<object>,
    flag_reset_filters: Boolean,
    candidate: Candidate,
    activities: Activity[],
    candidate_type: string,
    flag_form_submit:Boolean,
    random_flag: string,
    conversations: Array<object>
    interview_candidate: [],
    position_candidate: Position,
    candidate_scorecards: Array<CandidateScorecard>,
    candidate_loading: boolean;
    score_card_position: Scorecard,
    _candidate_score_card: object,
    team_notes: Array<object>
    candidate_schedules: Array<object>
    candidate_notes: Array<object>,
    unsubscribeCandidateActivity: any,
    unsubscribeCandidateSchedules: any,
    unsubscribeCandidateConversation: any,
    unsubscribeCandidate: any,
    unsubscribePositionCandidate: any,
    unsubscribeCandidates: any,
    unsubscribeCandidateNotes: any,
    unsubscribePublicNotes: any,
    unsubscribeCandidateScorecards: any,
    unsubscribeCandidateSources:any,
    candidateOtherPositons:Array<object>,
    candidateSources : CandidateSources[],
    candidateQuestionnaires: CandidateQuestionnaire[],
    unsubscribeCandidateQuestionnaire: any,
    templateEmailSignature: String,
}

const store: Module<ICandidate, any> = { // wtf is this??
    // namespaced: true,
    state: {
        candidates: [],
        flag_reset_filters: false,
        candidate: null as Candidate,
        activities: [],
        candidate_type: "all",
        flag_form_submit: false,
        random_flag: "",
        conversations: [],
        interview_candidate: [],
        position_candidate: {} as Position,
        candidate_scorecards: [],
        candidate_loading: true,
        score_card_position:{} as Scorecard,
        _candidate_score_card: {} as CandidateScorecard,
        team_notes: [],
        candidate_schedules: [],
        candidate_notes: [],
        unsubscribeCandidateActivity: null,
        unsubscribeCandidateSchedules: null,
        unsubscribeCandidateConversation: null,
        unsubscribeCandidate: null,
        unsubscribePositionCandidate: null,
        unsubscribeCandidates: null,
        unsubscribeCandidateNotes: null,
        unsubscribePublicNotes: null,
        unsubscribeCandidateScorecards: null,
        unsubscribeCandidateSources:null,
        unsubscribeCandidateQuestionnaire: null,
        candidateOtherPositons:[],
        candidateSources:[],
        templateEmailSignature:
            `<p><%= dataMeeting %></p>` +
            `<p><%= signatureHtml %></p>`,
        candidateQuestionnaires: [],
    },
    mutations: {
        setCandidates(state, candidates) {
            state.candidates = candidates;
        },
        setCandidate(state, candidate){
            state.candidate = candidate
        },
        setCurrentCandidate(state, candidate){
            state.candidate = candidate;
        },
        setCurrentInterview(state, data){
            state.interview_candidate = data;
        },
        setCandidateScoreCard(state,data) {
            state._candidate_score_card = data
        },
        setCurrentConversations(state,data) {
            state.conversations = data
        },
        setPositionCandidate(state, position) {
            state.position_candidate = position;
        },
        setCandidateOrtherPosition(state,other_positions){
            state.candidateOtherPositons = other_positions
        },
        setCurrentCandidateType(state, type) {
            state.candidate_type = type;
        },
    },
    getters: {
        templateEmailSignature(state) {
            return state.templateEmailSignature;
        },
        currentConversations: state => {
            return state.conversations;
        },
        currentCandidates: (state, getters, rootState) => {
            if (state.candidates && rootState.user && rootState.user.role != 1) {
                return _.filter(state.candidates,  (candidate: Candidate) => {
                    return candidate.assignee_id == rootState.user.id || candidate.created_by == rootState.user.id;
                });
            }
            return state.candidates;
        },
        currentInterview: state => {
            return state.interview_candidate;
        },
        positionCandidate: state => {
            return state.position_candidate;
        },
        resetFilters: state => {
            return state.flag_reset_filters;
        },
        currentCandidate: state => {
            return state.candidate;
        },
        activities: state => {
            return state.activities
        },
        currentCandidateType: state => {
            return state.candidate_type;
        },
        currentFlagFormSubmit: state => {
            return state.flag_form_submit;
        },
        randomFlag: state => {
            return state.random_flag;
        },
        candidateLoading: state => {
            return state.candidate_loading;
        },
        scorecardPosition: state => {
            return state.score_card_position;
        },
        currentScoreCardCandidateByUser(state){
            return state._candidate_score_card
        },
        candidateScorecards(state) {
            return state.candidate_scorecards;
        },
        teamNotes(state){
            return state.team_notes
        },
        candidateSchedules(state){
            return state.candidate_schedules
        },
        candidateNotes(state){
            return state.candidate_notes
        },
        listCandidateSource(state){
            return state.candidateSources
        },
        currentCandidateQuestionnaires(state){
            return state.candidateQuestionnaires
        },
        unsubscribeCandidateActivity(state) {return state.unsubscribeCandidateActivity},
        unsubscribeCandidateSchedules(state) {return state.unsubscribeCandidateSchedules},
        unsubscribeCandidateConversation(state) {return state.unsubscribeCandidateConversation},
        unsubscribeCandidate(state) {return state.unsubscribeCandidate},
        unsubscribePositionCandidate(state) {return state.unsubscribePositionCandidate},
        unsubscribeCandidates(state) {return state.unsubscribeCandidates},
        unsubscribeCandidateNotes(state) {return state.unsubscribeCandidateNotes},
        unsubscribePublicNotes(state) { return state.unsubscribePublicNotes},
        unsubscribeCandidateScorecards(state) {return state.unsubscribeCandidateScorecards},
        otherPositionsOfCandidate(state){return state.candidateOtherPositons},
        unsubscribeCandidateSources(state) {return state.unsubscribeCandidateSources},
        unsubscribeCandidateQuestionnaire(state){return state.unsubscribeCandidateQuestionnaire}
    },
    actions: {
        setCurrentCandidateType: async (context, data) => {
            context.state.candidate_type = data;
        },

        createActivity: async (context, data) => {
            let activity = new Activity();
            Object.assign(activity, data);
            activity.company_id = context.rootState.company.id;
            activity.actor = context.rootState.user.cloneNewObject();
            activity.insert()
        },

        updateActivity: async (context, data) => {
            let activity = new Activity();
            Object.assign(activity, data);
            activity.update()
        },

        deleteActivity: async (context, data) => {
             let activity = new Activity();
             Object.assign(activity, data);
             activity.delete()
        },

        getActivity: async (context, candidate_id) =>{
            let query = new Activity().getActivity(candidate_id);
            if (context.state.unsubscribeCandidateActivity) {
                context.state.unsubscribeCandidateActivity();
            }
            context.state.unsubscribeCandidateActivity = query.onSnapshot((querySnapshot) => {
                let activities: Activity[] = [];
                querySnapshot.forEach(function(doc) {
                    let record = new Activity();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    activities.push(record);
                });
                context.state.activities = activities;
            });
        },
        getCandidateSources: (context)=>{
            let query = new CandidateSources().getAllCandidateSources();
            if (context.state.unsubscribeCandidateSources) {
                context.state.unsubscribeCandidateSources();
            }
            context.state.unsubscribeCandidateSources = query.onSnapshot((querySnapshot) => {
                let candidate_sources : CandidateSources[] =[];
                if(querySnapshot.empty){
                    let new_candidate_source = new CandidateSources;
                    new_candidate_source.company_id = context.rootState.company.id
                    new_candidate_source.sources = {
                        id : "Portal",
                        name : "Portal",
                        type : "Portal",
                    }
                    new_candidate_source.insert()
                }
                querySnapshot.forEach(function(doc) {
                    let record = new CandidateSources();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    candidate_sources.push(record);
                });
                context.state.candidateSources = candidate_sources;
            });
        },
        getCandidateSchedules: async (context, candidate_id) =>{
            let query = new Schedule().getCandidateSchedules(candidate_id);
            if (context.state.unsubscribeCandidateSchedules) {
                context.state.unsubscribeCandidateSchedules();
            }
            context.state.unsubscribeCandidateSchedules = query.onSnapshot((querySnapshot) => {
                let schedules: Schedule[] = [];
                querySnapshot.forEach(function(doc) {
                    let record = new Schedule();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    schedules.push(record);
                });
                context.state.candidate_schedules = schedules;
            });


        },

        getConversations: async (context, candidate_id) =>{
            let query = new Conversation().getConversation(candidate_id);
            if (context.state.unsubscribeCandidateConversation) {
                context.state.unsubscribeCandidateConversation();
            }
            context.state.unsubscribeCandidateConversation = query.onSnapshot((querySnapshot) => {
                let conversations: Conversation[] = [];
                querySnapshot.forEach(function(doc) {
                    let record = new Conversation();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    conversations.push(record);
                });
                context.state.conversations = conversations;
            });
            return new Promise(((resolve, reject) => {
                query.get().then(() => {
                    resolve()
                })
            }))
        },

        getCandidate: async (context, candidate_id) => {
            context.state.interview_candidate = [];
            let candidate = await new Candidate().getById(candidate_id) as Candidate;
            let position_id = candidate.position_id || candidate.position._id;
            /**
             * An's note: await to get position
             * - because when to know that user can read candidate or not
             * we need know both candidate info & position together
             * -> so load candidate & positonCandidate sync
             */
            await context.dispatch('getPositionCandidate', position_id).then((positionData)=>{
                context.dispatch('getListPipelineStage', positionData.pipeline_id);
            });
            context.dispatch('getCurrentInterview', candidate_id);

            let query = new Candidate().getDocumentById(candidate_id);
            if (context.state.unsubscribeCandidate) {
                context.state.unsubscribeCandidate();
            }
            context.state.unsubscribeCandidate = query.onSnapshot((doc) => {
                let record = new Candidate();
                Object.assign(record, doc.data());
                record.id = doc.id;
                context.state.candidate = record;
            })
            if(candidate.id && candidate.email_address.trim() != '') {
                context.dispatch('getOtherPositionsCandidate', candidate)
            }
            if(candidate.id){
                context.dispatch('getCandidateQuesitionnaire',candidate)
            }
            context.dispatch('getCandidateSources')

            return candidate
        },

        getCurrentInterview: async (context, candidate_id) => {
            let interview = await new Schedule().getCurrentScheduleByCandidateID(candidate_id);
            // @ts-ignore
            context.state.interview_candidate = interview;
            return interview
        },

        getScoreCardCadidate:async (context,scorecard_id)=>{
            let score_card = {} as Scorecard;
            if (scorecard_id) {
                score_card = await new Scorecard().getById(scorecard_id) as Scorecard;
            }
            context.state.score_card_position = score_card;
            return score_card

        },

        getPositionCandidate: async (context, position_id) => {
            let position = {} as Position;
            if (position_id) {
                position = await new Position().getById(position_id) as Position;
                context.state.position_candidate = position;
                let scorecard_id = position.scorecard_id
                context.dispatch('getScoreCardCadidate',scorecard_id);

                let query = new Position().getDocumentById(position_id);
                if (context.state.unsubscribePositionCandidate) {
                    context.state.unsubscribePositionCandidate();
                }
                context.state.unsubscribePositionCandidate = query.onSnapshot((doc) => {
                    let record = new Position();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    context.state.position_candidate = record;
                    context.dispatch('getScoreCardCadidate', record.scorecard_id)
                });
            }
            return position
        },

        resetCandidate(context){
            // alert('resetCandidate')
            let empty_canidate = new Candidate()
            context.commit('setCandidate', empty_canidate);

        },

        getCandidates: async (context) => {
            context.state.candidates = [];
            let query: Query = new Candidate().getCandidates();
            if (context.state.unsubscribeCandidates) {
                context.state.unsubscribeCandidates();
            }
            context.state.unsubscribeCandidates = query.onSnapshot((querySnapshot) => {
                let candidates: Candidate[] = [];
                querySnapshot.forEach(function(doc) {
                    let record = new Candidate();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    candidates.push(record);
                });
                context.state.candidates = candidates;

            });
            return await new Promise((resolve, reject) => {
                query.get().then(() => {
                    context.state.candidate_loading = false;
                    resolve();
                })
            })
        },

        getCandidatesByPosition: async (context, position_id) => {
            context.state.candidates = [];
            let query: Query = new Candidate().getCandidatesByPosition(position_id);
            if (context.state.unsubscribeCandidates) {
                context.state.unsubscribeCandidates();
            }
            context.state.unsubscribeCandidates = query.onSnapshot((querySnapshot) => {
                let candidates: Candidate[] = [];
                querySnapshot.forEach(function(doc) {
                    let record = new Candidate();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    candidates.push(record);
                });
                context.state.candidates = candidates;
            });
            return new Promise((resolve, reject) => {
                query.get().then(() => {
                    resolve();
                })
            })
        },
        getCandidateQuesitionnaire: async (context, candidate) => {
            context.state.candidateQuestionnaires = [];
            let query: Query = new CandidateQuestionnaire().getCandidateQuestionnaire(candidate);
            if (context.state.unsubscribeCandidateQuestionnaire) {
                context.state.unsubscribeCandidateQuestionnaire();
            }
            context.state.unsubscribeCandidateQuestionnaire = query.onSnapshot((querySnapshot) => {
                let candidateQuestionnaires: CandidateQuestionnaire[] = [];
                querySnapshot.forEach(function(doc) {
                    let record = new CandidateQuestionnaire();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    candidateQuestionnaires.push(record);
                });
                context.state.candidateQuestionnaires = candidateQuestionnaires;
            });

            return new Promise((resolve, reject) => {
                query.get().then(() => {
                    resolve();
                })
            })
        },

        setFlagResetFilter: (context, flag: boolean) => {
            context.state.flag_reset_filters = flag;
        },

        setCandidateType: (context, type) => {
            context.state.candidate_type = type;
        },

        setFlagFormSubmit: (context, flag) => {
            context.state.flag_form_submit = flag;
        },

        setRamdomFlag: (context) => {
            context.state.random_flag = Math.random().toString(36).substring(7);
        },

        setRamdomFlagDefault: (context) => {
            context.state.random_flag = "";
        },

        getCandidateScorecards: async (context, candidate_id) => {
            let candidate_scorecards: CandidateScorecard[];
            let query: Query = new CandidateScorecard().getCandidateScoreCards(candidate_id);
            if (context.state.unsubscribeCandidateScorecards) {
                context.state.unsubscribeCandidateScorecards();
            }
            context.state.unsubscribeCandidateScorecards = query.onSnapshot((querySnapshot) => {
                let candidateScorecards: CandidateScorecard[] = [];
                querySnapshot.forEach(function(doc) {
                    let record = new CandidateScorecard();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    candidateScorecards.push(record);
                });
                context.state.candidate_scorecards = candidateScorecards;
            });

        },

        getScoreCardCandidateByUser: async (context,{candidate_id,user_id})=>{
            let _score_card = await new CandidateScorecard().getCandidateScoreCardsByUserID(candidate_id,user_id)
            context.commit('setCandidateScoreCard',_score_card)
        },

        getPublicNotes: async (context, candidate_id) => {
            let team_notes: Activity[];
            let query: Query = new Activity().getPublicNotes(candidate_id);
            if (context.state.unsubscribePublicNotes) {
                context.state.unsubscribePublicNotes();
            }
            context.state.unsubscribePublicNotes = query.onSnapshot((querySnapshot) => {
                let activities: Activity[] = [];
                querySnapshot.forEach(function(doc) {
                    let record = new Activity();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    activities.push(record);
                });
                context.state.team_notes = activities;
            });
        },

        getCandidateNotes: async (context, candidate_id) => {
            let candidate_notes: Activity[];
            let query: Query = new Activity().getCandidateNotes(candidate_id);
            if (context.state.unsubscribeCandidateNotes) {
                context.state.unsubscribeCandidateNotes();
            }
            context.state.unsubscribeCandidateNotes = query.onSnapshot((querySnapshot) => {
                let activities: Activity[] = [];
                querySnapshot.forEach(function(doc) {
                    let record = new Activity();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    activities.push(record);
                });
                context.state.candidate_notes = activities;
            });
        },

        removeCandidate: async(context, candidate_id) =>{
            new Candidate().getById(candidate_id).then((candidateObject)=>{
                candidateObject.delete().then((dataDeleted)=>{
                    let index = context.rootGetters.currentUser.candidate_star.findIndex((_candidate) => {
                        return _candidate.id == candidateObject.id;
                    });
                    if (index > -1){
                        context.rootGetters.currentUser.candidate_star.splice(index, 1);
                        context.rootGetters.currentUser.update();
                    }
                })
            })
        },
        getOtherPositionsCandidate:(context,candidate)=>{
            new Candidate().getPositionOfCopyCandidate(candidate).then((candidates)=>{
                context.commit('setCandidateOrtherPosition',candidates);
                console.log(candidates)
            })
        },
    },
};

export default store;
