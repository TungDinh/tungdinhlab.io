import {Action, MutationTree, StoreOptions, Module} from "vuex";
import Vue from 'vue';
import Vuex from 'vuex';
import Candidate from "@/models/Candidate";
import {ListenRealtimeCallbacks} from "@/models/base";
import PipelineStage from "@/models/PipelineStage";
import Activity from "@/models/Activity";
// import PositionPipeline from "@/models/Pipeline";
import {messages} from "@/messages";
import moment from 'moment';
import Task from "@/models/Task";
import {dispatch} from "vuex-pathify";
import {server_time} from "@/firebase";

export interface KanbanPositionState {
    listCandidate: Array<object>,
    listCandidateToMove: Array<object>,
    listStage: Array<object>,
    listActions: Array<object>,
    isLoading: Boolean,
    positionKanban: Object,
    needRefetchKanban: Object,
    unsubscribeCandidateKanban: any,
    loading: Boolean,
}

const kanban_store: Module<KanbanPositionState, any> = {
    state: {
        unsubscribeCandidateKanban:null,
        listCandidate: [],
        listCandidateToMove: [],
        listStage: [],
        listActions: [],
        isLoading: false,
        positionKanban:{},
        needRefetchKanban: {
            status: false,
            stage: {}
        },
        loading: false,
    },
    mutations: {
        setLoadingKanban(state, data){
            state.loading = data;
        },
        setNeedRefetchKanban(state, data){
            state.needRefetchKanban =  data;
        },
        setPositionKanban(state, data){
            state.positionKanban = data;
        },
        setIsLoading(state, status){
            state.isLoading = status;
        },
        setListActions(state, data){
            state.listActions = data;
        },
        setListStage(state, data){
            state.listStage = data;
        },
        setListCandidate(state, data){
            state.listCandidate = data
        },
        setListCandidateToMove(state, data){
            state.listCandidateToMove = data
        }
    },
    getters: {
        loadingKanban(state){
            return state.loading;
        },
        unsubscribeCandidateKanban(state){
            return state.unsubscribeCandidateKanban;
        },
        needRefetchKanban(state){
            return state.needRefetchKanban;
        },
        positionKanban(state){
            return state.positionKanban;
        },
        isLoading(state){
            return state.isLoading;
        },
        listActions(state){
            return state.listActions;
        },
        listStage(state){
            return state.listStage;
        },
        listCandidate(state){
            return state.listCandidate;
        },
        listCandidateToMove(state){
            return state.listCandidateToMove;
        }
    },
    actions: {
        async doActionSetted(context, data){
            // Data include { actions, candidate, position };
            let actions = data.actions || [];
            let candidate = data.candidate;
            let position = data.position;
            let companyUsers = data.companyUsers;
            let currentUser = data.currentUser;
            let senderName = data.senderName || '';

            let sendEmailAction = actions.find((action)=>{
                return action.action_type == 'emails';
            });
            let sendScoreCard = actions.find((action)=>{
                return action.action_type == "candidate_scorecard";
            });
            if (sendEmailAction){
                new Candidate().sendMailByActions(sendEmailAction.currentTemplate, candidate, position, senderName)
            }
            if (sendScoreCard && candidate.assignee_id){
                let task = new Task();
                task.title = `${messages.SR_021_DS_ICS_TITLE3}: ${candidate.name}`;
                task.description = `${messages.SR_020_SCORECARD_ICS} ${candidate.name} ${messages.SR_020_SCORECARD_ICS_TITLE1} ${position.title}`;
                //@ts-ignore
                task._deadline = moment(server_time.now).add(5, 'days').format();
                let assignee = companyUsers.find((user)=>{
                    return user.id == candidate.assignee_id;
                });
                try{
                    // task.assignee = assignee.cloneNewObject();
                    // task.assignee_id = assignee.id;
                    task.position_id = position.id;
                    task.candidate_id = candidate.id;
                    task.add_log.push({
                        timeUpdate: server_time.now,
                        title: messages.SR_009_CREATED_TASK,
                        created_by: currentUser.cloneNewObject()
                    });
                    task.insert().then((taskInserted)=>{
                        dispatch('createdTasks', {task: taskInserted, users: [assignee]}, {root: true})
                    })
                }
                catch(e){
                    console.log('Error', e)
                }
            }
        },
        setListCandidate(context, data){
            context.commit('setListCandidate',data);
        },
        async getStages(context, data){
            //@ts-ignore
            await new PipelineStage().getAllStages(context.state.positionKanban.pipeline_id).then(async (stages) => {
                context.commit('setListStage', stages);
            })
        },
        async getListCandidate(context, position_id){
            context.state.loading = true;
            //@ts-ignore
            let query = new Candidate().getCandidatesByPosition(position_id || context.state.positionKanban.id);
            if (context.state.unsubscribeCandidateKanban){
                context.state.unsubscribeCandidateKanban();
            }
            context.state.unsubscribeCandidateKanban = query.onSnapshot((querySnapshot) => {
                let candidates: Candidate[] = [];
                querySnapshot.forEach(function(doc) {
                    let record = new Candidate();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    candidates.push(record);
                });
                context.state.listCandidate = candidates;
            });
            return new Promise((resolve, reject) => {
                query.get().then(() => {
                    context.state.loading = false;
                    resolve();
                })
            })
        }
    },
};

export default kanban_store;
