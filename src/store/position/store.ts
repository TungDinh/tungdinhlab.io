import {Action, MutationTree, StoreOptions, Module} from "vuex";
import Vue from 'vue';
import Vuex from 'vuex';
import Position from "@/models/Position";
import {messages} from "@/messages";
import firebase from 'firebase/app';
import User from "@/models/User";
import Candidate from "@/models/Candidate";
import Conversation from "@/models/Conversation";
import Query = firebase.firestore.Query;
import _ from "lodash";

Vue.use(Vuex);

export interface PositionStage {
    currentPosition: Object;
    steps: Array<object>;
    templatePosition: Object;
    listPosition: Array<object>;
    cachePosition: Object;
    unsubscribeAllListPosition: any;
    showArchived: boolean;
    listAddress: Array<String>;
}

const store: Module<PositionStage, any> = {
    // namespaced: true,
    state: {
        unsubscribeAllListPosition: null,
        templatePosition: {
            title: '',
            department: '',
            internal_id: '',
            job_description: {
                'title': messages.SR_020_MODAL_NEW_POSITION_PLACEHOLDER_12,
                'value': null,
                'description': '',
            },
            location: {
                'title': messages.SR_020_MODAL_NEW_POSITION_PLACEHOLDER_4,
                'value': null,
            },
            position_type: {
                'title': messages.SR_020_MODAL_NEW_POSITION_PLACEHOLDER_6,
                'value': null,
            },
            category: {
                'name': messages.SR_020_MODAL_NEW_POSITION_PLACEHOLDER_7,
                'value': null,
            },
            education: {
                'title': messages.SR_020_MODAL_NEW_POSITION_PLACEHOLDER_8,
            },
            experience: {
                'title': messages.SR_020_MODAL_NEW_POSITION_PLACEHOLDER_9,
                'value': null,
            },
            state: '',
            tags: [],
            other_information: [],
            is_remote: false,
            candidate_type: 'enabled',
            application_form: {},
            questionnaire_id: '',
            position_pipeline_action: [],
            pipeline_id: {},
            automated_email_sender: "",
            scorecard_id: '',
            address: ''
        },
        steps: [
            {
                title: messages.SR_020_BUTTON_STEP_TOOLTIP_1,
                active: true,
                done: false
            },
            {
                title: messages.SR_020_BUTTON_STEP_TOOLTIP_2,
                active: false,
                done: false
            },
            {
                title: messages.SR_020_BUTTON_STEP_TOOLTIP_3,
                active: false,
                done: false
            },

            {
                title: messages.SR_020_BUTTON_STEP_TOOLTIP_4,
                active: false,
                done: false
            },
        ],
        currentPosition: {
            job_description: {
                'title': messages.SR_020_MODAL_NEW_POSITION_PLACEHOLDER_12,
                'value': null,
                'description': '',
            },
            id:"",
            name:"",
            address: '',
        },
        listPosition: [],
        cachePosition: {},
        showArchived: false,
        listAddress: [],
    },
    mutations: {
        setCurrentPosition(state, data) {
            state.currentPosition = data;
        },
        setsteps(state, data) {
            state.steps = data;
        },
        setListPosition(state, data) {
            state.listPosition = data;
        },
        setCachePosition(state, data) {
            state.cachePosition = data;
        },
        setShowArchived(state, data) {
            state.showArchived = data;
        }
    },
    getters: {
        listAddress(state){
            return state.listAddress;
        },
        currentPosition(state) {
            return state.currentPosition;
        },
        steps(state) {
            return state.steps;
        },
        templatePosition(state) {
            return state.templatePosition;
        },
        listPosition(state) {
            return state.listPosition;
        },
        cachePosition(state) {
            return state.cachePosition;
        },
        listAllPosition(state) {
            return state.listPosition;
        },
        currentShowArchived(state){
            return state.showArchived;
        }
    },
    actions: {
        setStepByClick: (context, index) => {
            let steps = context.getters['steps'];
            steps.map((step, _index) => {
                if (index == _index) {
                    step.active = true;
                } else {
                    step.active = false;
                }
                return step;
            })
            context.dispatch('setsteps', steps);
        },
        revertSteps: (context, data) => {
            let steps = context.getters['steps'].map((step, index) => {
                if (index == 0) {
                    step.active = true;
                    step.done = false;
                } else {
                    step.active = false;
                }
                return step;
            })
            context.commit('setsteps', steps)
        },
        setCurrentPosition: async (context, data) => {
            context.commit("setCurrentPosition", data);
        },
        setsteps: async (context, data) => {
            context.commit("setsteps", data);
        },
        setListPosition: async (context, data) => {
            context.commit("setListPosition", data);
        },
        async getPosition({commit, state}, position_id: string) {
            const list = state.listPosition as Position[];
            let position = list.find((pos: Position)=>pos.id === position_id);
            if (position) {
                commit('setCurrentPosition', position);
                return position;
            }

            position = await Position.instance.getById(position_id) as Position;
            commit('setCurrentPosition', position);
            return position;
        },

        getListPosition: async (context) => {
            context.state.listPosition = [];
            let query: Query = new Position().getListPositions();
            if (context.state.unsubscribeAllListPosition) {
                context.state.unsubscribeAllListPosition();
            }
            context.state.unsubscribeAllListPosition = query.onSnapshot((querySnapshot) => {
                let positions: Position[] = [];
                querySnapshot.forEach(function(doc) {
                    let record = new Position();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    positions.push(record);
                });
                context.state.listPosition = positions;
                context.state.listAddress = [];
                positions.forEach((position)=>{
                    // Just push address not exist in listAddress
                    let hasExistInListAddress = _.findIndex(context.state.listAddress, (address) => {
                        return address.toLowerCase() == position.address.toLowerCase();
                    }) > -1;
                    if(position.address && !hasExistInListAddress){
                        context.state.listAddress.push(position.address);
                    }
                })

            });
            return new Promise((resolve, reject) => {
                query.get().then(() => {
                    resolve();
                })
            })
        },
    },
};

export default store;
