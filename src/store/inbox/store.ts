import {Action, MutationTree, StoreOptions, Module} from "vuex";
import Vue from 'vue';
import Vuex from 'vuex';
import Candidate from "@/models/Candidate";
import User from "@/models/User";
import firebase from 'firebase/app';
import Inbox from "@/models/Inbox";
import {perf} from "@/firebase";
import Task from "@/models/Task";
// import PositionPipeline from "@/models/Pipeline";

Vue.use(Vuex);

export interface IInbox {
    inboxes: Array<object>,
    positionInboxes: Array<object>,
    unsubscribeInboxes: any,
    showEmailTool: boolean
}

const store: Module<IInbox, any> = {
    // namespaced: true,
    state: {
        inboxes: [],
        positionInboxes: [],
        unsubscribeInboxes: null,
        showEmailTool:false,
    },
    mutations: {
        setInbox(state, inboxes) {
            state.inboxes = inboxes;
        },
        setpositionInboxes(state, positionInboxes) {
            state.positionInboxes = positionInboxes;
        },
        setShowEmailTool(state,showEmailTool) {
            state.showEmailTool = showEmailTool;
        }
    },
    getters: {
        currentInboxes: state => {
            return state.inboxes;
        },
        currentPositionInboxes: state => {
            return state.positionInboxes;
        },
        unsubscribeInboxes: state => {
            return state.unsubscribeInboxes;
        },
        currentShowEmailTool: state => {
            return state.showEmailTool;
        }
    },
    actions: {

        getInboxes: async (context, userflag?) => {
            let fuser = firebase.auth().currentUser;
            if (!fuser) {
                return null;
            }
            context.state.inboxes = [];
            let query;
            if (userflag) {
                let user = await new User().getById(fuser.uid) as User;
                query = new Inbox().getInboxForDashboard(user);
            } else {
                query = new Inbox().getInboxForDashboard();
            }
            context.state.unsubscribeInboxes = query.onSnapshot((querySnapshot) => {
                let inboxes: Inbox[] = [];
                querySnapshot.forEach(function(doc) {
                    let record: any = new Inbox();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    if (!record.position || (record.position && record.position.status != "archived")) {
                        inboxes.push(record);
                    }
                });
                context.state.inboxes = inboxes;
            });
            return new Promise((resolve, reject) => {
                query.get().then(() => {
                    resolve();
                })
            })

        },


        getPositionInboxes: async (context,{position_id, userflag, user}) => {
            let company = context.rootState.company;
            let query;
            if (userflag) {
                query = new Inbox().getPositionInboxes(position_id, user);
            } else {
                query = new Inbox().getPositionInboxes(position_id);
            }
            if (context.state.unsubscribeInboxes) {
                context.state.unsubscribeInboxes();
            }
            context.state.unsubscribeInboxes = query.onSnapshot((querySnapshot) => {
                let inboxes: Inbox[] = [];
                querySnapshot.forEach(function(doc) {
                    let record: any = new Inbox();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    inboxes.push(record);
                });
                context.state.positionInboxes = inboxes;
            });
            return new Promise((resolve, reject) => {
                query.get().then(() => {
                    resolve();
                })
            })
        },

    },
};

export default store;
