import {Action, MutationTree, StoreOptions, Module} from "vuex";
import Vuex from 'vuex';
import User from "@/models/User";
import Company from "@/models/Company";
import Router from "@/router";
import Questionnaire from "@/models/Questionnaire";
import Question from "@/models/Question";
import questionnaires from "@/components/company/questionnaires.vue";
import _ from "lodash";

export interface ModalState {
    _questionnaires: Array<object>,
    _questions: Array<object>,
    _deleted_questions: Array<object>
}

const store: Module<ModalState, any> = {
    //namespaced: true,
    state: {
        _questions: [],
        _questionnaires: [],
        _deleted_questions: []
    },
    mutations: {
        setQuestionnaires(state, data) {
            state._questionnaires = data;
        },
        addQuestionnaire(state, data) {
            state._questionnaires = [...state._questionnaires, data];
        },
        updateQuestion(state, {question, index}) {
            let questions = [...state._questions];
            questions[index] = question;
            state._questions = questions;
        },
        setQuestions(state, data) {
            state._questions = data;
        },
        addQuestion(state, data) {
            state._questions = [...state._questions, data];
        },
        clearQuestions(state) {
            state._questions = [];
        },
        addDeleteQuestion(state, questions) {
            state._deleted_questions = [...state._deleted_questions, ...questions];
        },
        clearDeleteQuestions(state){
            state._deleted_questions = []
        },
        updateQuestionnaire(state, questionnaire){
            let questionnaires = [...state._questionnaires];
            let index = _.findIndex(questionnaires, (q: any) => {
                return q.id == questionnaire.id
            });
            if (index != -1) {
                questionnaires[index] = questionnaire;
                state._questionnaires = questionnaires
            }


        }
    },
    getters: {
        questionnaires(state) {
            return state._questionnaires;
        },
        questions(state) {
            return state._questions;
        },
    },
    actions: {
        addQuestionAction: async (context, data) => {
            context.commit('addQuestion', data);
        },
        updateQuestionAction: async (context, {question, index}) => {
            context.commit("updateQuestion", {question, index})
        },
        addTempRemoveQuestion: async (context, index) => {
            let deleted_questions = context.state._questions.splice(index, 1);
            context.commit("addDeleteQuestion", deleted_questions);

        },
        removeQuestion: async (context, questions: Array<object>) => {
            questions.forEach(function (question) {
                if ('id' in question) {
                    let temp = new Question();
                    Object.assign(temp, question);
                    temp.delete()
                }
                context.state._deleted_questions = [];
            })
        },
        clearDeleteQuestions: async (context) =>{
            context.commit("clearDeleteQuestions")
        },
        getQuestions: async (context, questionnaire_id): Promise<Question[]> => {
            let results: Question[] = new Array();
            results = await new Question().getAllQuestion(questionnaire_id);
            context.commit("setQuestions", results)
            return results;
        },
        getQuestionnaires: async context => {
            // Should be use Promise for this. In template, sometime this action return [].
            // Make sure get done => resolve.
            return new Promise(resolve => {
                new Questionnaire().getAllQuestionnaires().then((data) => {
                    context.commit("setQuestionnaires", data)
                    resolve();
                });
            })
        },
        createQuestionnaire: async (context , data) => {
            let questionnaire = new Questionnaire();
            data.company_id = context.getters.currentCompany.id;
            Object.assign(questionnaire, data);
            questionnaire.insert().then((data) => {
                let question;
                context.state._questions.forEach((item, index) => {
                    question = new Question();
                    Object.assign(question, item);
                    question.questionnaire_id = data.id;
                    question.order = index;
                    question.insert();
                });
                context.dispatch('getQuestionnaires');
            });
        },
        updateQuestionnaire: async (context, questionnaire) => {
            let _questionnaire = new Questionnaire();
            Object.assign(_questionnaire, questionnaire);
            _questionnaire.update().then( (data) => {
                let question;
                context.state._questions.forEach((item, index) => {
                    question = new Question();
                    Object.assign(question, item);
                    question.questionnaire_id = data.id;
                    question.order = index;
                    if (question.id) {
                        question.update()
                    } else {
                        question.insert();
                    }
                });
                context.commit("updateQuestionnaire", data);
                context.dispatch('removeQuestion', context.state._deleted_questions);
            });
        },
        removeQuestionnaire: async (context, data) => {
            let questionnaire = new Questionnaire();
            Object.assign(questionnaire, data);
            new Question().getAllQuestion(questionnaire.id).then((questions)=>{
                questions.forEach((question)=> {
                    question.delete()
                });
            });
            questionnaire.delete();
            context.dispatch('getQuestionnaires')
        },

        duplicateQuestionnaire: async  (context, questionnaire) =>{
            let new_questionnaire = new Questionnaire();
            let data = questionnaire.cloneNewObject();
            delete data['id'];
            Object.assign(new_questionnaire, data);
            new_questionnaire.name += " (Copy)";
            new_questionnaire.insert().then((new_questionnaire) => {
                new Question().getAllQuestion(questionnaire.id).then((questions)=>{
                    questions.forEach(async (question)=> {
                        let new_question = new Question();
                        let data = question.cloneNewObject();
                        delete data['id'];
                        data["questionnaire_id"] = new_questionnaire.id;
                        Object.assign(new_question, data);
                        new_question.insert();
                    })
                });
                context.dispatch('getQuestionnaires');
            });
        },
        clearQuestions: async (context) => {
            context.commit("clearQuestions");
        }
    }
};

export default store;
