import {Action, MutationTree, StoreOptions, Module} from "vuex";
import Vue from 'vue';
import Vuex from 'vuex';
import Position from "@/models/Position";
import {messages} from "@/messages";
import firebase from 'firebase/app';
import User from "@/models/User";
import Candidate from "@/models/Candidate";
import Task from "@/models/Task";
import Schedule from "@/models/Schedule";
import Conversation from "@/models/Conversation";
import Inbox from "@/models/Inbox";
import Query = firebase.firestore.Query;
import any = jasmine.any;
import moment from 'moment';
import _ from "lodash";
import PositionStateHistory from "../../models/PositionStateHistory";
import PipelineReport from "@/models/PipelineReport";
moment.locale('vi');
moment.updateLocale('vi', {
    meridiem : function (hours, minute, isLowercase) {
        if (hours < 12) {
            return  'SA' ;
        } else {
            return  'CH' ;
        }
    }
});

Vue.use(Vuex);

export interface PositionStage {
    dashboard_positions: Array<object>,
    my_tasks: Task[],
    dashboard_candidates: Array<object>,
    count_positions_activated: Number,
    count_new_candidates: Number,
    count_candidates_moved: Number,
    count_candidate_hired: Number,
    my_schedules: Array<Schedule>,
    dashboard_inbox: Array<Inbox>,
    unsubscribePositionDashboard: any
    unsubscribeCandidateDashboard: any,
    unsubscribeDashboardInbox: any
}

const store: Module<PositionStage, any> = {
    // namespaced: true,
    state: {
        dashboard_positions: [],
        my_tasks: [],
        dashboard_candidates: [],
        count_positions_activated: 0,
        count_new_candidates: 0,
        count_candidates_moved: 0,
        count_candidate_hired: 0,
        my_schedules: [],
        dashboard_inbox: [],
        unsubscribePositionDashboard: null,
        unsubscribeCandidateDashboard: null,
        unsubscribeDashboardInbox: null
    },
    mutations: {
        setDashboardPositions(state, positions) {
            state.dashboard_positions = positions;
        },
        setMyTask(state, tasks) {
            state.my_tasks = tasks;
        },
    },
    getters: {
        dashboardPositions(state) {
            return state.dashboard_positions;
        },
        myTasks(state) {
            return state.my_tasks;
        },
        dashboardCandidates(state) {
            return state.dashboard_candidates;
        },
        countPositionsActivated(state) {
            return state.count_positions_activated;
        },
        countNewCandidates(state) {
            return state.count_new_candidates;
        },
        countCandidatesMoved(state) {
            return state.count_candidates_moved;
        },
        countCandidateHired(state) {
            return state.count_candidate_hired;
        },
        mySchedules(state) {
            return state.my_schedules;
        },
        dashboardInbox(state) {
            return state.dashboard_inbox;
        },
        unsubscribeCandidateDashboard(state) {
            return state.unsubscribeCandidateDashboard;
        },
        unsubscribePositionDashboard(state) {
            return state.unsubscribePositionDashboard;
        },
        unsubscribeDashboardInbox(state) {
            return state.unsubscribeDashboardInbox;
        }
    },
    actions: {
        getDashboardPositions: async (context, {company_id, createdBy}) => {
            let query: any;
            if (createdBy) {
                query = new Position().getFilteredPositions(company_id, createdBy)
            } else {
                query = new Position().getFilteredPositions(company_id)
            }
            if (context.state.unsubscribePositionDashboard) {
                context.state.unsubscribePositionDashboard();
            }
            context.state.unsubscribePositionDashboard = query.onSnapshot((querySnapshot) => {
                let positions: Position[] = [];
                querySnapshot.forEach(function(doc) {
                    let record = new Position();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    if (record.status != 'archived') {
                        positions.push(record);
                    }
                });
                context.commit('setDashboardPositions', positions);
            });
            return new Promise((resolve, reject) => {
                query.get().then(() => {
                    resolve();
                })
            })
        },
        getMyTasks: async (context) => {
            let tasks = await new Task().getTaskByAssigneeID(context.getters.currentUser.id);
            context.commit('setMyTask', tasks);

        },
        getCandidateForDashboard: async (context, {option, value}) => {
            let query: Query;
            if (option == 'new_candidates') {
                query = new Candidate().getNewCandidates();
            } else if (option == "my_candidates") {
                query = new Candidate().getCandidatesByAssignee(value);
            } else if (option == "unseen_candidate") {
                query = new Candidate().getUnseenCandidates();
            }

            if (context.state.unsubscribeCandidateDashboard) {
                context.state.unsubscribeCandidateDashboard();
            }
            context.state.unsubscribeCandidateDashboard = query.onSnapshot((querySnapshot) => {
                let candidates: Candidate[] = [];
                querySnapshot.forEach(function(doc) {
                    let record = new Candidate();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    if (!record.position || (record.position && record.position.status != "archived")) {
                        candidates.push(record);
                    }
                });
                context.state.dashboard_candidates = candidates;
            });
            return new Promise((resolve, reject) => {
                query.get().then(() => {
                    resolve();
                })
            })
        },
        aggregatePositionsActivated: async (context): Promise<Number> => {
            let count = await new PositionStateHistory().aggregatePositionsActivated();
            context.state.count_positions_activated = count;
            return count;
        },
        aggregateNewCandidates: async (context): Promise<Number> => {
            let count = await new PipelineReport().aggregateNewCandidates();
            context.state.count_new_candidates = count;
            return count;
        },
        aggregateCandidatesMoved: async (context): Promise<Number> => {
            let count = await new PipelineReport().aggregateCandidatesMoved();
            context.state.count_candidates_moved = count;
            return count;
        },
        aggregateCandidateHired: async (context): Promise<Number> => {
            let count = await new PipelineReport().aggregateCandidateHired();
            context.state.count_candidate_hired = count;
            return count;
        },
        getRecentStats: async (context) => {
            context.dispatch('aggregatePositionsActivated');
            context.dispatch('aggregateNewCandidates');
            context.dispatch('aggregateCandidatesMoved');
            context.dispatch('aggregateCandidateHired');
        },
        hideMyTaskIsDone: (context, task) => {
            let index = context.state.my_tasks.findIndex((item) => {
                return item.id == task.id
            });
            if (index != -1) {
                context.state.my_tasks.splice(index, 1)
            }
        },
        getMySchedules: async (context) => {
            let user = context.rootState.user;
            let my_schedules = await new Schedule().getMySchedules(user);
            context.state.my_schedules = my_schedules;
            return my_schedules;
        },
        getDashboardInbox: async (context, user?: User) => {
            let query = new Inbox().getInboxForDashboard(user) as any;
            if (context.state.unsubscribeDashboardInbox) {
                context.state.unsubscribeDashboardInbox();
            }
            context.state.unsubscribeDashboardInbox = query.onSnapshot((querySnapshot) => {
                let inboxes: Inbox[] = [];
                querySnapshot.forEach(function(doc) {
                    let record: any = new Inbox();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    if (!record.position || (record.position && record.position.status != "archived")) {
                        inboxes.push(record);
                    }
                });
                context.state.dashboard_inbox = inboxes;
            });
            return new Promise((resolve, reject) => {
                query.get().then(() => {
                    resolve();
                })
            })
        }

    },
};

export default store;
