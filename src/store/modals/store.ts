import {Action, MutationTree, StoreOptions, Module} from "vuex";
import Vuex from 'vuex';

export interface ModalState {
    addTask: boolean;
    addCandidate: boolean;
    addCandidateWithStage: object;
}

const store: Module<ModalState, any> = {
    namespaced: true,
    state: {
        addTask: false,
        addCandidate: false,
        addCandidateWithStage: {},
     },
    mutations: {
        showAddTask(state, val = true) {
            state.addTask = val;
        },
        showAddCandidate(state, val = true) {
            if (val == false){
                if (state.addCandidateWithStage.hasOwnProperty('status')){
                    //@ts-ignore
                    state.addCandidateWithStage.status = false;
                }
            }
            state.addCandidate = val;
        },
        setAddCandidateWithStage(state, val){
            state.addCandidateWithStage = val;
        }
    },
    getters: {
        addTask(state): boolean {
            return state.addTask;
        },
        addCandidate(state): boolean {
            return state.addCandidate;
        },
        addCandidateWithStage(state): object{
            return state.addCandidateWithStage;
        }
    }
};

export default store;
