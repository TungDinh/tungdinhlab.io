import {Module} from 'vuex';
import moment from "moment";
import * as firebase from "firebase";
import Schedule from "@/models/Schedule";
import User from "@/models/User";
import Conversation from "@/models/Conversation";
import Query = firebase.database.Query;
import {messages} from "@/messages";
import {server_time} from "@/firebase";

export interface ScheduleMeeting {
    listScheduleMeeting: Array<object>;
    templateMeeting: String;
    listAllScheduleMeeting: Array<object>;
    currentScheduleMeeting: object;
    unsubscribeSchedules: any;
    templateMeetingSignature: String;
}

const store: Module<ScheduleMeeting, any> = {
    namespaced: false,
    state: {
        currentScheduleMeeting: {
            interviewDate: moment(server_time.now).add(1, 'days').format(),
            listMeeting: [
                {
                    title :'',
                    start_time_by_number: 'Time',
                    interview_guide: 'Interview Guide',
                    interviewers: [],
                    description: '',
                    location: '',
                    duration: 'Duration'
                }
            ],
            timezone: '+07',
        },
        listScheduleMeeting: [],
        templateMeeting:
            `<p>Hi <%= candidateName %>,</p>
<p>${messages.SR_021_INVITE_TITLE1} <%= dateMeeting %>:</p>
<%= somethingAboutMeeting %>
<p>${messages.SR_021_INVITE_TITLE2}</p>
<p>${messages.SR_021_INVITE_TITLE3}</p><p><%= regardByWho %></p>`,
        templateMeetingSignature:
            `<p><%= dataMeeting %></p>` +
            `<div class="custom-signaturel" ><%= signatureHtml %></div>`,
        listAllScheduleMeeting: [],
        unsubscribeSchedules: null
    },
    mutations: {
        setListScheduleMeeting(state, data) {
            state.listScheduleMeeting = data;
        },
        setListAllScheduleMeeting(state, data) {
            state.listAllScheduleMeeting = data;
        },
        setCurrentScheduleMeeting(state, data){
            state.currentScheduleMeeting =  data;
        },
    },
    getters: {
        listScheduleMeeting(state) {
            return state.listScheduleMeeting;
        },
        templateMeeting(state) {
            return state.templateMeeting;
        },
        templateMeetingSignature(state) {
            return state.templateMeetingSignature;
        },
        listAllScheduleMeeting(state) {
            return state.listAllScheduleMeeting;
        },
        currentScheduleMeeting(state){
            return state.currentScheduleMeeting;
        },
        unsubscribeSchedules(state) {
            return state.unsubscribeSchedules;
        }
    },
    actions: {
        getAllSchedules: async context => {
            let fuser = firebase.auth().currentUser;
            context.state.listAllScheduleMeeting = [];
            let user = await new User().getById(fuser.uid) as User;
            let query = new Schedule().getAllSchedules();
            if (context.state.unsubscribeSchedules) {
                context.state.unsubscribeSchedules();
            }
            context.state.unsubscribeSchedules = query.onSnapshot((querySnapshot) => {
                let schedules: Schedule[] = [];
                querySnapshot.forEach(function(doc) {
                    let record = new Schedule();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    schedules.push(record);
                });
                context.state.listAllScheduleMeeting = schedules;
            });
        },

        getPositionSchedules: async (context, position_id) => {
            context.state.listAllScheduleMeeting = [];
            let query = new Schedule().getPositionSchedules(position_id);
            if (context.state.unsubscribeSchedules) {
                context.state.unsubscribeSchedules();
            }
            context.state.unsubscribeSchedules = query.onSnapshot((querySnapshot) => {
                let schedules: Schedule[] = [];
                querySnapshot.forEach(function(doc) {
                    let record = new Schedule();
                    Object.assign(record, doc.data());
                    record.id = doc.id;
                    schedules.push(record);
                });
                context.state.listAllScheduleMeeting = schedules;
            });
        }
    },

};

export default store;
