export function urlLandingpage(relative_path: string, domain: string = '') {
    let NODE_ENV = process.env.NODE_ENV || "";
    NODE_ENV = NODE_ENV.toLowerCase();

    switch (NODE_ENV) {
        case "development":
        default:
            return `${window.location.origin}/job/${domain}${relative_path}`;
        /**
         * PREVIEW LINK ON DEVELOPMENT BUILD
         */

        case "qc":
            return `https://job-cloudjetpotential.netlify.com/${domain}${relative_path}`;
        /**
         * LINK FOR QC
         */
        case "qc-stable":
            return `https://job-stable-potential.netlify.com/${domain}${relative_path}`;

        case "production":
            return `https://${domain}.cloudjetpotential.com${relative_path}`;
    }
}
export function urlCandidate(candidate_id:string) {
    let NODE_ENV = process.env.NODE_ENV || "";
    NODE_ENV = NODE_ENV.toLowerCase();

    switch (NODE_ENV) {
        case "development":
        default:
            return `${window.location.origin}/candidate/${candidate_id}/experience`;
        /**
         * PREVIEW LINK ON DEVELOPMENT BUILD
         */

        case "qc":
            return `https://qc-cloudjetpotential.netlify.com/candidate/${candidate_id}/experience`;
        /**
         * LINK FOR QC
         */
        case "qc-stable":
            return `https://stable-potential.netlify.com/candidate/${candidate_id}/experience`;

        case "production":
            return `https://pro.cloudjetpotential.com/candidate/${candidate_id}/experience`;
    }

}
