import moment from "moment";
import {messages} from '@/messages'

export interface IAnalyticsPositionFilterOption {
    type: string,
    value: string,
    text?: string,
}

const AnalyticsPositionFilterBySystemOptions:IAnalyticsPositionFilterOption[]=
    [
        {
            text: messages.SR_050_FILTER_POSITION_TEXT_ALL_COMPANY_POSITIONS,
            value: "all",
        },
        {
            text: messages.SR_050_FILTER_POSITION_TEXT_ALL_MY_POSITIONS,
            value: "my-all",
        },
        {
            text: messages.SR_050_FILTER_POSITION_PUBLISHED_POSITIONS,
            value: "my-published",
        },
        {
            text: messages.SR_050_FILTER_POSITION_TEXT_MY_DRAFT_POSITIONS,
            value: "my-draft",
        },
    ].map((obj)=>{
        return {...obj,
            type: "by_system",
        };
    });

export default AnalyticsPositionFilterBySystemOptions

