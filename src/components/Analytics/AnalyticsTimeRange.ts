import moment from "moment";
import {messages} from '@/messages'
import {server_time} from "@/firebase";

export interface IAnalyticsTimeRange {
    text?: string,
    start: moment.Moment,
    end: moment.Moment
}

const AnalyticsTimeRanges:IAnalyticsTimeRange[]=[
    {
        text: messages.SR_050_FITLER_DATE_TEXT_LAST_7_DAYS,
        start: moment(server_time.now).subtract(7-1, "day"),
        end: moment(server_time.now),
    },
    {
        text: messages.SR_050_FITLER_DATE_TEXT_LAST_14_DAYS,
        start: moment(server_time.now).subtract(14-1, "day"),
        end: moment(server_time.now),
    },
    {
        text: messages.SR_050_FITLER_DATE_TEXT_THIS_MONTH,
        start: moment(server_time.now).startOf('month'),
        end: moment(server_time.now)
    },


    {
        text: messages.SR_050_FITLER_DATE_TEXT_LAST_MONTH,
        start: moment(server_time.now).subtract(1, 'months').startOf('month'),
        end: moment(server_time.now).subtract(1, 'months').endOf('month'),
    },
    {
        text: messages.SR_050_FITLER_DATE_TEXT_LAST_30_DAYS,
        start: moment(server_time.now).subtract(30-1, "day"),
        end: moment(server_time.now),
    },
    {
        text: messages.SR_050_FITLER_DATE_TEXT_LAST_90_DAYS,
        start: moment(server_time.now).subtract(90-1, "day"),
        end: moment(server_time.now),
    },
    {
        text: messages.SR_050_FITLER_DATE_TEXT_LAST_180_DAYS,
        start: moment(server_time.now).subtract(180-1, "day"),
        end: moment(server_time.now),
    },
];
// alert(`${AnalyticsTimeRanges[0].start.format()} - ${AnalyticsTimeRanges[0].end.format()}`)

export default AnalyticsTimeRanges;
