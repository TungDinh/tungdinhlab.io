import {messages} from "@/messages";

export default [
    [
        {
            icon: "globe",
            text: "Cài đặt trang tuyển dụng",
            to: 'portal-type',
        },
        // {
        //     icon: "briefcase",
        //     text: "Cấu hình  Portal Setting",
        //     to: 'portal-employees',
        // },
        {
            icon: "comments",
            text: "Cấu hình thông điệp",
            to: 'portal-messaging',
        },
    ],
    [
        {
            icon: "columns",
            text: messages.SR_007_PIPELINE_TITLE,
            to: 'setting-pipeline',
        },
        {
            icon: "book",
            text: messages.SR_006_INTERVIEW_GUIDES_TITILE,
            to: 'setting-interview-guides',
        },
        {
            icon: "list",
            text: messages.SR_005_QUESTIONNAIRES,
            to: 'setting-questionnaires',
        },
        {
            icon: "thumbs-up",
            text: messages.SR_053_TITLE,
            to: 'setting-scorecards',
        },
        {
            icon: "envelope",
            text: messages.SR_052_MSG_TEMPLATE_TITLE,
            to: 'messages-template',
        },
    ]
    /// ....
]
