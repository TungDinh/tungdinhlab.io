export const educations = [
    {title: "Unspecifiled", value: "unspecifiled"},
    {title: "Trung học cơ sở", value: "Trung học cơ sở"},
    {title: "Trung học phổ thông", value: "Trung học phổ thông"},
    {title: "Trung cấp", value: "Trung cấp"},
    {title: "Cao đẳng", value: "Cao đẳng"},
    {title: "Đại học ", value: "Đại học "},
    {title: "Thạc sĩ", value: "Thạc sĩ"},
    {title: "Tiến sĩ", value: "Tiến sĩ"},
    {title: "Chứng chỉ liên quan", value: "Chứng chỉ liên quan"},
];
