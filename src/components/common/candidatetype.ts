import {messages} from "@/messages";

export const candidatetype = [
    {
        "value": "applied",
        "name": messages.COMMON_APPLIED
    },
    {
        "value": "sourced",
        "name": messages.COMMON_SOURCED
    },
];
