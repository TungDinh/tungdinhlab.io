import * as moment from "moment";

export interface IDateFilter {
    text?: string;
    start: number;
    end: number;
    default?: boolean;
    id:string;
}
