import moment from 'moment';
import {IDateFilter} from "@/components/OptionDropdown/IDateFilter";
import {messages} from "@/messages";
import {server_time} from "@/firebase";

export default [
    {   id:"1",
        default: true,
        text: messages.SR_028_CANDIDATE_FILTER_DATE_LAST_7_DAYS,
        start: moment(server_time.now).startOf('day').subtract(6, "day").valueOf(),
        end: moment(server_time.now).endOf('day').valueOf(),
    },
    {   id:"2",
        text: messages.SR_028_CANDIDATE_FILTER_DATE_LAST_30_DAYS,
        start: moment(server_time.now).startOf('day').subtract(29, "day").valueOf(),
        end: moment(server_time.now).endOf('day').valueOf(),
    },
    {   id:"3",
        text: messages.SR_028_CANDIDATE_FILTER_DATE_LAST_WEEK,
        start: moment(server_time.now).startOf("week").subtract(1, "week").valueOf(),
        end: moment(server_time.now).endOf("week").subtract(1, "week").valueOf(),
    },
    {   id:"4",
        text: messages.SR_028_CANDIDATE_FILTER_DATE_THIS_WEEK,
        start: moment(server_time.now).startOf("week").valueOf(),
        end: moment(server_time.now).endOf('week').valueOf(),
    },
    {   id:"5",
        text: messages.SR_028_CANDIDATE_FILTER_DATE_TODAY,
        start:moment(server_time.now).startOf('day').valueOf(),
        end: moment(server_time.now).endOf('day').valueOf(),
    },
    {   id:"6",
        text: messages.SR_028_CANDIDATE_FILTER_DATE_YESTERDAY,
        start: moment(server_time.now).startOf('day').subtract(1, "day").valueOf(),
        end: moment(server_time.now).endOf('day').subtract(1, "day").valueOf(),
    },

] as IDateFilter[];
