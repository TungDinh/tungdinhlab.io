import {messages} from "@/messages";

export default {
    'profile_photo_url': {text: messages.SR_028_COLUMN_PHOTO, check: true,},
    'name': {text: messages.SR_028_COLUMN_NAME, check: true, sort: true, disabled: true},
    'overall_score': {text: messages.SR_028_COLUMN_SCORE, check: true, sort: true},
    'headline': {text: messages.SR_028_COLUMN_HEADLINE, check: true,},
    'resume': {text: messages.SR_028_COLUMN_RESUME, check: true},
    'email_address': {text: messages.SR_028_COLUMN_EMAIL, check: true},
    'position': {text: messages.SR_028_COLUMN_POSITION, check: true, sort: true},
    'phone_number': {text: messages.SR_028_COLUMN_PHONE, check: false,},
    'location': {text: messages.SR_028_COLUMN_LOCATION, check: false, sort: true},
    'stage': {text: messages.SR_028_COLUMN_STAGE, check: true, sort: true},
    'source': {text: messages.SR_028_COLUMN_SOURCE, check: true, sort: true},
    'createdAt': {text: messages.SR_028_COLUMN_CREATED_AT, check: true, sort: true},
    'updatedAt': {text: messages.SR_028_COLUMN_UPDATED_AT, check: true, sort: true},
    'bookmark': {text: '', check: true, disabled: true,hided:true},
}
