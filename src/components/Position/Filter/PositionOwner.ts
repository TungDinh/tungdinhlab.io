//created: AN Hoang

export default [
    {
        value: 'all',
        text: "All Positions (Admin)",
    },
    {
        value: 'mine',
        text: "My Positions",
    },
];
