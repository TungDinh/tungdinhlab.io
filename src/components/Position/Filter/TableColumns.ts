// created: An Hoang

import {messages} from "@/messages";

// TODO: replace message's languages
export default {
    'state':        {text: messages.SR_051_COLUMN_STATE,    check: true, sort: true , disabled: true},
    'name':         {text: messages.SR_051_COLUMN_NAME,     check: true, sort: true, disabled: true},
    'location':     {text: messages.SR_051_COLUMN_LOCATION,    check: true, sort: true},
    'category':     {text: messages.SR_051_COLUMN_CATEGORY, sort: true},
    'pipeline':     {text: messages.SR_051_COLUMN_PIPELINE, sort: true},
    'experience':   {text: messages.SR_051_COLUMN_EXPERIENCE, check: true, sort: true},
    'count_candidate':   {text: messages.SR_051_COLUMN_CANDIDATES,    check: true, sort: true},
    'createdAt':      {text: messages.SR_051_COLUMN_CREATED,   check: true, sort: true},
    'updatedAt': {text: messages.SR_051_COLUMN_LAST_UPDATED, check: true, sort: true},
}
