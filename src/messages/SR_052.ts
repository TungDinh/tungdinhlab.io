export const SR_052 = {
    SR_052_MSG_TEMPLATE_TITLE:"Mẫu tin nhắn",
    SR_052_MSG_TEMPLATE_DESC:"Tạo và tùy chỉnh mẫu tin nhắn dùng để trao đổi với ứng viên.",
    SR_052_TEXT_SORT:"Sắp xếp",
    SR_052_SORT_ALPHABETICAL:"Xếp theo thứ tự ABC",
    SR_052_SORT_DEFAULT:"Mặc định",
    SR_052_TXT_DEFAULT_SUBJECT:"Tiêu đề mặc định",
    SR_052_TXT_DEFAULT_SUBJECT_DESC:"Điều chỉnh tiêu đề mặc định được sử dụng khi gửi email cho ứng viên.",
    SR_052_BTN_UPDATE_SUBJECT:"Cập nhật tiêu đề",
    SR_052_BTN_ADD_EMAIL_TEMPLATE:"Thêm mẫu email",
    SR_052_TITLE_ADD_EMAIL_TEMPLATE:"Thêm mẫu",
    SR_052_TITLE_UPDATE_EMAIL_TEMPLATE:"Tùy chỉnh mẫu",
    SR_052_TEMLATE_NAME_PLACEHOLDER:"Tên mẫu (Bắt buộc)",
    SR_052_EMAIL_SUBJECT_PLACEHOLDER:"Tiêu đề email",
    SR_052_EMAIL_BODY:"Nội dung Email",
    SR_052_UPLOADING:"Đang tải lên...",
    SR_052_TXT_ATTACH_DOCUMENT:"Đính kèm tài liệu",
    SR_052_TXT_TEMPLATE_VAR:"Biến sử dụng trong mẫu",
    SR_052_VAR_C_FIRST_NAME:"Tên ứng viên",
    SR_052_VAR_C_FULL_NAME:"Tên đầy đủ ứng viên",
    SR_052_VAR_C_EMAIL:"Email ứng viên",
    SR_052_VAR_POS_TITLE:"Tên vị trí",
    SR_052_VAR_POS_LINK:"Link đến vị trí tuyển dụng",
    SR_052_VAR_COMPANY_NAME:"Tên công ty",
    SR_052_VAR_COMPANY_USER:"Nhân viên",
    SR_052_VAR_COMPANY_U_FIRST_NAME:"Tên nhân viên",
    SR_052_EMAIL_TEMPLATE_1_NAME:"Đã ứng tuyển",
    SR_052_EMAIL_TEMPLATE_1_CONTENT:"Tôi đã đọc Chính Sách Bảo Mật bên dưới và đồng ý cung cấp dữ liệu của tôi như một phần của việc nộp sơ xin việc.",
    SR_052_EMAIL_TEMPLATE_2_NAME:"Đưa ra đề nghị",
    SR_052_EMAIL_TEMPLATE_2_CONTENT:`<p>[[candidate_first_name]] thân mến,</p>

<p>Chúng tôi vui mừng được thông báo bạn đã trúng tuyển vào vị trí [[position_title]] tại [[company_user_first_name]]. Chúng tôi gửi kèm tài liệu chi tiết bên dưới. Ứng viên vui lòng phản hồi trước ngày {INSERT DATE HERE}. Nếu đồng ý, ngày bắt đầu làm việc của ứng viên là ngày {INSERT DATE HERE}.</p>

<p>Trong thời gian từ nay đến khi bạn đưa ra quyết định của mình, nếu bạn có bất kỳ câu hỏi gì bạn có thể liên lạc tôi để được giải đáp. Chúng tôi rất mong sẽ nhận được phản hồi sớm từ bạn.</p>

<p>Trân trọng,</p>
<p>[[company_user_first_name]]</p>`,
    SR_052_EMAIL_TEMPLATE_3_NAME:"Từ chối (Sau phỏng vấn)",
    SR_052_EMAIL_TEMPLATE_3_CONTENT:`<p>[[candidate_first_name]] thân mến,</p>

<p>Trước hết, [[company_name]] chân thành cám ơn bạn đã ứng tuyển vị trí [[position_title]] của công ty.</p>

<p>Chúng tôi rất tiếc phải thông báo với bạn rằng chúng tôi đã quyết định lựa chọn một ứng viên khác phù hợp hơn với vị trí [[position_title]] bởi ở thời điểm này chúng tôi nhận thấy { lí do từ chối }.</p>

<p>Thực sự chúng tôi cũng rất ấn tượng với kĩ năng, thành tích mà bạn đã đạt được và những nỗ lức bạn đã thể hiện qua vòng phỏng vấn. Chính vì vậy chúng tôi tin rằng bạn có thể phù hợp với công ty chúng tôi cho những vị trí trong tương lai, và chúng tôi sẽ liên lạc lại với bạn ngay khi có cơ hội.</p>

<p>Chúc bạn mọi điều may mắn trong sự nghiệp!</p>

<p>Trân trọng,</p>
<p>[[company_user_first_name]]</p>`,
    SR_052_EMAIL_TEMPLATE_4_NAME:"Từ chối",
    SR_052_EMAIL_TEMPLATE_4_CONTENT:`<p>[[candidate_first_name]] thân mến,</p>

<p>Chúng tôi chân thành cám ơn bạn đã dành thời gian quan tâm đến vị trí này. Chúng tôi rất tiếc phải thông báo với bạn rằng chúng tôi đã quyết định lựa chọn một ứng viên khác phù hợp hơn với vị trí này. </p>

<p>Chúng tôi cũng rất ấn tượng với kĩ năng và các thành tích bạn đã đạt được và tin rằng bạn có thể phù hợp với công ty chúng ôi cho những vị trí trong tương lai, và chúng tôi sẽ liên lạc với bạn ngay khi có cơ hội. </p>

<p>Trân trọng,</p>
<p>[[company_user_first_name]]</p>`,
    SR_052_EMAIL_TEMPLATE_5_NAME:"Hẹn phỏng vấn",
    SR_052_EMAIL_TEMPLATE_5_CONTENT:`<p>[[candidate_first_name]] thân mến,</p>

<p>Qua hồ sơ của bạn, chúng tôi đánh giá cao tố chất của bạn đã thể hiện những kinh nghiệm và kĩ năng phù hợp với vị trí mà công ty chúng tôi đang tìm kiếm.</p>

<p>Chúng tôi rất hy vọng có thể trao đổi thêm với bạn trong một buổi phỏng vấn. Nếu đồng ý, bạn vui lòng trả lời lại email này trước 23:59 ngày mai để xác nhận khả năng tham gia buổi phỏng vấn. Nếu có bất kì điều gì bất tiện, bạn có thể liên hệ ngay qua email này. Rất mong sớm được gặp và trò chuyện với bạn.</p>

<p>Trân trọng,</p>
<p>[[company_user_first_name]]</p>`,
    SR_052_EMAIL_TEMPLATE_6_NAME:"Hẹn phỏng vấn qua điện thoại",
    SR_052_EMAIL_TEMPLATE_6_CONTENT:`<p>[[candidate_first_name]] thân mến,</p>

<p>Qua hồ sơ của bạn, chúng tôi đánh giá cao tố chất của bạn đã thể hiện những kinh nghiệm và kĩ năng phù hợp với vị trí mà công ty chúng tôi đang tìm kiếm.</p>

<p>Chúng tôi rất hy vọng có thể trao đổi thêm với bạn trong một buổi phỏng vấn điện thoại. Nếu đồng ý, bạn vui lòng trả lời lại email này trước 23:59 ngày mai để xác nhận khả năng tham gia buổi phỏng vấn. Nếu có bất kì điều gì bất tiện, bạn có thể liên hệ ngay qua email này. Rất mong sớm được gặp và trò chuyện với bạn.</p>

<p>Trân trọng,</p>
<p>[[company_user_first_name]]</p>`,
    SR_052_TEMPLATE_VAR_TITLE:"Các mẫu email ",
    SR_052_LBL_FILTER: "Tìm mẫu tin nhắn",
    SR_052_LBL_DEFAULT_SUB_DESC:"Nếu bạn không đặt tiêu đề, hệ thống sẽ sử dụng tiêu đề mặc định",
    SR_052_CONFIRM_MODAL_TITLE1: "Vui lòng xác nhận",
    SR_052_CONFIRM_MODAL_TITLE2: "Bạn có chắc bạn muốn xóa mẫu tin nhắn này không?"
};
