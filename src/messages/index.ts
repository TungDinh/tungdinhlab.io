import {common} from "./common";
import {sr_001} from "./SR_001";
import {sr_002} from "./SR_002";
import {sr_003} from "./SR_003";
import {sr_005} from "./SR_005";
import {sr_006} from "./SR_006";
import {SR_007} from "./SR_007";
import {SR_009} from "./SR_009";
import {SR_032} from "./SR_032";
import {SR_052} from "./SR_052";
import {sr_053} from "./SR_053";
import {sr_028} from "./SR_028";
import {SR_020} from "./SR_020";
import {SR_046} from "./SR_046";
import {SR_051} from "./SR_051";
import {SR_045} from "./SR_045";
import {SR_047} from "./SR_047";
import {SR_048} from "./SR_048";
import {SR_033} from "./SR_033";
import {SR_008} from "./SR_008";
import {SR_021} from "./SR_021";
import {SR_010} from "./SR_010";
import {SR_037} from "./SR_037";
import {SR_038} from "./SR_038";
import {SR_050} from "./SR_050";
import {SR_027} from "./SR_027";
import {SR_I81} from "./SR_I81";

export const messages = {
    ...common,
    ...sr_001,
    ...sr_002,
    ...sr_003,
    ...sr_005,
    ...sr_006,
    ...SR_007,
    ...SR_008,
    ...SR_009,
    ...SR_032,
    ...SR_051,
    ...SR_052,
    ...sr_053,
    ...sr_028,
    ...SR_020,
    ...SR_045,
    ...SR_046,
    ...SR_047,
    ...SR_048,
    ...SR_033,
    ...SR_021,
    ...SR_010,
    ...SR_037,
    ...SR_038,
    ...SR_050,
    ...SR_027,
    ...SR_I81,
};

