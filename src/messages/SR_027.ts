export const SR_027 = {
    SR_027_OVERVIEW_HEADING_PIPELINE_STAGES:"Báo cáo theo vòng tuyển dụng",
    SR_027_PERFORMANCE_HEADING_PIPELINE_MOVEMENT_HISTORY:"Lịch sử di chuyển ứng viên",
    SR_027_PERFORMANCE_HEADING_PIPELINE_MOVEMENT:"Thống kê số ứng viên ở các vòng",
};
