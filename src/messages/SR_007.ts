export const SR_007 = {
    SR_007_PIPELINE_TITLE:"Quy trình tuyển dụng",
    SR_007_PIPELINE_SUBTITLE:"Quy trình tuyển dụng một vị trí",
    SR_007_PIPELINE_TITLE_DESCRIPTION:"Quy trình tuyển dụng bao gồm các vòng tuyển dụng giúp bạn dễ dàng tổ chức và quản lý ứng viên",
    SR_007_PIPELINE_SUBTITLE_DESCRIPTION:"Quy trình tuyển dụng mặc định được áp dụng cho mọi vị trí nến bạn không điều chỉnh hay thiết lập một quy trình mới phù hợp.",
    SR_007_DEFAULT_PIPELINE_TITLE:"Quy trình tuyển dụng mặc định",
    SR_007_BTN_ADD_PIPELINE:"Thêm mới",
    SR_007_MODAL_PIPELINE_TITLE:"Chỉnh sửa quy trình",
    SR_007_MODAL_PIPELINE_INPUT_NAME_TITLE:"Tên quy trình",
    SR_007_MODAL_PIPELINE_TYPE_TITLE:"Vòng tuyển dụng",
    SR_007_MODAL_PIPELINE_BUTTON_ADD:"Thêm vòng",
    SR_007_MODAL_PIPELINE_BUTTON_SAVE:"Lưu",
    SR_007_MODAL_PIPELINE_STAGE_APPLIED:"Nhận hồ sơ",
    SR_007_MODAL_PIPELINE_STAGE_FEEDBACK:"Lọc hồ sơ",
    SR_007_MODAL_PIPELINE_STAGE_INTERVIEW:"Phỏng vấn ",
    SR_007_MODAL_PIPELINE_STAGE_MADEOFFER:"Đưa ra đề nghị",
    SR_007_MODAL_PIPELINE_STAGE_DISQUALIFIED:"Không phù hợp",
    SR_007_MODAL_PIPELINE_STAGE_HIRED:"Tuyển được",
    SR_007_MODAL_PIPELINE_STAGE_PHONESCREEN:"Phỏng vấn từ xa",
    SR_007_MODAL_PIPELINE_STAGE_SOURCED:"Tự nhập",
    SR_007_MODAL_PIPELINE_STAGE_OTHER:"Khác",
    SR_007_MODAL_STAGE_TITLE:"Chỉnh sửa",
    SR_007_MODAL_STAGE_ADDING:"Đang thêm",
    SR_007_MODAL_STAGE_UPDATING:"Đang cập nhật",
    SR_007_MODAL_STAGE_INPUT_TITLE:"Tên vòng tuyển dụng",
    SR_007_MODAL_STAGE_INPUT_REQUIRED:"Tên vòng tuyển dụng (Bắt buộc)",
    SR_007_MODAL_PIPELINE_STAGE_INPUT_REQUIRED:"Tên quy trình (Bắt buộc)",
    SR_007_MODAL_STAGE_STAGE_TITLE:"Loại vòng tuyển dụng",
    SR_007_MODAL_STAGE_STAGE_ACTIONS:"Thao tác tự động",
    SR_007_MODAL_STAGE_STAGE_ACTIONS_DESCRIPTION:"Thêm thao tác tự động cho một vòng tuyển dụng để hệ thống tự thực hiện nhóm hành động lặp đi lặp lại mà bạn thực hiện mỗi khi ứng viên chuyển từ vòng này sang vòng tiếp theo.",
    SR_007_MODAL_STAGE_STAGE_ACTIONS_ITEM_AVAIABLE:"Chọn tùy chỉnh tự động",
    SR_007_MODAL_STAGE_STAGE_ACTIONS_ITEM_SEND_EMAIL:"Gửi tin nhắn cho ứng viên",
    SR_007_MODAL_STAGE_STAGE_ACTIONS_ITEM_SEND_QUESTIONAIRE:"Gửi câu hỏi tuyển dụng",
    SR_007_MODAL_STAGE_STAGE_ACTIONS_ITEM_TEAM_FEEDBACK:"Nhận xét về ứng viên",
    SR_007_MODAL_STAGE_STAGE_ACTIONS_ITEM_CANDIDATE_SCORECARD:"Đánh giá ứng viên theo thẻ điểm",
    SR_007_ADDING:"Đang thêm ...",
    SR_007_MODAL_STAGE_ADD_BUTTON:"Thêm",
    SR_007_MODAL_STAGE_TITLE_CONFIGURED:"Tùy chỉnh thao tác tự động",
    SR_007_MODAL_STAGE_ACTION_EMPTY:"Không có thao tác tự động cho vòng tuyển dụng này",
    SR_007_MODAL_STAGE_REMOVE_BUTTON:"Xóa",
    SR_007_MODAL_PIPELINE_TOTAL_ACTIONS:"Cho phép thao tác tự động",
    SR_007_MODAL_ADD_TITLE:"Thêm quy trình tuyển dụng",
    SR_007_MODAL_PIPELINE_USED_BY:"Được dùng bởi",
    SR_007_DELETE_PIPELINE_CONFIRM_DESC:"Xóa quy trình tuyển dụng này sẽ ảnh hưởng",
    SR_007_DELETE_PIPELINE_CONFIRM:"Xóa quy trình tuyển dụng?",
    SR_007_CANDIDATE_SCORECARD:"Thẻ điểm các ứng viên",
    SR_007_CANDIDATE_SCORECARD_PLACEHOLDER:"Chọn một thẻ điểm",
    SR_007_ACTIONS_SEND_EMAIL_CONTENT:"Tự động gửi một email tới các ứng viên khi vào vòng này.",
    SR_007_CANDIDATE_SCORECARD_DESC:"Tự động phân công các công việc cho nhóm tuyển dụng để đánh giá thẻ điểm cho ứng viên khi vào vòng này.",
    SR_007_TEXT_POSITION: "vị trí",

    SR_007_AVAILABLE_ACTIONS: "Chọn tùy chỉnh tự động"
};
