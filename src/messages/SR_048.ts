export const SR_048 = {
    SR_048_FORM_TITLE:"Thông tin cá nhân",
    SR_048_FORM_LABLE_NAME:"Tên đầy đủ",
    SR_048_FORM_MESSAGE_ERROR_NAME:"Vui lòng nhập tên đầy đủ",
    SR_048_FORM_LABLE_EMAIL:"Địa chỉ email",
    SR_048_FORM_MESSAGE_ERROR_EMAIL:"Vui lòng nhập địa chỉ email",
    SR_048_FORM_LABLE_PHONE:"Số điện thoại",
    SR_048_FORM_MESSAGE_ERROR_PHONE:"Vui lòng nhập số điện thoại",
    SR_048_FORM_MESSAGE_LABLE_EXPERIENCE:"Kinh nghiệm",
    SR_048_FORM_MESSAGE_LABLE_WORK_HISTORY:"Kinh nghiệm làm việc",
    SR_048_FORM_BTN_DEL:"Xóa",
    SR_048_FORM_BTN_ADD_POSITION:"Thêm kinh nghiệm làm việc",
    SR_048_FORM_MESSAGE_ERROR_RESPONSE:"Vui lòng nhập câu trả lời",
    SR_048_FORM_LABLE_EDUCATION:"Học vấn",
    SR_048_FORM_BTN_ADD_EDUCATION:"Thêm học vấn",
    SR_048_FORM_LABLE_EXP_SUMMARY:"Mô tả kinh nghiệm",
    SR_048_FORM_MESSAGE_ERROR_EXP_SUMMARY:"Vui lòng nhập mô tả kinh nghiệm",
    SR_048_FORM_LABLE_RESUME:"Đơn ứng tuyển",
    SR_048_FORM_MESSAGE_ERROR_RESUME:"Vui lòng đính kèm đơn ứng tuyển",
    SR_048_FORM_BTN_UPLOAD_RESUME:"Tải lên đơn ứng tuyển",
    SR_048_FORM_LABLE_COVER_LETTER:"Thư tự giới thiệu",
    SR_048_FORM_MESSAGE_ERROR_COVER_LETTER:"Vui lòng đính kèm Thư tự giới thiệu",
    SR_048_FORM_BTN_SUBMIT_APPLY:"Nộp đơn",
    SR_048_FORM_MESSAGE_ERROR_FORM:"Hồ sơ ứng tuyển của bạn có lỗi",
    SR_048_FORM_MESSAGE_ERROR_APPLIED:"Oops. Hình như bạn đã nộp đơn ứng tuyển vị trí này rồi.",
    SR_048_TITLE_SUBMITTED:"Nộp đơn thành công",
    SR_048_DES_SUBMITTED:"Đơn ứng tuyển của bạn đã được gửi thành công. Chúc bạn may mắn!",
    SR_048_LINK_JOB:"Những vị trí đang tuyển",
    SR_048_LINK_APPLY:"Ứng tuyển ngay",
    SR_048_FORM_PLACEHODER_NAME:"Họ tên",
    SR_048_FORM_PLACEHODER_EMAIL:"Địa chỉ email",
    SR_048_FORM_PLACEHODER_PHONE:"Số điện thoại",
    SR_048_FORM_PLACEHODER_COMPANY:"Công ty",
    SR_048_FORM_PLACEHODER_COMPANY_TITLE:"Vị trí",
    SR_048_FORM_PLACEHODER_COMPANY_SUMMARY:"Mô tả chi tiết",
    SR_048_FORM_PLACEHODER_SCHOOL:"Trường học",
    SR_048_FORM_PLACEHODER_SCHOOL_FIELD:"Chuyên ngành",
    SR_048_FORM_PLACEHODER_SCHOOL_SUMMARY:"Mô tả chi tiết",

    SR_048_MGS_ERR_VALIDATE:"Câu trả lời là bắt buộc",
    SR_048_MGS_ERR_VALIDATE_FILE:"Tài liệu đính kèm là bắt buộc",
    SR_048_ATTACHED: "Đính kèm",
    SR_048_UPLOAD_FILE:"Tải lên tài liệu"
};
