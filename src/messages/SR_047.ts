export const SR_047 = {
    SR_047_MENU_JOB_OPENING:"Những vị trí đang tuyển",
    SR_047_BTN_APPLY_POSITIONS:"Nộp đơn ứng tuyển",
    SR_047_BTN_INDEED:"Sử dụng đơn ứng tuyển Indeed",
    SR_047_BTN_LINKEDIN:"Sử dụng tài khoản Linkedin",
    SR_047_BTN_INDEED_SIMPLE:"Sử dụng đơn ứng tuyển Indeed",
    SR_047_BTN_LINKEDIN_SIMPLE:"Sử dụng tài khoản Linkedin",
}
