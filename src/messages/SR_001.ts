export const sr_001 = {
    SR_001_UPLOADING:"Đang tải lên",
    SR_001_UPLOADED:"Tải lên thành công",
    SR_001_WRONG_PASS:"Sai mật khẩu",
    SR_001_PASS_MISMATCHED:"Mật khẩu đã nhập không trùng khớp",
    SR_001_PASS_MIN:"Mật khẩu phải có ít nhất 6 ký tự",
    SR_001_PASS_SAVE_FAIL:"Mật khẩu không cập nhật được",
    SR_001_PASS_NULL:"Mật khẩu không được để trống",
    SR_001_PASSWORD:"Mật khẩu",
    SR_001_CURRENT_PASSWORD:"Mật khẩu cũ",
    SR_001_NEW_PASSWORD:"Mật khẩu mới",
    SR_001_CONFIRM_PASSWORD:"Xác nhận lại mật khẩu",
    SR_001_PASSWORD_DESCR:"Một mật khẩu mạnh là mật khẩu mà bạn có thể nhớ nhưng người khác gần như không thể đoán được.",
    SR_001_PASSWORD_TITLE:"Mật khẩu",
    SR_001_PROFILE_TITLE:"Thông tin ",
    SR_001_USER_PROFILE:"Thông tin cá nhân",
    SR_001_USER_PROFILE_DESCR:"Nhập đầy đủ thông tin cá nhân giúp thuyết phục những người bạn liên hệ",
    SR_001_UPLOAD_BUTTON:"Cập nhật ảnh đại diện",
    SR_001_NOTIFICATION_TITLE:"Thông báo ",
    SR_001_NOTIFICATION:"Thông báo ",
    SR_001_NOTIFICATION_DESCR:"Tùy chỉnh cài đặt thông báo ",
    SR_001_NOTIFICATION_MEETING_REMIND:"Nhắc nhở lịch họp",
    SR_001_NOTIFICATION_GENERAL:"Thông báo chung",
    SR_001_NOTIFICATION_CANDIDATE:"Thông báo về ứng viên",
    SR_001_NOTIFICATION_CANDIDATE_ASSIGN:"Nhận thông báo khi được phân công phụ trách một ứng viên",
    SR_001_NOTIFICATION_TASKS:"Thông báo công việc",
    SR_001_NOTIFICATION_TASK_ASSIGN_TO_YOU:"Công việc phân công cho bạn ",
    SR_001_NOTIFICATION_TASK_DUAL_DAILY:"Công việc đến hạn (Hằng ngày)",
    SR_001_SIGNATURE_TITLE:"Chữ ký",
    SR_001_SIGNATURE:"Chữ ký email",
    SR_001_SIGNATURE_DESCR:"Chữ ký email có thể là thông tin liên hệ cá nhân được tự động thêm vào cuối email giống như chân trang.",
    SR_001_USER_SETTING_TITLE:"Cài đặt cá nhân",
    SR_001_USER_NULL_NAME:"Tên người dùng không được để trống",
    SR_001_UPLOAD_IMAGE_ONLY:"Chỉ được up file ảnh !",
    SR_001_TOO_MANY_REQUEST: "Too Many Request",
    SR_001_RECRUITING_PREFERENCES: "Cài đặt tuyển dụng"
}
