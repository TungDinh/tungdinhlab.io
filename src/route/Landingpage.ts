import LandingPage from "@/views/LandingPage.vue";
import DetailPosition from "@/components/LandingPage/DetailPositionPage.vue";
import Home from "@/components/LandingPage/HomePage.vue";
import AppyPosition from "@/components/LandingPage/AppyPositionPage.vue";
import ApplySuccess from "@/components/LandingPage/ApplySuccess.vue";
import NotFound from '@/views/404.vue';
import VueRouter, {Route} from "vue-router";
import JobStore from '@/store/landingPage/store';
import {matchSubDomain, matchHostname} from '@/proxy/matchDomain';

async function beforeEnter(to: Route, from: Route, next: Function) {
    if (JobStore.getters.currentCompanyLandingPage.id) return next();

    let domain = matchSubDomain();
    if (!domain) domain = to.params.domain;
    console.debug({domain});

    if (!domain) {
        // try find by whole hostname
        const company = await matchHostname();
        if (!company) return next('/_404');

        await JobStore.commit('setCurrentCompanyLandingPage', company);
        return next();
    }

    try {
        await JobStore.dispatch("getCompanyLandingPageByDomain", domain);
        return next();
    } catch (e) {
        console.error({e});
        return next('/_404');
    }
}

const pathMatch = process.env.NODE_ENV === 'production' ? '/' : '/:domain([A-Za-z0-9\\-]+)';

export const router = new VueRouter({
    mode: "history",
    base: process.env.NODE_ENV === 'production' ? '' : '/job', // in production, domain will be proxy
    routes: [
        {
            path: pathMatch,
            name: 'LandingPage',
            component: LandingPage,
            beforeEnter,
            children: [
                {
                    path: '/',
                    name: 'LandingPageHome',
                    component: Home,
                },{
                    path: 'position/:position_id/',
                    name: 'LandingPageDetailPosition',
                    component: DetailPosition,
                },
                {
                    path: 'position/:position_id/apply/',
                    name: 'LandingPageApplyPosition',
                    component: AppyPosition,
                },
                {
                    path: 'position/:position_id/apply/submitted',
                    name: 'LandingPageApplyPositionSubmitted',
                    component: ApplySuccess,
                },
            ],
        },
        {
            path: '/_404',
            name: 'NotFound',
            component: NotFound,
        },
        {
            path: '*',
            name: "NotMatch",
            component: NotFound,
        }
    ]
});
