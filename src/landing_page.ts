import Vue from 'vue'
import LandingPageLayout from './LandingPage.vue'
import {router} from './route/Landingpage';
import VueRouter from "vue-router";
Vue.use(VueRouter);

import BootstrapVue from 'bootstrap-vue';
import 'bootstrap-vue/dist/bootstrap-vue.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery/dist/jquery.slim.min';
import 'bootstrap/dist/js/bootstrap.min';

// fix blurry text in dropdown
// https://github.com/twbs/bootstrap/issues/22610
import Popper from 'popper.js';
Popper.Defaults.modifiers.computeStyle.gpuAcceleration = false;

Vue.use(BootstrapVue);

import '@/assets/app-layout.scss';
import '@/assets/custom.css';
import store from "@/store/landingPage/store";

Vue.config.productionTip = false;
Vue.config.devtools = process.env.NODE_ENV !== 'production';

// messages
import {messages} from '@/messages';
import StorePlugins from "@/plugins/StorePlugins";
Vue.prototype.$cjpmessages = messages;

Vue.config.productionTip = false;
Vue.$router = router;
let vm;

Vue.use(StorePlugins, {
    store,
});

if (!vm)
    vm = new Vue({
        router,
        store,
        render: h => h(LandingPageLayout)
    }).$mount('#landing-page');
