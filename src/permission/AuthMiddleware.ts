import {Route} from "vue-router";
import {fauth} from "@/firebase";
import {store} from "@/store/store";

async function AuthMiddleware(to: Route, from: Route, next: Function) {
    if (to.matched.some(route=>route.meta.noAuth)) {
        return next();
    }

    try {
        if (!fauth.currentUser || !store.getters.currentUser) {
            await store.dispatch("setUser");
        }
    } catch (e) {
        console.error(e + "");
        return next('/login');
    }

    return next();
}

export default AuthMiddleware;
