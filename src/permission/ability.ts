// https://github.com/stalniy/casl-vue-api-example/blob/master/src/store/ability.js
// created by Dan An <an.hd@cloudjetkpi.com>

import {Ability, AbilityBuilder} from '@casl/ability';
import {Store} from "vuex";
import ROLE from './type';
import User from "@/models/User";
import Candidate from "@/models/Candidate";

const memberRules = [
    'view calendar',
    'view discussion',
    'view tasks',
    'view scorecards',
    'create'
];
const assigneeRules = [
    ...memberRules,
    'modify',
    'schedule_interview',
    'move',
    'email',
    'view details',
    'modify details',
    'view experience',
    'modify experience',
    'view resume',
    'view attachments',
    'view questionnaire',
    'view references',
    'view email',
    'view notes',
    'read',
];

const adminRules = [
    ...assigneeRules,
    'admin',
    'delete',
];

const ownerRules = [
    ...assigneeRules,
   'delete'
];

const subjectName = function (subject) {
    if (!subject) return null;
    if (typeof subject === "string") {
        return subject;
    }
    return subject.COLLECTION || subject.name || typeof subject;
};

export const ability = AbilityBuilder.define({subjectName}, can=>{});

export const abilityPlugin = (store: Store<any>)=>{
    return store.subscribe((mutation)=>{
        switch (mutation.type) {
            case "setUser":
                const user = mutation.payload as User;
                ability.update([]);// reset

                if (user.role === ROLE.ADMIN) {
                    ability.update([
                        {
                            actions: adminRules,
                            subject: "all",
                            conditions: {
                                company_id: user.company_id,
                            }
                        },
                    ]);
                } else {
                    ability.update([
                        {
                            actions: memberRules,
                            subject: 'all',
                        },
                        // {
                        //     actions: [
                        //         'read',
                        //         'create',
                        //     ],
                        //     subject: ["Candidate", 'candidates'],
                        // },
                        {
                            actions:assigneeRules,
                            subject: ['Candidate','candidates'],
                            conditions: {
                                assignee_id: user.id,
                            },
                        },
                        {
                            actions: ownerRules,
                            subject: ['Position', 'Candidate','candidates','Conversation','Inbox'],
                            conditions: {
                                created_by: user.id,
                            },
                        },
                        {
                            actions: adminRules,
                            subject: ['Position', 'Candidate','candidates'],
                            conditions: {
                                created_by: user.id,
                            },
                        },
                    ]);
                }

                break;

            case "setCurrentCandidate":
                break;
            // case "setPositionCandidate": {
            //     debugger;
            //     const user = store.getters.currentUser;
            //     const oldRule = ability.rules;
            //     if (!user) {
            //         ability.update([]);
            //         break;
            //     }
            //     const rules = [
            //         ...oldRule,
            //         {
            //             actions: assigneeRules,
            //             subject: ["Candidate", 'candidates'],
            //         },
            //     ];
            //     if (mutation.payload.created_by === user.id) {
            //         rules.push({
            //             actions: adminRules,
            //             subject: ["Candidate", 'candidates'],
            //         });
            //     }
            //     // debugger;
            //
            //     ability.update(rules);
            //     break;
            // }
        }
    })
};
