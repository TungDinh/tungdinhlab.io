/**
 *  This BeforeEnter will check 2 things
 *  - Candidate (assignee, created_by)
 *  - Position (created_by)
 *  > If one of them is true, pass
 *  > Else redirect to /401
 *
 *  Using: Put it in beforeEnter of the route
 */

import {Route} from "vue-router";
import {store} from '@/store/store';
import Position from "@/models/Position";
import {ability} from "@/permission/ability";
import {messages} from "@/messages";
import _ from "lodash";

async function BeforeEnter(to: Route, from: Route, next: Function)
{
    const candidate_id = to.params.candidate_id;
    if (!candidate_id) {
        return next(new Error("Candidate ID not found"));
    }

    const user = store.getters.currentUser;
    if (!user) return next('/login');
    const candidate = await store.dispatch("getCandidate", candidate_id);
    // if (candidate.unseen) {
    //     candidate.unseen = false;
    //     candidate.update();
    // }

    if(!_.includes(candidate.seen_list, user.id)) {
        candidate.seenMe();
    }

    const position = Position.from(store.getters.positionCandidate);

    console.debug(ability.can('read', candidate), candidate);
    console.debug(ability.rules);
    if (ability.can('read', candidate) || ability.can('admin', position)) {
        return next();
    }

    next(new Error(messages.COMMON_DENIED_CANDIDATE));
}

export default BeforeEnter;
