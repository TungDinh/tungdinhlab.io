import {Route} from "vue-router";
import {store} from "@/store/store";
import ROLE from './type';

export default function (to: Route, from: Route, next: Function) {
    if (to.matched.some(route => route.meta.onlyAdmin)) {
        // check currentUser
        const currentUser = store.getters.currentUser;
        console.debug(currentUser);
        if (!currentUser || currentUser.role !== ROLE.ADMIN) {
            // to.matched.length = 1;
            console.debug("You're not admin");
            return next('/401');
        }
    }
    return next();
}
