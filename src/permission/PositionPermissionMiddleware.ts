/**
 *   This Middleware will check 2 things of 'read' ability
 *   - created_by
 *   - assignee_id
 *   if pass, next()
 *   else, return home and throw Error
 */
import {Route} from "vue-router";
import {store} from '@/store/store';
import {ability} from "@/permission/ability";
import {messages} from "@/messages";

const middleware = async (to: Route,  from: Route, next: Function) => {
    console.debug(to.matched);
    if (to.matched.some(r=>r.meta.positionCheck)) {
        const position_id = to.params.position_id || store.getters.positionKanban.id;
        if (!position_id) {
            return next(new Error("ID Not Found"));
        } // no next, return

        const position = await store.dispatch('getPosition', position_id);
        if (ability.can('read', position)) {
            return next();
        }

        return next(new Error(messages.COMMON_DENIED_POSITION));
    }
    next();
};

export default middleware;
