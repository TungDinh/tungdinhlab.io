import Vue from 'vue'
import App from './App.vue'
import router from './router'
import {ability} from '@/permission/ability';
import {abilitiesPlugin} from '@casl/vue';
import './registerServiceWorker'
import Vuex from 'vuex';
import VueRouter from "vue-router";
// @ts-ignore
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';

import firebase from 'firebase/app';

// https://shakee93.github.io/vue-toasted/
import Toasted from 'vue-toasted';

import axios from 'axios'
import VueAxios from 'vue-axios'
import VeeValidate, {Validator} from 'vee-validate';
import VeeVi from 'vee-validate/dist/locale/vi';

Vue.use(VueRouter);
Vue.use(abilitiesPlugin, ability);
Vue.use(VueAxios, axios);
Vue.use(NProgress);

Vue.use(VeeValidate, {
    locale: 'vi',
    events: 'change|blur',
});
Validator.localize('vi', VeeVi);

// @ts-ignore
Vue.use(Toasted, {
    position: 'top-right',
    iconPack: 'fontawesome',
    duration: 2000
});

// bulma UI framework for Vue https://buefy.org/documentation/button
// import Buefy from 'buefy';
// import 'buefy/dist/buefy.min.css';
// Vue.use(Buefy);
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap-vue/dist/bootstrap-vue.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery/dist/jquery.slim.min';
import 'bootstrap/dist/js/bootstrap.min';

// fix blurry text in dropdown
// https://github.com/twbs/bootstrap/issues/22610
import Popper from 'popper.js';
Popper.Defaults.modifiers.computeStyle.gpuAcceleration = false;

Vue.use(BootstrapVue);

import '@/assets/app-layout.scss';
import '@/assets/custom.css';
import VueLogger from 'vuejs-logger';
import {fauth} from "@/firebase";
import {store} from "@/store/store";

Vue.use(VueLogger, {
    logLevel: process.env.NODE_ENV === 'production' ? 'error' : 'debug',
});

Vue.config.productionTip = false;
Vue.config.devtools = process.env.NODE_ENV !== 'production';

import VModal from 'vue-js-modal'

Vue.use(VModal);

import Datetime from 'vue-datetime'
// You need a specific loader for CSS files
import 'vue-datetime/dist/vue-datetime.css'

Vue.use(Datetime)


import VuejsDialog from 'vuejs-dialog';


// include the default style
import 'vuejs-dialog/dist/vuejs-dialog.min.css';

// Tell Vue to install the plugin.
Vue.use(VuejsDialog);

/*
By default, the plugin will use "modal" name for the component.
If you need to change it, you can do so by providing "componentName" param.

Example:

Vue.use(VModal, { componentName: "foo-modal" })
...
<foo-modal name="bar"></foo-modal>
*/

import VTooltip from 'v-tooltip'
//set trigger tooltip
Vue.use(VTooltip, {
    defaultTrigger: 'hover',
    disposeTimeout: 500,
    popover: {
        defaultTrigger: 'hover',
    }
});


import 'v-tooltip/dist/v-tooltip.css';

import 'moment/locale/vi';
import * as moment from "moment";
moment.locale('vi');
moment.updateLocale('vi', {
    meridiem : function (hours, minute, isLowercase) {
        if (hours < 12) {
            return  'SA' ;
        } else {
            return  'CH' ;
        }
    }
});

Vue.config.productionTip = false;


import ElementUI from 'element-ui';
import vi from 'element-ui/lib/locale/lang/vi';
import StorePlugins from "@/plugins/StorePlugins";
Vue.use(ElementUI, vi);

Vue.use(StorePlugins, {store});

// Mr.Duan use `window.vm` for alert when insert or update candidate with exist email.
window['vm'] = new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
