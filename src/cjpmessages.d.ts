import {messages} from "./messages";

declare module 'vue/types/vue' {
    export interface Vue {
        $cjpmessages: typeof messages;
    }
}

