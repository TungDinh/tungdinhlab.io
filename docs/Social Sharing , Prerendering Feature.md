Please check following articles

1. https://stackoverflow.com/questions/50968603/sharing-on-social-media-the-url-does-not-render-any-meta-data
2. https://cloudonaut.io/rich-social-sharing-with-single-page-applications-hosted-on-s3-and-delivered-via-cloudfront/


Sau đó phát hiện ra rằng config trên Cloudfront khá phức tạp mà không xem log được (chính xác là xem được nhưng rất rắc rối) --> bõ phương án này sau 10h nghiên cứu . 

Sau đó, chuyển qua phương án tự cài server, sử dụng reverse proxy, nginx các kiểu:
- https://dev.to/domysee/setting-up-a-reverse-proxy-with-nginx-and-docker-compose-29jg 
- https://prerender.io

Nhưng vấn đề gặp phải là việc maintaining về sau sẽ phức tạp vì ít người biết config và việc setup app mới khá là phiền hà rắc rối

Cuối cùng, chuyển qua phương án sử dụng Netlify Prerender, không ngờ đây là phương án quá tuyệt. 


Hiện tại, wildcast domain cần phải upgrade lên Pro plan mới sử dụng được và phải contact nó:
https://omarchehab.com/2018/09/05/netlify-site-with-wildcard-subdomain

Đã upgrade lên pro và đã contact. 


Đã bật chế độ prerender:
![prerender](https://i.imgur.com/di6CjRr.png  "prerender")



Hiện tại còn 1 vấn đề về https , thấy trong này thì nó nói đã support rồi, nhưng khi chuyển qua thì chưa: https://www.netlify.com/blog/2018/08/20/enabling-free-wildcard-domain-certificates-with-lets-encrypt/

Thế nên, tạm thời sử dụng https://www.sslforfree.com/ để tạo , nhưng 3 tháng sẽ hết hạn và phải tạo lại chứ không tự động renew. 

https://app.netlify.com/sites/career-cloudjetpotential/settings/domain


