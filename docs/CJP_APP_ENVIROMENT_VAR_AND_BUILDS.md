# CJP APP (Builds & Enviroment Variables)

> Author: Dan An

## Các biến môi trường sử dụng trong app CJP
1. [NODE_ENV](#1-node_env)
2. [VUE_APP_USE_QC_DATABASE](#2-vue_app_use_qc_database)
3. [VUE_APP_USE_PROD_DATABASE](#3-vue_app_use_prod_database)
4. [VUE_APP_USE_FIREBASE_ENV_CONFIG](#4-vue_app_use_firebase_env_config)
5. [VUE_APP_FIREBASE_CONFIG](#5-vue_app_firebase_config)
6. [BUILD_JOB_PAGE](#6-build_job_page)

### 1. NODE_ENV

Gồm `development`, `qc`, `qc-stable` và `production`.

- Đối với `development`, app sẽ build bundle ra 1 file `app.js` khá nặng và chưa compress (tầm ~ 30MB),
có source map và giữ nguyên tên biến. Trang tuyển dụng khi build ở `development` sẽ cùng domain là localhost,
path là `http://localhost:8080/job/{subdomain_cong_ty}`.

- Đối với `qc`, app sẽ build bundle như ở development nhưng sẽ có compress và minify lại,
file `app.js` lớn khoảng ~ 17MB và có source map cho file bundle. Trang tuyển dụng khi build ở `qc` sẽ 
giống `development`, nhưng gồm 2 path là `/{domain}/...` và `/job/{domain}...`.

- Đối với `qc-stable`, giống với `qc`, nhưng khác domain trang
là `stable-potential.netlify.com` và `job-stable-potential.netlify.com`

- Đối với `production`, app sẽ build và compress bundle dưới cả 3 dạng: *plain minify*, *gzip* và *brotli*. 
Khi build ở `production`, các file js sẽ được split (chia nhỏ) thành có file (<2MB) và tổng size khi hard reload đầu tiên sẽ là ~ 8MB (với compress *brotli*). 
Trang tuyển dụng khi build ở `production` sẽ không còn nằm cùng domain với app nữa mà sẽ tách riêng. 
Việc tách riêng và build Gitlab Pages sẽ được chạy riêng ở 2 pipeline ở 2 repos khác nhau, [xem bên dưới](#6-build_job_page).

### 2. VUE_APP_USE_QC_DATABASE

> To Be Continued

### 3. VUE_APP_USE_PROD_DATABASE

> To Be Continued

### 4. VUE_APP_USE_FIREBASE_ENV_CONFIG

> To Be Continued

### 5. VUE_APP_FIREBASE_CONFIG

> To Be Continued

### 6. BUILD_JOB_PAGE

> To Be Continued
