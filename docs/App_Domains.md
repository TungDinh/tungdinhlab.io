# APP'S DOMAINS & BUILD INSTRUCTIONS

## Các domains của app Cloudjet Pontential hiện tại

Cloudjet Potential được chia làm 2 bundle là `app` và `job`(trang tuyển dụng)

1. **DEV**: 
    - **app:** [localhost:8080]()
    - **job:** [localhost:8080/job/:domain]()

2. **QC**:
    - **app:** [qc-cloudjetpotential.netlify.com](http://qc-cloudjetpotential.netlify.com)
    - **job:** [job-cloudjetpotential.netlify.com/:domain](http://qc-cloudjetpotential.netlify.com/cloudjet-solutions)
        
3. **Production**:
    - **app:** [pro.cloudjetpotential.com](https://pro.cloudjetpotential.com)
    - **job:** [{domain}.cloudjetpotential.com](https://mieu.cloudjetpotential.com)

